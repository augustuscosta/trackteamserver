var io = require('socket.io').listen(9090);

console.log('Servidor node iniciado com sucesso !');

var usernames = {};

var clientBrowser = {};
var servidores = {};
var devices = {};
var listaTokens = {};

var agentes = {};
var veiculos = {};

var eventoPendentesVeciulo = {};
var eventoPendentesAgente = {};
var historicos = [];


function enviaParaTodosBrowsers(metodo, mensagem) {

	for (x in clientBrowser) {			
		for (y in clientBrowser[x]) {	
						
			if (clientBrowser[x] != null && clientBrowser[x][y] != null && clientBrowser[x][y].emit != null) {
				
				clientBrowser[x][y].emit(metodo, mensagem);
			}
		}
	}	
}

function enviaParaTodosDevices(metodo, mensagem) {

	for (x in devices) {	
		devices[x].emit(metodo, mensagem);
	}	
}

function enviaParaTodosServidores(metodo, mensagem) {

	for (x in servidores) {	
		servidores[x].emit(metodo, mensagem);
	}	
}

function parseJSON(data) {
    return window.JSON && window.JSON.parse ? window.JSON.parse( data ) : (new Function("return " + data))(); 
}

function adicionaToken(token, agente_id) {
	
	for (x in listaTokens) {
		
		if (listaTokens[x] == agente_id) {
			delete listaTokens[x];
		}
	}
	
	listaTokens[token] = agente_id;
}

io.sockets.on('connection', function (socket) {
	
//gerenciando as sessoes
	
  socket.on('sessao/token', function (data) {
/* TODO */	
	if (data.tipo == 'browser') {

		var listaTokensBrowser = [];
		
		if (clientBrowser[data.token] == null) {
	
		} else {
	
			listaTokensBrowser = clientBrowser[data.token];
		}
		
		this.tipo = data.tipo;
		this.token = data.token;
		this.nome = data.nome;
			
		listaTokensBrowser.push(this);		
		clientBrowser[data.token] = listaTokensBrowser;

		console.log('Novo cliente browser registrado - usuario id :' + data.token + ' - sessão :' + this.id + ' - ' + new Date());

	} else if (data.tipo == 'servidor') {

		this.tipo = data.tipo;
		servidores[this.id] = this;

		console.log('Novo servidor registrado - sessão :' + this.id + ' - ' + new Date());

	} else {
	
		this.tipo = "movel";
		this.token = data.token;
		
		devices[data.token] = this;	
		
		var token = Object();
		token.token = data.token;
		
		enviaParaTodosServidores("device/get/id", token);
		console.log('Novo device - sessão :' + this.id + ' - token :' + this.token + ' - ' + new Date());				
	}	
  });
  
  socket.on("device/notifica/id", function (data) {

	  data = data.result;
	  adicionaToken(data.token, data.agente_id);
	  
	  if (data.agente_id != null && devices[data.token] != null) {		  
		  agentes[data.agente_id] = data.token;
		  devices[data.token].agente_id = data.agente_id; 
		  enviaEventosPendentes(data.agente_id);
	  }
	  
	  if (data.veiculo_id != null && devices[data.token] != null) {	
		  
		  veiculos[data.veiculo_id] = data.token;		  
		  devices[data.token].veiculo_id = data.veiculo_id;
	  }

	  enviaEventoPendente(data.agente_id);
  });

	socket.on('servidor/token/sincroniza', function (data) {
	
		var tokens = data.result.tokens;
			
		for (x in tokens) {
						
			if (listaTokens[tokens[x].token] == null) {
				
				listaTokens[tokens[x].token] = {};
				
			}
			
			listaTokens[tokens[x].token] = tokens[x].agente_id;
		}
		
		console.log('Sicronizado com servidor');
	});
/* TODO */	
  socket.on('disconnect', function (data) {

		if (this.tipo == "browser") {
		
			console.log('Desconectou um cliente browser - usuario id :' + this.token + ' - sessão :' + this.id + ' - ' + new Date());	
			listaTokensBrowser = clientBrowser[this.token];
		
			if (listaTokensBrowser != undefined) {
		
				for (x in listaTokensBrowser) {
										
					if (listaTokensBrowser[x].token == this.token) {
						delete listaTokensBrowser[x];
					}
				}
				
				if (Object.keys(listaTokensBrowser).length <= 0) {
		
					delete clientBrowser[this.token];
				}
			}
		
		} else if (this.tipo == 'servidor') {
		
			console.log('Desconectou um servidor - sessão :' + this.id + ' - ' + new Date());	
			delete servidores[this.id];
		
		} else {
		
			console.log('Desconectou um device - sessão :' + this.id + ' - ' + new Date());

			for (x in devices) {				
				
				if (devices[x].id == this.id) {
					
					if (devices[x].agente_id != null) {
						delete agentes[devices[x].agente_id];
					}
					if (devices[x].veiculo_id != null) {
						delete veiculos[devices[x].veiculo_id];
					}
					
					delete devices[x];		
				}
			}			
		}

  	});

/* ----- Métodos para as ocorrências ----- */

  	socket.on('ocorrencia/add', function (data) {

  		console.log('-------> Metodo ocorrencia/add excluido, mas sendo utilizado.');
  	});
  	
  	socket.on('ocorrencia/browser/add', function (data) {
	  
		var mensagem = Object();
		mensagem.result = data.result.mensagem;
//		listaTokensBrowser = data.result.listaTokens;
			
		enviaParaTodosBrowsers('ocorrencia/add', mensagem);		
  	});
  	
  	socket.on('ocorrencia/device/add', function (data) {
 		  		
		var mensagem = Object();
		mensagem.result = data.result.mensagem;
		listaTokensDevice = data.result.listaTokens;		
		
		for (x in listaTokensDevice) {			
			
			enviaEventoAgente('ocorrencia/add', listaTokensDevice[x], mensagem);
		}
  	});
 
	enviaEventoAgente = function (metodo, agente_id, mensagem) {
		
		if (agentes == null || 
				agentes[agente_id] == null || 
				devices[agentes[agente_id]] == null) {
			
			adicionarEventoComoPendente(metodo, agente_id, mensagem);
			
		} else {
			
			devices[agentes[agente_id]].emit(metodo, mensagem);				
		};
	};
	
	var adicionarEventoComoPendente = function(metodo, id, mensagem) {
		
		var pendencias = [];
		var pendencia = {};

		if (eventoPendentesAgente[id] == null) {
			
			eventoPendentesAgente[id] = {};

		} else {
			
			pendencias = eventoPendentesAgente[id].pendencias;		
		}

		pendencia.metodo = metodo;
		pendencia.mensagem = mensagem;

		pendencias.push(pendencia);
		eventoPendentesAgente[id].pendencias = pendencias;

		console.log('Adicionado evento para ser enviado ao conectar o device.');

	};

	var enviaEventosPendentes = function (id) {

		var total = 0;

		if (eventoPendentesAgente[id] != null) {

			for (x in eventoPendentesAgente[id].pendencias) {
				
				devices[agentes[id]].emit(eventoPendentesAgente[id].metodo, eventoPendentesAgente[id].mensagem);	
				
				notificaDeviceEventos(agentes[id], eventoPendentesAgente[id].pendencias[x]);
				total = total + 1;
			}
			
			delete eventoPendentesAgente[id];
			console.log('Enviados ' + total + ' eventos ao device.');
		};
	};
  
//muda uma ocorrencia
	
  	socket.on('ocorrencia/change', function (data) {

  		console.log('-------> Metodo ocorrencia/change excluido, mas sendo utilizado.');
  	});
  	
  	socket.on('ocorrencia/browser/change', function (data) {
	  
		var mensagem = Object();
		mensagem.result = data.result.mensagem;
//		listaToken = data.result.listaTokens;
			
		enviaParaTodosBrowsers('ocorrencia/change', mensagem);		
  	});
  	
	socket.on('ocorrencia/device/change', function (data) {
		
		var mensagem = Object();
		mensagem.result = data.result.mensagem;
		listaToken = data.result.listaTokens;
		
		for (x in listaToken) {

			enviaEventoAgente('ocorrencia/change', listaToken[x], mensagem);					
		};	
	});
	
// apaga uma ocorrencia

  	socket.on('ocorrencia/delete', function (data) {

  		console.log('-------> Metodo ocorrencia/delete excluido, mas sendo utilizado.');
  	});
  	
  	socket.on('ocorrencia/browser/delete', function (data) {
	  
		var mensagem = Object();
		mensagem.result = data.result.mensagem;
//		listaToken = data.result.listaTokens;
			
		enviaParaTodosBrowsers('ocorrencia/delete', mensagem);		
  	});

  	socket.on('ocorrencia/device/delete', function (data) {
  	  
		var mensagem = Object();
		mensagem.result = data.result.mensagem;
//		listaToken = data.result.listaTokens;			
		
		for (x in listaToken) {

			enviaEventoAgente('ocorrencia/delete', listaToken[x], mensagem);		
		}
  	});
  	
/* ----- Métodos para o histórico ----- */

  	
/* Deprecated */
  socket.on('historico/change', function (data) {

	console.log('historico/change');  
	  
	enviaParaTodosBrowsers('historico/change', data);
//	enviaParaTodosDevices('historico/change', data);	

  });

	socket.on('historico/servidor', function (data) {

		console.log('historico/servidor');  

		enviaParaTodosBrowsers('historico/change', data);
//		enviaParaTodosDevices('historico/change', data);
	
	});


/* ----- Métodos para o veículo ----- */

	socket.on('veiculo/add', function (data) {
		io.sockets.emit('veiculo/add',data);
	});
	  
	socket.on('veiculo/change', function (data) {
		io.sockets.emit('veiculo/change',data);
	});
	  
	socket.on('veiculo/delete', function (data) {
		io.sockets.emit('veiculo/delete',data);
	});

  
/* ----- Métodos para o agente ----- */
  
  
  socket.on('agente/add', function (data) {
	io.sockets.emit('agente/add',data);
  });
  socket.on('agente/change', function (data) {
	io.sockets.emit('agente/change',data);
  });
  socket.on('agente/delete', function (data) {
	io.sockets.emit('agete/delete',data);
  });

  
/* ----- Métodos para chat ----- */
  
  
	// abrir a conexão passando quem abriu a conexão com quem, como no json:
  
	socket.on('chat/conectar', function(data) {

	});
	
	// fechar a conexão passando quem fechou a conexão com quem, como no json:
	socket.on('chat/desconectar', function(data) {

	});
	
	// lista os operadores:
	socket.on('chat/operadores', function(data) {

		var listaOperadores = [];
		
		for(x in clientBrowser) {
												
			for (y in clientBrowser[x]) {
			
				var operador = Object();
			
				operador.name = clientBrowser[x][y].nome + ' - ' + clientBrowser[x][y].token;
				operador.token = clientBrowser[x][y].token;				

				listaOperadores.push(operador);
			};						
		}
		
		result = Object();
		result.result = listaOperadores;

		var device = null;
		
		for (x in devices) {
			
			if (devices[x].id == this.id) {
				
				device = devices[x]; 
			};			
		}
  
		if (device != null) {		
			device.emit('chat/operadores', {"result": listaOperadores});
		};	
	});

	socket.on('chat/mensagem', function(data) {
	  	
		mensagem = data.result;
	
		if (devices[mensagem.to] != null) {
			
			enviaEventoAgente('chat/mensagem/receber', devices[mensagem.to].agente_id, data);
			
		} else {
					
			enviaEventoAgente('chat/mensagem/receber', listaTokens[mensagem.to], data);			
		}
		
		if (clientBrowser[mensagem.from] != null) {
			
			for (x in clientBrowser[mensagem.from]) {	
				
				data.result.nodeId = clientBrowser[mensagem.from][x].token;
				clientBrowser[mensagem.from][x].emit('chat/mensagem', data);
			}
		}		
	});
		
	// enviar a menssagem passando de quem enviou para quem, como no json.
	socket.on('chat/mensagem/enviar', function(data) {


		var recebido = new Object();
		recebido.result = data;
		data = recebido;
		
		mensagem = data.result;
			  		  
		if (clientBrowser[mensagem.to] != null) {
								
			for (x in clientBrowser[mensagem.to]) {	
								
				data.result.nodeId = clientBrowser[mensagem.to][x].id;
				clientBrowser[mensagem.to][x].emit('chat/mensagem', data);
			}
		}	

		this.emit('chat/mensagem/receber', data);

	});
	
	// recebe a menssagem passando de quem enviou para quem, como no json.
	socket.on('chat/mensagem/receber', function(data) {


	});

/* ----- Métodos para camera ----- */

	socket.on('camera/open', function (data) {

		enviaEventoAgente('camera/open', data.id, data);					
  });
	
  socket.on('camera/close', function (data) {
	  
		enviaEventoAgente('camera/close', data.id, data);					
  });

  socket.on('camera/show/browser', function (data) {
	  
	  enviaParaTodosBrowsers('camera/show/browser', data);				
});
  
/* ----- Métodos para monitoramento ----- */

  socket.on('sessao/get/agentes', function (data) {

	  enviaParaTodosBrowsers('sessao/get/agentes', agentes);		
  });  

  socket.on('sessao/get/veiculos', function (data) {
	  
	  enviaParaTodosBrowsers('sessao/get/veiculos', veiculos);		
  });  

  socket.on('sessao/get/devices', function (data) {
	  
	  var listaDevice = {};
	  
	  for (x in devices) {
		  
		  listaDevice[x] = {};
		  listaDevice[x].agente_id = devices[x].agente_id;
		  listaDevice[x].veiculo_id = devices[x].veiculo_id;
	  }

	  enviaParaTodosBrowsers('sessao/get/devices', listaDevice);		
  });  

  socket.on('sessao/get/tokens', function (data) {

	  enviaParaTodosBrowsers('sessao/get/tokens', listaTokens);		
  }); 
  
  socket.on('sessao/get/servidores', function (data) {
	  
	  var listaServidores = {};
	  
	  for (x in servidores) {
		listaServidores[x] = x;  
	  }
	  
	  enviaParaTodosBrowsers('sessao/get/servidores', listaServidores);
//	  io.sockets.emit('sessao/get/servidores', servidores);
  });  

  socket.on('sessao/get/browsers', function (data) {

	  	var listaBrowsers = {};
	  
		for (x in clientBrowser) {			
			for (y in clientBrowser[x]) {	
							
				if (clientBrowser[x] != null && clientBrowser[x][y] != null && clientBrowser[x][y].emit != null) {
					
					listaBrowsers[x] = {};
					listaBrowsers[x].nome = clientBrowser[x][y].nome;
					listaBrowsers[x].token = clientBrowser[x][y].token
					
				}
			}
		}
		
		enviaParaTodosBrowsers('sessao/get/browsers', listaBrowsers);		
  });   

});

var adicionarEventoPendente = function(id, metodo, ocorrencia) {
	
	var pendencias = [];
	var pendencia = {};

	if (eventoPendentesVeciulo[id] == null) {
		
		eventoPendentesVeciulo[id] = Object();

	} else {
		
		pendencias = eventoPendentesVeciulo[id].pendencias;		
	}

	pendencia.metodo = metodo;
	pendencia.ocorrencia = ocorrencia;

	pendencias.push(pendencia);
	eventoPendentesVeciulo[id].pendencias = pendencias;

	console.log('Adicionado evento para ser enviado ao conectar o device.');

};



var enviaEventoPendente = function (id) {

	var total = 0;

	if (eventoPendentesVeciulo[id] != null) {

		for (x in eventoPendentesVeciulo[id].pendencias) {
			
			notificaDeviceEventos(id, eventoPendentesVeciulo[id].pendencias[x]);
			total = total + 1;
		}
		
		delete eventoPendentesVeciulo[id];
		console.log('Enviados eventos para o device - ' + total + '.');
	}
};


var notificaDeviceEventos = function(token, pendencia) {
	
	var mensagem = Object();
	mensagem.result = pendencia.ocorrencia;

	devices[token].emit(pendencia.metodo, pendencia.mensagem);
};