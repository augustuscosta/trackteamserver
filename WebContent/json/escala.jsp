<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="escala" items="${listaEscala}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${escala.id}",	
	"nome": "${escala.nome}",
	"entrada": "${escala.entrada}",
	"saida": "${escala.saida}",
	"segunda": "${escala.segunda}",
	"terca": "${escala.terca}",
	"quarta": "${escala.quarta}",
	"quinta": "${escala.quinta}",
	"sexta": "${escala.sexta}",
	"sabado": "${escala.sabado}",
	"domingo": "${escala.domingo}",
	"observacoes": "${escala.observacoes}"
	}
</c:forEach>
]}