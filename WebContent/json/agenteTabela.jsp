<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="agente" items="${listaAgentes}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${agente.id}",
	"matricula": "${agente.usuario.matricula}",
	"nome": "${agente.usuario.nome}",
	"login": "${agente.usuario.login}",
	"status": "${agente.usuario.status.valor}",	
	"grupo": "${agente.usuario.grupo.nome}"
	}
</c:forEach>
],
"total" : "${total}"}