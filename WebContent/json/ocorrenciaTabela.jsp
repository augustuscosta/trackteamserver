<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="ocorrencia" items="${listaOcorrencias}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${ocorrencia.id}",
	"numero": "${ocorrencia.endereco.numero}",
	"logradouro": "${ocorrencia.endereco.logradouro}",
	"latitude": "${ocorrencia.endereco.latitude}",
	"longitude": "${ocorrencia.endereco.longitude}", 
	"resumo": "${ocorrencia.resumo}",
	"natureza1": "${ocorrencia.natureza1.valor}",	
	"operador": "${ocorrencia.usuario.nome}"}
</c:forEach>
],
"total" : "${total}"}