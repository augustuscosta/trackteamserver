<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="regra" items="${listaRegras}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${regra.id}",
	"regra": "${regra.role}",
	}
</c:forEach>
],
"total" : "${total}"}