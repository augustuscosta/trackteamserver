<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="rota" items="${listaRotas}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>
	{"id": "${rota.id}",
	"nome": "${rota.nome}"}
</c:forEach>
], 
"total" : "${total}"}