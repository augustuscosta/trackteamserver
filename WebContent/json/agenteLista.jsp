<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="natureza" items="${listaAgentes}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${usuario.id}",
	"nome": "${usuario.nome}"
	}
</c:forEach>
]}