<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="usuario" items="${listaUsuarios}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${usuario.id}",
	"matricula": "${usuario.matricula}",
	"nome": "${usuario.nome}",
	"login": "${usuario.login}",
	"status": "${usuario.status.valor}",	
	"grupo": "${usuario.grupo.nome}"
	}
</c:forEach>
],
"total" : "${total}"}