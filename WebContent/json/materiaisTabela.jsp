<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="materiais" items="${listaMateriais}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${materiais.id}",
	"titulo": "${materiais.titulo}",
	"funcionalidade": "${materiais.funcionalidade.valor}"}
</c:forEach>
],
"total" : "${total}"}