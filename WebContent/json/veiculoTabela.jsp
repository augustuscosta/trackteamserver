<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="veiculo" items="${listaVeiculos}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>
	{"id": "${veiculo.id}",
	"placa": "${veiculo.placa}",
	"ano": "${veiculo.ano}",
	"cor": "${veiculo.cor}",
	"marca": "${veiculo.marca}",
	"modelo": "${veiculo.modelo}",
	"renavam": "${veiculo.renavam}"}
</c:forEach>
], 
"total" : "${total}"}