<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="ocorrencia" items="${listaOcorrencia}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${ocorrencia.id}",
	"numero": "${ocorrencia.endereco.numero}",
	"logradouro": "${ocorrencia.endereco.logradouro}",
	"cor": "${ocorrencia.niveisEmergencia.cor}",
	"nivelEmergencia": {
		"id":"${ocorrencia.niveisEmergencia.id}", 
		"descricao": "${ocorrencia.niveisEmergencia.descricao}",
		"cor": "${ocorrencia.niveisEmergencia.cor}",
		"iconBorda": "${ocorrencia.niveisEmergencia.iconBorda}"
	},
	"latitude": "${ocorrencia.endereco.latitude}",
	"longitude": "${ocorrencia.endereco.longitude}", 
	"resumo": "${ocorrencia.resumo}",
	"natureza1": "${ocorrencia.natureza1.id}",	
	"veiculos": [ 
	<c:forEach var="veiculo" items="${ocorrencia.ocorrenciasVeiculos}" varStatus="rowCounter">
		<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${veiculo.veiculo.id}",
		"placa": "${veiculo.veiculo.placa}"
		}
	</c:forEach>	
	], 
	"usuario_id": "${ocorrencia.usuario.id}"}
</c:forEach>
]}