<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
{"list":[
<c:forEach var="veiculo" items="${listaVeiculos}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>
	{"id": "${veiculo.id}",
	"placa": "${veiculo.placa}",
	"latitude": "${veiculo.historico[0].latitude}",
	"longitude": "${veiculo.historico[0].longitude}",
	"ocorrencias": [ 
	<c:forEach var="ocorrencia" items="${veiculo.listaOcorrencia}"
		varStatus="rowCounter">
		<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${ocorrencia.id}"
		}
	</c:forEach>	
	]
	}
</c:forEach>
]}
