<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="objeto" items="${listaObjeto}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${objeto.id}",	
	"material": "${objeto.materiais.titulo}",
	"controle": "${objeto.numero_controle_interno}",
	"descricao": "${objeto.descricao}",
	"estado": "${objeto.estado.valor}"}
</c:forEach>
]}