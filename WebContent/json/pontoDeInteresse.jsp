<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="ponto" items="${listaPontoDeInteresse}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${ponto.id}",
	"nome": "${ponto.nome}",
	"telefone": "${ponto.telefone}",
	"email": "${ponto.email}",
	"contato": "${ponto.contato}",
	"latitude": "${ponto.latitude}",
	"longitude": "${ponto.longitude}",
	"bairro": "${ponto.bairro}",
	"endGeoref": "${ponto.endGeoref}",
	"logradouro": "${ponto.logradouro}",
	"numero": "${ponto.numero}"
	}
</c:forEach>
]}