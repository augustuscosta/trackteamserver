<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="regioes" items="${listaRegioes}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>
	{"id": "${regioes.id}",
	"nome": "${regioes.nome}"}
</c:forEach>
], 
"total" : "${total}"}