<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
{"list":[
<c:forEach var="agente" items="${listaAgentes}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${agente.id}",
	"latitude": "${agente.posicao.latitude}",
	"longitude": "${agente.posicao.longitude}",
	"matricula": "${agente.usuario.matricula}",
	"nome": "${agente.usuario.nome}",
	"veiculo": { "id": "${agente.veiculo.id}", "latitude": "${agente.veiculo.historico[0].latitude}", "longitude": "${agente.veiculo.historico[0].longitude}", "placa": "${agente.veiculo.placa}"}
	}
</c:forEach>
]}