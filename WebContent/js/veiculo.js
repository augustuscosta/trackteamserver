
var listaVeiculos = {};
var ocorrenciasVeiculo = [];
var limparVeiculo = new Object();
var balaoMapa;

limparVeiculo.clean = function () { 
	desmarcaVeiculo(); 
};

function setMapVeiculos(mapa) {
	
	if (mapa == null) {
		
		desmarcaVeiculo();
	}	
						
	for (x in listaVeiculos)
	{
		listaVeiculos[x].marcador.setMap(mapa);
	}
		
}

function iniciaViaturas() {

	getJSON("./veiculo/listaVeiculos",
		function(json) {
		
			json = json.result;
			
			for(var i=0; i < json.length; i++) {        
				$('#listaVeiculos').append(adicionaViaturaDiv(json[i]));
			}	
			
			getJSON("./veiculo/listaVeiculosHistorico",
				function(json) {
					
					json = json.result;
					
					for (x in json) {
						
						if (json[x].veiculo_id != null && json[x].veiculo_id > 0) {
							
							atualizaVeiculo(json[x]);
						}
					}
							
//					iniciaOcorrencias();
					
				}, function() { 
					console.log("algo deu errado"); 
				});			
						
	}, function() { 
			console.log("algo deu errado"); 
	});		
}

function adicionaViaturaDiv(viatura) {
	
	var marcador = new Object();

	marcador.setMap = function () { };
	marcador.getPosition = function() { };
	resizeIcon(marcador);
	
	marcador.id = viatura.id;
	marcador.placa = viatura.placa;
	marcador.selecionado = false;
	marcador.veiculo = viatura;

	viatura.cerca = setCerca(viatura.cerca.geoPonto);
	viatura.marcador = marcador;
	viatura.selecionado = false;
	viatura.foraDaCerca = true;
	viatura.emAtendimento = false;
	viatura.ocorrencia = 0;
	viatura.statusCor = "#FF5353";
	
	listaVeiculos[viatura.id] = viatura;
	
	if (viatura.idOrgao == null || viatura.idOrgao == "") {
		viatura.idOrgao = "-";
	}
	
	var div = "";
	div += '<li class="veiculo" id="veiculo' + viatura.id + '" style="border-left: 12px solid #FF5353">';
	div += '<div onclick="detalhesVeiculo(\'' + viatura.id + '\')" style="cursor: pointer;">';
	div += '<span> ' + viatura.idOrgao + '</span>' + viatura.placa;
	div += '</div>';
	div += '<div id="detalheVeiculo' + viatura.id + '" class="veiculoDetalhe">';
	div += '</div>';        		
	div += '</li>';
	
	return div;
}

function setCerca(geoPontos) {

	var cerca = new Array();

	if (geoPontos != null && geoPontos.length > 0) {
				
		for ( var p = 0; p < geoPontos.length; p++) {
			
			cerca.push(new google.maps.LatLng(geoPontos[p].latitude, geoPontos[p].longitude));
		}
	}
	
	return cerca;
}

function findVeiculo(id) {

	return listaVeiculos[id];
}

function detalhesVeiculo(id) {
	
	veiculo = findVeiculo(id);
	
	if (!veiculo.selecionado) {
				
		limparAtual.clean();
		
		veiculo.selecionado = true;
		
		limparVeiculo.veiculo = veiculo;
		limparAtual = limparVeiculo;

		$('#veiculos').trigger('click');

		$('#listaVeiculos').find('.veiculoDetalhe').hide();
		
		if (balaoMapa != null) {
			balaoMapa.close();
		}
		
		if (listaVeiculos[id].marcador.getPosition() != null) {

			if (listaFiltros["veiculos-cerca"].ativo) {
				
				showCerca();
			}

			
			if (listaFiltros["veiculos-rota"].ativo) {
				
				showRota();
			}
			
			listaVeiculos[id].selecionado = true;
			destacaVeiculo(listaVeiculos[id]);		
			
		}
		
		$('#detalheVeiculo' + id).html(formataDivVeiculoDetalhes(listaVeiculos[id]));
		$('#detalheVeiculo' + id).slideDown(500);

    	$('#veiculo' + id).css('background-color', '#FFF');
//    	$('#veiculo' + id).css('border-left', '12px solid #999');	
    	
	} else {

		limparAtual.clean();
	}	
}

function destacaVeiculo(veiculo) {

	var marcador = veiculo.marcador;

	resizeIcon(marcador, 'big');
	
//	showCerca(veiculo.id);
	listaLigacoes = [];
		
	var ligacao = new Object();
	ligacao.id = veiculo.id;
	ligacao.tipo = "veiculo";
	ligacao.posicao = marcador.getPosition();
	
	listaLigacoes.push(ligacao);

	for (x in listaOcorrencias) {

		for(y in listaOcorrencias[x].veiculos) {
						
			if (veiculo.id != null && veiculo.id == listaOcorrencias[x].veiculos[y].id) {
												
				var ligacao = new Object();
				ligacao.id = listaOcorrencias[x].id;
				ligacao.tipo = "ocorrencia";
				ligacao.posicao = listaOcorrencias[x].marcador.getPosition();

				listaLigacoes.push(ligacao);					
				
			}
		}
	}
		
	if (isOcorrenciasVisiveis()) {

		exibeLigacao();
	}
	
	// Calcula posi��o do veiculo na lista
	
	altura = 0;
	for (var i=0; i < $('.veiculo').size(); i++) {
		
		elemento = $('.veiculo')[i];
		if ($(elemento).attr('id') == 'veiculo' + marcador.id) {
			
			break;
		}
		
		altura = altura + $(elemento).height() + 30;
	}
	
	// Modifica a posi��o da lista para exibir o veiculo selecionado
	
	$('#listaVeiculosDiv').scrollTop(altura);

	if (!filtroMapaCalor && listaFiltros["veiculos"].ativo) {

		map.panTo(listaVeiculos[veiculo.id].marcador.getPosition());

		// Exibe o bal�o no ve��culo
		
		if (listaFiltros["veiculos-cerca"].ativo) {
			
			showCerca();
		}
		
		if (listaFiltros["veiculos-rota"].ativo) {
			
			showRota();
		}	
	
//		marcador.setZIndex(991);

		balaoMapa = new google.maps.InfoWindow({			
		    content: '<div class="balaoMaps" veiculo_id="' + veiculo.id + '">' + formataDivBalaoVeiculoDetalhes(veiculo) + '</div>'
		});
	
		balaoMapa.open(map,marcador);
	}
}

function atualizaBalao(veiculo) {
	
//	$('.balaoMaps').html(formataDivBalaoVeiculoDetalhes(veiculo));
	
	$('#balaoInfo').html(formataDivBalaoVeiculoDetalhes(veiculo, "", true));	
	$('#detalheVeiculo' + veiculo.id).html(formataDivVeiculoDetalhes(veiculo));
}

function desmarcaVeiculo() {

	for (x in listaVeiculos)
	{
		if (listaVeiculos[x].selecionado) {

			listaVeiculos[x].selecionado = false;
			
			if (listaVeiculos[x].marcador.setZIndex == 'function' ) {
				
//				listaVeiculos[x].marcador.setZIndex(10);
			}
			
			veiculo = listaVeiculos[x].marcador; 
			listaLigacoes = [];
			cercaPath.clear();
			rotaPath.clear();
	
			if (balaoMapa != null) {
				
				balaoMapa.close();
			}
				
			resizeIcon(listaVeiculos[x].marcador);			

			break;			
		} 
	}

	limpaRotaOcorrencia();
	ligacaoVeiculoOcorrencia.setPath([]);
	$('#listaVeiculos').find('.veiculoDetalhe').hide();
	
	$('#listaVeiculos').find('li').css('background-color', '#EEE');
//	$('#listaVeiculos').find('li').css('border-left', 'none');				
	
}

function showCerca() {
    
	if (!filtroMapaCalor && !listaFiltros["veiculos"].ativo) 
		return null;
	
	for (x in listaVeiculos)
	{
		if (listaVeiculos[x].selecionado) {

			$.ajax({
				type: "POST",
				url: "./veiculo/cerca?id=" + listaVeiculos[x].id,
				success: function( resposta ) {
					
		        	json = eval('(' + resposta + ')').list;       
					listaCercas = json;			       	
					
					cercaPath.clear();
					
					if (listaCercas.length > 0) {
						for (var f=0; f < listaCercas[0].geoPontos.length; f++) {
							var ponto = new google.maps.LatLng(listaCercas[0].geoPontos[f].latitude,listaCercas[0].geoPontos[f].longitude);
							cercaPath.push(ponto);
						}
					}
				}
			});			
			break;
		}
	}
}

function hideCerca() {

	listaCercas = [];
	cercaPath.clear();
}

function showRota() {

	if (!filtroMapaCalor && !listaFiltros["veiculos"].ativo) 
		return null;
		
	for (x in listaVeiculos)
	{
		if (listaVeiculos[x].selecionado) {

			$.ajax({
				type: "POST",
				url: "./veiculo/rota?id=" + listaVeiculos[x].id,
				success: function( resposta ) {
					
		        	json = eval('(' + resposta + ')').list;       
					listaRotas = json;			       	
					
					rotaPath.clear();
					
					if (listaRotas.length > 0) {
						for (var f=0; f < listaRotas[0].geoPontos.length; f++) {
							var ponto = new google.maps.LatLng(listaRotas[0].geoPontos[f].latitude,listaRotas[0].geoPontos[f].longitude);
							rotaPath.push(ponto);
						}
					}
				}
			});		
			break;
		}
	}
}

function hideRota() {

	listaRotas = [];
	rotaPath.clear();
}

function cameraVeiculo(veiculo_id, estado) {
	
	
	var veiculo = listaVeiculos[veiculo_id];
	
	var camera = new Object();
	camera.camera = "on";
		
	if (estado == "on") {
		
		divVideo = '<p style="width: 320px height: 240px;"><video width="320" height="240" controls="controls"><source src="http://www.w3schools.com/html5/movie.ogg" type="video/ogg" />	 Seu browser n�o suporta o video, favor atualizar o browser para uma vers�o mais recente.</video>';		
		balaoMapa.setContent(formataDivBalaoVeiculoDetalhes(veiculo, divVideo));
//		formataDivBalaoVeiculoDetalhes(veiculo, divVideo);
		
//		socket.emit('camera/open', camera);
		
	} else if (estado == "off") {

		divVideo = '';		
		balaoMapa.setContent(formataDivBalaoVeiculoDetalhes(veiculo, divVideo));
		
//		socket.emit('camera/close', camera);
	}
}

function formataDivVeiculoDetalhes(veiculo) {
	
	var div = "";
	div += '<p><a href="controle_geral/veiculo/editar/?id=' + veiculo.id + '">editar</a> ';
	div += '| <a href="estatistica/veiculo/historico/' + veiculo.id + '">historico</a> '; 
	div += '</p>';	

	if (veiculo.historico != null) {
		
		var date = new Date(veiculo.historico.dataHora);
		
		var formattedTime = date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();		
		div += '<p>ultima atualiza��o: ' + formattedTime + '</p> ';
	}
	
	return div;
}

function formataDivBalaoVeiculoDetalhes(veiculo, divVideo, atualizacao) {

	if (divVideo == null) {
		
		divVideo = "";
	}
	

	var date = new Date(veiculo.historico.dataHora);
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

	var year = date.getFullYear();	
	var month = date.getMonth()+1;
	var dia = date.getDate();
     
	var formattedTime = dia + '/' + month + '/' + year + ' ' + hours + ':' + minutes + ':' + seconds;
	
	var div = '<div id="balaoInfo">';
	div += '<p>' + veiculo.placa + '</p>';
	div += '<p>ultima atualiza��o: ' + formattedTime + '</p> ';
	if (veiculo.historico.rpm != null) { div += '<p>rpm: ' + veiculo.historico.rpm + '</p> '; }
	if (veiculo.historico.temperatura != null) { div += '<p>temperatura: ' + veiculo.historico.temperatura + ' �C</p> '; }
	if (veiculo.historico.tensao_bateria != null) { div += '<p>tensao bateria: ' + veiculo.historico.tensao_bateria + ' V</p> '; }
	if (veiculo.historico.velocidade != null) { div += '<p>velocidade: ' + veiculo.historico.velocidade + ' km/h</p> '; }
	
	div += '<div class="botaoBalao"><p><a href="controle_geral/veiculo/editar/?id=' + veiculo.id + '">editar</a> ';
	div += '| <a href="estatistica/veiculo/historico/' + veiculo.id + '">historico</a> ';
	div += '| <a href="javascript:;" onclick="ativaChatVeiculo(\'' + veiculo.id + '\')">chat</a> ';
	
	if (veiculo.emAtendimento) {
		div += '| <a href="javascript:;" onclick="exibeRotaOcorrencia(\'' + veiculo.id + '\')">calcular rota</a> ';
	}
	
	div += '| <a href="javascript:;" onclick="ativaCamera(\'' + getAgenteFromVeiculo(veiculo.id) + '\', \'on\');">ligar camera</a>';
//	div += divCamera;
	div += '</p></div>';
	
	div += '</div>';
	
	if (atualizacao == null || atualizacao != true) {	
		div += '<div id="video" class="video">' + divVideo + '</div>';
	}
	
	return div;
}

function ativaChatVeiculo(id) {

	$.post( "comunicacao/token/veiculo/", { 'veiculo_id': id },
	 	function( resposta ) {

			if (resposta != null && resposta.result != null) {
				
				ativaChat(resposta.result);
			} else {
				
				alert('O dispositivo est� offline');
			}
		});	
}

function adicionaMarcador(historico) {
		
	var imageVeiculo = new google.maps.MarkerImage(
			"images/icones_mapa/viatura.png",
			new google.maps.Size(78,80),
			new google.maps.Point(0, 0),
			new google.maps.Point(30,77)
	);

	if (filtroMapaCalor) 
		mapa = null;
	else
		mapa = map;

	var txtViatura ="";
	
	if (listaVeiculos[historico.veiculo_id].idOrgao !== null && listaVeiculos[historico.veiculo_id].idOrgao != "-") {
		
		txtViatura = String(listaVeiculos[historico.veiculo_id].idOrgao + " - " + listaVeiculos[historico.veiculo_id].placa);
		
	} else {
		
		txtViatura = String(listaVeiculos[historico.veiculo_id].placa);
	}
	
	marcador = new google.maps.Marker({
		position: new google.maps.LatLng(historico.latitude, historico.longitude),
		map: mapa,
		title: txtViatura, 
		draggable: false,
		icon: imageVeiculo,
		zIndex: 10
	});	

	resizeIcon(marcador);
	marcador.id = historico.veiculo_id;
	marcador.tipo = "veiculo";
	
	google.maps.event.addListener(marcador, 'click', function(event) {

		detalhesVeiculo(this.id);
	});
	
	return marcador;
}
	
function atualizaVeiculo(historico) {
					
	if (listaVeiculos[historico.veiculo_id] == null) {
		
		return;
	}

	if (listaVeiculos[historico.veiculo_id].marcador == null || listaVeiculos[historico.veiculo_id].marcador.getPosition() == null) {
		
		listaVeiculos[historico.veiculo_id].marcador = adicionaMarcador(historico);
	}
		
	listaVeiculos[historico.veiculo_id].marcador.setPosition(new google.maps.LatLng(historico.latitude, historico.longitude));	
	exibeLigacao(listaVeiculos[historico.veiculo_id]);
	
	historicoOld = listaVeiculos[historico.veiculo_id].historico;
	listaVeiculos[historico.veiculo_id].historico = historico;
	
	atualizaStatusVeiculo(listaVeiculos[historico.veiculo_id]);		
	
// verifica se o veiculo est� na cerca	
	
	if (poligonoContemPonto(new google.maps.LatLng(historico.latitude, historico.longitude), listaVeiculos[historico.veiculo_id].cerca)) {
		
		listaVeiculos[historico.veiculo_id].foraDaCerca = false;

	} else {
		
		if (!listaVeiculos[historico.veiculo_id].foraDaCerca) {
			
			addNotificacao(listaVeiculos[historico.veiculo_id].id + ' - ' + listaVeiculos[historico.veiculo_id].placa, 'Saiu da sua cerca', 'javascript:;', 'detalhesVeiculo(\'' + listaVeiculos[historico.veiculo_id].id + '\')', 'warn');
		}
		
		listaVeiculos[historico.veiculo_id].foraDaCerca = true;
	}
}

function atualizaStatusVeiculoOcorrencia(ocorrencia) {
		
	if (ocorrencia.inicio != null && (ocorrencia.fim == null || ocorrencia.fim == 0)) {
		
		for (x in ocorrencia.veiculos) {
			
			listaVeiculos[ocorrencia.veiculos[x].id].emAtendimento = true;
			listaVeiculos[ocorrencia.veiculos[x].id].ocorrencia = ocorrencia.id;
			atualizaStatusVeiculo(listaVeiculos[ocorrencia.veiculos[x].id]);
		}
		
	} else if (ocorrencia.fim != null && ocorrencia.fim > 0) {

		for (x in ocorrencia.veiculos) {
			
			listaVeiculos[ocorrencia.veiculos[x].id].emAtendimento = false;
			listaVeiculos[ocorrencia.veiculos[x].id].ocorrencia = 0;
			atualizaStatusVeiculo(listaVeiculos[ocorrencia.veiculos[x].id]);
		}		
	}
}

function atualizaStatusVeiculo(veiculo) {
		
	if (veiculo == null) {
		return;
	}
	
	var dataLimite = new Date();
	dataLimite.setMinutes(dataLimite.getMinutes()-tempoEspera);
	
	var date = 0;
	
	if (veiculo.historico != null) {
		
		date = new Date(veiculo.historico.dataHora);
	}
	
	var imageVeiculo = null;

	if (dataLimite <= date) {
		
		imageVeiculo = new google.maps.MarkerImage(
				"images/icones_mapa/viatura.png",
				new google.maps.Size(78,80),
				new google.maps.Point(0, 0),
				new google.maps.Point(30,77)
		);

		cor = "#F78C1E";
		
		if (listaVeiculos[veiculo.id] != null) {
			
			clearTimeout(listaVeiculos[veiculo.id].timeOut);
			listaVeiculos[veiculo.id].timeOut = window.setTimeout(function() { console.log("atualizando"); atualizaStatusVeiculo(listaVeiculos[veiculo.id]); }, 40000);
		}
		
	} else {
		
		imageVeiculo = new google.maps.MarkerImage(
				"images/icones_mapa/viatura_cinza.png",
				new google.maps.Size(78,80),
				new google.maps.Point(0, 0),
				new google.maps.Point(30,77)
		);
		
		cor = "#BBB";
	}
	
	if (listaVeiculos[veiculo.id] != null && listaVeiculos[veiculo.id].marcador.getIcon != null) {
		
		var tamanhoIcon = listaVeiculos[veiculo.id].marcador.getIcon().size.width;
		listaVeiculos[veiculo.id].marcador.setIcon(imageVeiculo);
		
		if (tamanhoIcon < 55) {
			
			resizeIcon(listaVeiculos[veiculo.id].marcador);
			
		} else {
			
			resizeIcon(listaVeiculos[veiculo.id].marcador, 'big');
		}
		
		listaVeiculos[veiculo.id].statusCor = cor;
	}
	

	if (veiculo.selecionado) {
		
		atualizaBalao(veiculo);
	}
	
	$('#veiculo' + veiculo.id).css('border-left', '12px solid ' + cor);	
	
	if (veiculo.historico != null && veiculo.historico.agente_id != null && listaAgentes[veiculo.historico.agente_id] != null && listaAgentes[veiculo.historico.agente_id].veiculo_id == veiculo.id) {
		
		$('#agente' + veiculo.historico.agente_id).css('border-left', '12px solid ' + cor);
	}
}

function exibeRotaOcorrencia(veiculo_id) {
	
	var rotaInicio = new google.maps.LatLng(listaVeiculos[veiculo_id].historico.latitude, listaVeiculos[veiculo_id].historico.longitude);
	var rotaFim = listaOcorrencias[listaVeiculos[veiculo_id].ocorrencia].marcador.getPosition();
	
	rotaVeiculoOcorrencia = {origin : rotaInicio, destination : rotaFim, travelMode : google.maps.DirectionsTravelMode.DRIVING}

	directionsService.route(rotaVeiculoOcorrencia, function(result, status) {
		if (status === google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(result);

			if (directionsDisplay.getMap() == null) { directionsDisplay.setMap(map); }					
		}
	});	
}

function limpaRotaOcorrencia() {
	
	if (directionsDisplay.getMap() != null) { directionsDisplay.setMap(null); }
}

function isVeiculosVisiveis() {
	
	return listaFiltros["veiculos"].ativo;
	
}