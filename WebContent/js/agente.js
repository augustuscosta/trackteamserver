
var agentes = [];
var listaAgentes = {};

var limparAgente = new Object();

limparAgente.clean = function () { 
	desmarcaAgente(); 
};

function findAgente(id) {

	return listaAgentes[id];
}

function setMapAgentes(mapa) {
	
	if (mapa == null) {
		
		desmarcaAgente();
	}	
						
	for (x in listaAgentes)
	{
		
		if (typeof listaAgentes[x].marcador.setMap == 'function') {

			listaAgentes[x].marcador.setMap(mapa);
		}		
	}
}

function iniciaAgentes() {

	getJSON("./agente/listaAgentes",
			function(json) {
			
				json = json.result;
		
				for(var i=0; i < json.length; i++) {        
					$('#listaAgentes').append(adicionaAgentes(json[i]));
				}		
				
				getJSON("./agente/listaAgentesHistorico",
					function(json) {
						
						json = json.result;
						
						for (x in json) {
						
//							if (json[x].veiculo_id != null && json[x].veiculo_id > 0) {
//							if (json[x].veiculo_id == null || json[x].veiculo_id <= 0) {
							if (json[x].agente_id != null) {
								
								atualizaAgente(json[x]);
							}
						}
														
					}, function() { 
						console.log("algo deu errado"); 
				});	
				
			}, function() { 
				console.log("algo deu errado"); 
		});	
}

function atualizaAgente(agente) {
		
	if (listaAgentes[agente.agente_id] == null) {
		
		return;		
	}
	
	var historico = agente;
	listaAgentes[agente.agente_id].historico = historico;
	
	var dataLimite = new Date();
	dataLimite.setMinutes(dataLimite.getMinutes()-tempoEspera);
	
	var date = 0;
	
	if (listaAgentes[agente.agente_id].historico != null) {
		
		date = new Date(listaAgentes[agente.agente_id].historico.dataHora);
	}
	
	
	if (agente.veiculo_id != null) {
	
		if (listaAgentes[agente.agente_id].marcador.getPosition() == null) {
	
			
			if (listaVeiculos[agente.veiculo_id] != null) {
				
				console.log(listaVeiculos[agente.veiculo_id]);
				listaAgentes[agente.agente_id].marcador = listaVeiculos[agente.veiculo_id].marcador;
				date = listaVeiculos[agente.veiculo_id].historico.dataHora;
			}
		}
		
		cor = "#F78C1E";
		
	} else {
		
		
		if (listaAgentes[agente.agente_id].marcador.getPosition() == null && listaAgentes[agente.agente_id].marcador.tipo != "agente") {
			
			listaAgentes[agente.agente_id].marcador = adicionaMarcadorAgente(agente);
		}
		
		listaAgentes[agente.agente_id].marcador.setPosition(new google.maps.LatLng(agente.latitude, agente.longitude));
		
		if (agente.veiculo_id != null) {
			
			clearTimeout(listaAgentes[agente.agente_id].timeOut);
			listaAgentes[agente.agente_id].timeOut = window.setTimeout(function() { console.log("atualizando"); atualizaAgente(listaAgentes[agente.agente_id]); }, 60000);
		} 
				
		cor = "#F78C1E";
	}
		
	if (!estaAtivo(date)) {	
			
		cor = "#BBB";
	}
	
	if (agente.veiculo_id == null) {
		atualizaMarcadorAgente(listaAgentes[agente.agente_id]);		
	}
	
	atualizaBalaoAgente(listaAgentes[agente.agente_id]);
		
	listaAgentes[agente.agente_id].statusCor = cor;
	$('#agente' + agente.agente_id).css('border-left', '12px solid ' + cor);	

}

function atualizaMarcadorAgente(agente) {
	
	var imageAgente = "";
		
	if (listaAgentes[agente.id] != null && 
			listaAgentes[agente.id].marcador != null && 			
			listaAgentes[agente.id].marcador.getIcon != null) {
		
		if (estaAtivo(new Date(listaAgentes[agente.id].historico.dataHora))) {
			
			imageAgente = new google.maps.MarkerImage(
					"images/icones_mapa/agente.png",
					new google.maps.Size(78,80),
					new google.maps.Point(0, 0),
					new google.maps.Point(30,77)
			);
			
			listaAgentes[agente.id].timeOut = window.setTimeout(function() { atualizaMarcadorAgente(agente); }, 400000);

		} else {

			imageAgente = new google.maps.MarkerImage(
					"images/icones_mapa/agente_cinza.png",
					new google.maps.Size(78,80),
					new google.maps.Point(0, 0),
					new google.maps.Point(30,77)
			);
		}
		
		var tamanhoIcon = listaAgentes[agente.id].marcador.getIcon().size.width;
		listaAgentes[agente.id].marcador.setIcon(imageAgente);
		
		if (tamanhoIcon < 55) {
			
			resizeIcon(listaAgentes[agente.id].marcador);
			
		} else {
			
			resizeIcon(listaAgentes[agente.id].marcador, 'big');
		}		
	}
}

function estaAtivo(data) {
	
	var dataLimite = new Date(data);
	dataLimite.setMinutes(dataLimite.getMinutes() + tempoEspera);	
	
	if (dataLimite >= new Date()) {	

		return true;
		
	} else {
		
		return false;
	}
}

function adicionaAgentes(agente) {
		
	var marcador = new Object();
	agente.statusCor = "#FF5353";
	
	marcador.getPosition = function() { };
	marcador.id = agente.id;

	agente.selecionado = false;
	agente.marcador = marcador;
	listaAgentes[agente.id] = agente;
		
	var div = "";
	div += '<li class="agente" id="agente' + agente.id + '" style="border-left: 12px solid ' + agente.statusCor + '">';
	div += '<div onclick="detalhesAgente(\'' + agente.id + '\')" style="cursor: pointer;">';	
	div += '<span>' + agente.matricula + '</span>' + agente.nome;
	div += '</div>';	
	div += '<div id="detalheAgente' + agente.id + '" class="agenteDetalhe">';
	div += '</div>';        		
	div += '</li>';
	
	return div;
}

function adicionaMarcadorAgente(agente) {
		
	var imageAgente = new google.maps.MarkerImage(
			"images/icones_mapa/agente_cinza.png",
			new google.maps.Size(78,80),
			new google.maps.Point(0, 0),
			new google.maps.Point(30,77)
	);
	
	marcador = new google.maps.Marker({
		position: new google.maps.LatLng(agente.latitude, agente.longitude),
		map: map,
		title: String(agente.id),
		draggable: false,
		zIndex: 10,
		icon: imageAgente
	});	
	
	resizeIcon(marcador);
	marcador.id = agente.agente_id;
	
	google.maps.event.addListener(marcador, 'click', function(event) {

		detalhesAgente(this.id);
	});
	
	return marcador;
}

function detalhesAgente(id) {

	agente = findAgente(id);
		
	if (!agente.selecionado) {
		
		limparAtual.clean();
		limparAgente.agente = agente;
		limparAtual = limparAgente;
		
		agente.selecionado = true;
		
		$('#agentes').trigger('click');

		$('#listaAgentes').find('.agenteDetalhe').hide();
		
		if (balaoMapa != null) {
			balaoMapa.close();
		}
	
		if (agente.veiculo_id != null && agente.veiculo_id > 0) {
			
			var veiculo = findVeiculo(agente.veiculo_id);	

			if (veiculo.marcador != null && veiculo.marcador.getPosition() != null) {

				listaAgentes[id].marcador = veiculo.marcador;
				destacaAgente(listaAgentes[id]);				
			}
			
		} else if (agente.marcador.getPosition() != null) {
			
			map.panTo(agente.marcador.getPosition());
			agente.selecionado = true;
			destacaAgente(agente);
		}
		
		$('#detalheAgente' + id).html(formataDivAgenteDetalhes(agente));
		$('#detalheAgente' + id).slideDown(500);
		
	} else {
		
		limparAtual.clean();
	}	
}

function destacaAgente(agente) {

	var marcador = agente.marcador;
	
//	var ligacao = new google.maps.MVCArray;
//	ligacao.push(marcador.getPosition());

	resizeIcon(marcador, 'big');

	
	altura = 0;
	for (var i=0; i < $('.agente').size(); i++) {
		
		elemento = $('.agente')[i];
		if ($(elemento).attr('id') == 'agente' + agente.id) {
			
			break;
		}
		
		altura = altura + $(elemento).height() + 30;
	}
	
	
	$('#listaAgentes').scrollTop(altura);

		
	balaoMapa = new google.maps.InfoWindow({
	    content: '<div class="balaoMaps" agente_id="' + agente.id + '">' + formataDivBalaoAgenteDetalhes(agente) + '</div>'		
	});

	balaoMapa.open(map,marcador);
}

function desmarcaAgente() {

	for (x in listaAgentes) {

		if (listaAgentes[x].veiculo_id != null && listaAgentes[x].veiculo_id > 0) {
			
			listaAgentes[x].selecionado = false;
			
			var veiculo = findVeiculo(listaAgentes[x].veiculo_id);		
			
			if (veiculo.marcador != null && veiculo.marcador.getPosition() != null) {
				
				resizeIcon(veiculo.marcador);			
			}
			
		} else if (listaAgentes[x].selecionado && typeof listaAgentes[x].marcador.setIcon == 'function') {

			listaAgentes[x].selecionado = false;			
			resizeIcon(listaAgentes[x].marcador);
			
		} else {
			
			listaAgentes[x].selecionado = false;
		}
	}

	ligacaoVeiculoOcorrencia.setPath([]);
	$('#listaAgentes').find('.agenteDetalhe').hide();
	
	if (balaoMapa != null) {
	
		balaoMapa.close();
	}
	
}

function formataDivAgenteDetalhes(agente) {

	var div = "";

	if (agente.historico != null && agente.historico.dataHora != null) {		
		div += '<p>última atualização: ' + formataData(agente.historico.dataHora) + '</p> ';
	}

	div += '<p>';
	div += '<a href="controle_geral/agentes/editar/?id=' + agente.id + '">editar</a> '; 
	div += '</p>';	
	
	return div;
}

function formataDivBalaoAgenteDetalhes(agente, divVideo, atualizacao) {	
	
	if (divVideo == null) {
		
		divVideo = "";
	}
	
	var div = '<div id="balaoInfo">';
	div += '<p>' + agente.matricula + ' - ';
	div += '' + agente.nome + '</p>';
	
	if (agente.historico != null && agente.historico.dataHora != null) {		
		div += '<p style="width: 250px;">última atualização: ' + formataData(agente.historico.dataHora) + '</p> ';
	}
	
	div += '<p>';
	div += '<a href="controle_geral/agentes/editar/?id=' + agente.id + '">editar</a> '; 
	div += '| <a href="javascript:;" onclick="ativaChatAgente(\'' + agente.id + '\')">chat</a> ';
	
	div += '| <a href="javascript:;" onclick="ativaCamera(\'' + agente.id + '\', \'on\');">ligar camera</a>';
	div += '</p>';	
	div += '</div>';
	
	if (atualizacao == null || atualizacao != true) {
		div += '<div id="video" class="video">' + divVideo + '</div>';		
	}
	
	
	return div;
}

function ativaChatAgente(id) {

	$.post( "comunicacao/token/agente/", { 'agente_id': id },
	 	function( resposta ) {

			if (resposta != null && resposta.result != null) {
				
				ativaChat(resposta.result);
			} else {
				
				alert('O dispositivo está offline');
			}
		});	
}

function atualizaBalaoAgente(agente) {

	if (agente.selecionado) {
		
//		$('.balaoMaps').html(formataDivBalaoAgenteDetalhes(agente));
		$('#balaoInfo').html(formataDivBalaoAgenteDetalhes(agente, "", false));
		$('.balaoMaps').attr('agente_id', agente.id);
		$('#detalheAgente' + agente.id).html(formataDivAgenteDetalhes(agente));
	}

}

function formataData(data) {
	
	var date = new Date(data);
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

	var year = date.getFullYear();	
	var month = date.getMonth()+1;
	var dia = date.getDate();
	
	if (minutes < 10) {
		minutes = "0" + minutes;
	}

	if (seconds < 10) {
		seconds = "0" + seconds;
	}
		
	return dia + '/' + month + '/' + year + ' ' + hours + ':' + minutes + ':' + seconds;
}

function getAgenteFromVeiculo(id) {
	
	for (x in listaAgentes) {
		
		if (listaAgentes[x].veiculo_id == id) {
			return listaAgentes[x].id;
		}
	}
}