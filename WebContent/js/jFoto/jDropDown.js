(function($) {
	$.fn.jDropDown = function(settings) {
		
		settings = $.extend({
			mensagem : "msg_padrao"
		}, settings);
			
		$(this).each(function() {		

			$(this).sortable({
				revert: true
			});

			$(this).find('ul').each(function () {

				$(this).sortable({
					revert: true
				});
			});
				
			$(this).children('li').each(function () {
				
				if ($(this).children('a').attr('ativo') != 'true') {

					$(this).children('a').attr('ativo', 'true');

					$(this).children('a').click(function() {
						
						$(this).parent().children('ul').each(function () {
							
							$(this).toggle();							
						});
						return false;
					});					
				}
				
				$(this).children('ul').children('li').each(function () {
					
					if ($(this).children('a').attr('ativo') != 'true') {

						$(this).children('a').attr('ativo', 'true');

						$(this).children('a').click(function() {

							$(this).parent().children('ul').each(function () {
								
								$(this).toggle();
							});
							
							return false;
						});	
					}
				});
				
			});
		});	
	};
})(jQuery);