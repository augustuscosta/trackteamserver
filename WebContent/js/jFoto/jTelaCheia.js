(function($) {
	
	$.fn.jTelaCheia = function(settings) {

		$.fn.extend({refresh: function() {  		
			ajustaAltura();		
			console.log('ssss');
			}		
		}); 
		
		var fn = null;
		
		if (typeof(arguments[arguments.length - 1]) == 'function') {
			
			fn = arguments[arguments.length - 1];
		}
		
		settings = $.extend({
			cabecalho : 200,
			rodape: 100
		}, settings);

		$(window).resize(function(e) {
			
			ajustaAltura();
		});
		
		$(this).each(function() {		
			
			ajustaAltura();
		});

		function ajustaAltura() {
			
			var altura = $(window).height() - settings.cabecalho - settings.rodape;
			var alturaMin = 0;

			for (var i=0; i < settings.divs.length; i++) {

				$('#' + settings.divs[i]).height('auto');
			
//				console.log($('#' + settings.divs[i]).height());
				
				if (alturaMin < $('#' + settings.divs[i]).height()) {
					
					alturaMin = $('#' + settings.divs[i]).height();
				}
			}			

			if (alturaMin > altura) {
				
				altura = alturaMin;
			};							
	
			$(this).height(altura);
			
			for (var i=0; i < settings.divs.length; i++) {

				$('#' + settings.divs[i]).height(altura);
			}
			
			if (fn != null) {				
				fn.call(this);
			}
		}
	};
})(jQuery);