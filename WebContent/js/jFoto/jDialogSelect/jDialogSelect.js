(function($) {
	 
	$.fn.jDialogSelect = function(settings) {

		var funcaoInicio = null;
		var funcaoRetorno = null;

		if (arguments.length > 2 && typeof(arguments[arguments.length - 2]) == 'function') {			
			funcaoInicio = arguments[arguments.length - 2];
			funcaoRetorno = arguments[arguments.length - 1];
		
		} else if (typeof(arguments[arguments.length - 1]) == 'function') {
			
			funcaoRetorno = arguments[arguments.length - 1];
		}
		
		settings = $.extend({
			url : '',
		}, settings);

		var lista;
		var parametros = settings;	
		var div = this;
		
		$.fn.extend({ setParametros: function(parametros) {  
			
				settings.parametros = parametros;
				
			}		
		}); 
		
		
		$(this).each(function() {		
			
			lista = settings.lista;
			
			$(div).click(function() {				
				
				if (funcaoInicio != null) {
					funcaoInicio.call(this);
				}
				
				iniciaDialogo();
				$('#dialogJDialogSelect').dialog('open');
			});
			
			
		});
				
		function iniciaDialogo(pagina) {
			
			$("body").prepend('<div id="dialogJDialogSelect" style="display: none;"></div>');
			$('#dialogJDialogSelect').load(settings.url + '/jDialogSelect.html', function() { 
				iniciaLista();
				
				$('#jDialogSelectCancelar').click(function() {
					$('#dialogJDialogSelect').dialog('close');
				});
				
				$('#jDialogSelectFiltro').keyup(function() { 

					var txtBusca = $(this).val().toUpperCase();					
					$('#jDialogSelectLista').html('');
								
					for (x in lista) {
				
						if (lista[x].toUpperCase().lastIndexOf(txtBusca) >= 0 ) {
							$('#jDialogSelectLista').append('<li class="jDialogSelectLi" id="jDialogSelect-' + x +'" rel="' + x + '">' + lista[x] + '</li>');
						}
					}
					
					adicionaPropriedadeClickNaLista();
				});
			});
			
			$('#dialogJDialogSelect').dialog({ minWidth : 400, maxHeight : 500, autoOpen : false,	resizable : false });
			
//			$('#jDialogSelectFiltro').keyup(function() { alert("opa"); });
				
//			$('#jDialogSelectFiltro').bind('keyup', function() { alert('hi'); } );
			
		}
		
		function iniciaLista() {
			
			$('#jDialogSelectLista').html("");
			for (x in lista) {
				$('#jDialogSelectLista').append('<li class="jDialogSelectLi" id="jDialogSelect-' + x +'" rel="' + x + '">' + lista[x] + '</li>');
			}		
			
			adicionaPropriedadeClickNaLista();
		}
		
		function adicionaPropriedadeClickNaLista() {
			
			$('.jDialogSelectLi').click(function() {

				if (funcaoRetorno != null) {
					funcaoRetorno.call(this, $(this).attr('rel'));
				}
				
				$('#dialogJDialogSelect').dialog('close');
			});	
		}
	};
})(jQuery);