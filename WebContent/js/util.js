
function ativaCamera(id) {
	
	camera = {};
	camera.id = id;
	camera.camera = "on";
	
	socket.emit('camera/open', camera);	
	
}

function getJSON(url, funcaoOk, funcaoErro) {
	
	$.ajax({
        type: "POST",
        url: url,
        async: false,
        success: function( resposta )
        {
//        	console.log(resposta);
//        	json = eval('(' + resposta + ')'); 
        	
        	json = resposta;
        	
            if(typeof(funcaoOk)=="function") {
            	funcaoOk.call(this, json);
            } 
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
            if(typeof(funcaoErro)=="function") {
            	funcaoErro.call;
            } 
            return null;
        }
	});	
}

function getJSON_method(url, method, funcaoOk, funcaoErro) {
	
	$.ajax({
        type: method,
        url: url,
        success: function( resposta )
        {
//        	console.log(resposta);
//        	json = eval('(' + resposta + ')'); 
        	
        	json = resposta;
        	
            if(typeof(funcaoOk)=="function") {
            	funcaoOk.call(this, json);
            } 
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
            if(typeof(funcaoErro)=="function") {
            	funcaoErro.call;
            } 
            return null;
        }
	});	
}

function formataData(timestamp) {
	
	var date = new Date(timestamp);
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

//	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
//	var year = date.getFullYear();
// 	var month = months[date.getMonth()];
	var year = date.getFullYear();	
	var month = date.getMonth()+1;
	var dia = date.getDate();
     
	var formattedTime = dia + '/' + month + '/' + year + ' ' + hours + ':' + minutes + ':' + seconds;
	
	return formattedTime;
}

function listFindObject(lista, id) {
	
	for(var i=0; i < lista.length; i++) {
		
		if (lista[i].id == id) {
			break;
		}		
	}		
	return lista[i];
}

function listSetObject(lista, objeto) {
	
	for(var i=0; i < lista.length; i++) {
		
		if (lista[i].id == objeto.id) {
			lista[i] = objeto;
			break;
		}		
	}		
}

function removeElementoDaLista(id, texto, lista, url) {
	
	var answer = confirm(texto);
	
	if (answer) {
		
		$.post( url, { 'id': id },
	   		function( resposta ) {

				resposta = resposta.list;       

				if (resposta.status == 'error') {
					
				} else {
				
					$('#mensagem-sucesso').find('h4').html(resposta.sucesso);
					$('#mensagem-sucesso').show();	
					$('#mensagem-erro').hide();
					
					$(lista).refresh();
					
					$(window).trigger('resize');							
				}
			});
	}
}

function exibeLigacao(veiculo) {
/*			
	if (listaLigacoes.length > 0) {
		
		if (arguments.length > 0) {
													
			for (var f=0; f < listaLigacoes.length; f++) {
				
				if (listaLigacoes[f].id == veiculo.id && listaLigacoes[f].tipo == "veiculo") {
					
					listaLigacoes[f].posicao = veiculo.marcador.getPosition();
				}
			}				
		}
	
		
		var ligacaoLinhas = new google.maps.MVCArray;
	
		for (var i=1; i < listaLigacoes.length; i++) {

			ligacaoLinhas.push(listaLigacoes[i].posicao);
			ligacaoLinhas.push(listaLigacoes[0].posicao);
		}
		
		ligacaoVeiculoOcorrencia.setPath(new google.maps.MVCArray([ligacaoLinhas]));
	}
*/	
}

function resizeIcon(marcador, tamanho) {
	
	if (typeof(marcador.getIcon) != 'function') {
		
		return marcador;
	}	
	
	var zIndex = 0;

	if (tamanho == 'big') {
		
		x = 78;
		y = 80;
		ancoraX = 32;
		ancoraY = 74;
		
		if ((marcador.getZIndex() > 10 && marcador.getZIndex() < 100) || (marcador.getZIndex() > 990)) {
			
			zIndex = 991;
		} else {
			
			zIndex = 900;
		}
		
	} else {
	
		x = 42;
		y = 43;
		ancoraX = 17;
		ancoraY = 40;
		
		if ((marcador.getZIndex() > 10 && marcador.getZIndex() < 100) || (marcador.getZIndex() > 990)) {
			
			zIndex = 90;
		} else {
			
			zIndex = 10;
		}
		
	}
	
	var icon = marcador.getIcon();
	
	icon.scaledSize = icon.size;
	icon.scaledSize.height = y;
	icon.scaledSize.width = x;
	icon.anchor.x = ancoraX;
	icon.anchor.y = ancoraY;
		
	marcador.setIcon(icon);
	marcador.setZIndex(zIndex);

	return marcador;
}