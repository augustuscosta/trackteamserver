var equipesProximasPath = new google.maps.MVCArray;

var equipesProximasLinha = new google.maps.Polyline({
    strokeOpacity: 0.5,
    strokeColor: "#006600",
    strokeWeight: 4
});

var imageViatura = new google.maps.MarkerImage(
	"../../../images/icones_mapa/viatura.png",
	new google.maps.Size(78,80),
	new google.maps.Point(0, 0),
	new google.maps.Point(32,74)
);

var imageAgente = new google.maps.MarkerImage(
	"../../../images/icones_mapa/agente.png",
	new google.maps.Size(78,80),
	new google.maps.Point(0, 0),
	new google.maps.Point(32,74)
);

function plotaVeiculos(listaVeiculos) {

	var classe = "";
	var veiculosOld = [];
	
	for (var i=0; i < veiculos.length; i++) {
		
		if (veiculos[i].selecionado) {			
			veiculosOld.push(veiculos[i]);
		}
	}
	
	for (x in veiculos) {
		
		veiculos[x].setMap(null);
	}

	$('#listaVeiculos').html('');	
	veiculos = [];
	var div = "";
	
	for(var i=0; i < listaVeiculos.length; i++) {
		
		var selecionado = false;
		
		for (var f=0; f < veiculosOld.length; f++) {
			
			if (veiculosOld[f].id == listaVeiculos[i].id) {
				
				selecionado = true;
				classe = ' class="selecionado"';
			}
		}
		
		var imageViatura = new google.maps.MarkerImage(
				"../../../images/icones_mapa/viatura.png",
				new google.maps.Size(78,80),
				new google.maps.Point(0, 0),
				new google.maps.Point(32,74)
			);
		
		var txtViatura ="";
		
		if (listaVeiculos[i].idOrgao !== null && listaVeiculos[i].idOrgao != "") {
			
			txtViatura = String(listaVeiculos[i].idOrgao + " - " + listaVeiculos[i].placa + " (" + listaVeiculos[i].total_ocorrencias + ")");
			
		} else {
			
			txtViatura =  String(listaVeiculos[i].placa + " (" + listaVeiculos[i].total_ocorrencias + ")");
		}
		
		var marcador = new google.maps.Marker({
		    position: new google.maps.LatLng(listaVeiculos[i].latitude, listaVeiculos[i].longitude),
		    map: mapVeiculos,
		    draggable: false,
		    title: txtViatura,
		    icon: imageViatura
		});	    

		if (listaVeiculos[i].idOrgao != null && listaVeiculos[i].idOrgao != "") {
			
			veiculoTxt = listaVeiculos[i].idOrgao + " - " + listaVeiculos[i].placa + " (" + listaVeiculos[i].total_ocorrencias + ")";
			
		} else {
			
			veiculoTxt = listaVeiculos[i].placa + " (" + listaVeiculos[i].total_ocorrencias + ")";
		}
		
		var classeEmAtendimento = 'naoEmAtendimento';
			
		if (listaVeiculos[i].emAtendimento) {
			
			classeEmAtendimento = 'emAtendimento';
		}		
		
		veiculoTxt = '<div class="' + classeEmAtendimento + '"></div>' + veiculoTxt;						
		div += '<li id="veiculoM-' + listaVeiculos[i].id +'"' + classe + '><a href="javascript:;" onclick="addRemoveVeiculoSel(' + listaVeiculos[i].id + ')">' + veiculoTxt + '</li>';	
		
		if (selecionado) {

			resizeIcon(marcador, 'big');
			
		} else {
			
			resizeIcon(marcador);
		}
		
		marcador.id = listaVeiculos[i].id;
		marcador.selecionado = selecionado;
		marcador.placa = listaVeiculos[i].placa;
		
		google.maps.event.addListener(marcador, 'click', function(event) {
			
			addRemoveVeiculo(this);	
		});
		
		veiculos.push(marcador);		        		
	}		

	$('#listaVeiculos').html(div);	
	
	equipesProximas = [];

	for(var i=0; i < 2; i++) {

		if (i < listaVeiculos.length) {
			
			equipesProximas.push(new google.maps.LatLng(listaVeiculos[i].latitude, listaVeiculos[i].longitude));
		};
	}
	
	plotaLinhaEntreOcorrenciaEquipes(equipesProximas, new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val()), mapVeiculos);	
}

function plotaAgentes(listaAgentes) {

	var agentesOld = [];
	
	for (var i=0; i < agentes.length; i++) {
					
		if (agentes[i].selecionado) {	
			agentesOld.push(agentes[i]);
		};
	}
	
	for (x in agentes) {
		
		if (agentes[x].setMap != null) {
			agentes[x].setMap(null);
		};
	}

	$('#listaAgentes').html('');	
	agentes = [];
	
	var div = "";
	
	for(var i=0; i < listaAgentes.length; i++) {

		var classe = "";
		var selecionado = false;
		
		for (var f=0; f < agentesOld.length; f++) {
			
			if (agentesOld[f].id == listaAgentes[i].id) {
				
				selecionado = true;
				classe = ' class="selecionado"';
			}
		}
		
		var imageAgente = new google.maps.MarkerImage(
				"../../../images/icones_mapa/agente.png",
				new google.maps.Size(78,80),
				new google.maps.Point(0, 0),
				new google.maps.Point(32,74)
			);
		
		var txtAgente ="";		
		var marcador = {};
		var classeEmAtendimento = "naoEmAtendimento";
		
		if (listaAgentes[i].emAtendimento) {
			
			classeEmAtendimento = 'emAtendimento';
		}		
		
		txtAgente = '<div class="' + classeEmAtendimento + '"></div>' + listaAgentes[i].matricula + " - " + listaAgentes[i].nome + "(" + listaAgentes[i].qntOcorrencias + ")";
		
		if (listaAgentes[i].latitude != null && listaAgentes[i].longitude != null) {    			    								
			
    		marcador = new google.maps.Marker({
    		    position: new google.maps.LatLng(listaAgentes[i].latitude, listaAgentes[i].longitude),
    		    map: mapAgentes,
    		    draggable: false,
    		    title: txtAgente,
    		    icon: imageAgente
    		});
    		
    		if (selecionado) {	
    			resizeIcon(marcador, 'big');	    			
    		} else {	    			
    			resizeIcon(marcador);
    		};
		}
		    		
		div += '<li id="agenteM-' + listaAgentes[i].id +'"' + classe + '><a href="javascript:;" onclick="addRemoveAgenteSel(' + listaAgentes[i].id + ')">' + txtAgente + '</li>';	    		
		
		marcador.id = listaAgentes[i].id;
		marcador.selecionado = selecionado;
		marcador.nome = listaAgentes[i].nome;
		marcador.matricula = listaAgentes[i].matricula;
		    		
		google.maps.event.addListener(marcador, 'click', function(event) {    			
			addRemoveAgente(this);	
		});
		
		agentes.push(marcador);		        		
	}		

	$('#listaAgentes').html(div);	  

	equipesProximas = [];

	for(var i=0; i < 2; i++) {

		if (i < listaAgentes.length) {
			equipesProximas.push(new google.maps.LatLng(listaAgentes[i].latitude, listaAgentes[i].longitude));
		};
	}
	
	plotaLinhaEntreOcorrenciaEquipes(equipesProximas, new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val()), mapAgentes);
}

var plotaLinhaEntreOcorrenciaEquipes = function(equipes, ocorrencia, mapa) {

	equipesProximasPath.clear();
	
	if (equipes.length > 0) {
		
		for (var f=0; f < equipes.length; f++) {
			equipesProximasPath.push(equipes[f]);
			equipesProximasPath.push(ocorrencia);
		}
	}
	
	equipesProximasLinha.setMap(mapa);
	equipesProximasLinha.setPath(equipesProximasPath);
};

var showDialogEquipe = function() {
	
	getEquipesProximos();
	
	$("#dialogAddEquipe").dialog('open');
	google.maps.event.trigger(mapAgentes, 'resize'); 	

	var posicao = new google.maps.LatLng(endereco.latitude, endereco.longitude);
	ocorrenciaAgenteMark.setPosition(posicao);
	mapAgentes.setCenter(posicao);
	
};
		
var getEquipesProximos = function() {       

	var ocorrenciaGeoPonto = new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val());
	$('#loading').show();		
	var equipesJSON = [];
	
	$.ajax({
	        type: "POST",
	        url: '../../../ocorrencia/agentesProximos',
	        async: false,
	        success: function( resposta )
	        {
	        	json = resposta.list;   
	        	
	        	for (x in json) {       	
	        	
	        		if (json[x].veiculo_id == null) {
	        			
	        			json[x].distancia = (Math.sqrt(Math.pow((json[x].latitude - ocorrenciaGeoPonto.lat()),2) + Math.pow((json[x].longitude - ocorrenciaGeoPonto.lng()), 2)));
	        			json[x].tipo = 'agente';	        		
	        			equipesJSON.push(json[x]);
	        		}
	        	}	
	        	
	        },
	        error:function(XMLHttpRequest, textStatus, errorThrown)
	        {
	        	console.log("algo deu errado");
	        	$('#loading').hide();
	        }
	});

	$.ajax({
        type: "POST",
        url: '../../../ocorrencia/veiculosProximos',
        async: false,        
        success: function( resposta )
        {
        	json = eval('(' + resposta + ')').list;        	
        	
        	for (x in json) {
        		
        		var cerca = [];
        		for (y in json[x].cerca) {
        			
        			cerca.push(new google.maps.LatLng(json[x].cerca[y].latitude, json[x].cerca[y].longitude));
        		}
        		
	        	if (poligonoContemPonto(ocorrenciaGeoPonto, cerca)) {
	        		
	        		json[x].distancia = (Math.sqrt(Math.pow((json[x].latitude - ocorrenciaGeoPonto.lat()),2) + Math.pow((json[x].longitude - ocorrenciaGeoPonto.lng()), 2)));
	        		json[x].tipo = 'veiculo';
	        		equipesJSON.push(json[x]);
	        	}; 		        	
        	}	
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
        	console.log("algo deu errado");
        	$('#loading').hide();

        }
	});	
	
	equipesJSON.sort( 
    	function(a,b){
    		return a.distancia - b.distancia;
    });
	
	plotaEquipes(equipesJSON);
	
	$('#loading').hide();
};

function plotaEquipes(listaAgentes) {

	$('#listaAgentes').html('');	
	agentes = [];
	
	var div = "";
	
	for(var i=0; i < listaAgentes.length; i++) {

		var classe = "";
		var selecionado = false;
						
		var txtLista ="";		
		var marcador = {};
		var classeEmAtendimento = "naoEmAtendimento";
		
		if (listaAgentes[i].emAtendimento) {
			
			classeEmAtendimento = 'emAtendimento';
		}		
		
		txtLista = '<div class="' + classeEmAtendimento + '"></div>' + listaAgentes[i].matricula + " - " + listaAgentes[i].nome + "(" + listaAgentes[i].qntOcorrencias + ")";
		
		if (listaAgentes[i].latitude != null && listaAgentes[i].longitude != null) {    			    								
			
			var tipoMarcador = imageAgente;
			
			if (listaAgentes[i].tipo == 'veiculo') {
				
				tipoMarcador = imageViatura;
			}
    		
			marcador = new google.maps.Marker({
				position: new google.maps.LatLng(listaAgentes[i].latitude, listaAgentes[i].longitude),
				map: mapAgentes,
				draggable: false,
				title: txtLista,
				icon: tipoMarcador
			});

			if (selecionado) {	
    			resizeIcon(marcador, 'big');	    			
    		} else {	    			
    			resizeIcon(marcador);
    		};
		}
		    		
		div += '<li id="agenteM-' + listaAgentes[i].id +'"' + classe + '><a href="javascript:;" onclick="addRemoveAgenteSel(' + listaAgentes[i].id + ')">' + txtLista + '</li>';	    		
		
		marcador.id = listaAgentes[i].id;
		marcador.selecionado = selecionado;
		marcador.nome = listaAgentes[i].nome;
		marcador.matricula = listaAgentes[i].matricula;
		    		
		google.maps.event.addListener(marcador, 'click', function(event) {    			
			addRemoveAgente(this);	
		});
		
		agentes.push(marcador);		        		
	}		

	$('#listaAgentes').html(div);	  

	equipesProximas = [];

	for(var i=0; i < 2; i++) {

		if (i < listaAgentes.length) {
			
			equipesProximas.push(new google.maps.LatLng(listaAgentes[i].latitude, listaAgentes[i].longitude));
		};
	}
	
	plotaLinhaEntreOcorrenciaEquipes(equipesProximas, new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val()), mapAgentes);
}

function plotaPontosDeInteresse(listaPontosDeInteresse) {

	var pontosDeInteresseOld = [];
	
	for (var i=0; i < pontosDeInteresse.length; i++) {
					
		if (pontosDeInteresse[i].selecionado) {	
			pontosDeInteresseOld.push(pontosDeInteresse[i]);
		};
	}
	
	for (x in pontosDeInteresse) {
		
		if (pontosDeInteresse[x].setMap != null) {
			pontosDeInteresse[x].setMap(null);
		};
	}

	$('#listaPontosDeInteresse').html('');	
	pontosDeInteresse = [];
	
	var div = "";
	
	for(var i=0; i < listaPontosDeInteresse.length; i++) {

		var classe = "";
		var selecionado = false;
		
		for (var f=0; f < pontosDeInteresseOld.length; f++) {
			
			if (pontosDeInteresseOld[f].id == listaPontosDeInteresse[i].id) {
				
				selecionado = true;
				classe = ' class="selecionado"';
			}
		}
		
		var imagePontoDeInteresse = new google.maps.MarkerImage(
				"../../../images/icones_mapa/ocorrencia_amarelo.png",
				new google.maps.Size(78,80),
				new google.maps.Point(0, 0),
				new google.maps.Point(32,74)
			);
		
		var txtPontodeInteresse ="";		
		var marcador = {};
		var classeEmAtendimento = "naoEmAtendimento";	
		
		txtPontodeInteresse = '<div class="' + classeEmAtendimento + '"></div>' + listaPontosDeInteresse[i].id + " - " + listaPontosDeInteresse[i].nome;
		
		if (listaPontosDeInteresse[i].latitude != null && listaPontosDeInteresse[i].longitude != null) {    			    								
			
    		marcador = new google.maps.Marker({
    		    position: new google.maps.LatLng(listaPontosDeInteresse[i].latitude, listaPontosDeInteresse[i].longitude),
    		    map: mapPontos,
    		    draggable: false,
    		    title: txtPontodeInteresse,
    		    icon: imagePontoDeInteresse
    		});
    		
    		if (selecionado) {	
    			resizeIcon(marcador, 'big');	    			
    		} else {	    			
    			resizeIcon(marcador);
    		};
		}
		    		
		div += '<li id="pontoDeInteresseM-' + listaPontosDeInteresse[i].id +'"' + classe + '><a href="javascript:;" onclick="addRemovePontoDeInteresseSel(' + listaPontosDeInteresse[i].id + ')">' + txtPontodeInteresse + '</li>';	    		
		
		marcador.id = listaPontosDeInteresse[i].id;
		marcador.selecionado = selecionado;
		marcador.nome = listaPontosDeInteresse[i].nome;
		    		
		google.maps.event.addListener(marcador, 'click', function(event) {    			
			addRemovePontoDeInteresse(this);	
		});
		
		pontosDeInteresse.push(marcador);		        		
	}		

	$('#listaPontosDeInteresse').html(div);	  

	equipesProximas = [];

	for(var i=0; i < 2; i++) {

		if (i < listaPontosDeInteresse.length) {
			
			equipesProximas.push(new google.maps.LatLng(listaPontosDeInteresse[i].latitude, listaPontosDeInteresse[i].longitude));
		};
	}
	
	plotaLinhaEntreOcorrenciaEquipes(equipesProximas, new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val()), mapPontos);
}

function plotaEnquetes(listaEnquetes) {

	var enquetesOld = [];
	
	for (var i=0; i < enquetes.length; i++) {
					
		if (enquetes[i].selecionado) {	
			enquetesOld.push(enquetes[i]);
		};
	}

	$('#listaEnquetes').html('');	
	enquetes = [];
	
	var div = "";
	
	for(var i=0; i < listaEnquetes.length; i++) {

		var classe = "";
		var selecionado = false;
		
		for (var f=0; f < enquetesOld.length; f++) {
			
			if (enquetesOld[f].id == listaEnquetes[i].id) {
				
				selecionado = true;
				classe = ' class="selecionado"';
			}
		}
		
		var txtEnquete ="";		
		var classeEmAtendimento = "naoEmAtendimento";
		txtEnquete = '<div class="' + classeEmAtendimento + '"></div>' + listaEnquetes[i].id + " - " + listaEnquetes[i].nome;
		marcador = new Object();
		if (selecionado) {	
			resizeIcon(marcador, 'big');	    			
		} else {	    			
			resizeIcon(marcador);
		};
		
		div += '<li id="enqueteM-' + listaEnquetes[i].id +'"' + classe + '><a href="javascript:;" onclick="addRemoveEnqueteSel(' + listaEnquetes[i].id + ')">' + txtEnquete + '</li>';	
		
		
		marcador.id = listaEnquetes[i].id;
		marcador.selecionado = selecionado;
		marcador.nome = listaEnquetes[i].nome;
		
		enquetes.push(marcador);	
		
	}		

	$('#listaEnquetes').html(div);
}
		