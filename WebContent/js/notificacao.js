
var listaNotificacao = [];
var listaNotificacaoPush = [];
var notificacaoAtiva = false;
var notificacaoPushAtiva = false;
var notificacaoId = 0;

function addNotificacao(titulo, mensagem, link, onclick, nivel) {
			
	notificacaoId++;
	
	var notificacaoPush = Object();
	notificacaoPush.texto = '<li id="notificacao-' + notificacaoId + '">';
	notificacaoPush.texto += '<b><a href="' + link + '" onclick="' + onclick + '">' + titulo + '</a></b><br />';
	notificacaoPush.texto += '<a href="' + link + '" onclick="' + onclick + '">' + mensagem + '</a>';
	notificacaoPush.texto += '</li>';
	notificacaoPush.id = notificacaoId;
	
	var notificacao = Object();
	notificacao.texto = '<li>';
	notificacao.texto += '<b><a href="' + link + '" onclick="' + onclick + '">' + titulo + '</a></b><br />';
	notificacao.texto += '<a href="' + link + '" onclick="' + onclick + '">' + mensagem + '</a>';
	notificacao.texto += '</li>';
	notificacao.id = notificacaoId;
	
	listaNotificacaoPush.push(notificacaoPush);
	listaNotificacao.push(notificacao);
	
	if (listaNotificacao.length > 5) {
		listaNotificacao.shift();
	}

	if (!notificacaoAtiva) {
		
		showNotificacaoPush();
		
	} else {
		
		notificacaoAtiva = false;
		showNotificacao();
	}
	
	if (nivel == null) {
		
		$('#notificacoes_push').css('background-color', '#00376E');
		
	} else if (nivel == 'warn') {
		
		$('#notificacoes_push').css('background-color', '#B50306');
	}
	
//	window.setTimeout(function() { hideNotificacao(); }, 6000);	
}

function showNotificacaoPush() {
	
	if (listaNotificacaoPush.length > 0 && $('#notificacoes_push').find('li').length <= 0) {

		$('#notificacao').css({'bottom' : '0px'});
		notificacaoPushAtiva = true;
		
		var notificacao = listaNotificacaoPush.shift();
		$('#notificacoes_push').prepend(notificacao.texto);
		notificacao.timmer = window.setTimeout(function() { hideNotificacaoPush(notificacao.id); }, 6000);	
	} 	
}

function showNotificacao() {
	
	if (notificacaoAtiva) {
	
		hideNotificacao();
		notificacaoAtiva = false;
		
	} else {
		
		listaNotificacaoPush = [];
		$('#notificacoes_push').find('li').remove();
		notificacaoPushAtiva = false;

		$('#notificacoes').find('li').remove();	

		if (listaNotificacao.length > 0) {
			
			for (x in listaNotificacao) {
				
				$('#notificacoes').prepend(listaNotificacao[x].texto);
			}
			
		} else {
			
			$('#notificacoes').prepend('<li>Nenhuma notificação recebida</li>');
			
		}
		notificacaoAtiva = true;
	}
}

function hideNotificacao() {
	
	$('#notificacoes').find('li').remove();	
}

function hideNotificacaoPush(id) {
	
	$('#notificacao-' + id).hide(300, function() {
		
		$(this).remove();
		
//		$('#notificacao').css({'bottom' : '-20px'});
		showNotificacaoPush();
	});	
}