
var ocorrencias = [];
var listaOcorrencias = {};
var ocorrencia = new Object();
var listaVeiculos = [];
var balaoMapa;

var limparOcorrencia = new Object();
limparOcorrencia.clean = function () { 
	desmarcaOcorrencia(); 
};

var ligacaoVeiculoOcorrencia = new google.maps.Polyline({
	path: [],
	strokeColor: "#FF0000",
	strokeOpacity: 1.0,
	strokeWeight: 2
});

function adicionaOcorrencia(ocorrencia) {
		
	if (filtroMapaCalor) 
		mapa = null;
	else
		mapa = map;
	
	var imageOcorrencia = new google.maps.MarkerImage(
			"images/icones_mapa/"  + ocorrencia.nivelEmergencia.iconBorda,
			new google.maps.Size(78,80),
			new google.maps.Point(0, 0),
			new google.maps.Point(32,74)
		);
		
	var marcador = new google.maps.Marker({
	    position: new google.maps.LatLng(ocorrencia.endereco.latitude, ocorrencia.endereco.longitude),
	    map: mapa,
	    title: String(ocorrencia.id),
	    draggable: false,
	    zIndex: 90,
	    icon: imageOcorrencia
	});
	
	resizeIcon(marcador);

	marcador.id = ocorrencia.id;
	marcador.selecionado = false;
	marcador.ocorrencia = ocorrencia;
	marcador.setZIndex(90);
	
//	ocorrencias.push(marcador);
	
	ocorrencia.marcador = marcador;
	listaOcorrencias[ocorrencia.id] = ocorrencia;

	google.maps.event.addListener(marcador, 'click', function(event) {

		detalhesOcorrencia(this.id);
	});
	
	// atualiza o status do carro
	
	atualizaStatusVeiculoOcorrencia(ocorrencia);
	
	//
	
	return formataDivOcorrencia(ocorrencia);
}

function formataDivOcorrencia(ocorrencia) {
		
	var corDestaque;
	
	if (ocorrencia.fim == "" || ocorrencia.fim == null || ocorrencia.fim <= 0) {
		
		corDestaque = ocorrencia.nivelEmergencia.cor;	
		
	} else {
		
		corDestaque = '#AAA';
	}
	
	var div = "";
	div += '<li style="background-color: #EEE; border-left: 12px solid ' + corDestaque + '" class="ocorrencia" id="ocorrencia' + ocorrencia.id + '">';
	div += '	<div onclick="detalhesOcorrencia(\'' + ocorrencia.id + '\')" style="cursor: pointer;">';
	div += '		<span>' + ocorrencia.id + '</span>';
	div += '		<div class="enderecoOcorrencia">' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero + '</div>';
	div += '		</div>';
	div += '		<div id="detalheOcorrencia' + ocorrencia.id + '" class="ocorrenciaDetalhe">';
	div += '	</div>';        		
	div += '</li>';
	
	return div;
	
}

function alteraOcorrencia(ocorrencia) {
	
	var ocorrenciaOld = listaOcorrencias[ocorrencia.id];
	var marcador = listaOcorrencias[ocorrencia.id].marcador;
	
	marcador.setMap(null);

	if (filtroMapaCalor) 
		mapa = null;
	else
		mapa = map;
	
	var imageOcorrencia = new google.maps.MarkerImage(
			"images/icones_mapa/" + ocorrencia.nivelEmergencia.iconBorda,
			new google.maps.Size(78,80),
			new google.maps.Point(0, 0),
			new google.maps.Point(32,74)
		);
		
	var marcador = new google.maps.Marker({
	    position: new google.maps.LatLng(ocorrencia.endereco.latitude, ocorrencia.endereco.longitude),
	    map: mapa,
	    title: String(ocorrencia.id),
	    draggable: false,
	    zIndex: 90,
	    icon: imageOcorrencia
	});

	marcador.id = ocorrencia.id;
	marcador.setZIndex(90);
	
	google.maps.event.addListener(marcador, 'click', function(event) {

		detalhesOcorrencia(this.id);
	});
	
	resizeIcon(marcador);	
	ocorrencia.marcador = marcador;	
	listaOcorrencias[ocorrencia.id] = ocorrencia;
	
	var div = "";
	div += '<div onclick="detalhesOcorrencia(\'' + ocorrencia.id + '\')" style="cursor: pointer;">';
	div += '<span>' + ocorrencia.id + '</span>';
	div += '<div class="enderecoOcorrencia">' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero + '</div>';
	div += '</div>';
	div += '<div id="detalheOcorrencia' + ocorrencia.id + '" class="ocorrenciaDetalhe">';
	div += '</div>';        		

	$('#ocorrencia' + ocorrencia.id).css('background-color', '#EEE');
	$('#ocorrencia' + ocorrencia.id).html(div);

	if (ocorrencia.fim == "" || ocorrencia.fim == null || ocorrencia.fim <= 0) {
		
		$('#ocorrencia' + ocorrencia.id).css('border-left', '12px solid ' + ocorrencia.nivelEmergencia.cor);	
		
	} else {
		
		$('#ocorrencia' + ocorrencia.id).css('border-left', '12px solid #AAA');
	}

	// atualiza o status do carro
	
	atualizaStatusVeiculoOcorrencia(ocorrencia);
	
	//
	
	var ativaAlerta = true;
	
	if (ocorrenciaOld.recebimento != ocorrencia.recebimento) {
		
		addNotificacao(ocorrencia.id + ' - ' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero , 'Atendimento recebido pelo agente', 'javascript:;', 'detalhesOcorrencia(\'' + ocorrencia.id + '\')');
		ativaAlerta = false;
	}

	if (ocorrenciaOld.inicio != ocorrencia.inicio) {

		addNotificacao(ocorrencia.id + ' - ' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero , 'Atendimento iniciado', 'javascript:;', 'detalhesOcorrencia(\'' + ocorrencia.id + '\')');
		ativaAlerta = false;
	}

	if (ocorrenciaOld.fim != ocorrencia.fim) {

		addNotificacao(ocorrencia.id + ' - ' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero , 'Atendimento conclu�do', 'javascript:;', 'detalhesOcorrencia(\'' + ocorrencia.id + '\')');
		ativaAlerta = false;
//		removeOcorrencia(ocorrencia);
	}

	if (ativaAlerta) {
		
		addNotificacao(ocorrencia.id + ' - ' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero , 'Atendimento alterada', 'javascript:;', 'detalhesOcorrencia(\'' + ocorrencia.id + '\')');
	}
	
//	console.log(ocorrenciaOld);
/*	
	if (ocorrenciaOld.selecionado) {
		detalhesOcorrencia(ocorrenciaOld.id);
	}
*/
}

function removeOcorrencia(ocorrencia) {

	$('#ocorrencia' + ocorrencia.id).remove();
	
	if (listaOcorrencias[ocorrencia.id] != null) {
		
		listaOcorrencias[ocorrencia.id].marcador.setMap(null);
	}
		
	addNotificacao(ocorrencia.id + ' - ' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero , 'Atendimento removido', 'javascript:;', '');
	delete listaOcorrencias[ocorrencia.id];

}

function adicionaNovaOcorrencia(id) {

	getJSON("./ocorrencia/ocorrencia?id=" + id,
		function(json) {
			for(var i=0; i < json.length; i++) {        
				$('#listaOcorrencias').prepend(adicionaOcorrencia(json[i]));
			}		
		}, function() { 
			console.log("algo deu errado"); 
	});	
}

function setMapOcorrencias(mapa) {
	
	if (mapa == null) {
		
		desmarcaOcorrencia();
	}

	for (x in listaOcorrencias) {
		
		listaOcorrencias[x].marcador.setMap(mapa);
	}
}

function iniciaOcorrencias() {

	getJSON("./ocorrencia/ocorrencias",
		function(json) {
		
			json = json.result;
			
			for(var i=0; i < json.length; i++) {        
				$('#listaOcorrencias').append(adicionaOcorrencia(json[i]));
			}		
		}, function() { 
			console.log("algo deu errado"); 
	});	
}

function findOcorrencia(id) {

	return listaOcorrencias[id];
}

function detalhesOcorrencia(id) {

	var ocorrencia = findOcorrencia(id);
	
	if (!ocorrencia.selecionado) {
				
		limparAtual.clean();			
			
		limparOcorrencia.ocorrencia = ocorrencia;
		limparAtual = limparOcorrencia;

		$('#ocorrencias').trigger('click');

		$('#listaOcorrencias').find('.ocorrenciaDetalhe').hide();

		ocorrencia.selecionado = true;
		
		ocorrenciaCentral = getOcorrenciaCentral(id);
		ocorrenciaCentral.selecionado = ocorrencia.selecionado;
		ocorrenciaCentral.marcador = ocorrencia.marcador;
				
//		console.log(ocorrenciaCentral);
		
    	$('#detalheOcorrencia' + id).html(formataDivOcorrenciaDetalhes(ocorrenciaCentral));
    	$('#detalheOcorrencia' + id).slideDown(500);
    	$('#ocorrencia' + id).css('background-color', '#FFF');
   	
		destacaOcorrencia(ocorrenciaCentral);

	} else {
		
		limparAtual.clean();
	}
}

function getOcorrenciaCentral(id) {
		
	var json;
	
	$.ajax({
        type: "POST",
        url: "./ocorrencia/" + id,
        async: false,
        success: function( resposta )
        {
        	json = resposta.list;
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
			console.log("algo deu errado"); 
        }
	});	
	
	return json;
}

function destacaOcorrencia(ocorrencia) {
	
	listaLigacoes = [];
		
	var marcador = ocorrencia.marcador;

	// Calcula posi��o da ocorrencia na lista
	
	altura = 0;
	for (var i=0; i < $('.ocorrencia').size(); i++) {
		
		elemento = $('.ocorrencia')[i];
		if ($(elemento).attr('id') == 'ocorrencia' + marcador.id) {
			
			break;
		}
		
		altura = altura + $(elemento).height() + 30;
	}
	
	// Modifica a posi��o da lista para exibir a atividade selecionada
	
	$('#listaOcorrenciasDiv').scrollTop(altura);
		
	if (!filtroMapaCalor && listaFiltros["ocorrencias"].ativo) {
		
		map.panTo(ocorrencia.marcador.getPosition());

		resizeIcon(marcador, 'big');
		
		// Exibe o bal�o na atividade
		
		if (balaoMapa != undefined) {
			balaoMapa.close();
		}
		
		balaoMapa = new google.maps.InfoWindow({
			content: '<div class="balaoMaps">' + formataDivBalaoDetalhes(ocorrencia) + '</div>'
		});
		
		balaoMapa.open(map, marcador);			
	}  		
}

function desmarcaOcorrencia() {

	if (filtroMapaCalor)  
		return null;
		
	for (x in listaOcorrencias) {
			
		if (listaOcorrencias[x].selecionado) {
			
			listaOcorrencias[x].selecionado = false;		
			$('#detalheOcorrencia' + limparAtual.ocorrencia.id).hide(500);
			listaLigacoes = [];
			
	    	$('#ocorrencia' + listaOcorrencias[x].id).css('background-color', '#EEE');
	
	    	resizeIcon(listaOcorrencias[x].marcador);
	    	
			if (balaoMapa != null) {
				
				balaoMapa.close();
			}	
			break;
		}
	}
		
	ligacaoVeiculoOcorrencia.setPath([]);
	
}

function formataDivOcorrenciaDetalhes(ocorrencia) {
	
	var div = "";
	div += '<p style="margin: 0px; color: #000; font-style: italic; text-align: justify;">' + ocorrencia.resumo + '</p>';	
	div += exibeDatas(ocorrencia) ;	
	div += '<p></p>';
	div += '<p style="margin: 0px; color: #000;"><b>Estado: </b>' + getStatus(ocorrencia) + '</p>';	
	div += '<p style="margin: 0px; color: #000;"><b>Natureza: </b>' + ocorrencia.naturezaObj.valor + '</p>';
	
	if (ocorrencia.meioCadastro.valor != null) {
		
		div += '<p style="margin: 0px; color: #000;"><b>Meio cadastro: </b>' + ocorrencia.meioCadastro.valor + '</p>';
	}

	if (ocorrencia.veiculos.length > 0) {
		
		div += '<div style="display: inline-block; width: 100%; margin: 5px 0 0 0; padding: 0px; border-bottom: 1px solid #ccc;">';
//		div += '	<img src="images/icones/viatura.png" style="float:left; margin: 2px 3px;">';
		div += '	<img src="images/icones_mapa/viatura_cinza.png" width="20" style="float:left; margin: 0px 3px;">';		
		div += '	<p style="font-weight:bold; margin: 0 5px;" >Viatura(s): </h1>';
		div += '</div>';
		
		div += '<ul class="listaViaturas">';
		
		for (var i=0; i < ocorrencia.veiculos.length; i++) {
			div += '<li onclick="detalhesVeiculo(\'' + ocorrencia.veiculos[i].id + '\')" style="cursor: pointer;">' + ocorrencia.veiculos[i].placa + '</li>';
		}
		div += '</ul>';				
	}

	if (ocorrencia.agentes.length > 0) {
		
		div += '<div style="display: inline-block; width: 100%; margin: 5px 0 0 0; padding: 0px; border-bottom: 1px solid #ccc;">';
		div += '	<img src="images/icones_mapa/agente_cinza.png" width="20" style="float:left; margin: 0px 3px;">';
		div += '	<p style="font-weight:bold; margin: 0 5px;" >Agente(s): </h1>';
		div += '</div>';
		
		div += '<ul class="listaViaturas">';
		
		for (var i=0; i < ocorrencia.agentes.length; i++) {
			div += '<li onclick="detalhesVeiculo(\'' + ocorrencia.agentes[i].id + '\')" style="cursor: pointer;">' + ocorrencia.agentes[i].matricula + " - " + ocorrencia.agentes[i].nome + '</li>';
		}
		div += '</ul>';				
	}

	div += '<p style="margin: 10px 0 0 0px;"><a href="javascript:;" onclick="exibeDetalhes(\'' +  ocorrencia.id + '\');">detalhes</a> ';

	
	if (ocorrencia.flag == null || ocorrencia.flag == 0 || ocorrencia.flag == '0') {
		
		div += '| <a href="./controle_geral/ocorrencias/editar/?id=' + ocorrencia.id +'">editar</a> ';
		div += '| <a href="javascript:;" onclick="validarOcorrencia(\'' + ocorrencia.id + '\')">validar</a> ';
		
	} else {
		
		if (ocorrencia.usuario.id == parseInt(usuario.id)) {
			
			div += '| <a href="./controle_geral/ocorrencias/editar/?id=' + ocorrencia.id +'">editar</a> ';
			div += '| <a href="javascript:;" onclick="showOcorrenciaEncerrar(\'' + ocorrencia.id + '\')">encerrar</a> '; 	
			div += '| <a href="javascript:;" onclick="showOcorrenciaComentario(\'' + ocorrencia.id + '\')">observações</a> ';	
		} else {
			
			div += '| <a href="javascript:;" onclick="showOcorrenciaAssumir(\'' + ocorrencia.id + '\')">assumir</a> ';
		}
	}

	div += '</p>';	
	
	return div;
}

function formataDivBalaoDetalhes(ocorrencia) {
		
	var div = "";
	div += '<p>' + ocorrencia.id + ' - ' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero + '</p>';
	div += '<p>' + ocorrencia.resumo + '</p>';
	div += '<p><b>Estado: </b>' + getStatus(ocorrencia) + '<br />';	
	div += '<b>N. emergência: </b>' + ocorrencia.nivelEmergencia.descricao + '</p>';
	div += exibeDatas(ocorrencia);	
	div += '<div class="botaoBalao">';
	div += '<p><a href="javascript:;" onclick="exibeDetalhes(\'' +  ocorrencia.id + '\');">detalhes</a> ';	

	if (ocorrencia.flag == null || ocorrencia.flag == 0 || ocorrencia.flag == '0') {
		
		div += '| <a href="javascript:;" onclick="validarOcorrencia(\'' + ocorrencia.id + '\')">validar</a> ';
		
	} else {
		
		if (ocorrencia.usuario.id == parseInt(usuario.id)) {
			
			div += '| <a href="javascript:;" onclick="showOcorrenciaComentario(\'' + ocorrencia.id + '\')">observações</a> ';
	
		} else {
			
			div += ' | <a href="javascript:;" onclick="showOcorrenciaAssumir(\'' + ocorrencia.id + '\')">assumir</a> ';
		}
	}
	
	div += '</p>';	
	div += '</div>';
	
	return div;
}

function getStatus(ocorrencia) {
	
	var status = "Aguardando atendimento";
	
	if (ocorrencia.inicio > 0) {		
		
		status = "Em atendimento";
	}
	
	if (ocorrencia.fim > 0) {
		
		status = "Encerrada pelo agente (device)";
	}
	
	return status;
}

function exibeDatas(ocorrencia) {
	
	div = "";

	div += "<p>";

	if (ocorrencia.usuario == null || ocorrencia.usuario.id == null) {
		ocorrencia.usuario == {};
		ocorrencia.usuario.nome = "Não definido";
	}
	
	div += '<p>Usuário responsável: ' + ocorrencia.usuario.nome + '<br />';

	div += '<p>Data criação: ' + formataData(ocorrencia.horaCriacao) + '<br />';

	if (ocorrencia.inicioProgramado > 0) {		
		div += '<p>Inicio programado: ' + formataData(ocorrencia.inicioProgramado) + '<br />';
	}
	if (ocorrencia.fimProgramado > 0) {		
		div += 'Fim programado: ' + formataData(ocorrencia.fimProgramado);
	}
	div += "</p>";
	
	div += "<p>";
	if (ocorrencia.inicio > 0) {		
		div += 'Início: ' + formataData(ocorrencia.inicio) + '<br />';
	}
	if (ocorrencia.horaChegada > 0) {		
		div += 'Chegada: ' + formataData(ocorrencia.horaChegada) + '<br />';
	}
	if (ocorrencia.fim > 0) {		
		div += 'Fim: ' + formataData(ocorrencia.fim) + '<br />';
	}
	div += "</p>";
	
	return div;
}

function isOcorrenciasVisiveis() {

	return listaFiltros["ocorrencias"].ativo;
}