<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
 
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../../css/tipTip.css" rel="stylesheet" />
	
</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
	<script type="text/javascript" src="../../../../js/jquery-ui-1.8.16.custom.min.js"></script>  
	<script type="text/javascript" src="../../../../js/gmap3.js"></script> 
	<script type="text/javascript" src="../../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../../js/jquery.tipTip.js"></script> 
	<script type="text/javascript" src="../../../../js/jFoto/jFotoForm.js"></script> 	
	
</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');

	var mapAddCerca;
	var path = new google.maps.MVCArray;
	var markers = [];
	
	var poly = new google.maps.Polygon({
	    fillColor: '#FF0000',
	    strokeOpacity: 0.3,
	    strokeColor: "#FF0000",
	    strokeWeight: 2,
	    fillOpacity: 0.05
	});
	
	var imagePontoCerca = new google.maps.MarkerImage(
		"../../../../images/square_transparent.png",
		new google.maps.Size(11, 11),
		new google.maps.Point(0, 0),
		new google.maps.Point(6, 6)
	);
		
	function inicio() {
		
		parent.location='../listar/';
	}

	$("#mapaAddCerca").ready( function() { 
		
		$("#mapaAddCerca").gmap3({ 
			action:'init',
			options:{
				center:[latlngMapa.lat(),latlngMapa.lng()],
				zoom: 14
			}	
		});
		
		mapAddCerca = $('#mapaAddCerca').gmap3('get');
		mapRef = mapAddCerca;
		   
		$('#mapaAddCerca').append('<input id="enderecoCerca" type="text" style="background-color: #FFF;" />');
	
	  	poly.setMap(mapAddCerca);
	    poly.setPaths(new google.maps.MVCArray([path]));
	    
	    google.maps.event.addListener(mapAddCerca, "click", function(event) {
	    	
	    	addPoint(event);
			addPontos();
		});
		
		$('#enderecoCerca').autocomplete({
			source: function() {
			    $("#mapaAddCerca").gmap3({
			    	action:'getAddress',
			      	address: $(this).val(),
			      	callback:function(results){
			        	if (!results) return;
			        	$('#enderecoCerca').autocomplete('display', results, false);
			      	}
			    });
			},
			cb:{
				cast: function(item){
				  	return item.formatted_address;
				},
				select: function(item) {				
					mapAddCerca.setCenter(item.geometry.location);				
				}
		 	}
		});
	}); 

	function addPoint(event) {
	  	
		path.insertAt(path.length, event.latLng);
			
		var marker = new google.maps.Marker({
		  position: event.latLng,
		  map: mapAddCerca,
		  draggable: true,
		  icon: imagePontoCerca
		});
		markers.push(marker);
		marker.setTitle("#" + path.length);
		
		google.maps.event.addListener(marker, 'click', function() {
		 
			marker.setMap(null);
			for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
			markers.splice(i, 1);
			path.removeAt(i);
			addPontos();
		
		});

		google.maps.event.addListener(marker, 'dragend', function() {
		 
			for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
			path.setAt(i, marker.getPosition());    
			addPontos();
		
		});
	}

	function addPontos() {
		
		cerca = "";
		
	    for (var i = 0; i < markers.length; ++i) {
			posicao = path.getAt(i);
//			cerca = cerca + posicao.lat() + "," + posicao.lng() + "; ";		
			cerca = cerca + '<input type="hidden" name="pontos[]" value="' + posicao.lat() + "," + posicao.lng() + '" />';
		}

	    if (markers.length > 0) {
	    	posicao = path.getAt(0);
//			cerca = cerca + posicao.lat() + "," + posicao.lng() + "; ";		
//			cerca = cerca + '<input type="hidden" name="pontos[]" value="' + posicao.lat() + "," + posicao.lng() + '" />';
			cerca = cerca + '<input type="hidden" name="passou" value="true" validate="vazio" />';
	    } else {
	    	
	    	cerca = '<input type="hidden" name="passou" value="" validate="vazio" />';
	    }		  

		$('#cerca').html(cerca);
			
	}
	
	function addCerca(form) {
		
		return true;	
	}

	function limparCerca() {
		
		path = new google.maps.MVCArray;
	    poly.setPaths(new google.maps.MVCArray([path]));

	    for(var i=0; i<markers.length; i++) {
	    
	    	markers[i].setMap(null);
	    }
	    
	    markers = [];	    
	}
	
	$(document).ready(function() {

		$("#formAddCerca").jFotoForm({ajax: false}, function () {
			return addCerca(this);
		});
		
<c:if test="${not empty cerca.geoPonto}">

	var limites = new google.maps.LatLngBounds();
<c:forEach var="ponto" items="${cerca.geoPonto}">

	var elem = Object();
	var lat = ${ponto.latitude};
	var lng = ${ponto.longitude};
	elem.latLng = new google.maps.LatLng(lat, lng);		
	addPoint(elem);
	limites.extend(elem.latLng);
	
</c:forEach>

	mapAddCerca.setCenter(limites.getCenter());
	addPontos();
</c:if>	
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>${cerca.id == null ? 'Adicionar' : 'Alterar'} cerca</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />

		<form id="formAddCerca" action="../editar/" method="post">
		 
		<div class="form">
			<div class="fields">
					
				<div class="field">
	
					<input name="id" class="medium" type="hidden" value="${cerca.id}" />
					<div class="label">
						<label for="input-medium">Nome<b style="color: #FF0000; font-size: 14px;"> * </b>:</label>
					</div>
										
					<div class="input" style="margin-bottom: 10px;">
						<div class="ajuda" style="float:right; margin-top: 5px;" title="Nome da cerca">?</div>
						<input name="nome" class="medium" type="text" value="${cerca.nome}" style="background-color: #FFF;" validate="vazio" tmMinimo="5" />						
						<div id="errorAddCerca" class="error"></div>
						<div id="cerca">
							<input type="hidden" name="passou" value="" validate="vazio" msgErro="nenhuma cerca definida"/>						
						</div>											
					</div>						  		
				</div>

				<div class="field">					
					<div id="mapaDivAddCerca" style="width: 100%;">
						<div id="mapaAddCerca" style="height: 400px; padding: 7px;"></div>
					</div>			  				
				</div>
				
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${cerca.id == null ? 'adicionar' : 'alterar'}" />
					</div>
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="limpar" onclick="limparCerca();" />
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />
				</div>
				
			</div>
		</div>
		
		</form>
				

	</div>
	
</jsp:body>

</layout:controleGeral>
