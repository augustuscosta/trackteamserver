<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
 
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../../css/tipTip.css" rel="stylesheet" />
	
</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
	<script type="text/javascript" src="../../../../js/jquery-ui-1.8.16.custom.min.js"></script>  
	<script type="text/javascript" src="../../../../js/gmap3.js"></script> 
	<script type="text/javascript" src="../../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../../js/jquery.tipTip.js"></script> 
	<script type="text/javascript" src="../../../../js/jFoto/jFotoForm.js"></script> 	
	<script type="text/javascript" src="../../../../js/contemPonto.js"></script> 	
	
</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');

var mapAddRota;
var path = new google.maps.MVCArray;
var markers = [];

var listaCercas = [];
var cercaPath = new google.maps.MVCArray;
var cercaRotaPath = new google.maps.MVCArray;
var cercaPontos = []; 

var tipoMarcar = false;
var rotaInicio = "";
var rotaFim = "";
var rota;
var directionsDisplay = "";

var marcadorA = Object();
var marcadorB = Object();
var comRota = false;
var rotaOk = true;

var directionsService = new google.maps.DirectionsService();
directionsDisplay = new google.maps.DirectionsRenderer();

var poly = new google.maps.Polygon({
    fillColor: '#FF0000',
    strokeOpacity: 0.3,
    strokeColor: "#FF0000",
    strokeWeight: 2,
    fillOpacity: 0.05
});

var cercaRota = new google.maps.Polyline({
    strokeOpacity: 0.3,
    strokeColor: "#FF0000",
    strokeWeight: 2
});

var rotaPath = new google.maps.MVCArray;
var rota = new google.maps.Polyline({
    strokeOpacity: 0.4,
    strokeColor: "#0000FF",
    strokeWeight: 5
});

var pontoA = new google.maps.MarkerImage(
	"../../../../images/icones/marker_greenA.png",
	new google.maps.Size(20, 34),
	new google.maps.Point(0, 0),
	new google.maps.Point(10, 32)
);
	
var pontoB = new google.maps.MarkerImage(
	"../../../../images/icones/marker_greenB.png",
	new google.maps.Size(20, 34),
	new google.maps.Point(0, 0),
	new google.maps.Point(10, 32)
);
	

function inicio() {
	
	parent.location='../listar/';
}

	$("#mapaAddRota").ready(function(){
		  
		$("#mapaAddRota").gmap3({ 
			action:'init',
			options:{
				center:[latlngMapa.lat(),latlngMapa.lng()],
				zoom: 14
			}	
		});
		
		mapAddRota = $('#mapaAddRota').gmap3('get');    
		$('#mapaAddRota').append('<input id="enderecoRota" type="text" style="background-color: #FFF;" />');
		directionsDisplay.setOptions({draggable: true, preserveViewport: true});
		directionsDisplay.setMap(null);
	
	  	cercaRota.setMap(mapAddRota);
	  	cercaRota.setPath(new google.maps.MVCArray([cercaRotaPath]));

	  	rota.setMap(mapAddRota);
	  	rota.setPath(new google.maps.MVCArray([rotaPath]));
	  	
	    google.maps.event.addListener(mapAddRota, "click", function(event) {
	  	  
	    	if (comRota) {	    		
				marcadorA.setMap(null);
				marcadorB.setMap(null);
				rota.setMap(null);
				comRota = false;
	    	}
			
	    	if (rotaInicio == "") {
	    		
	    		rotaInicio = event.latLng;     		
				rota = {origin : rotaInicio, destination : rotaInicio, travelMode : google.maps.DirectionsTravelMode.DRIVING}
				
	    	} else {
	    		
	    		rotaFim = event.latLng;   		    		
				rota = {origin : rotaInicio, destination : rotaFim, travelMode : google.maps.DirectionsTravelMode.DRIVING}
	    	}
  	
			directionsService.route(rota, function(result, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(result);

					if (directionsDisplay.getMap() == null) { directionsDisplay.setMap(mapAddRota); }					
				}
			});
	    });
	
	    google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {  
				
	    	rotaOk = true;
	    	
			if (directionsDisplay.getDirections() != null) {
				
				rota = directionsDisplay.getDirections().routes[0].overview_path;
				
				for (var i=0; i<rota.length; i++) {
					
					if (!poligonoContemPonto(rota[i], cercaPontos)) {
						
						rotaOk = false;
					}
				}
									
			} else {
				
				rotaOk = false;
			}
			
			if (!rotaOk) {
				
				$('#mensagem-erro').show();
				$('#mensagem-erro-rota').show();
				
			} else {

				$('#mensagem-erro').hide();
				$('#mensagem-erro-rota').hide();

			}
	    });
	
	    
		$('#enderecoRota').autocomplete({
			source: function() {
			    $("#mapaAddRota").gmap3({
			    	action:'getAddress',
			      	address: $(this).val(),
			      	callback:function(results){
			        	if (!results) return;
			        	$('#enderecoRota').autocomplete('display', results, false);
			      	}
			    });
			},
			cb:{
				cast: function(item){
				  return item.formatted_address;
				},
				select: function(item) {				
					mapAddRota.setCenter(item.geometry.location);				
				}
		 	}
		});
	});

	function exibeCercaRota(id) {
		
		if (id == null || id <= 0) {
			
			return;
		}
		
		$.post('../../../veiculo/cerca/', {'id': id}, function(data) {
			  
			var cerca = data.result;
			
			cercaRotaPath.clear();
			var limites = new google.maps.LatLngBounds();

			for (var f=0; f < cerca.geoPonto.length; f++) {
				var ponto = new google.maps.LatLng(cerca.geoPonto[f].latitude,cerca.geoPonto[f].longitude);
				cercaRotaPath.insertAt(cercaRotaPath.length, ponto);
				cercaPontos.push(ponto);
				limites.extend(ponto);
			}
			
			// Fecha a cerca
			
			var ponto = new google.maps.LatLng(cerca.geoPonto[0].latitude,cerca.geoPonto[0].longitude);
			cercaRotaPath.insertAt(cercaRotaPath.length, ponto);
						
			if (directionsDisplay != null && directionsDisplay.getDirections() != null) {
				
				var rota = directionsDisplay.getDirections().routes[0].overview_path;
				
				for (var i=0; i<rota.length; i++) {
					
					limites.extend(new google.maps.LatLng(rota[i].lat(),rota[i].lng()));
				}
			}
			
			mapAddRota.setCenter(limites.getCenter());
		});
	}
	
	function addRota(form) {
		
		if ((directionsDisplay.getDirections() == null && !comRota) || !rotaOk) {
	
			$('#mensagem-erro').show();
			$('#mensagem-erro-rota').show();
			
			return false;
		}
				
		if (directionsDisplay.getDirections() != null) {
			
			var pontos = "";
			
			rota = directionsDisplay.getDirections().routes[0].overview_path;
			
			for (var i=0; i<rota.length; i++) {
				
				pontos += '<input type="hidden" name="pontos[' + i +']" value="' + rota[i].lat() + ';' + rota[i].lng() + '">';
			}
				
			$('#geoPontos').html(pontos);
		}
		
		return true;
	}
	
	function limparRota() {

    	if (comRota) {	    		
			marcadorA.setMap(null);
			marcadorB.setMap(null);
			rota.setMap(null);
			comRota = false;
    	}
    	
		rotaInicio = "";
		rotaFim = "";
		directionsDisplay.setMap(null);	
	}

	function calculaNovaRota() {
		
		marcadorA.setMap(null);
		marcadorB.setMap(null);
		rota.setMap(null);
		
		rota = {origin : marcadorA.getPosition(), destination : marcadorB.getPosition(), travelMode : google.maps.DirectionsTravelMode.DRIVING}; 
    	
		directionsService.route(rota, function(result, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(result);
				
	    		if (directionsDisplay.getMap() == null) { directionsDisplay.setMap(mapAddRota); }					
			}
		});		
	}
	
	$(document).ready(function() {

		$("#formAddRota").jFotoForm({ajax: false}, function () {
			return addRota(this);
		});
		
		$('.ajuda').tipTip({maxWidth: "auto", edgeOffset: 10});
  	
<c:if test="${not empty rota.geoPonto}">

		comRota = true;
		
		rotaPath.clear();

		<c:forEach var="geoPonto" items="${rota.geoPonto}">
		var ponto = new google.maps.LatLng('${geoPonto.latitude}','${geoPonto.longitude}');
		rotaPath.insertAt(rotaPath.length, ponto);
		</c:forEach>
		
		marcadorA = new google.maps.Marker({
			position: new google.maps.LatLng('${rota.geoPonto[0].latitude}','${rota.geoPonto[0].longitude}'),
			map: mapAddRota,
			draggable: true,
			icon: pontoA
		});

		google.maps.event.addListener(marcadorA, 'dragend', function(event) {
					
			calculaNovaRota();			
		});
		
		marcadorB = new google.maps.Marker({
			position: new google.maps.LatLng('${rota.geoPonto[totalGeoPontos].latitude}','${rota.geoPonto[totalGeoPontos].longitude}'),
			map: mapAddRota,
			draggable: true,
			icon: pontoB
		});

		google.maps.event.addListener(marcadorB, 'dragend', function(event) {
			
			calculaNovaRota();
		});
	
</c:if>
		
<c:if test="${not empty listaCercas}">

		exibeCercaRota('${rota.cerca.id}');
</c:if>	
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>${rota.id == null ? 'Adicionar' : 'Alterar'} cerca</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />

		<form id="formAddRota" action="../salvar/" method="post">

		<div class="form">
		
			<input type="hidden" name="rota.id" value="${rota.id}" />
			<div id="geoPontos"></div>
			
			<div class="fields">
	
				<div class="field">
	
					<div class="label">
						<label for="input-medium">Nome da rota:</label>
					</div>
										
					<div class="input" style="margin-bottom: 10px;">
						<input name="rota.nome" type="text" value="${rota.nome}" class="medium" validate="vazio" />
					</div>	
				</div>
				
				<div class="field">
	
					<div class="label">
						<label for="input-medium">Cerca:</label>
					</div>
	
					<div class="select">
						<select class="cercaSel" name="cerca_id" style="width: 100%;" onchange="exibeCercaRota(this.value)">
						<c:forEach var="cerca" items="${listaCercas}">
							<option value="${cerca.id}" ${rota.cerca.id == cerca.id ? 'selected="selected"' : ''}>${cerca.nome}</option>
						</c:forEach>						
						</select>
					</div>																								  		
				</div>
								
				<div id="mensagem-erro-rota" class="error" style="display: none;">Rota inválida</div>
				
				<div class="field">
									
					<div id="mapaDivAddRota" style="width: 100%;" >
						<div id="mapaAddRota" style="height: 400px; padding: 7px;"></div>
					</div>			  				
				</div>
				
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${rota.id == null ? 'adicionar' : 'alterar'}" />
					</div>
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="limpar" onclick="limparRota();" />				
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />
				</div>
				
			</div>
		</div>	
		
		</form>				

	</div>
	
</jsp:body>

</layout:controleGeral>
