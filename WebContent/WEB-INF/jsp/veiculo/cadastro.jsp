<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="../../../js/gmap3.js"></script>
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script>
	<script type="text/javascript" src="../../../js/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script>

</jsp:attribute>

<jsp:attribute name="scripts">
	
	  
	<script type="text/javascript">
	/*
	var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');
	
		var mapCerca, mapAddCerca, mapRota, mapAddRota,  mapRef = "";
		var poly;
		var markers = [];
		var path = new google.maps.MVCArray;
		
		var listaCercas = [];
		var listaRotas = [];
		
		var cercaPath = new google.maps.MVCArray;
		var cercaRotaPath = new google.maps.MVCArray;
	
	
		var tipoMarcar = false;
		var rotaInicio = "";
		var rotaFim = "";
		var rota;
		var directionsDisplay = "";
	
		var directionsService = new google.maps.DirectionsService();
		directionsDisplay = new google.maps.DirectionsRenderer();
	
		var poly = new google.maps.Polygon({
			    fillColor: '#FF0000',
			    strokeOpacity: 0.3,
			    strokeColor: "#FF0000",
			    strokeWeight: 2,
			    fillOpacity: 0.05
			});
	
		var cerca = new google.maps.Polygon({
		    fillColor: '#FF0000',
		    strokeOpacity: 0.3,
		    strokeColor: "#FF0000",
		    strokeWeight: 2,
		    fillOpacity: 0.05
		});
	
		var cercaRota = new google.maps.Polyline({
		    strokeOpacity: 0.3,
		    strokeColor: "#FF0000",
		    strokeWeight: 2
		});
	
		var rotaMarkers = [];
		var rotaPath = new google.maps.MVCArray;
		var rota = new google.maps.Polyline({
		    strokeOpacity: 0.4,
		    strokeColor: "#0000FF",
		    strokeWeight: 5
		});
	
		var imagePontoCerca = new google.maps.MarkerImage(
			"../../../images/square_transparent.png",
			new google.maps.Size(11, 11),
			new google.maps.Point(0, 0),
			new google.maps.Point(6, 6)
		);
	
		var pontoA = new google.maps.MarkerImage(
				"../../../images/icones/marker_greenA.png",
				new google.maps.Size(20, 34),
				new google.maps.Point(0, 0),
				new google.maps.Point(10, 32)
			);
			
		var pontoB = new google.maps.MarkerImage(
				"../../../images/icones/marker_greenB.png",
				new google.maps.Size(20, 34),
				new google.maps.Point(0, 0),
				new google.maps.Point(10, 32)
			);
	
		$("#mapaCerca").ready( function() { 
	
			$("#mapaCerca").gmap3({ 
				action:'init',
				options:{
					center:[latlngMapa.lat(),latlngMapa.lng()],
					zoom: 14
				}	
			});
			
			mapCerca = $('#mapaCerca').gmap3('get');
			   
		  	cerca.setMap(mapCerca);
		  	cerca.setPaths(new google.maps.MVCArray([cercaPath]));
		    
		 }); 
	
		$("#mapaRota").ready( function() { 
	
			$("#mapaRota").gmap3({ 
				action:'init',
				options:{
					center:[-3.8,-38.457275],
					zoom: 14
				}	
			});
			
			mapRota = $('#mapaRota').gmap3('get');
			   
		  	rota.setMap(mapRota);
		  	rota.setPath(new google.maps.MVCArray([rotaPath]));
		    
		 }); 
	
		function showCercaDialog() {
		    
			getCercas();
			
			$("#dialogCerca").dialog('open');
			google.maps.event.trigger(mapCerca, 'resize');	
		}
		
		function getCercas() {

			$.ajax({
				type: "POST",
				url: "../../../veiculo/listaCercas",
				success: function( resposta ) {
					
		        	json = eval('(' + resposta + ')').list;       
		        	listaCercas = json;			       	

					div = "";
					
					for(x in listaCercas) {
						div += '<li id="cerca-' + listaCercas[x].id +'"><a href="javascript:;" onclick="exibeCerca(' + listaCercas[x].id + ')">' + listaCercas[x].nome + '</li>';	
					}
					
					$('#listaCercas').html(div);
					
					if (listaCercas.length > 0) {
						exibeCerca(listaCercas[0].id);
					}				
				}
			});			
		}

		function filtraCercas() {
			
			var txtBusca = $('#filtroCerca').val().toUpperCase();
			var div = "";
			$('#listaCercas').html('');
	
			for (x in listaCercas) {
		
				if (listaCercas[x].nome.toUpperCase().lastIndexOf(txtBusca) >= 0 ) {

					div += '<li id="cerca-' + listaCercas[x].id +'"><a href="javascript:;" onclick="exibeCerca(' + listaCercas[x].id + ')">' + listaCercas[x].nome + '</li>';														
				}
			}
			
			$('#listaCercas').html(div);
			
			if (listaCercas.length > 0) {
				exibeCerca(listaCercas[0].id);
			}
		}
		
		function showRotaDialog() {
	
			getRotas();
			
			$("#dialogRota").dialog('open');
			google.maps.event.trigger(mapRota, 'resize');		
		}

		function getRotas() {			
		
			var dados = "";
			
			if ($('#cercaCampo').val() != null && $('#cercaCampo').val() != "") {
				
				dados = 'cerca_id=' + $('#cercaCampo').val();
			}
				
			$.ajax({
				type: "POST",
				url: "../../../veiculo/listaRotas",
				data: dados,
				success: function( resposta ) {
					
			    	json = eval('(' + resposta + ')').list;       
					listaRotas = json;	   	

					div = "";
					
					for(x in listaRotas) {
						div += '<li id="rota-' + listaRotas[x].id +'"><a href="javascript:;" onclick="exibeRota(' + listaRotas[x].id + ')">' + listaRotas[x].nome + '</li>';	
					}
					
					$('#listaRotas').html(div);
																					
					if (listaRotas.length > 0) {
						exibeRota(listaRotas[0].id);
					}
				}
			});
		}

		function filtraRotas() {
			
			var txtBusca = $('#filtroRota').val().toUpperCase();
			var div = "";
			$('#listaRotas').html('');
	
			for (x in listaRotas) {
		
				if (listaRotas[x].nome.toUpperCase().lastIndexOf(txtBusca) >= 0 ) {

					div += '<li id="cerca-' + listaRotas[x].id +'"><a href="javascript:;" onclick="exibeCerca(' + listaRotas[x].id + ')">' + listaRotas[x].nome + '</li>';														
				}
			}
			
			$('#listaRotas').html(div);
			
			if (listaRotas.length > 0) {
				exibeCerca(listaRotas[0].id);
			}
		}
		
		function exibeRota(id) {

			$('#listaRotas').find('li').removeClass('selecionado');
			$('#rotaSel').val(id);
			
			for(var i=0; i < rotaMarkers.length; i++) {
				
				rotaMarkers[i].setMap(null);
			}
			
			rotaMarkers = [];
			
			rotaPath.clear();
			var limites = new google.maps.LatLngBounds();
				
			for (var i=0; i < listaRotas.length; i++) {
				
				if (listaRotas[i].id == id) {
					
					for (var f=0; f < listaRotas[i].geoPontos.length; f++) {
						var ponto = new google.maps.LatLng(listaRotas[i].geoPontos[f].latitude,listaRotas[i].geoPontos[f].longitude);
						rotaPath.insertAt(rotaPath.length, ponto);
						limites.extend(ponto);
					}
					break;
				}
			}
			
			var marker = new google.maps.Marker({
				  position: new google.maps.LatLng(listaRotas[i].geoPontos[0].latitude,listaRotas[i].geoPontos[0].longitude),
				  map: mapRota,
				  draggable: false,
				  icon: pontoA
				});
			
			rotaMarkers.push(marker);
	
			var marker = new google.maps.Marker({
				  position: new google.maps.LatLng(listaRotas[i].geoPontos[listaRotas[i].geoPontos.length-1].latitude,listaRotas[i].geoPontos[listaRotas[i].geoPontos.length-1].longitude),
				  map: mapRota,
				  draggable: false,
				  icon: pontoB
				});
			
			rotaMarkers.push(marker);
			
			mapRota.setCenter(limites.getCenter());
			
			$('#rota-' + id).addClass('selecionado');

		}

	
		function limparRota() {
			
			rotaInicio = "";
			rotaFim = "";
			directionsDisplay.setMap(null);	
		}

		function addCerca(form) {
		
			limparCerca(); 
	    	$('#dialogAddCerca').dialog('close');	
		}			
	
		function exibeCerca(id) {
	
			$('#listaCercas').find('li').removeClass('selecionado');
			$('#cercaSel').val(id);
			
			cercaPath.clear();
			var limites = new google.maps.LatLngBounds();
				
			for (var i=0; i < listaCercas.length; i++) {
				
				if (listaCercas[i].id == id) {
					
					for (var f=0; f < listaCercas[i].geoPontos.length; f++) {
						var ponto = new google.maps.LatLng(listaCercas[i].geoPontos[f].latitude,listaCercas[i].geoPontos[f].longitude);
						cercaPath.insertAt(cercaPath.length, ponto);
						limites.extend(ponto);
					}
					break;
				}
			}
			
			mapCerca.setCenter(limites.getCenter());
			
			$('#cerca-' + id).addClass('selecionado');
		}
	
		function fechaCerca() {
			
			$('#dialogCerca').dialog('close');
		}
		
		function fechaRota() {
			
			$('#dialogRota').dialog('close');
		}
	
		function selecionaCerca(id) {
	
			for (var i=0; i < listaCercas.length; i++) {
				
				if (listaCercas[i].id == id) {
					break;
				}
			}
	
			$('#cercaNome').html(listaCercas[i].nome + '<img style="margin-left: 20px; cursor: pointer;" onclick="zeraCerca();" src="../../../images/ico_excluir.gif" />');
			$('#cercaCampo').val(listaCercas[i].id);
			$('#addCerca').hide(300);
			
		}
	
		function zeraCerca() {
			$('#cercaNome').html('');
			$('#cercaCampo').val('');
			$('#addCerca').show();
		}
	
		function selecionaRota(id) {
	
			for (var i=0; i < listaRotas.length; i++) {
				
				if (listaRotas[i].id == id) {
					break;
				}
			}
	
			$('#rotaNome').html(listaRotas[i].nome + '<img style="margin-left: 20px; cursor: pointer;" onclick="zeraRota();" src="../../../images/ico_excluir.gif" />');
			$('#rotaCampo').val(listaRotas[i].id);
			$('#addRota').hide(300);
			
		}
	
		function zeraRota() {
			$('#rotaNome').html('');
			$('#rotaCampo').val('');
			$('#addRota').show();
		}
	
		function addRota(form) {
						
			if (directionsDisplay.getDirections() == null) {
				
				return false;
			}
					
			var nome = 'nome=' + form.elements["nome"].value;
			var pontos = "";
			
			rota = directionsDisplay.getDirections().routes[0].overview_path;
			
			for (var i=0; i<rota.length; i++) {
				
				pontos += '&pontos[' + i +']=' + rota[i].lat() + ';' + rota[i].lng();
			}
			
			var dataString = nome + pontos;
			
			$.ajax({
				type: "POST",
				url: "../../../veiculo/adicionarRota",
				data: dataString,
				success: function( resposta ) {
					
					limparRota(); 
		        	$('#dialogAddRota').dialog('close');
				}
			});
	
			return false;
		}
		
		*/
	
		$(document).ready( function() {
			
			$("#placaVeiculo").mask("aaa9999");			
		    $("#anoVeiculo").mask("9999");
			
			$("#dialogLocal").dialog({ minWidth: 600, autoOpen: false, resizable: false });	
			
			$("#formVeiculo").jFotoForm();

			$('#formAddLocal').submit(function() {
				return false;
			});	

			$('#formCerca').submit(function() {
				selecionaCerca(this.elements["cercaSel"].value);
				fechaCerca();		
				return false;
			});

			$('#formRota').submit(function() {
				selecionaRota(this.elements["rotaSel"].value);	
				fechaRota();				
				return false;
			});		 
	

			$("#dialogCerca").dialog({ minWidth: 900, maxHeight: 500, autoOpen: false, resizable: false });	
			$("#dialogRota").dialog({ minWidth: 900, maxHeight: 500, autoOpen: false, resizable: false });	
			
			$('.ajuda').tipTip({maxWidth: "auto", edgeOffset: 10});
			
		});
	
		function inicio() {
			
			parent.location='../listar/';
		}


	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

	<div id="dialogLocal" title="Adicionar novo local">
	
		<form id="formAddLocal" action="">
	
			<div class="form">
				<div class="fields">
						
					<div class="field">
						<div class="label">
							<label for="input-medium">Municipio:</label>
						</div>
						<div class="input">
							<input id="local.municipio" type="text" value="${local.municipio}" />							  		
						</div>  				
					</div>
	
					<div class="field">
						<div class="label">
							<label for="input-medium">Uf:</label>
						</div>
						<div class="input">
							<input id="local.uf" type="text" value="${local.uf}" />							  		
						</div>  				
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="adicionar" onclick="addLocal();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="$('#dialogLocal').dialog('close');" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>
	

	
	<div id="dialogRota" title="Selecionar rota">
	
		<form id="formRota">
		<div class="form">
		
			<input type="hidden" id="rotaSel" name="rotaSel" value="" />
		
			<div class="fields">				
				<div class="field">						
					<div id="listaRotasDiv">		
					
						<input type="text" id="filtroRota" name="filtro" value="" style="width: 100%;" onkeyup="filtraRotas();" />	
								
						<ul id="listaRotas">						
						</ul>
					</div>
					
					<div id="mapaDivRota" style="width: 100%;">
						<div id="mapaRota" style="height: 420px; padding: 7px;"></div>
					</div>	
				</div>
				
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="selecionar rota" />
					</div>				
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="$('#dialogRota').dialog('close');" />
				</div>
				
			</div>
		</div>	
		</form>
	</div>
	
	<div id="dialogCerca" title="Selecionar cerca">
	
		<form id="formCerca">
		
		<div class="form">
		
			<input type="hidden" id="cercaSel" name="cercaSel" value="" />
			<div class="fields">					
				<div class="field">						
					<div id="listaCercasDiv">		
					
						<input type="text" id="filtroCerca" name="filtro" value="" style="width: 100%; onkeyup="filtraCercas();" />	
								
						<ul id="listaCercas">
							
						</ul>
					</div>
												
					<div id="mapaDivCerca" style="width: 100%;">
						<div id="mapaCerca" style="height: 420px; padding: 7px;"></div>
					</div>			  				
				</div>
				
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="selecionar cerca" />
					</div>
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="$('#dialogCerca').dialog('close');" />
				</div>
				
			</div>
		</div>
		
		</form>
			
	</div>

</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		 
		<div class="title">
			<h5>${veiculo.id == null ? 'Cadastrar' : 'Editar'} Novo veículo</h5>
		   <!--
		    <ul class="links">
				<li class="ui-state-active">
				<a href="/smc/controle_geral/veiculo/materiais/${veiculo.id}">Materiais</a>
				</li>
		    </ul>
			<ul class="links">
				<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
				<a href="/smc/controle_geral/veiculo/editar/?id=${veiculo.id}">Geral</a>
				</li>
		    </ul>
		    -->
		</div>
	 	
		<jsp:include page="../includes/mensagens.jsp" />
	
		<form id="formVeiculo" action="../../../controle_geral/veiculo/adicionar/" method="post">
			
			<input type="hidden" name="veiculo.id" value="${veiculo.id}"/>
			
			<div class="form">
				<div class="fields">

					<div class="field">
						<div class="label">
							<label for="select">Id:<div class="ajuda" title="Id do veículo">?</div></label>
						</div>
											
						<div class="input">
							<input class="medium" type="text" name="veiculo.idOrgao" value="${veiculo.idOrgao}" />
						</div>				
					</div>
									
					<div class="field">
						<div class="label">
							<label for="select">Placa:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Placa do veículo">?</div></label>
						</div>
											
						<div class="input">
							<input class="medium${erroPlaca eq true ? ' error' : ''}" id="placaVeiculo" type="text" name="veiculo.placa" value="${veiculo.placa}" MAXLENGTH="10" validate="vazio"/>
						</div>				
					</div>
								
					<div class="field">
						<div class="label">
							<label for="input-medium">Modelo:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Modelo do veículo">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroModelo eq true ? ' error' : ''}" type="text" name="veiculo.modelo" value="${veiculo.modelo}" MAXLENGTH="50" validate="vazio"/>
						</div>
					</div>
			
					<div class="field">
						<div class="label">
							<label for="input-medium">Renavam:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Renavam do veículo">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroRenavam eq true ? ' error' : ''}" type="text" name="veiculo.renavam" value="${veiculo.renavam}" MAXLENGTH="30" validate="vazio"/>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="input-medium">Ano:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Ano do veículo">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroAno eq true ? ' error' : ''}" id="anoVeiculo" type="text" name="veiculo.ano" value="${veiculo.ano}" MAXLENGTH="4" validate="ano"/>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="input-medium">Marca:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Modelo do veículo">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroMarca eq true ? ' error' : ''}" type="text" name="veiculo.marca" value="${veiculo.marca}" MAXLENGTH="45" validate="vazio"/>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="input-medium">Cor:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Cor do veículo">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroCor eq true ? ' error' : ''}" type="text" name="veiculo.cor" value="${veiculo.cor}" MAXLENGTH="45" validate="vazio"/>
						</div>
					</div>
									
					<div class="field">
						<div class="label">
							<label for="select">Local:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Local do veículo">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="veiculo.local.id">
								<c:forEach var="local" items="${listaLocal}">
									<option value="${local.id}" ${veiculo.local.id == local.id ? 'selected="selected"' : ''}>${local.municipio}</option>
								</c:forEach>
							</select>
						
							<div style="margin:10px 0 0 0px;">
								<a href="javascript:;" onclick="$('#dialogLocal').dialog('open');"><img src="../../../images/plus.png" /> adicionar novo local</a>
							</div>
						</div>
					</div>
																	
					<div class="field">
						<div class="label">
							<label for="select">Tipo:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Tipo do veículo">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="veiculo.tipoVeiculo.id">
								<c:forEach var="tipo" items="${listaTipo}">
									<option value="${tipo.id}" ${veiculo.tipoVeiculo.id == tipo.id ? 'selected="selected"' : ''}>${tipo.valor}</option>
								</c:forEach>
							</select>
							
							<div style="margin:10px 0 0 0px;">
								<a href="javascript:;"><img src="../../../images/plus.png" /> adicionar novo tipo de veiculo</a>
							</div>						
						</div>
					</div>
					<!-- 
					<div class="field">
						<div class="label">
							<label for="select">Cerca:<div class="ajuda" title="Cerca a qual o veículo deverá permanecer">?</div></label>
						</div>
						<div class="select">				
							<div style="margin:10px 0 0 0px;">
								<input type="hidden" id="cercaCampo" name="veiculo.cerca.id" value="${veiculo.cerca.id}"/>	
								<div id="cercaNome" style="display: inline;">
<c:if test="${veiculo.cerca.id > 0}">								
									${veiculo.cerca.nome} <img style="margin-left: 20px; cursor: pointer;" onclick="zeraCerca();" src="../../../images/ico_excluir.gif" />
</c:if>								
								</div>
								<div id="addCerca"<c:if test="${veiculo.cerca.id != null}"> style="display: none;"</c:if>>
									<a href="javascript:;" onclick="showCercaDialog();">selecionar cerca</a><br /><br />
								</div>
							</div>						
						</div>
					</div>
												
					<div class="field">
						<div class="label">
							<label for="select">Rota:<div class="ajuda" title="Rota a qual o veículo deverá seguir">?</div></label>
						</div>
						
						<div class="select">				
							<div style="margin:10px 0 0 0px;">
								<input type="hidden" id="rotaCampo" name="veiculo.rota.id" value="${veiculo.rota.id}"/>
								<div id="rotaNome" style="display: inline;">
<c:if test="${veiculo.rota.id > 0}">
									${veiculo.rota.nome} <img style="margin-left: 20px; cursor: pointer;" onclick="zeraRota();" src="../../../images/ico_excluir.gif" />
</c:if>									
								</div>
								<div id="addRota"<c:if test="${veiculo.rota.id != null}"> style="display: none;"</c:if>>							
									<a href="javascript:;" onclick="showRotaDialog();">selecionar rota</a><br /><br />													
								</div>
							</div>																		
						</div>
						 
					</div>
					-->
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${veiculo.id == null ? 'adicionar' : 'alterar'}"/>
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
