<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Simular Viaturas</title>

<script type="text/javascript" src="../js/jquery-1.6.2.min.js" ></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

</head>
<body>

<script>

var listaViatura;
var listaViaturaLat;
var listaViaturaLng;

var listaInterval;

function simula (id)
{

	listaViaturaLng[id] = listaViaturaLng[id] - .00002;

	var posicao = new google.maps.LatLng(listaViaturaLat[id], listaViaturaLng[id]);

//	console.log(viatura.latitude +"," + viatura.longitude);
	
	var dirRequest = {
		origin: posicao,
		destination: posicao,
		travelMode: google.maps.DirectionsTravelMode.DRIVING,
		unitSystem:  google.maps.DirectionsUnitSystem.METRIC,
		provideRouteAlternatives: false
	};
		       
	var dirService = new google.maps.DirectionsService();  

	dirService.route(dirRequest, function(dirResult, dirStatus) {
		
		if (dirStatus != google.maps.DirectionsStatus.OK) {
			console.log("Erro");
			return;
		} else {

			var posicao = dirResult.routes[0].legs[0].start_location; // new google.maps.LatLng(viaturas[i].latitude, viaturas[i].longitude);
			
			if ((posicao.lat() - .00002) != listaViaturaLat[id] && posicao.lng() != listaViaturaLng[id]) {
				listaViaturaLat[id] = posicao.lat();
				listaViaturaLng[id] = posicao.lng();
			}
			
			$.ajax({
		        type: "GET",
		        url: "http://localhost:8000/add/",
		        data: "&latitude=" + listaViaturaLat[id] + 
		    	"&longitude=" + listaViaturaLng[id] + 
		    	"&viatura_id=" + listaViatura[id],
			  	dataType: "jsonp",
			    jsonpCallback: "_testcb",
			    cache: false,
			    timeout: 5000,
			        
		        success: function( resposta )
		        {
		        	
		        	console.log(resposta);
		       	
//		        	console.log("Enviado " + id);
		        },
		        error:function(XMLHttpRequest, textStatus, errorThrown)
		        {
		        	console.log("Erro de comunicação");
		        }
			});
			
		}
	});
	
	
/*	
	$.ajax({
        type: "POST",
        url: "http://localhost:8080/smc/viaturaRota/add",
        data: "viaturaRota.latitude=" + listaViaturaLat[id] + 
        	"&viaturaRota.longitude=" + listaViaturaLng[id] + 
        	"&viaturaRota.viatura_id=" + listaViatura[id],
        	
        success: function( resposta )
        {
        	
//        	console.log("Enviado " + id);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
        	console.log("Erro de comunicação");
        }
	});
*/
/*
	$.ajax({
        type: "GET",
        url: "http://localhost:8000/add/",
        data: "&latitude=" + listaViaturaLat[id] + 
    	"&longitude=" + listaViaturaLng[id] + 
    	"&viatura_id=" + listaViatura[id],
	  	dataType: "jsonp",
	    jsonpCallback: "_testcb",
	    cache: false,
	    timeout: 5000,
	        
        success: function( resposta )
        {
        	
        	console.log(resposta);
       	
//        	console.log("Enviado " + id);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
        	console.log("Erro de comunicação");
        }
	});
*/
}

function inicia() {
	
	listaViatura = new Array();
	listaViaturaLat = new Array();
	listaViaturaLng = new Array();
	
	listaInterval = new Array();
	
	var qnt = $('#quantidade').val();
	var i;
	
	for (i=0; i<qnt; i++) {
			
		listaInterval[i] = setInterval ( "simula(" + i + ")", 2000 );

		listaViatura[i] = i;	
/*		
		listaViaturaLat[i] = -3.7947758229820288 - (0.002 * i); 
		listaViaturaLng[i] = -38.45006522216795;
*/	
		listaViaturaLat[i] = -3.79483; 
		listaViaturaLng[i] = -38.47149999999999;


	}
}

function para() {
	
	var i;
	
	for (i=0; i < listaInterval.length; i++) {
		clearInterval(listaInterval[i]);
	}
}

</script>

<form action="simular" id="formSimulacao" method="post">

	<input type="text" id="quantidade" name="quantidade" />
	<input type="button" onclick="inicia()" value="iniciar" />
	<input type="button" onclick="para()" value="para" />

</form>

</body>
</html>