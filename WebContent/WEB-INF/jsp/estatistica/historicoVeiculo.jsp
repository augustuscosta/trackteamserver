<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:estatistica keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">
	
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>	
	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jQuery/jquery-ui-timepicker-addon.js"></script>	
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 	
	<script type="text/javascript" src="../../../js/highchart/highcharts.js"></script> 	
	
</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	var rotaPath = new google.maps.MVCArray;
	var rota = new google.maps.Polyline({
	    strokeOpacity: 0.4,
	    strokeColor: "#0000FF",
	    strokeWeight: 5
	});
	
	var listaMarcadores = [];
	var balaoMapa;
	var chart;
	
	var percurso;
	var valores = {};
	var tipo = 'mapa';

	var titulo;
	var tituloX;
	var tituloY = 'data';
	
	var imagePonto = new google.maps.MarkerImage(
		"../../../images/square_transparent.png",
		new google.maps.Size(11, 11),
		new google.maps.Point(0, 0),
		new google.maps.Point(6, 6)
	);
	
	$("#mapa").ready(function(){
		
	    var latlng = new google.maps.LatLng(-3.8,-38.457275);
	    
	    var myOptions = {
	      zoom: 11,
	      center: latlng,
	      mapTypeControl: false,
	      panControl: false,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    
	    map = new google.maps.Map(document.getElementById("mapa"), myOptions);

	  	rota.setMap(map);
	  	rota.setPath(new google.maps.MVCArray([rotaPath]));

		google.maps.event.addListener(map, 'click', function() {
			
			if (balaoMapa != null) {
			
				balaoMapa.close();
			}		
		});
		
		$.datepicker.regional['ru'] = {
			prevText : 'Anterior',
			nextText : 'Próximo',
			monthNames : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro',	'Novembro', 'Dezembro' ],
			monthNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNames : [ 'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo' ],
			dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D' ],
			dateFormat : 'dd/mm/yy'
		};

		$.datepicker.setDefaults($.datepicker.regional['ru']);

		$('#dataInicio').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});

		$('#dataFim').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});		
	});
	
	function tracaRota(rota) {
	
		rotaPath.clear();
		count = 0;
		
		for (x in listaMarcadores) {
			
			listaMarcadores[x].setMap(null);
		}
		
		listaMarcadores = [];
		valoresVelocidade = new Array();
		valoresRPM = new Array();
		valoresBateria = new Array();
		valoresTemperatura = new Array();
		
		ultimaHora = 0;
		var limites = new google.maps.LatLngBounds();
		
		for (x in rota) {
			
			if (rota[x].latitude != "0.0" && rota[x].longitude != "0.0") {
				
				var ponto = new google.maps.LatLng(rota[x].latitude,rota[x].longitude);
				rotaPath.insertAt(rotaPath.length, ponto);
				
			}			

			if (rota[x].velocidade == null) {
				rota[x].velocidade = 0;
			}

			if (rota[x].rpm == null) {
				rota[x].rpm = 0;
			}
			
			if (rota[x].tensao_bateria == null) {
				rota[x].tensao_bateria = 0;
			}
			
			if (rota[x].temperatura == null) {
				rota[x].temperatura = 0;
			}
			
			valoresVelocidade.push([rota[x].dataHora, parseInt(rota[x].velocidade, 10)]);
			valoresRPM.push([rota[x].dataHora, parseInt(rota[x].rpm, 10)]);
			valoresBateria.push([rota[x].dataHora, parseInt(rota[x].tensao_bateria, 10)]);
			valoresTemperatura.push([rota[x].dataHora, parseInt(rota[x].temperatura, 10)]);
			
			valores['velocidade'] = valoresVelocidade; 
			valores['rpm'] = valoresRPM;
			valores['bateria'] = valoresBateria;
			valores['temperatura'] = valoresTemperatura;
			
			if (rota[x].dataHora > (ultimaHora + 180000)) {

				var date = new Date(rota[x].dataHora);
				var hours = date.getHours();
				var minutes = date.getMinutes();
				var seconds = date.getSeconds();
				var year = date.getFullYear();	
				var month = date.getMonth()+1;
				var dia = date.getDate();
			     
				var formattedTime = dia + '/' + month + '/' + year + ' ' + hours + ':' + minutes + ':' + seconds;

				if (rota[x].latitude != "0.0" && rota[x].longitude != "0.0") {

					var marcador = new google.maps.Marker({
						position: new google.maps.LatLng(rota[x].latitude, rota[x].longitude),
						map: map,
						icon: imagePonto
					});		
	
					marcador.data = formattedTime;
					
					google.maps.event.addListener(marcador, 'click', function() {
					
						
						if (balaoMapa != null) {
						
							balaoMapa.close();
						}
						
						balaoMapa = new google.maps.InfoWindow({
						    content: '<div class="balaoMaps">' + this.data + '</div>'
						});
	
						balaoMapa.open(map, this);
						
					});
					
					limites.extend(new google.maps.LatLng(rota[x].latitude, rota[x].longitude));
				}
	
				listaMarcadores.push(marcador);				
				ultimaHora = rota[x].dataHora;
			}
		}
	
		map.setZoom(14);
		map.setCenter(limites.getCenter());		
	}
	
	function criarGrafico() {
		
		chart = new Highcharts.Chart({
			chart: {
				renderTo: 'grafico',
				type: 'spline'
			},
			title: {
				text: titulo
			},
			xAxis: {
				title: {
					text: tituloX
				},
				type: 'datetime',
				dateTimeLabelFormats: { // don't display the dummy year
					month: '%e. %b',
					year: '%b'
				}
			},
			yAxis: {
				title: {
					text: tituloY
				},
				min: 10
			},
			tooltip: {
				formatter: function() {
						return '<b>'+ this.y +'</b><br/>'+
						Highcharts.dateFormat('%e. %b', this.x) +': '+ this.y +' m';
				}
			},
			legend: {
				enabled: false
			},			
			series: [{
				data: valores[tipo]
			}]
		});	
	}
	
	function atualizaLink(div) {
		
		$('.links').find('li').removeClass('ui-tabs-selected');
		$(div).parent().addClass('ui-tabs-selected');
			
		tipo = $(div).attr('tipo');
		
	}
	
	function exibeGrafico(t, tX, tY) {
		
		titulo = t;
		tituloX = tX;
		tituloY = tY;
		
		$('#divTabMapa').hide();
		$('#divTabGraficos').show(0, function() { 
			criarGrafico(); 
		});
		
	}
	
	function exibeMapa() {

		$('#divTabGraficos').hide();
		$('#divTabMapa').show();
		
		google.maps.event.trigger(map, 'resize'); 
		tracaRota(percurso);	
		
	}
	
	$(document).ready(function() {

		$.post('../historico/', {'veiculo_id': '${veiculo_id}', 'dataInicio': '${dataInicio}', 'dataFim': '${dataFim}'}, function(data) {
			  
			percurso = data.result;
			tracaRota(data.result);	
			criarGrafico();
		});
				
		$("#formHistorico").jFotoForm({ajax: false}, function () {
			
			var dataInicio = $('#dataInicio').val();
			var dataFim = $('#dataFim').val();
			
			$.post('../historico/', {'veiculo_id': '${veiculo_id}', 'dataInicio': dataInicio, 'dataFim': dataFim}, function(data) {
			
				percurso = data.result;
				tracaRota(data.result);
				criarGrafico();
			});
			
			return false;
		});
	});
	
</script>
		
</jsp:attribute>

<jsp:body>

	<div id="conteudo" class="box box-inline">
		
		<div class="title">
			<h5>Histórico veiculo</h5>
			
			<ul class="links">
				<li class="ui-state-active">
				<a href="javascript:;" tipo="bateria" onclick="atualizaLink(this); exibeGrafico('CAM - Bateria','Bateria (V)');">CAM - Bateria</a>
				</li>
		    </ul>
		    
		    <ul class="links">
				<li class="ui-state-active">
				<a href="javascript:;" tipo="temperatura" onclick="atualizaLink(this); exibeGrafico('CAM - Temperatura','Temperatura (°C)');">CAM - Temperatura</a>
				</li>
		    </ul>
		    
		    <ul class="links">
				<li class="ui-state-active">
				<a href="javascript:;" tipo="rpm" onclick="atualizaLink(this); exibeGrafico('CAM - RPM','RPM');">CAM - RPM</a>
				</li>
		    </ul>
		    
			<ul class="links">
				<li class="ui-state-default ui-corner-top ui-state-active">
				<a href="javascript:;" tipo="velocidade" onclick="atualizaLink(this); exibeGrafico('CAM - Velocidade','Velocidade (km/h)');">CAM - Velocidade</a>
				</li>
		    </ul>
		    
		    <ul class="links">
				<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
				<a href="javascript:;" tipo="mapa" onclick="atualizaLink(this); exibeMapa()">Mapa</a>
				</li>
		    </ul>
		</div>

		<jsp:include page="../includes/mensagens.jsp" />

		<form id="formHistorico" action="" method="post">
			
			<input type="hidden" name="veiculo.id" value="${veiculo.id}"/>
			
			<div class="form">
				<div class="fields">
					
					<div class="field">
						<div class="label" style="width: auto;">
							<label for="select">Data início:</label>
						</div>						
						<div class="input" style="margin-left: 0px;">
							<input type="text" id="dataInicio" name="dataInicio" value="${dataInicio}" />
						</div>
						
						<div class="label" style="width: auto; margin-left: 30px;">
							<label for="select">Data fim:</label>
						</div>
						<div class="input" style="margin-left: 0px;">
							<input type="text" id="dataFim" name="dataFim" value="" />
						</div>

						<div class="buttons" style="margin: 1px 0 0 0;">
							<div class="highlight" style="margin: 0 0 0 30px;">
								<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="filtrar" />
							</div>
						</div>						
					</div>
				</div>	
			</div>		
		</form>
		
		<div id="divTabMapa">
		
			<div id="mapa" style="height: 500px; margin: 0 20px;">	</div>
		</div>

		<div id="divTabGraficos" style="display: none;">
		
			<div id="grafico" style="margin: 20px 20px;">
			
			</div>
			
		</div>
	</div>		
</jsp:body>

</layout:estatistica>