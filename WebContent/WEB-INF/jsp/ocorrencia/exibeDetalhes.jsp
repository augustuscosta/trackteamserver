<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="form">

	<div class="fields">

		<c:if test="${fn:length(ocorrencia.solicitantes) > 0}">
			<div class="field">
				<div class="label">
					<label for="select">Solicitantes:</label>
				</div>

				<table style="padding: 0; margin: 0; width: 685px;" cellspacing="0">
					<thead>
						<tr>
							<th class="left"><b>cpf</b>
							</td>
							<th class="left" style="width: 100%"><b>nome</b>
							</td>
							<th class="left"><b>telefone</b>
							</td>
							<th class="left last"><b>telefone</b>
							</td>
						</tr>
					</thead>

					<c:forEach var="solicitante" items="${ocorrencia.solicitantes}">

						<tr>
							<td class="left">${solicitante.cpf}</td>
							<td>${solicitante.nome}</td>
							<td>${solicitante.telefone1}</td>
							<td class="last">${solicitante.telefone2}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</c:if>

		<div class="field">
			<div class="label">
				<label for="input-medium">Responsável pela criação:</label>
			</div>
			<div class="inputTexto">${ocorrencia.usuarioCriacao.nome}</div>
		</div>

		<div class="field">
			<div class="label">
				<label for="input-medium">Operador responsável:</label>
			</div>
			<div class="inputTexto">${ocorrencia.usuario.nome}</div>
		</div>

		<div class="field">
			<div class="label">
				<label for="input-medium">Resumo:</label>
			</div>
			<div class="inputTexto">${ocorrencia.resumo}</div>
		</div>

		<div class="field">
			<div class="label">
				<label for="input-medium">Detalhes:</label>
			</div>
			<div class="inputTexto">${ocorrencia.descricao}</div>

		</div>

		<div class="field">
			<div class="label">
				<label for="select">Status:</label>
			</div>
			<div class="inputTexto">${ocorrencia.ocorrenciasStatus.valor}</div>

		</div>

		<div class="field">
			<div class="label">
				<label for="select">Natureza:</label>
			</div>
			<div class="inputTexto">${ocorrencia.natureza1.valor}</div>

		</div>

		<div class="field">
			<div class="label">
				<label for="select">Nível da Emergência:</label>
			</div>
			<div class="inputTexto">${ocorrencia.niveisEmergencia.descricao}</div>

		</div>

		<div class="field">
			<div class="label">
				<label for="input-medium">Endereço:</label>
			</div>
			<div class="inputTexto">${ocorrencia.endereco.endGeoref}</div>

		</div>

		<c:if test="${fn:length(ocorrencia.vitimas) > 0}">
			<div class="field">
				<div class="label">
					<label for="select">Vítimas:</label>
				</div>

				<table style="padding: 0; margin: 0; width: 685px;" cellspacing="0">
					<thead>
						<tr>
							<th class="left" style=""><b>nome</b>
							</td>
							<th style="width: 0px;"><b>idade</b>
							</td>
							<th style="width: 0px;"><b>sexo</b>
							</td>
							<th style="width: 0px;"><b>gravidade</b>
							</td>
							<th class="last" style="width: 0px;"><b>condição</b>
							</td>
						</tr>
					</thead>
					<c:forEach var="vitima" items="${ocorrencia.vitimas}">

						<tr>
							<td class="left">${vitima.descricao}</td>
							<td>${vitima.idade}</td>
							<td>${vitima.sexo}</td>
							<td>${vitima.gravidade.valor}</td>
							<td class="last">${vitima.tipo.valor}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</c:if>

		<c:if test="${fn:length(ocorrencia.listaVeiculosEnvolvidos) > 0}">

			<div class="field">
				<div class="label">
					<label for="select">Veículos envolvidos:</label>
				</div>

				<ul id="listaVeiculosCadastro" style="margin: 0 0 10px 170px;">
					<c:forEach var="veiculos"
						items="${ocorrencia.listaVeiculosEnvolvidos}">
						<li>${veiculos.placa} - ${veiculos.descricao}</li>
					</c:forEach>
				</ul>

			</div>

		</c:if>

		<c:if test="${fn:length(ocorrencia.ocorrenciasVeiculos) > 0}">
			<div class="field">
				<div class="label">
					<label for="select">Veículos atendimento:</label>
				</div>

				<ul id="listaVeiculosCadastro" style="margin: 0 0 10px 170px;">
					<c:forEach var="veiculos" items="${ocorrencia.ocorrenciasVeiculos}">
						<li>${veiculos.veiculo.placa} - ${veiculos.veiculo.modelo}</li>
					</c:forEach>
				</ul>

			</div>
		</c:if>

		<c:if test="${fn:length(ocorrencia.listaAssinaturas) > 0}">
			<div class="field">
				<div class="label">
					<label for="select">Assinaturas:</label>
				</div>

				<ul id="listaImagens">
					<c:forEach var="assinatura" items="${ocorrencia.listaAssinaturas}">
						<li><a
							href="${contexto}/ocorrencia/assinatura/${ocorrencia.id}/${assinatura.nome}.jpg"
							class="fancybox"> <img
								src="${contexto}/ocorrencia/assinatura/${ocorrencia.id}/${assinatura.nome}.jpg"
								width="150" height="90">
						</a></li>
					</c:forEach>
				</ul>

			</div>
		</c:if>

		<c:if test="${fn:length(ocorrencia.listaImagens) > 0}">
			<div class="field">
				<div class="label">
					<label for="select">Imagens:</label>
				</div>

				<ul id="listaImagens">
					<c:forEach var="imagem" items="${ocorrencia.listaImagens}">
						<li><a
							href="${contexto}/ocorrencia/imagem/${ocorrencia.id}/${imagem.nome}.jpg"
							class="fancybox"> <img
								src="${contexto}/ocorrencia/imagem/${ocorrencia.id}/${imagem.nome}.jpg"
								width="150" height="90">
						</a></li>
					</c:forEach>
				</ul>

			</div>
		</c:if>

		<c:if test="${fn:length(ocorrencia.listaVideos) > 0}">
			<div class="field">
				<div class="label">
					<label for="select">Filmagens:</label>
				</div>

				<ul id="listaImagens">
					<c:forEach var="videos" items="${ocorrencia.listaVideos}">
						<li><video controls="controls" width="320" height="240">
								<source
									src="${contexto}/ocorrencia/video/${ocorrencia.id}/${videos.nome}.mp4"
									type="video/mp4"></source>
								<source
									src="${contexto}/ocorrencia/video/${ocorrencia.id}/${videos.nome}.webm"
									type="video/webm"></source>
							</video></li>
					</c:forEach>
				</ul>

			</div>
		</c:if>

		<c:if test="${fn:length(ocorrencia.listaObjetos) > 0}">
			<div class="field">
				<div class="label">
					<label for="select">Materiais:</label>
				</div>

				<table style="padding: 0; margin: 0; width: 685px;" cellspacing="0">
					<thead>
						<tr>
							<th class="left" style="width: 50px"><b>Id</b>
							</td>
							<th class="left" style="width: 80px"><b>C. Interno</b>
							</td>
							<th class="left"><b>Descrição</b>
							</td>
						</tr>
					</thead>

					<c:forEach var="objeto" items="${ocorrencia.listaObjetos}">

						<tr>
							<td class="left">${objeto.id}</td>
							<td>${objeto.numero_controle_interno}</td>
							<td>${objeto.materiais.titulo} - ${objeto.descricao}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</c:if>

		<c:if test="${fn:length(ocorrencia.ocorrenciaPontosDeInteresse) > 0}">
			<div class="field">
				<div class="label">
					<label for="select">Pontos de interesse:</label>
				</div>

				<table style="padding: 0; margin: 0; width: 685px;" cellspacing="0">
					<thead>
						<tr>
							<th class="left" style="width: 50px"><b>Id</b>
							</td>
							<th class="left" style="width: 80px"><b>Nome</b>
							</td>
							<th class="left"><b>Observação</b>
							</td>
						</tr>
					</thead>

					<c:forEach var="ocorrenciaPontoDeInteresse"
						items="${ocorrencia.ocorrenciaPontosDeInteresse}">

						<tr>
							<td class="left">${ocorrenciaPontoDeInteresse.pontoDeinteresse.id}</td>
							<td>${ocorrenciaPontoDeInteresse.pontoDeinteresse.nome}</td>
							<td>${ocorrenciaPontoDeInteresse.pontoDeinteresse.observacao}</td>
						</tr>

						<c:if
							test="${fn:length(ocorrenciaPontoDeInteresse.pontoDeinteresse.listaObjetos) > 0}">

							<tr>
								<td></td>
								<td></td>
								<td>
									<div class="field">
										<div class="label">
											<label for="select">Materiais:</label>
										</div>

										<table style="padding: 0; margin: 0; width: 100px;"
											cellspacing="0">
											<thead>
												<tr>
													<th class="left" style="width: 50px"><b>Id</b>
													</td>
													<th class="left"><b>Descrição</b>
													</td>
												</tr>
											</thead>

											<c:forEach var="objeto"
												items="${ocorrenciaPontoDeInteresse.pontoDeinteresse.listaObjetos}">

												<tr>
													<td class="left">${objeto.id}</td>
													<td>${objeto.materiais.titulo} - ${objeto.descricao}</td>
												</tr>
											</c:forEach>
										</table>
									</div>
								</td>
							</tr>
						</c:if>



					</c:forEach>
				</table>
			</div>
		</c:if>

		<c:if test="${fn:length(ocorrencia.ocorrenciaEnquetes) > 0}">
		
			<c:forEach var="ocorrenciaEnquete" items="${ocorrencia.ocorrenciaEnquetes}">
				<div class="field">
					<div class="label">
						<label for="select">Enquete: ${ocorrenciaEnquete.enquete.nome}</label>
					</div>
					<table style="padding: 0; margin: 0; width: 685px;" cellspacing="0">
						<thead>
							<tr>
								<th class="left"><b>Pergunta</b>
								</td>
								<th class="left"><b>Resposta</b>
								</td>
							</tr>
						</thead>
							<c:if test="${fn:length(ocorrenciaEnquete.enquete.perguntas) > 0}">
								<c:forEach var="pergunta" items="${ocorrenciaEnquete.enquete.perguntas}">
									<tr>
										<td>${pergunta.pergunta}</td>
										<td>
											<c:if test="${fn:length(pergunta.respostas) > 0}">
												${pergunta.respostas[0].resposta}
											</c:if>
										</td>
									</tr>
								</c:forEach>
							</c:if>
					</table>
				</div>
			</c:forEach>
		</c:if>

		<div class="buttons" style="float: right;">
			<div class="highlight">
				<input class="ui-button ui-widget ui-state-default ui-corner-all"
					type="button" value="fechar"
					onclick="$('#ocorrenciaDetalhes').dialog('close');" />
			</div>
		</div>

		<div class="quebra"></div>
		<br />
	</div>
</div>

<script>
	$(".fancybox").fancybox();
</script>