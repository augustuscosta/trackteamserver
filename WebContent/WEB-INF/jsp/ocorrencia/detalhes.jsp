<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<p>${ocorrencia.descricao}</p>
<c:if test="${not empty ocorrencia.ocorrenciasVeiculos}">
	<h1>Veículos selecionados não confirmados</h1>
	<ul class="listaViaturas">
	<c:forEach var="ocorrenciasVeiculos" items="${ocorrencia.ocorrenciasVeiculos}">
		<li>${ocorrenciasVeiculos.veiculo.placa}</li>
	</c:forEach>
	</ul>
</c:if>
<p><a href="">detalhes</a>
<c:if test="${ocorrencia.usuario.id == 1}"> 
	| <a href="">editar</a>
	| <a href="">adicionar viatura</a> 
</c:if>
<c:if test="${ocorrencia.usuario.id != 1}">
	| <a href="">assumir</a> 
</c:if>
</p>

<script>

<c:forEach var="ocorrenciasVeiculos" items="${ocorrencia.ocorrenciasVeiculos}">

	var veiculoOcorrencia = new Object();
	veiculoOcorrencia.id = ${ocorrenciasVeiculos.veiculo.id};
	veiculoOcorrencia.placa = "${ocorrenciasVeiculos.veiculo.placa}";
	listaVeiculos.push(veiculoOcorrencia);
	
</c:forEach>
				
</script>