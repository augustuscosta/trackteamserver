<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
{"list":[
<c:forEach var="veiculo" items="${listaVeiculos}" varStatus="rowCounter">
	<c:if test="${rowCounter.count > 1}">,
	</c:if>
	{"id": ${veiculo.id},"idOrgao": "${veiculo.idOrgao}","placa": "${veiculo.placa}","latitude": "${veiculo.latitude}","longitude": "${veiculo.longitude}", "total_ocorrencias": "${veiculo.qntOcorrencias}", "emAtendimento": ${veiculo.emAtendimento},  
	"cerca": [<c:forEach var="gpt" items="${veiculo.cerca.geoPonto}" varStatus="rowCounter">
		<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${gpt.id}", "latitude" : "${gpt.latitude}", "longitude" : "${gpt.longitude}"}
	</c:forEach>	
	]}
</c:forEach>
]}
