<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<div id="ocorrencias_tab" class="aba">
	
	<form id="searchOcorrencia" class="search" action="" method="post" style="clear: both;">
		<input type="text" class="searchInput" id="searchOcorrenciaInput" name="pesquisa" />
		<input type="submit" class="searchBnt" value="">
	</form>
	
	<div style="margin: 5px 0 0 0; font-weight:bold;">
		<img src="images/plus.png" style="margin: 10px 5px 0 5px;">	
		<a href="<%=request.getContextPath()%>/controle_geral/ocorrencias/cadastro/">adicionar atividade</a>	
		<div class="botaoAbas">
			<select style="font-size: 10px;" id="ordenacaoOcorrencia" onclick="ordenaOcorrencias(this);">
				<option value="data">data criação</option>
				<option value="gravidade">gravidade</option>
				<option value="usuario">usuário responsável</option>
			</select>
<!-- 			<a class="selecionado" href="javascript:;" rel="data" onclick="ordenaOcorrencias(this);">data</a> <a href="javascript:;" rel="gravidade" onclick="ordenaOcorrencias(this);">gravidade</a>		 -->
		</div>
	</div>
	
	<div id="listaOcorrenciasDiv">
		<ul id="listaOcorrencias"></ul>
		
		<ul class="legendas">
<c:forEach var="item" items="${listaNiveisEmergencias}">
			<li><span style="background-color: ${item.cor};"></span><p> ${item.descricao}</p></li>
</c:forEach>			
			<li><span style="background-color: #AAA;"></span><p>aguardando fechamento</p></li>									
		</ul>
	</div>
	
	<div id="legendaOcorrenciasModel" style="display: none;">		
		<ul class="legendasMapa">
<c:forEach var="item" items="${listaNiveisEmergencias}">
			<li><span style="background-color: ${item.cor};"></span><p> ${item.descricao}</p></li>
</c:forEach>			
			<li><span style="background-color: #AAA;"></span><p>aguardando fechamento</p></li>									
		</ul>
	</div>	
</div>		
			
<script type="text/javascript">

var listarPorNiveis;

function ordenaOcorrencias(link) {

//	var funcao = $(link).val();
	var funcao = $('#ordenacaoOcorrencia').val();

	var txtBusca = $('#searchOcorrenciaInput').val().toUpperCase();
	
	if (funcao == "data") {
		
		$('#listaOcorrencias').html('');
		
		for (x in listaOcorrencias) {
			var texto = (listaOcorrencias[x].id + " " + listaOcorrencias[x].endereco.logradouro + " " + listaOcorrencias[x].endereco.numero).toUpperCase();
			if (texto.lastIndexOf(txtBusca) >= 0 ) {
				$('#listaOcorrencias').append(formataDivOcorrencia(listaOcorrencias[x]));
			}
		}

	} else 	if (funcao == "usuario") {
		
		getJSON("./ocorrencia/listaUsuario",
			function(json) {
			
				usuario = json.result.reverse();
				$('#listaOcorrencias').html('');
					
				for (x in listaOcorrencias) {
					
					var texto = (listaOcorrencias[x].id + " " + listaOcorrencias[x].endereco.logradouro + " " + listaOcorrencias[x].endereco.numero).toUpperCase();
					if (texto.lastIndexOf(txtBusca) >= 0 ) {
						if (listaOcorrencias[x].usuario.id == null) {
							$('#listaOcorrencias').prepend(formataDivOcorrencia(listaOcorrencias[x]));					
						}
					}
				}
				
				for (var i=0; i < usuario.length; i++) {
					for (x in listaOcorrencias) {
												
						var texto = (listaOcorrencias[x].id + " " + listaOcorrencias[x].endereco.logradouro + " " + listaOcorrencias[x].endereco.numero).toUpperCase();
						if (texto.lastIndexOf(txtBusca) >= 0 ) {
							if (usuario[i].id == listaOcorrencias[x].usuario.id) {
								$('#listaOcorrencias').prepend(formataDivOcorrencia(listaOcorrencias[x]));					
							}
						}
					}
				}				
				
			}, function() { 
				console.log("Erro!"); 
		});
		
	} else if (funcao == "gravidade") {

		getJSON("./ocorrencia/listaNiveisEmergencia",
			function(json) {
			
				niveis = json.result;
				$('#listaOcorrencias').html('');
					
				for (var i=0; i < niveis.length; i++) {
					for (x in listaOcorrencias) {
												
						var texto = (listaOcorrencias[x].id + " " + listaOcorrencias[x].endereco.logradouro + " " + listaOcorrencias[x].endereco.numero).toUpperCase();
						if (texto.lastIndexOf(txtBusca) >= 0 ) {
							if (niveis[i].id == listaOcorrencias[x].nivelEmergencia.id) {
								$('#listaOcorrencias').prepend(formataDivOcorrencia(listaOcorrencias[x]));					
							}
						}
					}
				}
			}, function() { 
				console.log("Erro!"); 
		});
	}		
};

$('#searchOcorrenciaInput').keyup(function() {
	
	ordenaOcorrencias("");
/*	
	var funcao = $('#ordenacaoOcorrencia').val();
	var txtBusca = $('#searchOcorrenciaInput').val().toUpperCase();
	$('#listaOcorrencias').html('');
	
	if (funcao == "gravidade") {
		
		getJSON("./ocorrencia/listaNiveisEmergencia",
				function(json) {
				
					niveis = json.result;
					$('#listaOcorrencias').html('');
						
					for (var i=0; i < niveis.length; i++) {
						for (x in listaOcorrencias) {
														
							var texto = (listaOcorrencias[x].id + " " + listaOcorrencias[x].endereco.logradouro + " " + listaOcorrencias[x].endereco.numero).toUpperCase();
							if (texto.lastIndexOf(txtBusca) >= 0 ) {
								if (niveis[i].id == listaOcorrencias[x].nivelEmergencia.id) {
									$('#listaOcorrencias').prepend(formataDivOcorrencia(listaOcorrencias[x]));					
								}
							}
						}
					}
				}, function() { 
					console.log("Erro!"); 
			});
	} else {
		for (x in listaOcorrencias) {
			var texto = (listaOcorrencias[x].id + " " + listaOcorrencias[x].endereco.logradouro + " " + listaOcorrencias[x].endereco.numero).toUpperCase();
			if (texto.lastIndexOf(txtBusca) >= 0 ) {
				$('#listaOcorrencias').prepend(formataDivOcorrencia(listaOcorrencias[x]));					
			}
		}
	}
*/
	return false;
	
});

</script>