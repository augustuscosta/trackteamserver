<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 
<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="./css/jquery.fancybox.css" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false"></script>	
	<script type="text/javascript" src="../../../js/jQuery/jquery-ui-1.8.18.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/gmap3.js"></script> 
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jquery.maskedinput.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 
	<script type="text/javascript" src="../../../js/contemPonto.js"></script>
	<script type="text/javascript" src="../../../js/jQuery/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="../../../js/ocorrencia/cadastro.js"></script>
	<script type="text/javascript" src="../../../js/jquery.fancybox.js"> </script> 

</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">

	var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');
	var regioesUsuario = ${regioes};
	regioesUsuario = regioesUsuario.list;

	var map = "";
	var mapVeiculos = "";
	var marcador = "";
	var idSolicitante = 1;
	var idVitima = 1;
	var endereco = new Object();
	var veiculos = [];
	var agentes = [];
	var pontosDeInteresse = [];
	var enquetes = [];
	
	var veiculosJSON = [];
	var agentesJSON = [];
	var pontosDeInteresseJSON = [];
	var enquetesJSON = [];
	
	var startItems = [];
	
	var idVeiculoEnvolvido = 0;
	
	endereco.latitude = latlngMapa.lat();
	endereco.longitude = latlngMapa.lng();
	
	var imageOcorrencia = new google.maps.MarkerImage(
		"../../../images/icones_mapa/ocorrencia_amarelo.png",
		new google.maps.Size(78,80),
		new google.maps.Point(0, 0),
		new google.maps.Point(32,74)
	);
	
	var imageViatura = new google.maps.MarkerImage(
		"../../../images/icones_mapa/viatura.png",
		new google.maps.Size(78,80),
		new google.maps.Point(0, 0),
		new google.maps.Point(32,74)
	);
		
	$("#mapa").ready( function() { 
		
		var myOptions = {
			      zoom: 14,
			      center: new google.maps.LatLng(latlngMapa.lat(), latlngMapa.lng()),
			      mapTypeControl: false,
			      streetViewControl: false,
			      panControl: false,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
			    
		map = new google.maps.Map(document.getElementById("mapa"), myOptions);
		
		google.maps.event.addListener(map, 'dblclick', function(event) {
			atualizaMarcador(event.latLng);
		});
		
		$('#endereco').autocomplete({
			source: function() {
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode( { 'address': $(this).val()}, function(results, status) {
				    if (status == google.maps.GeocoderStatus.OK) {
				    	if (!results) return;
				    	$('#endereco').autocomplete('display', results, false);
				    }
				  });
			},
			cb:{
				cast: function(item){
				  return item.formatted_address;
				},
				select: function(item) {
					
					atualizaMarcador(item.geometry.location);
					map.setCenter(item.geometry.location);
					
				}
		 	}
		});
	});
	
	$("#mapaVeiculos").ready( function() { 
	
		/*$("#mapaVeiculos").gmap3({ 
			action:'init',
			options:{
				center: new google.maps.LatLng(endereco.latitude, endereco.longitude),
				zoom: 14
			},
			events:{
	       		click: function(marker, event, data){
// 	       			atualizaMarcador(event.latLng);
	       		}
	       	}	
		});
		
		mapVeiculos = $('#mapaVeiculos').gmap3('get');*/
		
		var myOptions = {
			      zoom: 14,
			      center: new google.maps.LatLng(endereco.latitude, endereco.longitude),
			      mapTypeControl: false,
			      streetViewControl: false,
			      panControl: false,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
			    
		mapVeiculos = new google.maps.Map(document.getElementById("mapaVeiculos"), myOptions);
		
		ocorrenciaMark = new google.maps.Marker({
		    position: new google.maps.LatLng(endereco.latitude, endereco.longitude),
		    map: mapVeiculos,
		    title: 'Atividade',
		    draggable: false,
		    icon: imageOcorrencia
		});
	
		resizeIcon(ocorrenciaMark);

	});

	$("#mapaAgentes").ready( function() { 
		
		/*$("#mapaAgentes").gmap3({ 
			action:'init',
			options:{
				center: new google.maps.LatLng(endereco.latitude, endereco.longitude),
				zoom: 14
			}
		});
		
		mapAgentes = $('#mapaAgentes').gmap3('get');*/
		
		var myOptions = {
			      zoom: 14,
			      center: new google.maps.LatLng(endereco.latitude, endereco.longitude),
			      mapTypeControl: false,
			      streetViewControl: false,
			      panControl: false,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
			    
		mapAgentes = new google.maps.Map(document.getElementById("mapaAgentes"), myOptions);
		
		ocorrenciaAgenteMark = new google.maps.Marker({
		    position: new google.maps.LatLng(endereco.latitude, endereco.longitude),
		    map: mapAgentes,
		    title: 'Atividade',
		    draggable: false,
		    icon: imageOcorrencia
		});
	
		resizeIcon(ocorrenciaAgenteMark);

	});
	
	$("#mapaPontosDeInteresse").ready( function() { 
			
			/*$("#mapaPontosDeInteresse").gmap3({ 
				action:'init',
				options:{
					center: new google.maps.LatLng(endereco.latitude, endereco.longitude),
					zoom: 14
				}
			});
			
			mapPontos = $('#mapaPontosDeInteresse').gmap3('get');*/
			
			var myOptions = {
				      zoom: 14,
				      center: new google.maps.LatLng(endereco.latitude, endereco.longitude),
				      mapTypeControl: false,
				      streetViewControl: false,
				      panControl: false,
				      mapTypeId: google.maps.MapTypeId.ROADMAP
				    };
				    
			mapPontos = new google.maps.Map(document.getElementById("mapaPontosDeInteresse"), myOptions);
			
			ocorrenciaPontoMark = new google.maps.Marker({
			    position: new google.maps.LatLng(endereco.latitude, endereco.longitude),
			    map: mapPontos,
			    title: 'Atividade',
			    draggable: false,
			    icon: imageOcorrencia
			});
		
			resizeIcon(ocorrenciaPontoMark);
	
	});
	
	function findAddress(latLng) {
		
		var geocoder = new google.maps.Geocoder();
	    geocoder.geocode({latLng: latLng}, function(results, status) {
	    	if (status == google.maps.GeocoderStatus.OK) {
	        	if (results[0]) {
	          		$('#endereco').val(results[0].formatted_address); 
	          		
	          		endereco.numero = results[0].address_components[0].long_name;          		
	          		endereco.logradouro = results[0].address_components[1].long_name;
	          		endereco.bairro = results[0].address_components[2].long_name;    
	          		endereco.latitude = latLng.lat();
	          		endereco.longitude = latLng.lng();
	        	}
	    	}
	    });
	}
	
	function atualizaMarcador(latLng) {
	
		if (marcador == null || marcador == "") {
			marcador = new google.maps.Marker({
			    position: latLng,
			    map: map,
			    icon: imageOcorrencia,
			    draggable: true
			});
			
			google.maps.event.addListener(marcador, 'dragend', function(event) {
				
				findAddress(event.latLng);		
			});
			
		} else {
			
			marcador.setPosition(latLng);
		}
		
		findAddress(latLng);
	}
	
	function showMap() {
				
//		if ($('#enderecoLat').val != null && $('#enderecoLng').val != null) {
		if (isNaN($('#enderecoLat').val()) && isNaN($('#enderecoLng').val()) && $('input[name="ocorrencia.id"]').val() != "") {
			
			var latLng = new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val());
			
			if (marcador == null || marcador == "") {
				
				marcador = new google.maps.Marker({
				    position: latLng,
				    map: map,
				    icon: imageOcorrencia,
				    draggable: true
				});

//				map.panTo(new google.maps.LatLng(parseFloat($('#enderecoLat').val()) + parseFloat(.021), parseFloat($('#enderecoLng').val()) - parseFloat(.025)));		
				
				google.maps.event.addListener(marcador, 'dragend', function(event) {
					
					findAddress(event.latLng);		
				});
				
			} else {
				
				marcador.setPosition(latLng);
			}
	
			findAddress(latLng);
			
			map.panTo(new google.maps.LatLng(parseFloat($('#enderecoLat').val()), parseFloat($('#enderecoLng').val())));
			
		} else {

			map.panTo(latlngMapa);	
			map.setZoom(13);
		}
		
		$("#dialogMapa").dialog('open');
		google.maps.event.trigger(map, 'resize');

	}
	
	function closeMap() {
	
		$("#dialogMapa").dialog('close');
	}
	
	function showDialogVeiculos() {
	
		getVeiculosProximos();
		
		$("#dialogAddVeiculo").dialog('open');
		google.maps.event.trigger(mapVeiculos, 'resize'); 	
	
		var posicao = new google.maps.LatLng(endereco.latitude, endereco.longitude);
		ocorrenciaMark.setPosition(posicao);
		mapVeiculos.setCenter(posicao);
		
		if (veiculos.length > 0) {
			
			for (var i = 0; i < veiculos.length; i++) {
				veiculos[i].setMap(null);
			}
		}
	}

	function showDialogAgentes() {
		
		getAgentesProximos();
		
		$("#dialogAddAgente").dialog('open');
		google.maps.event.trigger(mapAgentes, 'resize'); 	
	
		var posicao = new google.maps.LatLng(endereco.latitude, endereco.longitude);
		ocorrenciaAgenteMark.setPosition(posicao);
		mapAgentes.setCenter(posicao);
	}
	
	function showDialogPontosDeInteresse() {
		
		getPontosDeInteresseProximos();
		
		$("#dialogAddPontoDeInteresse").dialog('open');
		google.maps.event.trigger(mapPontos, 'resize'); 	
	
		var posicao = new google.maps.LatLng(endereco.latitude, endereco.longitude);
		ocorrenciaPontoMark.setPosition(posicao);
		mapPontos.setCenter(posicao);
	}
	
	function showDialogEnquetes() {
		getEnquetes();
		$("#dialogAddEnquete").dialog('open');
	}
	
	function closeDialogAgentes() {
		
		$("#dialogAddAgente").dialog('close');
	}
	
	function closeDialogPontosDeInteresse() {
		
		$("#dialogAddPontoDeInteresse").dialog('close');
	}
	
	function closeDialogEnquetes() {
		
		$("#dialogAddEnquete").dialog('close');
	}
	

	function closeDialogEquipes() {
		
		$("#dialogAddEquipe").dialog('close');
	}	
	
	function closeDialogVeiculos() {
	
		$("#dialogAddVeiculo").dialog('close');
	}
	
	function getVeiculosProximos() {       

		var ocorrenciaGeoPonto = new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val());
		$('#loading').show();		
		
		// Envia o formulário via Ajax
		$.ajax({
		        type: "POST",
		        url: '../../../ocorrencia/veiculosProximos',
		        success: function( resposta )
		        {
		        	json = eval('(' + resposta + ')').list;        	
					veiculosJSON = [];
		        	
		        	for (x in json) {
		        		
		        		var cerca = [];
		        		for (y in json[x].cerca) {
		        			
		        			cerca.push(new google.maps.LatLng(json[x].cerca[y].latitude, json[x].cerca[y].longitude));
		        		}
		        		
			        	if (poligonoContemPonto(ocorrenciaGeoPonto, cerca)) {
			        		
			        		json[x].distancia = (Math.sqrt(Math.pow((json[x].latitude - ocorrenciaGeoPonto.lat()),2) + Math.pow((json[x].longitude - ocorrenciaGeoPonto.lng()), 2)));
			        		veiculosJSON.push(json[x]);
			        	}; 		        	
		        	}	
		        	
		        	veiculosJSON.sort( 
		        		function(a,b){
		        			return a.distancia - b.distancia;
		        	});
		        	
		        	plotaVeiculos(veiculosJSON);
		        	$('#loading').hide();
		        },
		        error:function(XMLHttpRequest, textStatus, errorThrown)
		        {
		        	console.log("algo deu errado");
		        	$('#loading').hide();

		        }
		});		
	};

	function getAgentesProximos() {       

		var ocorrenciaGeoPonto = new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val());
		$('#loading').show();		
		
		// Envia o formulário via Ajax
		$.ajax({
		        type: "POST",
		        url: '../../../ocorrencia/agentesProximos',
		        success: function( resposta )
		        {
		        	json = resposta.list;   
					agentesJSON = [];
		        	
		        	for (x in json) {
		        		
// 		        		var cerca = [];
// 		        		for (y in json[x].cerca) {
		        			
// 		        			cerca.push(new google.maps.LatLng(json[x].cerca[y].latitude, json[x].cerca[y].longitude));
// 		        		}
		        		
// 			        	if (poligonoContemPonto(ocorrenciaGeoPonto, cerca)) {
			        		
// 			        		agentesJSON.push(json[x]);
// 			        	} 		        	
		        	
		        		json[x].distancia = (Math.sqrt(Math.pow((json[x].latitude - ocorrenciaGeoPonto.lat()),2) + Math.pow((json[x].longitude - ocorrenciaGeoPonto.lng()), 2)));		        	
						agentesJSON.push(json[x]);
		        	}	

		        	agentesJSON.sort( 
			        		function(a,b){
			        			return a.distancia - b.distancia;
			        	});
		        	
		        	plotaAgentes(agentesJSON);
		        	$('#loading').hide();
		        },
		        error:function(XMLHttpRequest, textStatus, errorThrown)
		        {
		        	console.log("algo deu errado");
		        	$('#loading').hide();

		        }
		});		
	};
	
	
	function getPontosDeInteresseProximos() {       

		var ocorrenciaGeoPonto = new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val());
		$('#loading').show();		
		
		// Envia o formulário via Ajax
		$.ajax({
		        type: "POST",
		        url: '../../../ocorrencia/pontosDeInteresseProximos',
		        success: function( resposta )
		        {
		        	json = resposta.list;   
		        	pontosDeInteresseJSON = [];
		        	
		        	for (x in json) {
		        	
		        		json[x].distancia = (Math.sqrt(Math.pow((json[x].latitude - ocorrenciaGeoPonto.lat()),2) + Math.pow((json[x].longitude - ocorrenciaGeoPonto.lng()), 2)));		        	
		        		pontosDeInteresseJSON.push(json[x]);
		        	}	

		        	pontosDeInteresseJSON.sort( 
			        		function(a,b){
			        			return a.distancia - b.distancia;
			        	});
		        	
		        	plotaPontosDeInteresse(pontosDeInteresseJSON);
		        	$('#loading').hide();
		        },
		        error:function(XMLHttpRequest, textStatus, errorThrown)
		        {
		        	console.log("algo deu errado");
		        	$('#loading').hide();

		        }
		});		
	};
	
	function getEnquetes() {       

		$('#loading').show();		
		
		// Envia o formulário via Ajax
		$.ajax({
		        type: "POST",
		        url: '../../../ocorrencia/enquetes',
		        success: function( resposta )
		        {
		        	enquetesJSON = resposta.list;   
		        	
		        	plotaEnquetes(enquetesJSON);
		        	$('#loading').hide();
		        },
		        error:function(XMLHttpRequest, textStatus, errorThrown)
		        {
		        	console.log("algo deu errado");
		        	$('#loading').hide();

		        }
		});		
	};
	
	function filtraVeiculos() {
		
		var txtBusca = $('#filtroVeiculo').val().toUpperCase();
		var listaVeiculos = [];
		
		$('#listaVeiculos').html('');

		for (x in veiculosJSON) {
	
			if (veiculosJSON[x].placa.toUpperCase().lastIndexOf(txtBusca) >= 0 || veiculosJSON[x].idOrgao.toUpperCase().lastIndexOf(txtBusca) >= 0) {
				
				listaVeiculos.push(veiculosJSON[x]);														
			};
		}
		
		plotaVeiculos(listaVeiculos);
	}

	function filtraAgentes() {
		
		var txtBusca = $('#filtroAgente').val().toUpperCase();
		var listaAgentes = [];
		
		$('#listaAgentes').html('');
		
		for (x in agentesJSON) {
	
			if (agentesJSON[x].nome.toUpperCase().lastIndexOf(txtBusca) >= 0 || agentesJSON[x].matricula.toUpperCase().lastIndexOf(txtBusca) >= 0) {
				
				listaAgentes.push(agentesJSON[x]);														
			};
		}
		
		plotaAgentes(listaAgentes);
	}
	
	function filtraPontosDeInteresse() {
		
		var txtBusca = $('#filtroPontoDeInteresse').val().toUpperCase();
		var listaPontosDeInteresse = [];
		
		$('#listaPontosDeInteresse').html('');
		
		for (x in pontosDeInteresseJSON) {
	
			if (pontosDeInteresseJSON[x].nome.toUpperCase().lastIndexOf(txtBusca) >= 0 || pontosDeInteresseJSON[x].matricula.toUpperCase().lastIndexOf(txtBusca) >= 0) {
				
				listaPontosDeInteresse.push(pontosDeInteresseJSON[x]);														
			};
		}
		
		plotaPontosDeInteresse(listaPontosDeInteresse);
	}
	
	function filtraEnquetes() {
		
		var txtBusca = $('#filtroEnquete').val().toUpperCase();
		var listaEnquetes = [];
		
		$('#listaEnquetes').html('');
		
		for (x in enquetesJSON) {
	
			if(enquetesJSON[x]){
				if (enquetesJSON[x].nome.toUpperCase().lastIndexOf(txtBusca) >= 0 || enquetesJSON[x].descricao.toUpperCase().lastIndexOf(txtBusca) >= 0) {
					
					listaEnquetes.push(enquetesJSON[x]);														
				};
			}
		}
		
		plotaEnquetes(listaEnquetes);
	}
		
	function addRemoveVeiculoSel(id) {
		
		for (x in veiculos) {
			
			if (veiculos[x].id == id) {
				addRemoveVeiculo(veiculos[x]);
				break;
			}
		}
	}
	
	function addRemoveVeiculo(marcador) {
		
		if (marcador == null) {
			
			return;
		}
		
		if (marcador.selecionado) {
			
			resizeIcon(marcador);
			marcador.selecionado = false;
			$('#veiculoM-' + marcador.id).removeClass("selecionado");
			
		} else {
			
			resizeIcon(marcador, 'big');
			marcador.selecionado = true;
			$('#veiculoM-' + marcador.id).addClass("selecionado");
		}
	}

	function addRemoveAgenteSel(id) {
				
		for (x in agentes) {
			
			if (agentes[x].id == id) {
				addRemoveAgente(agentes[x]);
				break;
			}
		}
	}
	
	function addRemoveAgente(marcador) {
					
		if (marcador.selecionado) {
			
			resizeIcon(marcador);
			marcador.selecionado = false;
			$('#agenteM-' + marcador.id).removeClass("selecionado");
			
		} else {
			
			resizeIcon(marcador, 'big');
			marcador.selecionado = true;
			$('#agenteM-' + marcador.id).addClass("selecionado");
		}
	}
	
	
	function addRemovePontoDeInteresseSel(id) {
		
		for (x in pontosDeInteresse) {
			
			if (pontosDeInteresse[x].id == id) {
				addRemovePontoDeInteresse(pontosDeInteresse[x]);
				break;
			}
		}
	}
	
	function addRemoveEnqueteSel(id) {
		
		for (x in enquetes) {
			
			if (enquetes[x].id == id) {
				addRemoveEnquete(enquetes[x]);
				break;
			}
		}
	}
	
	function addRemoveEnquete(marcador) {
		
		if (marcador.selecionado) {
			
			resizeIcon(marcador);
			marcador.selecionado = false;
			$('#enqueteM-' + marcador.id).removeClass("selecionado");
			
		} else {
			
			resizeIcon(marcador, 'big');
			marcador.selecionado = true;
			$('#enqueteM-' + marcador.id).addClass("selecionado");
		}
	}
	
	function addRemovePontoDeInteresse(marcador) {
					
		if (marcador.selecionado) {
			
			resizeIcon(marcador);
			marcador.selecionado = false;
			$('#pontoDeInteresseM-' + marcador.id).removeClass("selecionado");
			
		} else {
			
			resizeIcon(marcador, 'big');
			marcador.selecionado = true;
			$('#pontoDeInteresseM-' + marcador.id).addClass("selecionado");
		}
	}
	
	function addVeiculos() {
		
		var listaVeiculos = "";
		
		for (var i=0; i< veiculos.length; i++) {
			
			if (veiculos[i].selecionado) {
	
				listaVeiculos += '<li id="veiculo-' + veiculos[i].id + '">' + veiculos[i].placa + ' <img style="margin-left: 20px; cursor: pointer;" onclick="removeVeiculo(' + veiculos[i].id + ');" src="../../../images/ico_excluir.gif" />' + 
					'<input type="hidden" name="ocorrencia.ocorrenciasVeiculos[].veiculo.id" value="' + veiculos[i].id + '" /></li>';
			}
		}
		
		$('#listaVeiculosCadastro').html(listaVeiculos);
		closeDialogVeiculos();
	}
	
	function addAgentes() {
		
		var listaAgentes = "";
		
		for (var i=0; i< agentes.length; i++) {
			
			if (agentes[i].selecionado) {
	
				listaAgentes += '<li id="agente-' + agentes[i].id + '">' + agentes[i].matricula + " - " + agentes[i].nome + ' <img style="margin-left: 20px; cursor: pointer;" onclick="removeAgente(' + agentes[i].id + ');" src="../../../images/ico_excluir.gif" />' + 
					'<input type="hidden" name="ocorrencia.ocorrenciaAgentes[].agente.id" value="' + agentes[i].id + '" /></li>';
			};
		}
		
		$('#listaAgentesCadastro').html(listaAgentes);
		closeDialogAgentes();
	}
	
	function addPontosDeInteresse() {
		var pontosSelecionados = new Array();
		var listaPontosDeInteresse = "";
		
		for (var i=0; i< pontosDeInteresse.length; i++) {
			
			if (pontosDeInteresse[i].selecionado) {
				
				pontosSelecionados.push(pontosDeInteresse[i]);
				
				listaPontosDeInteresse += '<li id="pontoDeInteresse-' + pontosDeInteresse[i].id + '">' + pontosDeInteresse[i].nome + '<img style="margin-left: 20px; cursor: pointer;" onclick="removePontoDeInteresse(' + pontosDeInteresse[i].id + ');" src="../../../images/ico_excluir.gif" />' + 
					'<input type="hidden" name="ocorrencia.ocorrenciaPontosDeInteresse[].pontoDeinteresse.id" value="' + pontosDeInteresse[i].id + '" /></li>';
			};
		}
		
		$('#listaPontosDeInteresseCadastro').html(listaPontosDeInteresse);
		closeDialogPontosDeInteresse();
		if(pontosSelecionados.length == 1){
			askForUsePontoDeInteresseAdresss(pontosSelecionados[0]);
		}
	}
	
	function askForUsePontoDeInteresseAdresss(ponto){
		var r=confirm("Deseja utilizar o endereço do ponto selecionado?");
		if (r==true)
		{
			var ctx = "http://" + window.location.host + "/smc/";
	 		var url = ctx + "controle_geral/pontos_de_interesse/" + ponto.id;
			$.post(url, function(data) {
				usePontoAdress(data.list);
			});
		  
		}
	}
	
	function usePontoAdress(ponto){
		$('#enderecoGeo').val(ponto.endGeoref);
		
		$('#enderecoNumero').val(ponto.numero);
		$('#enderecoRua').val(ponto.logradouro);
		$('#enderecoLat').val(ponto.latitude);
		$('#enderecoLng').val(ponto.longitude);
		$('#enderecoBairro').val(ponto.bairro);
	}
	
	function addEnquetes() {
		
		var listaEnquetes = "";
		
		for (var i=0; i< enquetes.length; i++) {
			
			if (enquetes[i].selecionado) {
	
				listaEnquetes += '<li id="enquete-' + enquetes[i].id + '">' + enquetes[i].nome + '<img style="margin-left: 20px; cursor: pointer;" onclick="removeEnquete(' + enquetes[i].id + ');" src="../../../images/ico_excluir.gif" />' + 
					'<input type="hidden" name="ocorrencia.ocorrenciaEnquetes[].enquete.id" value="' + enquetes[i].id + '" /></li>';
			};
		}
		
		$('#listaEnquetesCadastro').html(listaEnquetes);
		closeDialogEnquetes();
	}
	
	function removeVeiculo(id) {
		
		$('#veiculo-' + id ).remove();

		for (var i=0; i< veiculos.length; i++) {
			
			if (veiculos[i].id == id) {

				veiculos[i].selecionado = false;
			};
		};		
	}

	function removeAgente(id) {
		
		$('#agente-' + id ).remove();

		for (var i=0; i< agentes.length; i++) {
			
			if (agentes[i].id == id) {

				agentes[i].selecionado = false;
			};
		};		
	}
	
	function removePontoDeInteresse(id) {
		
		$('#pontoDeInteresse-' + id ).remove();

		for (var i=0; i< pontosDeInteresse.length; i++) {
			
			if (pontosDeInteresse[i].id == id) {

				pontosDeInteresse[i].selecionado = false;
			};
		};		
	}
	
	function removeEnquete(id) {
		
		$('#enquete-' + id ).remove();

		for (var i=0; i< enquetes.length; i++) {
			
			if (enquetes[i].id == id) {

				enquetes[i].selecionado = false;
			};
		};		
	}
	
	function addEndereco() {
	
		$('#enderecoGeo').val($('#endereco').val());
		
		$('#enderecoNumero').val(endereco.numero);
		$('#enderecoRua').val(endereco.logradouro);
		$('#enderecoLat').val(endereco.latitude);
		$('#enderecoLng').val(endereco.longitude);
		$('#enderecoBairro').val(endereco.bairro);
	  		
		$("#dialogMapa").dialog('close');	
		return false;
	}
	
	function addSolicitante(cpf, nome, telefone1, telefone2) {
		
		var id = idSolicitante;
		
		$('#solicitantes').append('<div id="solicitante' + idSolicitante 
				+ '" style="margin-top: 10px; display: inline-block;">' 
				+ $('#solicitanteModelo').html() + "</div>");				
		
		var solicitante = $('#solicitante' + idSolicitante); 
		
		solicitante.find('img').click(function() { delSolicitante(id); });	
		solicitante.find("input[name='ocorrencia.solicitantes[].cpf']").val(cpf);	
		solicitante.find("input[name='ocorrencia.solicitantes[].nome']").val(nome);
		solicitante.find("input[name='ocorrencia.solicitantes[].telefone1']").val(telefone1);
		solicitante.find("input[name='ocorrencia.solicitantes[].telefone2']").val(telefone2);
		
		idSolicitante++;
	}
	
	function delSolicitante(id) {
		
		$('#solicitante' + id ).remove();
	}
	
	function addVitima(descricao, gravidade, sexo, condicao, idade, id) {
		
		var id = idVitima;
		
		$('#vitimas').append('<div id="vitima' + idVitima 
				+ '" style="margin-top: 10px; display: inline-block;">' 
				+ $('#vitimaModelo').html() + "</div>");				
		
		var vitima = $('#vitima' + idVitima); 
		
		vitima.find('img').click(function() { delVitima(id); });		
		vitima.find("input[name='ocorrencia.vitimas[].descricao']").val(descricao);
		vitima.find("[name='ocorrencia.vitimas[].gravidade.id']").val(gravidade);
		vitima.find("[name='ocorrencia.vitimas[].sexo']").val(sexo);
		vitima.find("[name='ocorrencia.vitimas[].tipo.id']").val(condicao);
		vitima.find("[name='ocorrencia.vitimas[].idade']").val(idade);
		vitima.find("[name='ocorrencia.vitimas[].id']").val(id);
		
		idVitima++;
	}
	
	function delVitima(id) {
		
		$('#vitima' + id ).remove();
	}
	
	function addVeiculosEnvolvidos(placa, descricao) {

		idVeiculoEnvolvido++;
		var id = idVeiculoEnvolvido;
		
		$('#listaVeiculosEnvolvidos').append('<div id="veiculoEnvolvido-' + idVeiculoEnvolvido 
				+ '" style="margin-top: 10px; display: inline-block; width: 100%;">' 
				+ $('#veiculosEnvolvidosModelo').html() + "</div>");				
		
		var ve = $('#veiculoEnvolvido-' + idVeiculoEnvolvido); 
		
		ve.find('img').click(function() { delVeiculosEnvolvidos(id); });		
		ve.find("input[name='ocorrencia.listaVeiculosEnvolvidos[].placa']").val(placa);
		ve.find("input[name='ocorrencia.listaVeiculosEnvolvidos[].descricao']").val(descricao);
		
	}

	function delVeiculosEnvolvidos(id) {
		
		$('#veiculoEnvolvido-' + id ).remove();
	}
	
	$('#endereco').ready( function() {
		
		$('#enderecoGeo').focus( function() {
			
			showMap();											
		});	
	});
	
	function inicio() {
		
		parent.location='../../../controle_geral/ocorrencias/listar/';
	}
	
	function hashDiff(h1, h2) {
	  var d = {};
	  for (k in h2) {
	    if (h1[k] !== h2[k]) d[k] = h2[k];
	  }
	  return d;
	}

	function convertSerializedArrayToHash(a) { 
	  var r = {};
	  $.each(a, function(i, field){
	    r[a[i].name] = a[i].value;
	  });
	  return r;
	}
	
	$(document).ready( function() {
		
		$("#placaVeiculoEnvolvido").mask("aaa9999");
	    
	    $("#idadeVitima").mask("9999");
	    
	    $("#cpfSolicitante").mask("999.999.999-99");
	    $("#telefoneSolicitante1").mask("(99)9999-9999");
	    $("#telefoneSolicitante2").mask("(99)9999-9999");
	    
		$("#dialogMapa").dialog({ minWidth: 900, autoOpen: false, resizable: false });	
		$("#dialogAddVeiculo").dialog({ minWidth: 900, autoOpen: false, resizable: false });
		$("#dialogAddAgente").dialog({ minWidth: 900, autoOpen: false, resizable: false });
		$("#dialogAddEquipe").dialog({ minWidth: 900, autoOpen: false, resizable: false });
		$('#ocorrenciaDetalhes').dialog({ minWidth: 900, autoOpen: false, resizable: false });	
		$('#ocorrenciaComentario').dialog({ minWidth: 700, autoOpen: false, resizable: false });
		$("#dialogAddPontoDeInteresse").dialog({ minWidth: 900, autoOpen: false, resizable: false });
		$("#dialogAddEnquete").dialog({ minWidth: 300, autoOpen: false, resizable: false });
						
		$("#formOcorrencia").jFotoForm({"mensagem":"uma mensagem"});
		
		$('#formAddEndereco').submit(function() {
			return false;
		});	
	
		$('#formAddVeiculo').submit(function() {
			return false;
		});	

		$('#formAddAgente').submit(function() {
			return false;
		});
		
		$('#formAddPontoDeInteresse').submit(function() {
			return false;
		});
		
		$('#formAddEnquete').submit(function() {
			return false;
		});
		
		$.datepicker.regional['ru'] = {
			prevText: 'Anterior',
			nextText: 'Próximo',
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			dateFormat: 'dd/mm/yy'
		};
		
		$.datepicker.setDefaults($.datepicker.regional['ru']);

		$('#dataInicioProgramado').datetimepicker({			
			showSecond: false,
			showMillisec: false,
			timeFormat: 'hh:mm:ss',
			timeText: 'Horário',
			hourText: 'Hora',
			minuteText: 'Minuto',
			secondText: 'Segundo',
			currentText: 'Agora',
			closeText: 'Ok'			
		});

		$('#dataFimProgramado').datetimepicker({			
			showSecond: false,
			showMillisec: false,
			timeFormat: 'hh:mm:ss',
			timeText: 'Horário',
			hourText: 'Hora',
			minuteText: 'Minuto',
			secondText: 'Segundo',
			currentText: 'Agora',
			closeText: 'Ok'			
		});

		<c:if test="${not empty ocorrencia.id}">
		
			endereco.latitude = '${ocorrencia.endereco.latitude}';
			endereco.longitude = '${ocorrencia.endereco.longitude}';
		
		</c:if>
	
		<c:if test="${empty ocorrencia.solicitantes and ocorrencia.id == null}">
			addSolicitante("", "", "", "");
		</c:if>
		
		<c:forEach var="solicitante" items="${ocorrencia.solicitantes}">
			addSolicitante('${solicitante.cpf}', '${solicitante.nome}', '${solicitante.telefone1}', '${solicitante.telefone2}');
		</c:forEach>
		
		<c:forEach var="vitima" items="${ocorrencia.vitimas}">
			addVitima('${vitima.descricao}', '${vitima.gravidade.id}', '${vitima.sexo}', '${vitima.tipo.id}', '${vitima.idade}', '${vitima.id}');
		</c:forEach>			

		<c:forEach var="veiculoEnvolvido" items="${ocorrencia.listaVeiculosEnvolvidos}">
			addVeiculosEnvolvidos('${veiculoEnvolvido.placa}', '${veiculoEnvolvido.descricao}');
		</c:forEach>	
	
		<c:forEach var="veiculo" items="${ocorrencia.ocorrenciasVeiculos}">
			var veiculo = Object();
			veiculo.id = "${veiculo.veiculo.id}";
			veiculo.placa = "${veiculo.veiculo.placa}";
			veiculo.setMap = function() { } ;
			veiculo.selecionado = true;
		
			veiculos.push(veiculo);
		</c:forEach>

		addVeiculos();		

		<c:forEach var="agente" items="${ocorrencia.ocorrenciaAgentes}">
			var agente = Object();
			agente.id = "${agente.agente.id}";
			agente.nome = "${agente.agente.usuario.nome}";
			agente.matricula = "${agente.agente.usuario.matricula}";
			agente.setMap = function() { } ;
			agente.selecionado = true;
		
			agentes.push(agente);
		</c:forEach>

		addAgentes();
		
		<c:forEach var="pontoDeinteresse" items="${ocorrencia.ocorrenciaPontosDeInteresse}">
			var ponto = Object();
			ponto.id = "${pontoDeinteresse.pontoDeinteresse.id}";
			ponto.nome = "${pontoDeinteresse.pontoDeinteresse.nome}";
			ponto.setMap = function() { } ;
			ponto.selecionado = true;
		
			pontosDeInteresse.push(ponto);
		</c:forEach>
	
		addPontosDeInteresse();
		
		<c:forEach var="enquete" items="${ocorrencia.ocorrenciaEnquetes}">
			var enquete = Object();
			enquete.id = "${enquete.enquete.id}";
			enquete.nome = "${enquete.enquete.nome}";
			enquete.descricao = "${enquete.enquete.descricao}";
			enquete.selecionado = true;
		
			enquetes.push(enquete);
		</c:forEach>

	addEnquetes();
	
		$('.ajuda').tipTip({maxWidth: "auto", edgeOffset: 10});
		
		$('#endereco').keypress(function(event) {
			
			if(event.keyCode==13) { 
				event.keyCode=0; 
				event.returnValue=false; 
				return false; 
			}
		});
		startItems = convertSerializedArrayToHash($("#formOcorrencia").serializeArray());
	});
 	
 	function exibeDetalhes(id) {
 		
 		var ctx = "http://" + window.location.host + "/smc/";
 		var url = ctx + "ocorrencia/exibeDetalhes?id=" + id;
		$.post(url, function(data) {
			  $('#ocorrenciaDetalhes').html(data);
			  $('#ocorrenciaDetalhes').dialog('open');
		});
	}
 	
 	function showOcorrenciaComentario(id) {

		getComentarios(id);
		
		$('#ocorrenciaComentarioId').val(id);
		$('#ocorrenciaComentario').dialog('open');

	}
 	
 	function getComentarios(id) {
 		var ctx = "http://" + window.location.host + "/smc/";
 		var url = ctx + "ocorrencia/listaComentarios?id=" + id;
		getJSON_method(url, "GET",
			function(json) {
        	
				lista = json.result;
				var div = "";
				
				for (x in lista) {

					var date = new Date(lista[x].data);
					var hours = date.getHours();
					var minutes = date.getMinutes();

					var year = date.getFullYear();	
					var month = date.getMonth()+1;
					var dia = date.getDate();
				     
					var formattedTime = dia + '/' + month + '/' + year + ' ' + hours + ':' + minutes;
					
					div += "<li><b>" + lista[x].usuario + "<br />" + formattedTime + "</b>" + lista[x].descricao + "</li>";
				}
			
				$('#listaComentarios').html(div);
				
			}, function() { 
				console.log("algo deu errado"); 
		});

	}
 	
 	function closeOcorrenciaComentario() {

		$('#ocorrenciaComentarioText').val("");		
		$('#ocorrenciaComentario').dialog('close');
	}
 	
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">	
		
	<div id="dialogMapa" title="Selecionar endereço">
	
		<form id="formAddEndereco" action="">
	
			<div class="form">
				<div class="fields">
						
					<div class="field">
						<div class="label">
							<label for="input-medium">Endereço:</label>
						</div>
						<div class="input">
							<input id="endereco" type="text" style="background-color: #FFF;" />							  		
						</div>
						
						<div id="mapaDiv">
							<div id="mapa" style="height: 100%; padding: 7px;"></div>
						</div>			  				
	
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar" onclick="addEndereco();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="cancelar" onclick="closeMap();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>
	
	<div id="dialogAddVeiculo" title="Adicionar veiculos">
	
		<form id="formAddVeiculo" action="">
	
			<div class="form">
			
				<input type="hidden" id="cercaSel" name="cercaSel" value="" />			
				<div class="fields">						
					<div class="field">

						<div id="listaVeiculosDiv">		
						
							<input type="text" id="filtroVeiculo" name="filtro" value="" style="width: 100%;" onkeyup="filtraVeiculos();" />	
									
							<ul id="listaVeiculos" class="listaDialogSelect">
								
							</ul>
						</div>
											
						<div id="mapaDivVeiculos">
							<div id="mapaVeiculos" style="height: 420px; padding: 7px;"></div>
						</div>			  				
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar veiculos" onclick="addVeiculos();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="cancelar" onclick="closeDialogVeiculos();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>

	<div id="dialogAddAgente" title="Adicionar agentes">
	
		<form id="formAddAgente" action="">
	
			<div class="form">
			
				<div class="fields">						
					<div class="field">

						<div id="listaAgentesDiv">		
						
							<input type="text" id="filtroAgente" name="filtro" value="" style="width: 100%;" onkeyup="filtraAgentes();" />	
									
							<ul id="listaAgentes" class="listaDialogSelect">
								
							</ul>
						</div>
											
						<div id="mapaDivAgentes">
							<div id="mapaAgentes" style="height: 420px; padding: 7px;"></div>
						</div>			  				
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar agentes" onclick="addAgentes();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="cancelar" onclick="closeDialogAgentes();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>
	
	<div id="dialogAddPontoDeInteresse" title="Adicionar pontos de interesse">
	
		<form id="formAddPontoDeInteresse" action="">
	
			<div class="form">
			
				<div class="fields">						
					<div class="field">

						<div id="listaPontosDeInteresseDiv">		
						
							<input type="text" id="filtroPontoDeInteresse" name="filtro" value="" style="width: 100%;" onkeyup="filtraPontosDeInteresse();" />	
									
							<ul id="listaPontosDeInteresse" class="listaDialogSelect">
								
							</ul>
						</div>
											
						<div id="mapaDivPontosDeInteresse">
							<div id="mapaPontosDeInteresse" style="height: 420px; padding: 7px;"></div>
						</div>			  				
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar pontos" onclick="addPontosDeInteresse();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="cancelar" onclick="closeDialogPontosDeInteresse();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>
	
	<div id="dialogAddEnquete" title="Adicionar enquetes">
	
		<form id="formAddEnquete" action="">
	
			<div class="form">
			
				<div class="fields">						
					<div class="field">

						<div id="listaEnquetesDiv">		
						
							<input type="text" id="filtroEnquete" name="filtro" value="" style="width: 100%;" onkeyup="filtraEnquetes();" />	
									
							<ul id="listaEnquetes" class="listaDialogSelect">
								
							</ul>
						</div>
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar enquetes" onclick="addEnquetes();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="cancelar" onclick="closeDialogEnquetes();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>

	<div id="ocorrenciaDetalhes" title="Detalhes atividade"> </div>
	
	<div id="ocorrenciaComentario" title="Observações"> 
	
		<div id="comentarios" style="">
			<ul id="listaComentarios"></ul>
		</div>
	</div>
			
</jsp:attribute>

<jsp:body>

    <div id="loading">
    	<table style="width: 100%; height: 100%;">
    		<tr valign="middle">    			
    			<td align="center">Carregando...<br /><img src="../../../images/ajax-loader.gif" /></td>
    		</tr>
    	</table>    	
    </div>
    
	<div class="box box-inline">
		
		<div class="title">
			<h5>Dados da atividade</h5>
		</div>
	
	<c:if test="${not empty errors}">
	
		<br />
		<div class="messages">
			<div id="message-error" class="message message-error">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/error.png" />
				</div>
				<div class="text">
					<h6>Ocorrerão os seguintes erros</h6>
				    <c:forEach var="error" items="${errors}">   
				          <span>${error.message}</span>
				    </c:forEach>  		
				</div>
				<div class="dismiss">
					<a href="#message-error"></a>
				</div>
			</div>
		</div>
	
	</c:if>
	
	<c:if test="${not empty ocorrencia.id}">
	
		<div id="comandosDiv" >
			<a onclick="exibeDetalhes(${ocorrencia.id});" href="javascript:;">Detalhes</a>
			 |
			<a onclick="showOcorrenciaComentario(${ocorrencia.id})" href="javascript:;">Observações</a>
		</div>
		
	</c:if>

		<div id="solicitanteModelo" style="display: none;">
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>cpf</b></div>
				<input class="subDiv" name="ocorrencia.solicitantes[].cpf" id="cpfSolicitante" type="text" style="width:85px;" validate="cpf"/>
			</div>
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>nome</b></div>
				<input class="subDiv" name="ocorrencia.solicitantes[].nome" style="width: 317px;" type="text" validate="nome" />
			</div>
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>telefone</b></div>
				<input class="subDiv" name="ocorrencia.solicitantes[].telefone1" id="telefoneSolicitante1" style="width:85px;" type="text" validate="telefoneOuVazio" />
			</div>
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>telefone</b></div>
				<input class="subDiv" name="ocorrencia.solicitantes[].telefone2" id="telefoneSolicitante2" style="width:85px;" type="text" validate="telefone" />
			</div>
			<div class="inputSubDiv">
				<img style="margin: 25px 0 0 10px; cursor: pointer;" src="../../../images/ico_excluir.gif" />
			</div>
		</div>

		<div id="veiculosEnvolvidosModelo" style="display: none;">
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>placa</b></div>
				<input class="subDiv" name="ocorrencia.listaVeiculosEnvolvidos[].placa" id="placaVeiculoEnvolvido" type="text" style="width:85px;" validate="vazio"/>
			</div>
			
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>descrição</b></div>
				<input class="subDiv" name="ocorrencia.listaVeiculosEnvolvidos[].descricao" type="text" style="width:540px;" />
			</div>
						
			<div class="inputSubDiv">
				<img style="margin: 25px 0 0 10px; cursor: pointer;" src="../../../images/ico_excluir.gif" />
			</div>
		</div>
		
		<div id="vitimaModelo" style="display: none;">
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>descrição</b></div>
				<input class="subDiv" name="ocorrencia.vitimas[].descricao" style="width: 214px;" type="text" validate="vazio"/>
			</div>

			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>idade</b></div>
				<input class="subDiv" name="ocorrencia.vitimas[].idade" id="idadeVitima" style="width: 20px;" type="text" validate="idade" />
			</div>
			
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>sexo</b></div>

				<div class="select">
					<select name="ocorrencia.vitimas[].sexo" style="width: 120px;">
						<option value="M">Masculino</option>
						<option value="F">Feminino</option>
					</select>
				</div>						</div>
									
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>gravidade</b></div>
								
				<div class="select">
					<select name="ocorrencia.vitimas[].gravidade.id" style="width: 120px;">
					<c:forEach var="gravidade" items="${listaGravidade}">
						<option value="${gravidade.id}">${gravidade.valor}</option>
					</c:forEach>
					</select>
				</div>
			</div>

			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>condição</b></div>
								
				<div class="select">
					<select name="ocorrencia.vitimas[].tipo.id" style="width: 120px;"> 
					<c:forEach var="condicao" items="${listaVitimaCondicao}">
						<option value="${condicao.id}">${condicao.valor}</option>
					</c:forEach>
					</select>
				</div>
			</div>
			
			<div class="inputSubDiv">
				<img style="margin: 25px 0 0 10px; cursor: pointer;" src="../../../images/ico_excluir.gif" />
			</div>						
		</div>
													
		<form id="formOcorrencia" action="../../../ocorrencia/adicionar" method="post">
			
			<div class="form">
			
				<input name="ocorrencia.id" type="hidden" value="${ocorrencia.id}" />
			
				<div class="fields">
					
					<div class="field">
						<div class="label">
							<label for="select">Pontos de interesse<b style="color: #FF0000; font-size: 18px;">*</b>:<div class="ajuda" title="Pontos de interesse vinculados a atividade.">?</div></label>
						</div>
						
						<div class="input" style="margin:10px 0 0 185px;">
							<ul id="listaPontosDeInteresseCadastro">
							</ul>
						</div>
										
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="showDialogPontosDeInteresse();"><img src="../../../images/plus.png" /> adicionar pontos de interesse</a>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Endereço:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Endereço da atividade">?</div></label>
						</div>
						<div class="input">
							<input id="enderecoGeo" class="medium${erroEndereco eq true ? ' error' : ''}" type="text" name="ocorrencia.endereco.endGeoref" value="${ocorrencia.endereco.endGeoref}" />
		    				<img class="bnt_mapa" src="../../../images/icones/map.png" width="28px;" height="28px;" onclick="showMap();" />
		    				<input type="hidden" id="enderecoId" name="ocorrencia.endereco.id" value="${ocorrencia.endereco.id}" />
		    				<input type="hidden" id="enderecoNumero" name="ocorrencia.endereco.numero" value="${ocorrencia.endereco.numero}" />
		    				<input type="hidden" id="enderecoRua" name="ocorrencia.endereco.logradouro" value="${ocorrencia.endereco.logradouro}" validate="vazio" msgErro="endereço inválido" divDestaque="enderecoGeo" />
		    				<input type="hidden" id="enderecoLat" name="ocorrencia.endereco.latitude" value="${ocorrencia.endereco.latitude}" />
		    				<input type="hidden" id="enderecoLng" name="ocorrencia.endereco.longitude" value="${ocorrencia.endereco.longitude}" />	    				
		    				<input type="hidden" id="enderecoBairro" name="ocorrencia.endereco.bairro" value="${ocorrencia.endereco.bairro}" />
						</div>					
					</div>
				
					<div class="field">
						<div class="label">
							<label for="select">Solicitantes:<div class="ajuda" title="Dados da pessoa que solicitou a atividade.">?</div></label>
						</div>									
						
						<div id="solicitantes" class="input" style="margin-left: 185px;">
						
						</div>
						
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="addSolicitante();"><img src="../../../images/plus.png" /> adicionar solicitante</a>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="input-medium">Inicio programado:<div class="ajuda" title="Data e hora que deverá ser iniciado o atendimento da atividade.">?</div></label>
						</div>
						<div class="input">
							<input style="display: block; width: 200px; margin-right: 10px;" class="medium${erroResumo eq true ? ' error' : ''}" type="text" name="dataInicioProgramado" id="dataInicioProgramado" value="${dataInicioProgramado}" />
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="input-medium">Fim programado:<div class="ajuda" title="Data e hora que deverá ser encerrado o atendimento da atividade.">?</div></label>
							
						</div>
						<div class="input">
							<input style="display: block; width: 200px; margin-right: 10px;" class="medium${erroResumo eq true ? ' error' : ''}" type="text" name="dataFimProgramado" id="dataFimProgramado" value="${dataFimProgramado}" />							
						</div>
					</div>
												
					<div class="field">
						<div class="label">
							<label for="input-medium">Resumo:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Resumo da atividade. Será mostrado na aba das ocorrências.">?</div></label>
						</div>
						<div class="input">
							<input style="display: block;" class="medium${erroResumo eq true ? ' error' : ''}" type="text" name="ocorrencia.resumo" value="${ocorrencia.resumo}" validate="vazio"/>
						</div>
					</div>
			
					<div class="field">
						<div class="label">
							<label for="input-medium">Detalhes:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Destalhes da atividade">?</div></label>
						</div>
						<div class="">
							<textarea class="textarea-medium${erroDescricao eq true ? ' error' : ''}" name="ocorrencia.descricao">${ocorrencia.descricao}</textarea>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Meio de recebimento:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Meio por onde a atividade foi informada">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="ocorrencia.meioCadastro.id">
								<c:forEach var="meio" items="${listaMeios}">
									<option value="${meio.id}" ${ocorrencia.meioCadastro.id == meio.id ? 'selected="selected"' : ''}>${meio.valor}</option>
								</c:forEach>
							</select>
						</div>
					</div>
										
					<div class="field">
						<div class="label">
							<label for="select">Status:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Status atual da atividade">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="ocorrencia.ocorrenciasStatus.id">
								<c:forEach var="status" items="${listaStatus}">
									<option value="${status.id}" ${ocorrencia.ocorrenciasStatus.id == status.id ? 'selected="selected"' : ''}>${status.valor}</option>
								</c:forEach>
							</select>
						</div>
					</div>
									
					<div class="field">
						<div class="label">
							<label for="select">Natureza:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Natureza da atividade">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="ocorrencia.natureza1.id">
								<c:forEach var="natureza" items="${listaNatureza}">
									<option value="${natureza.id}" ${ocorrencia.natureza1.id == natureza.id ? 'selected="selected"' : ''}>${natureza.valor}</option>
								</c:forEach>
							</select>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="select">Nível da Emergência:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Nível da emergência">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="ocorrencia.niveisEmergencia.id">
								<c:forEach var="nivelEmergencia" items="${listaNiveisEmergencias}">
									<option value="${nivelEmergencia.id}" ${ocorrencia.niveisEmergencia.id == nivelEmergencia.id ? 'selected="selected"' : ''}>${nivelEmergencia.descricao}</option>
								</c:forEach>
							</select>
						</div>
					</div>
							
					<div class="field" style="display: none;">
						<div class="label">
							<label for="select">Vítimas:<div class="ajuda" title="Vítimas envolvidas na atividade">?</div></label>
						</div>
												
						<div id="vitimas" class="input" style="margin-left: 185px;"></div>
						
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="addVitima();"><img src="../../../images/plus.png" /> adicionar vítima</a>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="select">Veículos envolvidos:<div class="ajuda" title="Veículos envolvidos na atividade">?</div></label>
						</div>
										
						<div id="listaVeiculosEnvolvidos" class="input" style="margin-left: 185px;"></div>
																
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="addVeiculosEnvolvidos();"><img src="../../../images/plus.png" /> adicionar veículos envolvidos</a>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Veículos atendimento<b style="color: #FF0000; font-size: 18px;">*</b>:<div class="ajuda" title="Veículos que atenderão a atividade">?</div></label>
						</div>
						
						<div class="input" style="margin:10px 0 0 185px;">
							<ul id="listaVeiculosCadastro">
							</ul>
						</div>
										
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="showDialogVeiculos();"><img src="../../../images/plus.png" /> adicionar veículos</a>
						</div>
					</div>
									
					<div class="field">
						<div class="label">
							<label for="select">Agentes atendimento<b style="color: #FF0000; font-size: 18px;">*</b>:<div class="ajuda" title="Agentes que atenderão a atividade">?</div></label>
						</div>
						
						<div class="input" style="margin:10px 0 0 185px;">
							<ul id="listaAgentesCadastro">
							</ul>
						</div>
										
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="showDialogAgentes();"><img src="../../../images/plus.png" /> adicionar agentes</a>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Enquetes<b style="color: #FF0000; font-size: 18px;">*</b>:<div class="ajuda" title="Enquetes vinculadas a atividade.">?</div></label>
						</div>
						
						<div class="input" style="margin:10px 0 0 185px;">
							<ul id="listaEnquetesCadastro">
							</ul>
						</div>
										
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="showDialogEnquetes();"><img src="../../../images/plus.png" /> adicionar enquetes</a>
						</div>
					</div>

					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${ocorrencia.id == null ? 'adicionar' : 'alterar'}" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
