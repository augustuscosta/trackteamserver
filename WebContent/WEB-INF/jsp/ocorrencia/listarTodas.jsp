<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	function removeOcorrencia(id, nome) {
		
		var answer = confirm("Deseja realmente excluir a atividade " + nome + " ?");
		if (answer) {
			
		$.post( "../excluir/", { 'id': id },
	   		function( resposta ) {

				resposta = eval('(' + resposta + ')');       

				if (resposta.status == 'error') {
					
				} else {
				
					$('#message-success-senha').find('h6').html(resposta.msgSucesso);
					$('#message-success-senha').show();	
					$('#mensagem-erro-senha').hide();	
					$('#dialogSenha').dialog('close');		
				
					$(window).trigger('resize');
				}
			});			
		}
	}

	$(document).ready(function() {

		$("#lista").jTable({
			url: '../../../controle_geral/ocorrencias/listaOcorrencias/',
			cabecalho: [
				{'titulo': 'Id', 'width': 'auto', 'align': 'left', 'campo': 'id' },        
				{'titulo': 'Logradouro', 'width': 'auto', 'align': 'left', 'campo': 'logradouro' },
				{'titulo': 'Número', 'width': 'auto', 'align': 'left', 'campo': 'numero' },
				{'titulo': 'Resumo', 'width': 'auto', 'align': 'left', 'campo': 'resumo' },
				{'titulo': 'Natureza', 'width': 'auto', 'align': 'left', 'campo': 'natureza1' },
				{'titulo': 'Operador', 'width': 'auto', 'align': 'left', 'campo': 'operador' }
			],
			opcoesTitulo: 'Opções',
			editar: true,
			remover: false,
			linkEditar: '../../../controle_geral/ocorrencias/editar/',			
			campoRef: 'id',
			campoTituloRef: 'id',
			iconEditar: '../../../images/ico_alterar.png'
		});
		
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
	
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Lista atividades</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />

		<div class="messages">
			<div id="message-success-senha" class="message message-success" style="display: none;">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/success.png" />
				</div>
				<div class="text">
					<h6></h6>
				</div>
			</div>
		</div>
				
		<div class="table">
<div id="lista"></div>
		</div>
	</div>
	
</jsp:body>

</layout:controleGeral>
