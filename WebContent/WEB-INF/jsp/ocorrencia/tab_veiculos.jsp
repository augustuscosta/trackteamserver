<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<div id="veiculos_tab" class="aba" style="display:none;">

<!-- 	<form id="searchVeiculo" class="search" action="" method="post" style="clear: both;"> -->
<!-- 		<input type="text" class="searchInput" id="searchVeiculoInput" name="pesquisa" /> -->
<!-- 		<input type="submit" class="searchBnt" value=""> -->
<!-- 	</form> -->
		
	<div style="margin: 5px 0 0 0;">
		<img src="<c:url value="images/plus.png"/>" style="margin: 10px 5px 0 5px;">
		<a href="<%=request.getContextPath()%>/controle_geral/veiculo/adicionar/">adicionar veiculo</a>
	</div>
	
	<div id="listaVeiculosDiv">
	
		<ul id="listaVeiculos"></ul>
		
		<ul class="legendas">		
			<li><span style="background-color: #F78C1E;"></span><p>online</p></li>									
			<li><span style="background-color: #FFDE5B;"></span><p>em atendimento</p></li>
			<li><span style="background-color: #BBB;"></span><p>offline</p></li>
			<li><span style="background-color: #FF5353;"></span><p>sem informações</p></li>
		</ul>				
	</div>

	<div id="legendaVeiculosModel" style="display: none;">		
		<ul class="legendasMapa">
			<li><span style="background-color: #F78C1E;"></span><p>online</p></li>									
			<li><span style="background-color: #FFDE5B;"></span><p>atendimento</p></li>
			<li><span style="background-color: #BBB;"></span><p>offline</p></li>
			<li><span style="background-color: #FF5353;"></span><p>sem informações</p></li>								
		</ul>
	</div>
		
</div>
