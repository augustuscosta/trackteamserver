<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<head>
	<title> .: On The Go :. </title>
	
	<link type="text/css" href="<%=request.getContextPath()%>/css/ccpb.css" rel="stylesheet" />
	<link type="text/css" href="<%=request.getContextPath()%>/css/style.css" rel="stylesheet" />
	<link type="text/css" href="<%=request.getContextPath()%>/css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	
	<script src="<%=request.getContextPath()%>/js/jquery-1.7.1.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script> 	
	<script src="<%=request.getContextPath()%>/js/util.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/js/jFoto/jTelaCheia.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	<!--
		//verifica se o navegador é Internet Explorer versão 6 ou anterior
		var browser = navigator.appName
		var ver = navigator.appVersion
		var thestart = parseFloat(ver.indexOf("MSIE"))+1
		var brow_ver = parseFloat(ver.substring(thestart+4,thestart+7))
		if ((browser=="Microsoft Internet Explorer") && (brow_ver < 8)) {
			$('#alertaIE').append('<br />Essa aplicação pode não funcionar corretamente no Internet Explorer. <br /><br />Por favor, utilize outro navegador.');
			$('#alertaIE').dialog({ autoOpen: true, modal: true, closeOnEscape: false });
		}
		
	//-->
	$(document).ready( function() {
		$("#recuperarSenha").dialog({ minWidth: 373, maxHeight: 400, autoOpen: false, modal: true, resizable: false });	
	});
	</script>
	
</head>

<body>

<div style="margin: 65px auto 0px; width:200px;">
<img src="./images/logo.png">
</div>

<div id="content" style="width:40%; border-radius:12px; padding:5px; box-shadow: 0 0 10px #444;">

	<div class="box">
		
		<div style="width:60%; font-size:12px; margin: 15px auto; text-align:center; font-weight:bold; color: #555;">
			Bem vindo.
		</div>
		
		<c:if test="${not empty errors}">
				<div class="messages">
					<div id="message-error" class="message message-error" style="height:auto;">
						<div class="image">
							<img height="32" alt="Error" src="./images/icones/error.png" />
						</div>
						<div class="text">
							<h6>Ocorreram os seguintes erros:</h6>
						    <c:forEach var="error" items="${errors}">   
						          <span style="height: auto; padding: 0 0 0 20px; ">${error.message}</span>
						    </c:forEach>  		
						</div>
						<div class="dismiss">
							<a href="javascript:;" onclick="$('.messages').hide();"></a>
						</div>
					</div>
				</div>
		</c:if>

		<form method="post" action="login">
			
			<div class="form"  style="padding: 10px 20px; margin: 0 auto; border-radius:8px; width: 90%;">
				<div class="fields" style="margin: 0 auto; width: 450px;">
				
					<div class="field" style="margin-top: 10px; border: none; height: auto;">
						<div class="label" style="width: 40px; margin-right: 50px;">
							<label>Login:</label>
						</div>
						<div class="input" style="margin:0;">
							<input class="focus${erroLogin eq true ? ' error' : ''}" type="text" value="${login}" size="40" name="login" style="width:345px" />
						</div>
					</div>
					<div class="field" style="border: none; height: auto;">
						<div class="label" style="width: 40px; margin-right: 50px;">
							<label>Senha:</label>
						</div>
						<div class="input" style="margin:0;">
							<input class="focus${erroSenha eq true ? ' error' : ''}" type="password" size="40" name="senha" style="width:345px" />
						</div>
					</div>

					<div class="buttons" style="margin-top: 7px; width: 250px">
						<p style="width: 150px; font-weight: bold; float: left; margin: 8px 0 0 42px;"> <a href="javascript:;" onclick="$('#recuperarSenha').dialog('open');"> Esqueceu sua senha ? </a></p>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" style="float: right;" type="submit" value="login" />	
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="recuperarSenha" title="Recuperar Senha">
	
	<form method="post" action="login/recuperar">
		<div class="form" style="clear: both;">
			<p>Digite o seu Email:</p>
			<div class="fields">	
			
				<div class="field" style="border: none; height: auto;">
					<div class="input" style="margin:0;">
						<input class="focus${erroRecuperar eq true ? ' error' : ''}" type="text" value="${email}" size="40" name="email" style="width:345px" />
					</div>
				</div>
						
				<div class="buttons" style="margin-left: 0;">
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="Enviar" />
				</div>
			</div>
		</div>
	</form>
	
</div>

</body>


