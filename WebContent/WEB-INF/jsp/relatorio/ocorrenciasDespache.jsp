<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout"%>

<layout:controleGeral keywords="" description=""
	title=".On The Go.">

	<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/jquery-autocomplete.css"
			rel="stylesheet" />
	<link type="text/css"
			href="../../../css/flick/jquery-ui-1.8.16.custom.css"
			rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>
	<jsp:attribute name="link_scripts">

	<script type="text/javascript"
			src="../../../js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 

</jsp:attribute>
	<jsp:attribute name="scripts">

	<script type="text/javascript">
		var listaAgentes = [];

		function showAgenteDialog() {

			getAgentes();

			$("#dialogAgente").dialog('open');
		}

		function fechaCerca() {
			$('#dialogAgente').dialog('close');
		}

		function getAgentes() {

			$
					.ajax({
						type : "POST",
						url : "../../../agente/listaAgentes",
						success : function(resposta) {

							json = resposta.result;

							listaAgentes = json;

							div = "";

							for (x in listaAgentes) {
								div += '<li id="agente-' + listaAgentes[x].id +'"><a href="javascript:;" onclick="selecionaAgente('
										+ listaAgentes[x].id
										+ ')">'
										+ listaAgentes[x].nome + '</li>';
							}

							$('#listaAgentes').html(div);
							/*
							if (listaAgentes.length > 0) {
								exibeCerca(listaAgentes[0].id);
							}
							 */
						}
					});
		}
		function selecionaAgente(id) {

			for ( var i = 0; i < listaAgentes.length; i++) {

				if (listaAgentes[i].id == id) {
					break;
				}
			}
			console.log(listaAgentes[i].id);
			
			$('#AgenteNome').html(listaAgentes[i].matricula + ' - ' + listaAgentes[i].nome+ '<img style="margin-left: 20px; cursor: pointer;" onclick="zeraAgente();" src="../../../images/ico_excluir.gif" />');
			$('#agenteCampo').val(listaAgentes[i].id);
			$('#addAgente').hide(300);
			$('#dialogAgente').dialog('close');

		}
		
		
		function filtraAgentes(){
			

			var txtBusca = $('#filtroAgentes').val().toUpperCase();
			var div = "";
			$('#listaAgentes').html('');
	
			for (x in listaAgentes) {
		
				if (listaAgentes[x].nome.toUpperCase().lastIndexOf(txtBusca) >= 0 ) {
					div += '<li id="agente-' + listaAgentes[x].id +'"><a href="javascript:;" onclick="selecionaAgente('	+ listaAgentes[x].id+ ')">'+ listaAgentes[x].nome + '</li>';
				}
			}
			
			$('#listaAgentes').html(div);
			/*
			if (listaCercas.length > 0) {
				exibeCerca(listaCercas[0].id);
			}
			*/
			
		}

		function zeraAgente() {
			$('#AgenteNome').html('');
			$('#agenteCampo').val('');
			$('#addAgente').show();
		}
		$(document).ready(function() {
			
			$("#dataInicio").datepicker(propriedadesDatePicker);
			$("#dataFim").datepicker(propriedadesDatePicker);

			$('#formAgentes').submit(function() {
				selecionaAgente(this.elements["agenteSel"].value);
				fechaCerca();
				return false;
			});

			$("#dialogAgente").dialog({
				minWidth : 400,
				maxHeight : 500,
				autoOpen : false,
				resizable : false
			});

		});
		
		var propriedadesDatePicker = {
				dateFormat: 'dd/mm/yy',
				dayNames: [
				'Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'
				],
				dayNamesMin: [
				'D','S','T','Q','Q','S','S','D'
				],
				dayNamesShort: [
				'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'
				],
				monthNames: [
				'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',
				'Outubro','Novembro','Dezembro'
				],
				monthNamesShort: [
				'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set',
				'Out','Nov','Dez'
				],
				nextText: 'Próximo',
				prevText: 'Anterior'
				 
			};
	</script>
</jsp:attribute>

	<jsp:attribute name="dialogs">

	
	<div id="dialogAgente" title="Selecionar Agente">
	
		<form id="formAgentes">
		<div class="form">
			<input type="hidden" id="agenteSel" name="agenteSel" value="" />
			<div class="fields">					
				<div class="field">						
					<div id="listaAgentesDiv">		
					
						<input type="text" id="filtroAgentes" name="filtro" value="" style="width: 100%;" onkeyup="filtraAgentes();" />
							
						<ul id="listaAgentes">
							
						</ul>
					</div>
				</div>
				
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all"
									type="submit" value="selecionar agente" />
					</div>
					<input class="ui-button ui-widget ui-state-default ui-corner-all"
								type="button" value="cancelar"
								onclick="$('#dialogAgente').dialog('close');" />
				</div>
			</div>
		</div>
		
		</form>
	</div>

</jsp:attribute>

	<jsp:body>
	<div class="box box-inline">
		<form id="formVeiculo"
				action="../../../estatistica/relatorios/despache/relatorio" method="post">
		<div class="form">
			<div class="fields">
						
					<input type="hidden" id="agenteCampo" name="agenteCampo" value=""/>
					
					<div class="field">
						<div class="label">
							<label for="select">Data:<div class="ajuda" title="Selecione a data inicial e final para definir o período da pesquisa">?</div></label>
						</div>
						
						<div class="input" style="margin-bottom: 10px;">
							<input id="dataInicio" name="dataInicio" value="${dataInicio}" type="text" style="float: none; display: inline; margin-right: 10px; " /> a 						
							<input id="dataFim" name="dataFim" value="${dataFim}" type="text" style="float: none; display: inline; margin-left: 10px; " />
						</div>
											
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Despachante:<div class="ajuda" title="Selecione o Despachante que deverá ser criado o relatório">?</div></label>
						</div>
						<div class="select">				
							<div style="margin: 10px 0 0 0px;">
									
								<div id="AgenteNome" style="display: inline;">
									<c:if test="${veiculo.cerca.id > 0}"> ${veiculo.cerca.nome} 
										<img style="margin-left: 20px; cursor: pointer;" onclick="zeraCerca();" src="../../../images/ico_excluir.gif" />
									</c:if>
								</div>
								<div id="addAgente">
									<a href="javascript:;" onclick="showAgenteDialog();">selecione o Despachante</a><br /><br />
								</div>
							</div>						
						</div>
					</div>
					<div class="field">
					<div class="label">
							<label for="select">Formato:<div class="ajuda" title="Selecione o formato do relatório">?</div></label>
						</div>
						<div class="select">
							<input type="radio" checked="checked" value="pdf" name="formato">pdf
							<input type="radio" value="excel" name="formato">Excel
						</div>				
					</div>
			<div class="buttons">
				<div class="highlight">
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="gerar" />
				</div>
				<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
			</div>	
			</div>

		</div>
		</form>
</div>


	</jsp:body>
</layout:controleGeral>