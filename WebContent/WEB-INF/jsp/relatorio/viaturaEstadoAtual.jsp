<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout"%>

<layout:controleGeral keywords="" description=""
	title=".On The Go.">

	<jsp:attribute name="link_css">

	<link type="text/css" href="../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>
	<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script> 
 
	

</jsp:attribute>
	<jsp:attribute name="scripts">

	<script type="text/javascript">
		
		$(document).ready(function() {;
			
		});
		
	</script>
</jsp:attribute>

<jsp:attribute name="dialogs">

</jsp:attribute>

	<jsp:body>
		
	<div class="box box-inline">
	
		<div class="title">
			<h5>Gerar relatório - Veículos estado</h5>
		</div>

		<c:if test="${not empty errors}">
		
			<br />
			<div class="messages">
				<div id="message-error" class="message message-error">
					<div class="image">
						<img height="32" alt="Error" src="../../images/icones/error.png" />
					</div>
					<div class="text">
						<h6>Ocorrerão os seguintes erros</h6>
					    <c:forEach var="error" items="${errors}">   
					          <span>${error.message}</span>
					    </c:forEach>  		
					</div>
					<div class="dismiss">
						<a href="#message-error"></a>
					</div>
				</div>
			</div>
		
		</c:if>
				
		<form id="formVeiculo" action="${contexto}/relatorios/viatura/estado" method="post">
		<div class="form">
			<div class="fields">
						
				<div class="field">
					<div class="label">
						<label for="select">Formato:<div class="ajuda" title="Selecione o formato do relatório">?</div></label>
					</div>
					<div class="select">
						<input type="radio" checked="checked" value="pdf" name="formato">pdf
						<input type="radio" value="excel" name="formato">Excel
					</div>				
				</div>
				
				<div class="buttons">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="gerar" />
					</div>
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
				</div>	
			</div>
		</div>
		</form>
</div>


	</jsp:body>
</layout:controleGeral>