<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout"%>

<layout:controleGeral keywords="" description=""
	title=".On The Go.">

	<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>
	<jsp:attribute name="link_scripts">
	
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script>  
	
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 	
	<script type="text/javascript" src="../../../js/contemPonto.js"></script>
	<script type="text/javascript" src="../../../js/mapaCalor.js"></script>    	   
	

</jsp:attribute>
<jsp:attribute name="dialogs">

</jsp:attribute>
<jsp:attribute name="scripts">

<script type="text/javascript">

	var map = "";
	var urlApp = '${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.localPort}${pageContext.request.contextPath}/';
	
	$(document).ready(function() {
		
		var latlngMapa = new google.maps.LatLng(-3.735918, -38.525913);
		 var myOptions = {
			      zoom: 13,
			      center: latlngMapa,
			      mapTypeControl: false,
			      streetViewControl: false,
			      panControl: false,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
			    
		map = new google.maps.Map(document.getElementById("mapa"), myOptions);
        
	});
	
	var dataInicial = "";
	var dataFinal = "";
	
	function gerarCalor(){
		dataInicial = document.getElementById("dataInicio").value;
		dataFinal = document.getElementById("dataFim").value;
		
		criarMapaCalor();
	}
	
</script>

</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Relatório mapa de calor</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />

		<form id="formMapaCalor" action="" method="post">

		<div class="form">
		
			<input type="hidden" name="rota.id" value="${rota.id}" />
			<div id="geoPontos"></div>
			
			<div class="fields">
	
				<div class="field">
					<div class="label">
							<label for="select">Data:<div class="ajuda" title="Selecione a data inicial e final para definir o período da pesquisa">?</div></label>
						</div>
						
						<div class="input" style="margin-bottom: 10px;">
							<input id="dataInicio" name="dataInicio" value="${dataInicio}" type="text" style="float: none; display: inline; margin-right: 10px; " /> a 						
							<input id="dataFim" name="dataFim" value="${dataFim}" type="text" style="float: none; display: inline; margin-left: 10px; " />
						</div>
					</div>																								  		
				</div>
				
				<div class="field">
						<div class="label">
							<label for="select">Emergencia:<div class="ajuda" title="Selecione a emergencia que deverá ser criado o relatório">?</div></label>
						</div>
						<div class="select">				
							<div style="margin: 10px 0 0 0px;">
									
								<div id="nivelEmergenciaNome" style="display: inline;">
									<select id="select" name="ocorrencia.niveisEmergencia.id" onchange="selecionaNivelEmergencia()">
										
									</select>
								</div>
							</div>						
						</div>
					</div>
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="gerar Mapa" onclick="gerarCalor()" />
					</div>
				</div>
				<div id="mensagem-erro-rota" class="error" style="display: none;">Rota inválida</div>
				
				<div class="field">
									
					 <div id="mapa" style="height: 400px; padding: 7px;"></div>
					 			  				
				</div>
				
			</div>
		
		</form>				

	</div>
	
</jsp:body>

</layout:controleGeral>
