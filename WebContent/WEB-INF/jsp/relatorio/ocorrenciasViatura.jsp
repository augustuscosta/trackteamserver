<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout"%>

<layout:controleGeral keywords="" description=""
	title=".On The Go.">

	<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/jquery-autocomplete.css"
			rel="stylesheet" />
	<link type="text/css"
			href="../../../css/flick/jquery-ui-1.8.16.custom.css"
			rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>
	<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jDialogSelect/jDialogSelect.js"></script>  
	

</jsp:attribute>
	<jsp:attribute name="scripts">

	<script type="text/javascript">
		var listaViaturas = {};
		var listaNivelEmergencias = {};
		
		function getViaturas() {

			$.ajax({
				type : "POST",
				url : "../../../veiculo/listaVeiculos",
				success : function(resposta) {

					json = resposta.result;
					listaViaturasTemp = {};
					div = "";
					
					for (x in json) {
						
						listaViaturasTemp[json[x].id] = json[x].placa+ ' - '+ json[x].modelo; 
						listaViaturas[json[x].id] = json[x]; 
					}
					
					$('#selecionaViatura').jDialogSelect({
						lista: listaViaturasTemp,
						url: '${url}/js/jFoto/jDialogSelect'}, function(id) { selecionaViatura(id); });				
				}
			});
		}
		
		function getEmergencia() {

			$.ajax({
				type : "POST",
				url : "../../../relatorio/ocorrencia/veiculo/listaEmergencia",
				success : function(resposta) {

					json = resposta.result;
					listaNivelEmergenciaTemp = {};
					div = "";
					
					for (x in json) {
						
						listaNivelEmergenciaTemp[json[x].id] = json[x].id+ ' - '+ json[x].descricao; 
						listaNivelEmergencias[json[x].id] = json[x]; 
					}
					
					var sec=document.getElementById("select");
					
					for(y in listaNivelEmergencias){
						var option=document.createElement("option");
						option.text=listaNivelEmergencias[y].descricao;
						option.value=listaNivelEmergencias[y].id;
					try
					  {
					 	sec.add(option,sec.options[null]);
					  }
					catch (e)
					  {
						sec.add(option,null);
					  }
					}
					
					
					$('#nivelEmergenciaId').val(listaNivelEmergencias[1].id);
				}
			});
		}
		
		
		function selecionaViatura(id) {

			$('#ViaturaNome').html(listaViaturas[id].placa + ' - ' + listaViaturas[id].modelo + '<img style="margin-left: 20px; cursor: pointer;" onclick="zeraSelecao();" src="../../../images/ico_excluir.gif" />');
			$('#viaturaId').val(id);
			$('#addviatura').hide(300);
		}
		
		function zeraSelecao() {
			$('#ViaturaNome').html('');
			$('#viaturaCampo').val('');
			$('#addviatura').show();
		}
		
		function selecionaNivelEmergencia() {
			var cll=document.getElementById("select");
			$('#nivelEmergenciaId').val(cll.options[cll.selectedIndex].value);
			$('#addnivelEmergencia').hide(300);
		}
		
		function zeraSelecaoNivelEmergencia() {
			$('#nivelEmergenciaCampo').val('');
			$('#addnivelEmergencia').show();
		}
		
		var propriedadesDatePicker = {
			dateFormat: 'dd/mm/yy',
			dayNames: [
			'Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'
			],
			dayNamesMin: [
			'D','S','T','Q','Q','S','S','D'
			],
			dayNamesShort: [
			'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'
			],
			monthNames: [
			'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',
			'Outubro','Novembro','Dezembro'
			],
			monthNamesShort: [
			'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set',
			'Out','Nov','Dez'
			],
			nextText: 'Próximo',
			prevText: 'Anterior'
			 
		};
		
		$(document).ready(function() {;
			
			$("#dataInicio").datepicker(propriedadesDatePicker);
			$("#dataFim").datepicker(propriedadesDatePicker);
			
			getViaturas();
			getEmergencia();
		});
	</script>
</jsp:attribute>

<jsp:attribute name="dialogs">

</jsp:attribute>

	<jsp:body>
		
	<div class="box box-inline">
	
		<div class="title">
			<h5>Gerar relatório - Atividades por veículos</h5>
		</div>

		<c:if test="${not empty errors}">
		
			<br />
			<div class="messages">
				<div id="message-error" class="message message-error">
					<div class="image">
						<img height="32" alt="Error" src="../../../images/icones/error.png" />
					</div>
					<div class="text">
						<h6>Ocorrerão os seguintes erros</h6>
					    <c:forEach var="error" items="${errors}">   
					          <span>${error.message}</span>
					    </c:forEach>  		
					</div>
					<div class="dismiss">
						<a href="#message-error"></a>
					</div>
				</div>
			</div>
		
		</c:if>
				
		<form id="formVeiculo" action="${url}/relatorio/ocorrencia/veiculo" method="post">
		<div class="form">
			<div class="fields">
						
					<input type="hidden" id="viaturaId" name="viaturaId" value=""/>
					<input type="hidden" id="nivelEmergenciaId" name="nivelEmergenciaId" value=""/>

					<div class="field">
						<div class="label">
							<label for="select">Data:<div class="ajuda" title="Selecione a data inicial e final para definir o período da pesquisa">?</div></label>
						</div>
						
						<div class="input" style="margin-bottom: 10px;">
							<input id="dataInicio" name="dataInicio" value="${dataInicio}" type="text" style="float: none; display: inline; margin-right: 10px; " /> a 						
							<input id="dataFim" name="dataFim" value="${dataFim}" type="text" style="float: none; display: inline; margin-left: 10px; " />
						</div>
											
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Emergencia:<div class="ajuda" title="Selecione a emergencia que deverá ser criado o relatório">?</div></label>
						</div>
						<div class="select">				
							<div style="margin: 10px 0 0 0px;">
									
								<div id="nivelEmergenciaNome" style="display: inline;">
									<select id="select" name="ocorrencia.niveisEmergencia.id" onchange="selecionaNivelEmergencia()">
										
									</select>
								</div>
							</div>						
						</div>
					</div>
										
					<div class="field">
						<div class="label">
							<label for="select">Viatura:<div class="ajuda" title="Selecione a viatura que deverá ser criado o relatório">?</div></label>
						</div>
						<div class="select">				
							<div style="margin: 10px 0 0 0px;">
									
								<div id="ViaturaNome" style="display: inline;">
									<c:if test="${veiculo.cerca.id > 0}"> ${veiculo.cerca.nome}
										<img style="margin-left: 20px; cursor: pointer;" onclick="zeraCerca();" src="../../../images/ico_excluir.gif" />
									</c:if>
								</div>
								<div id="addviatura">
									<a href="javascript:;" id="selecionaViatura">selecione a Viatura</a><br /><br />
								</div>
							</div>						
						</div>
					</div>

					<div class="field">
					<div class="label">
							<label for="select">Status:<div class="ajuda" title="Selecione o estado da atividade que deverá ser criado o relatório">?</div></label>
						</div>
						<div class="select">
							<input type="radio" checked="checked" value="todos" name="status">Todos
							<input type="radio" value="aberta" name="status">Aberta
							<input type="radio" value="encerrada" name="status">Encerrada
							
						</div>				
					</div>
					
					<div class="field">
					<div class="label">
							<label for="select">Formato:<div class="ajuda" title="Selecione o formato do relatório">?</div></label>
						</div>
						<div class="select">
							<input type="radio" checked="checked" value="pdf" name="formato">pdf
							<input type="radio" value="excel" name="formato">Excel
						</div>				
					</div>
			<div class="buttons">
				<div class="highlight">
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="gerar" />
				</div>
				<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
			</div>	
			</div>

		</div>
		</form>
</div>


	</jsp:body>
</layout:controleGeral>