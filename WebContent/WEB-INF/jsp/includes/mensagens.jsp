<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
   
<c:if test="${not empty errors}">

	<br />
	<div id="mensagem-erro" class="messages">
		<div id="message-error" class="message message-error">
			<div class="image">
				<img height="32" alt="Error" src="<%=request.getContextPath()%>/images/icones/error.png" />
			</div>
			<div class="text">
				<h4>Ocorreram os seguintes erros</h4>
			    <c:forEach var="error" items="${errors}">   
			          <span>${error.message}</span>
			    </c:forEach>  		
			</div>
			<div class="dismiss">
				<a href="#message-error"></a>
			</div>
		</div>
	</div>

</c:if>

<c:if test="${empty errors}">

	<br />
	<div id="mensagem-erro" class="messages" style="display: none;">
		<div id="message-error" class="message message-error">
			<div class="image">
				<img height="32" alt="Error" src="<%=request.getContextPath()%>/images/icones/error.png" />
			</div>
			<div class="text"><h4></h4></div>
		</div>
	</div>
	
</c:if>
	
<c:if test="${not empty avisos}">

	<br />
	<div id="mensagem-sucesso" class="messages">
		<div id="message-success" class="message message-success">
			<div class="image">
				<img height="32" alt="Error" src="<%=request.getContextPath()%>/images/icones/success.png" />
			</div>
			<div class="text">
				<h4>${avisos.titulo}</h4>
			    <c:forEach var="aviso" items="${avisos.mensagens}">   
			          <span>${aviso.mensagem}</span>
			    </c:forEach>  		
			</div>
		</div>
	</div>

</c:if>

<c:if test="${empty avisos}">

	<br />
	<div id="mensagem-sucesso" class="messages" style="display: none;">
		<div id="message-success" class="message message-success">
			<div class="image">
				<img height="32" alt="Error" src="<%=request.getContextPath()%>/images/icones/success.png" />
			</div>
			<div class="text"><h4></h4></div>
		</div>
	</div>
	
</c:if>