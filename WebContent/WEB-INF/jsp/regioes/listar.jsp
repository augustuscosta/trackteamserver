<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	function removeRegiao(id, nome) {
		
		var answer = confirm("Deseja realmente excluir a Região " + nome + " ?");
		if (answer) {			
			$.post( "../excluir/", { 'id': id },
			   		function( resposta ) {

						resposta = eval('(' + resposta + ')');       

						if (resposta.status == 'error') {
							
						} else {
						
							$('#message-success-senha').find('h6').html(resposta.msgSucesso);
							$('#message-success-senha').show();	
							$('#mensagem-erro-senha').hide();	
							$('#dialogSenha').dialog('close');		
						
							$(window).trigger('resize');
						}
					});
		}
	}

	$(document).ready(function() {
/*
		<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${agente.id}",
			"matricula": "${agente.usuario.matricula}",
			"nome": "${agente.usuario.nome}",
			"login": "${agente.usuario.login}",
			"status": "${agente.usuario.status.valor}",	
			"grupo": "${agente.usuario.grupo.nome}"
			}
*/		
		$("#lista").jTable({
			url: '../../../controle_geral/regiao/listaRegioes/',
			cabecalho: [
				{'titulo': 'id', 'width': 'auto', 'align': 'left', 'campo': 'id' },        
				{'titulo': 'Nome', 'width': 'auto', 'align': 'left', 'campo': 'nome' }				
			],
			opcoesTitulo: 'Opções',
			editar: true,
			remover: true,
			linkEditar: '../../../controle_geral/regioes/editarRegiao/',
			funcaoRemover: 'removeRegiao',
			campoRef: 'id',
			campoTituloRef: 'nome',
			iconEditar: '../../../images/ico_alterar.png',
			iconExcluir: '../../../images/ico_excluir.png'
		});
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Lista regiões</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		<div class="messages">
			<div id="message-success-senha" class="message message-success" style="display: none;">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/success.png" />
				</div>
				<div class="text">
					<h6></h6>
				</div>
			</div>
		</div>		
		
		<div class="table">
		
<div id="lista"></div>
		</div>
	</div>
	
</jsp:body>

</layout:controleGeral>
