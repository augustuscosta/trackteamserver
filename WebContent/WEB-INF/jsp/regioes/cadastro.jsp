<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
 
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />
	
</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script>  
	<script type="text/javascript" src="../../../js/gmap3.js"></script> 
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 	
	
</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');

	var mapAddRegiao;
	var path = new Array();
	var markers = [];
	
	var poly = new google.maps.Polygon({
	    fillColor: '#FF0000',
	    strokeOpacity: 0.3,
	    strokeColor: "#FF0000",
	    strokeWeight: 2,
	    fillOpacity: 0.05
	});
	
	var imagePontoRegiao = new google.maps.MarkerImage(
		"../../../images/square_transparent.png",
		new google.maps.Size(11, 11),
		new google.maps.Point(0, 0),
		new google.maps.Point(6, 6)
	);
		
	function inicio() {
		
		parent.location='../listar/';
	}

	$("#mapaAddRegiao").ready( function() { 
		
		var myOptions = {
			      zoom: 14,
			      center: new google.maps.LatLng(latlngMapa.lat(), latlngMapa.lng()),
			      mapTypeControl: false,
			      streetViewControl: false,
			      panControl: false,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
			    
		mapAddRegiao = new google.maps.Map(document.getElementById("mapaAddRegiao"), myOptions);
		
		mapRef = mapAddRegiao;
		   
		$('#mapaAddRegiao').append('<input id="enderecoRegiao" type="text" style="background-color: #FFF;" />');
	
	  	poly.setMap(mapAddRegiao);
	    poly.setPaths(path);
	    
	    google.maps.event.addListener(mapAddRegiao, "click", function(event) {
	    	
	    	addPoint(event);
			addPontos();
		});
		
		$('#enderecoRegiao').autocomplete({
			source: function() {
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode( { 'address': $(this).val()}, function(results, status) {
				    if (status == google.maps.GeocoderStatus.OK) {
				    	if (!results) return;
				    	$('#enderecoRegiao').autocomplete('display', results, false);
				    }
				  });
			},
			cb:{
				cast: function(item){
				  	return item.formatted_address;
				},
				select: function(item) {				
					mapAddRegiao.setCenter(item.geometry.location);				
				}
		 	}
		});
	}); 

	function addPoint(event) {
	  	
		path.push(event.latLng);
			
		var marker = new google.maps.Marker({
		  position: event.latLng,
		  map: mapAddRegiao,
		  draggable: true,
		  icon: imagePontoRegiao
		});
		markers.push(marker);
		marker.setTitle("#" + path.length);
		
		google.maps.event.addListener(marker, 'click', function() {
		 
			marker.setMap(null);
			for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
			markers.splice(i, 1);
			path.pop(i);
			addPontos();
		
		});

		google.maps.event.addListener(marker, 'dragend', function() {
		 
			//for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
			//path.setAt(i, marker.getPosition());    
			addPontos();
		
		});
	}

	function addPontos() {
		
		regiao = "";
		
	    for (var i = 0; i < markers.length; ++i) {
			posicao = path[i];
//			cerca = cerca + posicao.lat() + "," + posicao.lng() + "; ";		
			regiao = regiao + '<input type="hidden" name="pontos[]" value="' + posicao.lat() + "," + posicao.lng() + '" />';
		}

	    if (markers.length > 0) {
	    	posicao = path[0];
//			cerca = cerca + posicao.lat() + "," + posicao.lng() + "; ";		
//			cerca = cerca + '<input type="hidden" name="pontos[]" value="' + posicao.lat() + "," + posicao.lng() + '" />';
			regiao = regiao + '<input type="hidden" name="passou" value="true" validate="vazio" />';
	    } else {
	    	
	    	regiao = '<input type="hidden" name="passou" value="" validate="vazio" />';
	    }		  

		$('#regiao').html(regiao);
			
	}
	
	function addRegiao(form) {
		
		return true;	
	}

	function limparRegiao() {
		
		path = new Array();
	    poly.setPaths(path);

	    for(var i=0; i<markers.length; i++) {
	    
	    	markers[i].setMap(null);
	    }
	    
	    markers = [];	    
	}
	
	$(document).ready(function() {

		$("#formAddRegiao").jFotoForm({ajax: false}, function () {
			return addRegiao(this);
		});
		
<c:if test="${not empty regiao.geoPonto}">

	var limites = new google.maps.LatLngBounds();
<c:forEach var="ponto" items="${regiao.geoPonto}">

	var elem = Object();
	var lat = ${ponto.latitude};
	var lng = ${ponto.longitude};
	elem.latLng = new google.maps.LatLng(lat, lng);		
	addPoint(elem);
	limites.extend(elem.latLng);
	
</c:forEach>

	mapAddRegiao.setCenter(limites.getCenter());
	addPontos();
</c:if>	
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>${regioes.id == null ? 'Adicionar' : 'Alterar'} regiões</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />

		<form id="formAddRegiao" action="../../../controle_geral/regioes/editar/" method="post">
		
		<div class="form">
			<div class="fields">
					
				<div class="field">
	
					<input name="id" class="medium" type="hidden" value="${regioes.id}" />
					<div class="label">
						<label for="input-medium">Nome<b style="color: #FF0000; font-size: 14px;"> * </b>:</label>
					</div>
										
					<div class="input" style="margin-bottom: 10px;">
						<div class="ajuda" style="float:right; margin-top: 5px;" title="Nome da regiao">?</div>
						<input name="nome" class="medium" type="text" value="${regioes.nome}" style="background-color: #FFF;" validate="vazio" tmMinimo="5" />						
						<div id="errorAddRegiao" class="error"></div>
						<div id="regiao">
							<input type="hidden" name="passou" value="" validate="vazio" msgErro="nenhuma regiao definida"/>						
						</div>											
					</div>						  		
				</div>

				<div class="field">					
					<div id="mapaDivAddRegiao" style="">
						<div id="mapaAddRegiao" style="height: 400px; padding: 7px;"></div>
					</div>			  				
				</div>
				
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${regioes.id == null ? 'adicionar' : 'alterar'}" />
					</div>
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="limpar" onclick="limparRegiao();" />
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />
				</div>
				
			</div>
		</div>
		
		</form>
				

	</div>
	
</jsp:body>

</layout:controleGeral>
