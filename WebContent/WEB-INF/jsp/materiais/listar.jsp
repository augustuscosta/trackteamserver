<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	function removeMateriais(id, nome) {
		
		var answer = confirm("Deseja realmente excluir o material " + nome + " ?");
		if (answer) {
			
			alert('apagou');
		}
	}

	$(document).ready(function() {

		$("#lista").jTable({
			url: '../../../controle_geral/materiais/listaMateriais/',
			cabecalho: [
				{'titulo': 'ID', 'width': 'auto', 'align': 'left', 'campo': 'id' },				
				{'titulo': 'Titulo', 'width': 'auto', 'align': 'left', 'campo': 'titulo' },
				{'titulo': 'Funcionalidade', 'width': 'auto', 'align': 'left', 'campo': 'funcionalidade' }
			],
			opcoesTitulo: 'Opções',
			editar: true,
			remover: true,
			linkEditar: '../../../controle_geral/materiais/editar/',
			funcaoRemover: 'removeMateriais',
			campoRef: 'id',
			campoTituloRef: 'titulo',
			iconEditar: '../../../images/ico_alterar.png',
			iconExcluir: '../../../images/ico_excluir.png'
		});
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Lista materiais</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		
		<div class="table">
<div id="lista"></div>
		</div>
	</div>
	
</jsp:body>

</layout:controleGeral>
