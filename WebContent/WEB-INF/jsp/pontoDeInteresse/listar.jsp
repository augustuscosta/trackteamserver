<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	function removePontoDeInteresse(id, nome) {
		
		var answer = confirm("Deseja realmente excluir o Ponto de interesse " + nome + " ?");
		if (answer) {
			
			$.post( "../excluir/", { 'id': id },
			   		function( resposta ) {

						resposta = eval('(' + resposta + ')');       

						if (resposta.status == 'error') {
							
						} else {
						
							$('#message-success-senha').find('h6').html(resposta.msgSucesso);
							$('#message-success-senha').show();	
							$('#mensagem-erro-senha').hide();
							
							$('#lista').refresh();
							
							$(window).trigger('resize');							
						}
					});		

		}
	}

	$(document).ready(function() {
	
		$("#lista").jTable({
			url: '../../../controle_geral/pontos_de_interesse/listaPontos/',
			cabecalho: [
						{'titulo': 'Nome', 'width': 'auto', 'align': 'left', 'campo': 'nome' },        
						{'titulo': 'Telefone', 'width': 'auto', 'align': 'left', 'campo': 'telefone' },
						{'titulo': 'Email', 'width': 'auto', 'align': 'left', 'campo': 'email' },
						{'titulo': 'Contato', 'width': 'auto', 'align': 'left', 'campo': 'contato' }
					],
			opcoesTitulo: 'Opções',
			campoRef: 'id',
			campoTituloRef: 'nome',
			opcoes: [{'icon' : '../../../images/ico_materiais.png', 'link':'../../../controle_geral/pontos_de_interesse/materiais/','funcao': '', 'alt':'materiais', 'params': ['id']},
			         {'icon' : '../../../images/ico_alterar.png', 'link':'../../../controle_geral/pontos_de_interesse/editar/','funcao': '', 'alt':'editar', 'params': ['id']},
			         {'icon' : '../../../images/ico_excluir.png', 'link':'','funcao': 'removePontoDeInteresse', 'ajax':'true', 'alt':'excluir'}]			
		});	
		
		$('#formImportPontos').submit(function() {
			return false;
		});	
		
		$("#dialogImport").dialog({ minWidth: 900, autoOpen: false, resizable: false });
	});
	
	function closeImport() {
		$("#dialogImport").dialog('close');
	}
	
	function importarDialog() {
		$("#dialogImport").dialog('open');
	}
	
	function readfile(f) {
	    var reader = new FileReader();  // Create a FileReader object
	    reader.readAsText(f, 'UTF-8'); // Read the file
	    reader.onload = function() {    // Define an event handler
	    	$.post( "../importar", { lista: reader.result})
	    	  .done(function( data ) {
	    		    alert( "Sucesso na importação");
	    		    location.reload();
	    	   }).fail(function() {
	    		alert( "Erro na importação" );
	    	});
	    }
	    reader.onerror = function(e) {  // If anything goes wrong
	        alert("Erro lendo o arquivo");
	    };
	}

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

<div id="dialogImport" title="Importar pontos de interesse">
	
		<form id="formImportPontos" action="">
	
			<div class="form">
				<div class="fields">
						
					<div class="field">
						
						<div class="label">
							<label for="input-medium">Arquivo:</label>
						</div>
						
						<div class="input">
							<input id="arquivo" onchange="readfile(this.files[0]);" type="file" style="background-color: #FFF;" />							  		
						</div>
						
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="cancelar" onclick="closeImport();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>
	
</jsp:attribute>

<jsp:body>

	
	<div class="box box-inline">
	
		
		
		<div class="title">
			<h5>Lista pontos de interesse</h5>
			<ul class="links">
				<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
				<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="Importar" onclick="importarDialog();" />
				</li>
		    </ul>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		
		<div class="table">
			<div id="lista"></div>
		</div>
	</div>
	
</jsp:body>

</layout:controleGeral>
