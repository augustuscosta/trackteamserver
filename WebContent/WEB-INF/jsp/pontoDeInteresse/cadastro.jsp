<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/style.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/ccpb.css" rel="stylesheet" />
<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false"></script>
	<script type="text/javascript" src="../../../js/gmap3.js"></script> 
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script>
	<script type="text/javascript" src="../../../js/jquery.maskedinput.js"></script> 
	
</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">
		
		var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');
		var regioesUsuario = ${regioes};
		regioesUsuario = regioesUsuario.list;
		var endereco = new Object();
		endereco.latitude = latlngMapa.lat();
		endereco.longitude = latlngMapa.lng();
		
		var imageOcorrencia = new google.maps.MarkerImage(
				"../../../images/icones_mapa/ocorrencia_amarelo.png",
				new google.maps.Size(78,80),
				new google.maps.Point(0, 0),
				new google.maps.Point(32,74)
			);
		
		var map = "";
		var marcador = "";
		
		function inicio() {
			
			parent.location='../../../controle_geral/pontos_de_interesse/listar/';
		}
		
		$(document).ready( function() {
			$('.ajuda').tipTip({maxWidth: "auto", edgeOffset: 10});

			$("#formPontoDeInteresse").jFotoForm();
			
			$("#dialogMapa").dialog({ minWidth: 900, autoOpen: false, resizable: false });	
			
			$('#formAddEndereco').submit(function() {
				return false;
			});	
			
			<c:if test="${not empty ponto.id}">
			
				endereco.latitude = '${ponto.endereco.latitude}';
				endereco.longitude = '${ponto.endereco.longitude}';
	
			</c:if>
			
			$('#endereco').keypress(function(event) {
				
				if(event.keyCode==13) { 
					event.keyCode=0; 
					event.returnValue=false; 
					return false; 
				}
			});

			$(window).trigger('resize');

		});
		
		function showMap() {
			
			if (isNaN($('#enderecoLat').val()) && isNaN($('#enderecoLng').val()) && $('input[name="ocorrencia.id"]').val() != "") {
				
				var latLng = new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val());
				
				if (marcador == null || marcador == "") {
					
					marcador = new google.maps.Marker({
					    position: latLng,
					    map: map,
					    icon: imageOcorrencia,
					    draggable: true
					});

					google.maps.event.addListener(marcador, 'dragend', function(event) {
						findAddress(event.latLng);		
					});
					
				} else {
					marcador.setPosition(latLng);
				}
		
				findAddress(latLng);
				map.panTo(new google.maps.LatLng(parseFloat($('#enderecoLat').val()), parseFloat($('#enderecoLng').val())));
				
			} else {

				map.panTo(latlngMapa);	
				map.setZoom(13);
			}
			
			$("#dialogMapa").dialog('open');
			google.maps.event.trigger(map, 'resize');

		}
		
		function findAddress(latLng) {
			var geocoder = new google.maps.Geocoder();
		    geocoder.geocode({latLng: latLng}, function(results, status) {
		    	if (status == google.maps.GeocoderStatus.OK) {
		        	if (results[0]) {
		          		$('#endereco').val(results[0].formatted_address); 
		          		endereco.numero = results[0].address_components[0].long_name;          		
		          		endereco.logradouro = results[0].address_components[1].long_name;
		          		endereco.bairro = results[0].address_components[2].long_name;    
		          		endereco.latitude = latLng.lat();
		          		endereco.longitude = latLng.lng();
		        	}
		    	}
		    });
		}
		
		function atualizaMarcador(latLng) {
			
			if (marcador == null || marcador == "") {
				marcador = new google.maps.Marker({
				    position: latLng,
				    map: map,
				    icon: imageOcorrencia,
				    draggable: true
				});
				
				google.maps.event.addListener(marcador, 'dragend', function(event) {
					
					findAddress(event.latLng);		
				});
				
			} else {
				
				marcador.setPosition(latLng);
			}
			
			findAddress(latLng);
		}
		
		$("#mapa").ready( function() { 
			
			var myOptions = {
				      zoom: 14,
				      center: new google.maps.LatLng(latlngMapa.lat(), latlngMapa.lng()),
				      mapTypeControl: false,
				      streetViewControl: false,
				      panControl: false,
				      mapTypeId: google.maps.MapTypeId.ROADMAP
				    };
				    
			map = new google.maps.Map(document.getElementById("mapa"), myOptions);
			
			google.maps.event.addListener(map, 'dblclick', function(event) {
				atualizaMarcador(event.latLng);
			});
			
			$('#endereco').autocomplete({
				source: function() {
					var geocoder = new google.maps.Geocoder();
					geocoder.geocode( { 'address': $(this).val()}, function(results, status) {
					    if (status == google.maps.GeocoderStatus.OK) {
					    	if (!results) return;
					    	$('#endereco').autocomplete('display', results, false);
					    }
					  });
				},
				cb:{
					cast: function(item){
					  return item.formatted_address;
					},
					select: function(item) {
						
						atualizaMarcador(item.geometry.location);
						map.setCenter(item.geometry.location);
						
					}
			 	}
			});
		}); 
		
		function closeMap() {
			$("#dialogMapa").dialog('close');
		}
		
		function addEndereco() {
			
			$('#enderecoGeo').val($('#endereco').val());
			
			$('#enderecoNumero').val(endereco.numero);
			$('#enderecoRua').val(endereco.logradouro);
			$('#enderecoLat').val(endereco.latitude);
			$('#enderecoLng').val(endereco.longitude);
			$('#enderecoBairro').val(endereco.bairro);
		  		
			$("#dialogMapa").dialog('close');	
			return false;
		}
		
		$('#endereco').ready( function() {
			
			$('#enderecoGeo').focus( function() {
				
				showMap();											
			});	
		});
		
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

<div id="dialogMapa" title="Selecionar endereço">
	
		<form id="formAddEndereco" action="">
	
			<div class="form">
				<div class="fields">
						
					<div class="field">
						<div class="label">
							<label for="input-medium">Endereço:</label>
						</div>
						<div class="input">
							<input id="endereco" type="text" style="background-color: #FFF;" />							  		
						</div>
						
						<div id="mapaDiv">
							<div id="mapa" style="height: 100%; padding: 7px;"></div>
						</div>			  				
	
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar" onclick="addEndereco();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="cancelar" onclick="closeMap();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>
	
</jsp:attribute>

<jsp:body>

	<div id="conteudo" class="box box-inline">
		
		<div class="title">
			<h5>Cadastrar ponto de interesse</h5>
		</div>

		<jsp:include page="../includes/mensagens.jsp"/>

		<div class="messages">
			<div id="message-success-senha" class="message message-success" style="display: none;">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/success.png" />
				</div>
				<div class="text">
					<h6></h6>
				</div>
			</div>
		</div>
		
		<form id="formPontoDeInteresse" action="../../../controle_geral/pontos_de_interesse/adicionar/" method="post">
			
			<input type="hidden" name="ponto.id" value="${ponto.id}"/>
			
			<div class="form">
				<div class="fields">
				
					<div class="field">
						<div class="label">
							<label for="input-medium">Endereço:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Endereço do ponto de interesse">?</div></label>
						</div>
						<div class="input">
							<input id="enderecoGeo" class="medium${erroEndereco eq true ? ' error' : ''}" type="text" name="ponto.endereco.endGeoref" value="${ponto.endereco.endGeoref}" />
		    				<img class="bnt_mapa" src="../../../images/icones/map.png" width="28px;" height="28px;" onclick="showMap();" />
		    				<input type="hidden" id="enderecoId" name="ponto.endereco.id" value="${ponto.endereco.id}" />
		    				<input type="hidden" id="enderecoNumero" name="ponto.endereco.numero" value="${ponto.endereco.numero}" />
		    				<input type="hidden" id="enderecoRua" name="ponto.endereco.logradouro" value="${ponto.endereco.logradouro}" validate="vazio" msgErro="endereço inválido" divDestaque="enderecoGeo" />
		    				<input type="hidden" id="enderecoLat" name="ponto.endereco.latitude" value="${ponto.endereco.latitude}" />
		    				<input type="hidden" id="enderecoLng" name="ponto.endereco.longitude" value="${ponto.endereco.longitude}" />	    				
		    				<input type="hidden" id="enderecoBairro" name="ponto.endereco.bairro" value="${ponto.endereco.bairro}" />
						</div>					
					</div>
								
					<div class="field">
						<div class="label">
							<label for="input-medium">Nome:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Nome do ponto de interesse">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroNome eq true ? ' error' : ''}" type="text" name="ponto.nome" value="${ponto.nome}" validate="vazio"/>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Código:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Código do ponto de interesse">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroCodigo eq true ? ' error' : ''}" type="text" name="ponto.codigo" value="${ponto.codigo}" validate="vazio"/>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Email:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Email do ponto de interesse">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroEmail eq true ? ' error' : ''}" type="text" name="ponto.email" value="${ponto.email}" validate="vazio"/>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Telefone:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Telefone do ponto de interesse">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroTelefone eq true ? ' error' : ''}" type="text" name="ponto.telefone" value="${ponto.telefone}" validate="vazio"/>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Contato:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Contato do ponto de interesse">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroContato eq true ? ' error' : ''}" type="text" name="ponto.contato" value="${ponto.contato}" validate="vazio"/>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Observação:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Observação do ponto de interesse">?</div></label>
						</div>
						<div class="">
							<textarea class="textarea-medium${erroObservacao eq true ? ' error' : ''}" name="ponto.observacao">${ponto.observacao}</textarea>
						</div>
					</div>
					
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${ponto.id == null ? 'adicionar' : 'alterar'}" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
