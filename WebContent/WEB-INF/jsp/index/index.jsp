<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<layout:pagina keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="./css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="./css/jquery.fancybox.css" />
		
</jsp:attribute>

<jsp:attribute name="link_scripts">
	
	<script src="./js/telacheia.js" type="text/javascript"></script>
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
	<script src="https://cdn.socket.io/socket.io-1.0.6.js"></script> 
	<!-- <script src="./js/socket.io.js" type="text/javascript"></script> -->
	
	<script src="./js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script> 
	
	<script src="./js/util.js" type="text/javascript"></script>
	<script src="./js/ocorrencia.js" type="text/javascript"></script>
	<script src="./js/veiculo.js" type="text/javascript"></script>
	<script src="./js/agente.js" type="text/javascript"></script>
	<script src="./js/camera.js" type="text/javascript"></script>
	<script src="./js/notificacao.js" type="text/javascript"></script>
	<script type="text/javascript" src="./js/jFoto/jFotoForm.js"></script> 	
	<script type="text/javascript" src="./js/contemPonto.js"></script>
	<script type="text/javascript" src="./js/mapaCalor.js"></script>    
	<script type="text/javascript" src="./js/jquery.fancybox.js"> </script>
		
</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">

		var urlApp = '${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.localPort}${pageContext.request.contextPath}/';
		var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');
		var zoomMapa = 11;
		
		var regioes = [];
		
<c:forEach var="regiao" items="${usuarioSession.regioes}">
		var regiao = Object();
		regiao.nome = '${regiao.nome}';
		regiao.lat = '${regiao.centro.latitude}';
		regiao.lng = '${regiao.centro.longitude}';		
		regioes.push(regiao);
		
</c:forEach>
		
		var regioesUsuario = ${regioes};
		regioesUsuario = regioesUsuario.list;
		
		var verificaSessao = window.setInterval(verificaSessao, 60000);

		var socket = new Object();
		var usuario = Object();
		var map = "";
		var isFastview = false;
		var isMapaFull = false;
		var filtro = new Object();
		var listaFiltros = {};
		var listaLigacoes = [];
		
		var listaSalaChat = {};
		var salaAtual = '0';
		
		var legendaAtiva = false;
		var aba_atual = "ocorrencias_tab";		
		var filtroMapaCalor = false;
		var tempoEspera = 5;
		
		var limparAtual = new Object();
		limparAtual.clean = function() { };

		var poly;
		var cercaPath = new Array();
		var cerca = new google.maps.Polygon({
		    fillColor: '#FF0000',
		    strokeOpacity: 0.3,
		    strokeColor: "#FF0000",
		    strokeWeight: 2,
		    fillOpacity: 0.05
		});

		var rotaPath = new Array();
		var rota = new google.maps.Polyline({
		    strokeOpacity: 0.4,
		    strokeColor: "#0000FF",
		    strokeWeight: 5
		});

		var directionsService = new google.maps.DirectionsService();
		var directionsDisplay = new google.maps.DirectionsRenderer();
		var rotaVeiculoOcorrencia = "";

		var borda_preta = new google.maps.MarkerImage(
				"images/icones_mapa/borda_preta.png",
				new google.maps.Size(80,93),
				new google.maps.Point(0, 0),
				new google.maps.Point(40,82)
			);
		
		usuario.id = "${usuarioSession.id}";
		usuario.nome = "${usuarioSession.nome}";
		
		function verificaSessao() {
			
			$.ajax({
			        type: "GET",
			        url: './verifica_login/',
			        success: function( resposta )
			        {
			        	json = resposta.result;
			        	
						if ( json.id == null || json.nome == null) {
							
							alert("Sua sessão expirou, faça o login novamente");
						}
			        },
			        error:function(XMLHttpRequest, textStatus, errorThrown)
			        {
			        	console.log("algo deu errado");
			        }
			});
		}
		
		$("#mapa").ready(function(){
			
			
//		    var latlngMapa = new google.maps.LatLng(-3.8,-38.457275);
//			var latlngMapa = new google.maps.LatLng(-23.1975,-45.8686);
			
		    var myOptions = {
		      zoom: zoomMapa,
		      center: latlngMapa,
		      mapTypeControl: false,
		      streetViewControl: false,
		      panControl: false,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    };
		    
		    map = new google.maps.Map(document.getElementById("mapa"), myOptions);

		  	cerca.setMap(map);
		  	cerca.setPaths(cercaPath);

		  	rota.setMap(map);
		  	rota.setPath(rotaPath);

			directionsDisplay.setOptions({draggable: true, preserveViewport: true});
			directionsDisplay.setMap(null);
			
			google.maps.event.addListener(map, 'click', function(event) {
		
				limparAtual.clean();
				hideNotificacao();
			});
			
			google.maps.event.addListener(map, 'idle', function() {		
				if(filtroMapaCalor)
					criarMapaCalor();
			});
						
			$("#mapa").append('<div id="maxmin" onclick="return mapaFull();"></div>');
			$("#mapa").append('<div id="logoFotoMapa" onclick="return mapaFull();"></div>');
			$("#mapa").append('<div id="fastview_maxmin_shadow" class="fastview_maxmin_shadow"></div>');
			$("#mapa").append('<div id="fastview_maxmin" class="fastview_maxmin" onclick="return fastview();"></div>');
			$("#mapa").append('<div id="home_mapa" class="home_mapa"><a href="javascript:;" onclick="goHome();"><img src="./images/icones/button_home_mapa.png" /></a></div>');
			$("#mapa").append('<div id="notificacao"><a href="javascript:;" onclick="showNotificacao()"><ul id="notificacoes_push"></ul><h2>notificações</h2></a><ul id="notificacoes"></ul></div>');
			
			todaATela();
			iniciaFiltros();
			
			ligacaoVeiculoOcorrencia.setMap(map);
			                      
		});
		
		function goHome() {			
			centralizaMapaNaRegiao();
		}
		
		function fastview() {
		
			if (isFastview) {	
		    	
		    	$("#fastview").animate({'margin-left': '-300'}, 300, function() { 	
		    		google.maps.event.trigger(map, 'resize'); 
		    	});
		      	$(".fastview_maxmin").css({"background-position": "-282px -194px"});
		      	
		      	isFastview = false;
		      	
			} else {
		
		    	$("#fastview").animate({'margin-left': '0'}, 300, function() { 	
		    		google.maps.event.trigger(map, 'resize'); 
		    	});
		      	$(".fastview_maxmin").css({"background-position": "-308px -194px"});
		      	isFastview = true;
			}					
		}
		
		function mapaFull() {
		
			if (isMapaFull) {
		
				isMapaFull = false;
				$("#content").css({"position": "relative", "width": "98%", "border": "3px solid #FFF"});
		      	$("#maxmin").css({"background-position": "0px 0px"});	
//		      	$('#home_mapa').show();
										
			} else {
		
				isMapaFull = true;	
				$("#content").css("position", "absolute");
				$("#content").css("width", "100%");			
				$("#content").css("border", "none");		
		      	$("#maxmin").css({"background-position": "-44px 0"});
//		      	$('#home_mapa').show();
			}	
		
			todaATela();					
		}
		
		function filtrosLegendas() {
		
			if (legendaAtiva) {
				apagaLegendas();
				legendaAtiva = false;
				
			} else {
				
				legendaAtiva = true;
				exibeLegendas();
			}
		}
		
		function apagaLegendas() {
			
			$(document).find('.abaLegendas').hide();
			$('#legendaMapaCalor').hide();
		}
		
		function exibeLegendas() {

			if (legendaAtiva) {
				
				apagaLegendas();
				
				if (filtroMapaCalor) {
					$('#legendaMapaCalor').show();				
				} else {				
					$('#' + aba_atual + '_legendas').show();
				}				
			}
		}
		
		function centralizaRegiao(id) {
			
			$('#tab_regioes').find('ul').find('li').removeClass('negrito');
			$('#regiaoSel-' + id).addClass('negrito');
			
			map.panTo(new google.maps.LatLng(regioes[id].lat,regioes[id].lng));
		}
		
		function insereRegioes() {
			
			var div = "";
			div += '<ul>';
			
			var classe = "";
			
			for(var i = 0; i < regioes.length; i++) {
				
				if (i==0) {
					classe = ' class="negrito"';
				} else {
					classe = "";
				}
			
				div += '<li' + classe + ' id="regiaoSel-' + i + '"><a href="javascript:;" onclick="centralizaRegiao(' + i + ')">' + regioes[i].nome + '</a></li>';
			}
			
			div += '</ul>';
			$('#tab_regioes').append(div);

		}
		
		function iniciaFiltros() {
		
			$("#mapa").append('<div id="divFiltros"></div>');
			
			$("#divFiltros").append('<div id="tabRegioes" title="Selecionar uma região" onclick="$(\'#tab_regioes\').toggle()">Regiões</div>');
			$("#divFiltros").append('<div id="tab_regioes"></div>');

			$("#divFiltros").append('<div id="tabFiltros" title="Exibir filtros" onclick="$(\'#filtrosMap\').toggle()">Filtros</div>');
			$("#divFiltros").append('<div id="filtrosMap"></div>');
		
			$("#divFiltros").append('<div id="tabLegendas" title="Exibe legendas" onclick="filtrosLegendas()">Legendas</div>');
			$("#divFiltros").append('<div id="legendaMapaCalor"></div>');
			$("#divFiltros").append('<div id="ocorrencias_tab_legendas" class="abaLegendas"></div>');
			$("#divFiltros").append('<div id="veiculos_tab_legendas" class="abaLegendas"></div>');
			$("#divFiltros").append('<div id="agentes_tab_legendas" class="abaLegendas"></div>');
			
			$('#filtrosMap').append('<input type="checkbox" checked="" name="filtros" value="agentes" onclick="ajustaFiltros(this);"> agentes<br>');
			$('#filtrosMap').append('<input type="checkbox" checked="" name="filtros" value="veiculos" onclick="ajustaFiltros(this);"> veiculos<br>');
			$('#filtrosMap').append('<input type="checkbox" name="filtros" value="veiculos-rota" onclick="ajustaFiltros(this);"> rota<br>');
			$('#filtrosMap').append('<input type="checkbox" name="filtros" value="veiculos-cerca" onclick="ajustaFiltros(this);"> cerca<br>');			
			$('#filtrosMap').append('<input type="checkbox" checked="" name="filtros" value="ocorrencias" onclick="ajustaFiltros(this);"> atividades<br>');
			$('#filtrosMap').append('<input type="checkbox" name="filtros" value="mapa-calor" onclick="ajustaFiltros(this);"> mapa térmico<br>');			
			
			legendaMapaCalor = '';
			legendaMapaCalor += '<table cellspacing="0">';
			legendaMapaCalor += '	<tbody>';
			legendaMapaCalor += '		<tr>';
			legendaMapaCalor += '			<td align="center" colspan="2" style="padding: 10px 20px 0 20px;"> <b id="tituloLegenda"> Densidade de <br />Atividades <br /><br /></b></td>';
			legendaMapaCalor += '	   	</tr>';
			legendaMapaCalor += '     		<tr align="left">';     		
			legendaMapaCalor += '			<td align="right"> <img src="./images/gradiente2.png" style="width:20px; height:6em; margin: 0 20px 0 20px;"></td><td align="left" id="valoresLegendaKernel">';		
			legendaMapaCalor += '				<table cellspacing="0" align="left" style="padding:4px; width:50px; cellpading="0">';
			legendaMapaCalor += '	     				<tbody>';
			legendaMapaCalor += '						<tr> <td> alta </td> </tr>';
			legendaMapaCalor += '		 				<tr> <td> &nbsp; </td></tr>';
			legendaMapaCalor += '	     					<tr> <td> média </td> </tr>';         			
			legendaMapaCalor += '						<tr> <td> &nbsp; </td></tr>';     				
			legendaMapaCalor += '						<tr> <td> baixa </td> </tr>';					
			legendaMapaCalor += '					</tbody>';
			legendaMapaCalor += '				</table>';     		
			legendaMapaCalor += '			</td>';         
			legendaMapaCalor += '		</tr>';		
			legendaMapaCalor += '	</tbody>';
			legendaMapaCalor += '</table>';
			
			$('#legendaMapaCalor').append(legendaMapaCalor);			
			insereRegioes();
			
			var filtro = new Object();
			filtro.nome = "agentes";
			filtro.ativo = true;
			listaFiltros["agentes"] = filtro;
		
			var filtro = new Object();
			filtro.nome = "veiculos";
			filtro.ativo = true;
			listaFiltros["veiculos"] = filtro;
		
			var filtro = new Object();
			filtro.nome = "ocorrencias";
			filtro.ativo = true;
			listaFiltros["ocorrencias"] = filtro;

			var filtro = new Object();
			filtro.nome = "veiculos-rota";
			filtro.ativo = false;
			listaFiltros["veiculos-rota"] = filtro;
			
			var filtro = new Object();
			filtro.nome = "veiculos-cerca";
			filtro.ativo = false;
			listaFiltros["veiculos-cerca"] = filtro;
			
			var filtro = new Object();
			filtro.nome = "mapa-calor";
			filtro.ativo = false;
			listaFiltros["mapa-calor"] = filtro;			
		}

		function atualizaFiltro(filtro) {
		
			listaFiltros[filtro.nome] = filtro;
		}

		function getFiltro(nome) {

			return listaFiltros[nome];
		}
		
		function ajustaFiltros(filtro) {
			
			var filtroAtivo = $(filtro).attr('value');
			
			if ($(filtro).attr('value') == 'veiculos') {
								
				if (filtro.checked) {
					
					filtroAtivo.ativo = true;		
					
					if (!filtroMapaCalor) {
						setMapVeiculos(map);
						exibeLigacao();											
					};
					
				} else {
					
					filtroAtivo.ativo = false;					
					setMapVeiculos(null);
				};
				
			} else if ($(filtro).attr('value') == 'ocorrencias') {

				if (filtro.checked) {

					filtroAtivo.ativo = true;		
					
					if (!filtroMapaCalor) {
						exibeLigacao();						
						setMapOcorrencias(map);								
					};
					
				} else {
					
					filtroAtivo.ativo = false;				
					setMapOcorrencias(null);
				};				

			} else if ($(filtro).attr('value') == 'agentes') {
				
				if (filtro.checked) {
					if (!filtroMapaCalor) {
						setMapAgentes(map);								
					};					
				} else {
					
					setMapAgentes(null);
				};			
				
			} else if ($(filtro).attr('value') == 'veiculos-cerca') {
				
				if (filtro.checked) {
					if (!filtroMapaCalor) {
						showCerca();								
					};
				} else {
					
					hideCerca();
				};				
			} else if ($(filtro).attr('value') == 'veiculos-rota') {
				
				if (filtro.checked) {
					if (!filtroMapaCalor) {
						showRota();						
					}
				} else {
					
					hideRota();
				}				
			} else if ($(filtro).attr('value') == 'mapa-calor') {
				
				if (filtro.checked) {
										
					filtroMapaCalor = true;
					criarMapaCalor();
					
					setMapVeiculos(null);
					setMapOcorrencias(null);
					setMapAgentes(null);
					hideCerca();
					hideRota();
					exibeLegendas();
					
				} else {
					
					$('#legendaMapaCalor').hide();
					
					filtroMapaCalor = false;				
					apagaMapaCalor();
					exibeLegendas();
					
					for (x in listaFiltros) {
						
						var elem = listaFiltros[x];
						
						if (elem.nome == 'veiculos' && elem.ativo)
							setMapVeiculos(map);

						if (elem.nome == 'ocorrencias' && elem.ativo)
							setMapOcorrencias(map);

						if (elem.nome == 'agentes' && elem.ativo)
							setMapAgentes(map);

						if (elem.nome == 'veiculos-cerca' && elem.ativo)
							showCerca();

						if (elem.nome == 'veiculos-rota' && elem.ativo)
							showRota();
					};
					
				};				
			}
			
			if (filtroAtivo != null) {
				
				filtroAtual = getFiltro($(filtro).attr('value'));
				filtroAtual.ativo = filtro.checked ? true : false;
				atualizaFiltro(filtroAtual);
			}
		}
		
		function validaJSON(varJson) {
		
			if (varJson.token == 'Afff') {
				
				return varJson.conteudo;
			} 
		}	
		
		function exibeDetalhes(id) {
		
			$.post('ocorrencia/exibeDetalhes?id=' + id, function(data) {
				  $('#ocorrenciaDetalhes').html(data);
				  $('#ocorrenciaDetalhes').dialog('open');
			});
		}
	
		function centralizaMapaNaRegiao() {

			var bounds = new google.maps.LatLngBounds();
			
			if (regioesUsuario != null) {
				
				for (x in regioesUsuario[0].geoPonto) {					
					bounds.extend (new google.maps.LatLng (regioesUsuario[0].geoPonto[x].latitude,regioesUsuario[0].geoPonto[x].longitude));	
				};
			};
	
			map.fitBounds(bounds);
		}

		function showOcorrenciaEncerrar(id) {
			
			getJSON("./ocorrencia/listaNatureza",
				function(json) {
	        	
		        	json = eval('(' + json + ')').list; 
		        	
					ocorrencia = findOcorrencia(id);
					$('#selectNaturezaEncerrar').html('');
					
					for(var i=0; i < json.length; i++) {     
						
						$('#selectNaturezaEncerrar').append('<option value="' + json[i].id + '">' + json[i].valor + '</select>');
					}
					
					$('#selectNaturezaEncerrar').val(ocorrencia.estado);
					$('#idNaturezaEncerrar').val(ocorrencia.id);					
					$('#logradouroNaturezaEncerrar').html(ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero);
					$('#resumoNaturezaEncerrar').html(ocorrencia.resumo);
					$('#ocorrenciaEncerrar').dialog('open');

				}, function() { 
					console.log("algo deu errado"); 
			});	
		}

		function closeOcorrenciaEncerrar() {
			
			$('#ocorrenciaEncerrar').dialog('close');
		}
		
		function getComentarios(id) {
					
			getJSON_method("./ocorrencia/listaComentarios?id=" + id, "GET",
				function(json) {
	        	
					lista = json.result;
					var div = "";
					
					for (x in lista) {

						var date = new Date(lista[x].data);
						var hours = date.getHours();
						var minutes = date.getMinutes();

						var year = date.getFullYear();	
						var month = date.getMonth()+1;
						var dia = date.getDate();
					     
						var formattedTime = dia + '/' + month + '/' + year + ' ' + hours + ':' + minutes;
						
						div += "<li><b>" + lista[x].usuario + "<br />" + formattedTime + "</b>" + lista[x].descricao + "</li>";
					}
				
					$('#listaComentarios').html(div);
					
				}, function() { 
					console.log("algo deu errado"); 
			});

		}
		
		function showOcorrenciaComentario(id) {

			getComentarios(id);
			
			$('#ocorrenciaComentarioId').val(id);
			$('#ocorrenciaComentario').dialog('open');

		}
		
		function closeOcorrenciaComentario() {

			$('#ocorrenciaComentarioText').val("");		
			$('#ocorrenciaComentario').dialog('close');
		}

		function showOcorrenciaAssumir(id) {
	
			ocorrencia = findOcorrencia(id);
			
			$('#idOcorrenciaAssumir').html(ocorrencia.id + " - " + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero);
			$('#idOcorrenciaAssumirInput').val(ocorrencia.id);
			$('#ocorrenciaAssumir').dialog('open');
			
		}

		function closeOcorrenciaAssumir() {
			
			$('#ocorrenciaAssumir').dialog('close');
		}					
	
		function ativaChat(token) {
					
			if (token != null && 
					token.agente_id != null && 
					token.agente_id > 0) {
										
				if (listaSalaChat[token.token] == null) {
					
					var salaChat = Object();
					salaChat.agente = Object();
					salaChat.mensagens = [];
					salaChat.push = 0;
					
					salaChat.agente.id = token.agente_id;
					salaChat.agente.nome = listaAgentes[token.agente_id].nome;
					listaSalaChat[token.token] = salaChat;								
				}
				
				exibeConteudoChat(token.token);
				
				$('#chat').dialog('open');
				
			} else {
				
				alert('O dispositivo está offline');
				
			}
		}
		
		function exibeConteudoChat(token) {
			
			$('#tabs_chat').html('');
			$('#listaMensagens').html('');
			salaAtual = token;
			
			showTab();
					
			for (x in listaSalaChat) {							
				
				if (x == token) {
					
					for (var i=0; i < listaSalaChat[x].mensagens.length; i++) {

						div = "<li><b>" + listaSalaChat[x].mensagens[i].fromName + "<br />" + listaSalaChat[x].mensagens[i].data_txt + "</b>" + listaSalaChat[x].mensagens[i].mensagem + "</li>";
//						div = "<li><b>" + mensagem.fromName + "<br />" + mensagem.data_txt + "</b>" + mensagem.mensagem + "</li>";			
						
						$('#listaMensagens').append(div);									
					}
				}
			}
			
			scrollChatParaBaixo();
			
			$('#tokenChat').val(token);

		}
	
		function encerraChat(token) {

			salaAtual = 0;
			delete listaSalaChat[token];

			for (x in listaSalaChat) {	
				
				salaAtual = x;
				break;
			}
			
			if (salaAtual == 0) {
				
				closeChat();
				
			} else {
				
				exibeConteudoChat(salaAtual);
			}
		}
		
		function showTab() {

			$('#tabs_chat').html('');
			
			for (x in listaSalaChat) {
				
				if (salaAtual == x) {
					
					listaSalaChat[x].push = 0;
					$('#tabs_chat').append('<li id="tab_chat_' + listaSalaChat[x].id + '" class="selecionado"><a href="javascript:;" onclick="exibeConteudoChat(\'' + x + '\')">' + listaSalaChat[x].agente.nome + '</a> <a href="javascript:;" onclick="encerraChat(\'' + x + '\');"><img src="./images/ico_excluir_branco.png" style="height: 12px; margin-left: 5px; width: 12px;"></a></li>');												
	
				} else {
					
					var push = "";
					
					if (listaSalaChat[x].push > 0) {
						push = '<b>' + listaSalaChat[x].push + '</b>';
					}
					
					$('#tabs_chat').append('<li id="tab_chat_' + listaSalaChat[x].id + '"><a href="javascript:;" onclick="exibeConteudoChat(\'' + x + '\')">' + listaSalaChat[x].agente.nome.substring(0,7) + push + '</a></li>');
				}			
			}
		}
		
		function closeChat() {
						
			$('#chat').dialog('close');
			$('#listaMensagens').html('');
			$('#mensagemChat').val('');
		}
		
		function adicionaMensagem(mensagem, id) {
			
			var date = new Date();			     
			var formattedTime = date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();			
			
			mensagem.data = date;
			mensagem.data_txt = formattedTime;
		
			var sala = listaSalaChat[id];

			if (mensagem.fromName == null) {
				mensagem.fromName = sala.agente.nome;
			}
			listaSalaChat[id].mensagens.push(mensagem);
			
			if (salaAtual == id) {
				
				div = "<li><b>" + mensagem.fromName + "<br />" + mensagem.data_txt + "</b>" + mensagem.mensagem + "</li>";			
				$('#listaMensagens').append(div);
				scrollChatParaBaixo();
				
			} else {
				
				listaSalaChat[id].push = listaSalaChat[id].push + 1;
			}

//			$("#mensagens").animate({ scrollTop: $("#mensagens").height() }, 300);
			
			showTab();		
		}

		function scrollChatParaBaixo() {
			
			altura = 0;
			elementos = $('#listaMensagens').find('li');
			
			for (var i=0; i < elementos.size(); i++) {
				
				elemento = elementos[i];
				if ($(elemento).attr('id') == 'ocorrencia' + marcador.id) {
					
					break;
				}
				
				altura = altura + $(elemento).height() + 30;
			}
	
			$('#mensagens').scrollTop(altura);
		}
		
		function validarOcorrencia(id) {
			
			$.post( "ocorrencia/validar/", { 'id': id },
			 	function( resposta ) {
								
				});			
		};

		
		$(document).ready( function() {
		
			fastview();
		
			try {
				socket = io.connect('${config.socketUrl}');

				socket.on('connect', function (data) {
					
					var dados = Object();
					dados.token = usuario.id;
					dados.nome = usuario.nome;
					
					dados.tipo = 'browser';
					this.emit('sessao/token', dados);
				});
				
				socket.on('historico/change', function (data) {
										
					
					var json = data.result;

					if (json.veiculo_id != null) {
						
						atualizaVeiculo(json);			    														
					} 

					if (json.agente_id != null) {

						atualizaAgente(json);
					};
				});
				
				socket.on('ocorrencia/add', function (data) {
					
					
					var ocorrencia = data.result;
					addNotificacao(ocorrencia.id + ' - ' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero , 'Foi adicionada', 'javascript:;', 'detalhesOcorrencia(\'' + ocorrencia.id + '\')');
					$('#listaOcorrencias').prepend(adicionaOcorrencia(data.result));	
					alert('Nova atividade adicionada - #' + data.result.id);

				});

				socket.on('ocorrencia/change', function (data) {
					
									
					var ocorrencia = data.result;
					
					alteraOcorrencia(ocorrencia);
									
					if (ocorrencia.id == $('#ocorrenciaComentarioId').val()) {
						
						getComentarios(ocorrencia.id);
					}

				});
				
				socket.on('notificao/change', function (data) {
					
					
					var mensagem = data.result;
					alteraOcorrencia(ocorrencia);
					
					if (ocorrencia.id == $('#ocorrenciaComentarioId').val()) {
						getComentarios(ocorrencia.id);
					}

				});
				
				socket.on('ocorrencia/delete', function (data) {
					
					
					console.log(data);
					removeOcorrencia(data.result);
				});
				
				socket.on('chat/operadores', function (data) {
					alert('chat/operadores ' + data.result);
				});				
				
				socket.on('chat/mensagem/receber', function (data) {
					alert('chat/mensagem/receber ' + data.result);
				});						

				socket.on('camera/show/browser', function (data) {
					exibeImagem(data.result);
				});					
				
				socket.on('chat/mensagem', function (data) {
													
					
					//if (data.result.nodeId == this.sessionid) {
	
						var mensagem = data.result;
	
						if (listaSalaChat[mensagem.from] == null) {
							
							$.post( "comunicacao/token/", { 'token': data.result.from }, function( resposta ) {
								
								if (resposta != null && 
										resposta.result != null && 
										resposta.result.agente_id != null && 
										resposta.result.agente_id > 0) {
															
									if (listaSalaChat[resposta.result.token] == null) {
										
										var salaChat = Object();
										salaChat.agente = Object();
										salaChat.mensagens = [];
										salaChat.push = 0;
										
										salaChat.agente.id = resposta.result.agente_id;
										salaChat.agente.nome = listaAgentes[resposta.result.agente_id].nome;
										listaSalaChat[resposta.result.token] = salaChat;				
									}								
								} 							

								if (!$('#chat').is(':visible') || salaAtual == '0') {	
									salaAtual = resposta.result.token;
								}

								adicionaMensagem(mensagem, data.result.from);
							});

						} else {

							adicionaMensagem(mensagem, data.result.from);							
						}						
					//} 
					
					if (!$('#chat').is(':visible')) {	
						
						exibeConteudoChat(data.result.from);	
						$('#chat').dialog('open');												
					} 																
				});
				
			} catch (e) {
				
				console.log("Erro iniciando NodeJS.socket.io");
			}
			
			$('#ocorrencias_tab_legendas').html($('#legendaOcorrenciasModel').html());
			$('#veiculos_tab_legendas').html($('#legendaVeiculosModel').html());
			$('#agentes_tab_legendas').html($('#legendaVeiculosModel').html());			
			
			$(document).find('.aba_link').click( function() {
		
				$(document).find('.aba_link').removeClass('ui-tabs-selected ui-state-active');
				$(this).addClass('ui-tabs-selected ui-state-active');
				$(document).find('.aba').not($('#' + $(this).attr('rel'))).hide();
				
				aba_atual = $(this).attr('rel');
				
				$('#' + $(this).attr('rel')).show();
				exibeLegendas();
			
				return false;
			});	
			
			$('#ocorrenciaDetalhes').dialog({ minWidth: 900, autoOpen: false, resizable: false });	
			$('#ocorrenciaEncerrar').dialog({ minWidth: 330, autoOpen: false, resizable: false });
			$('#ocorrenciaAssumir').dialog({ minWidth: 330, autoOpen: false, resizable: false });
			$('#ocorrenciaComentario').dialog({ minWidth: 700, autoOpen: false, resizable: false });
			$('#chat').dialog({ minWidth: 700, autoOpen: false, resizable: false });
			
			
			$('#formEncerrarOcorrencia').jFotoForm({"ajax": true}, function() {
				closeOcorrenciaEncerrar();
			});
			
			$('#formAssumirOcorrencia').jFotoForm({"ajax": true}, function() {

				listaOcorrencias[$('#idOcorrenciaAssumirInput').val()].usuario.id = usuario.id;
				listaOcorrencias[$('#idOcorrenciaAssumirInput').val()].usuario.nome = usuario.nome;
				detalhesOcorrencia($('#idOcorrenciaAssumirInput').val());
				closeOcorrenciaAssumir();
			});

			$('#formComentarioOcorrencia').jFotoForm({"ajax": true}, function(resposta) {
				
				if (resposta != null && resposta.status == 'ok') {

					$('#ocorrenciaComentarioText').val("");

					var ocorrencia = findOcorrencia(resposta.id);					
					addNotificacao(ocorrencia.id + ' - ' + ocorrencia.endereco.logradouro + ', ' + ocorrencia.endereco.numero , 'Comentário adicionado com sucesso', 'javascript:;', 'detalhesOcorrencia(\'' + ocorrencia.id + '\')');
					getComentarios(ocorrencia.id);

					//closeOcorrenciaComentario();

				} else {
					
					alert('Erro ao enviar comentário');
				}
			});

			$('#formChat').jFotoForm({"ajax": false}, function() {				
				
				var mensagem = Object();
				
				mensagem.mensagem = $('#mensagemChat').val();
				mensagem.to = $('#tokenChat').val();
				mensagem.from = usuario.id;
				mensagem.fromName = usuario.nome;
				mensagem.dataMensagem = new Date().getTime();
				
				var result = Object();
				result.result = mensagem;
				
				socket.emit('chat/mensagem', result);

				var date = new Date();
				var formattedTime = date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();				

				adicionaMensagem(mensagem, mensagem.to);
				$('#mensagemChat').val('');
				
				return false;
			});

			iniciaAgentes();			
			iniciaViaturas();
			iniciaOcorrencias();	// agora está sendo iniciado após carregar as viaturas.

			centralizaMapaNaRegiao();
		});
		
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

	<div id="ocorrenciaDetalhes" title="Detalhes atividade"> </div>
		
	<div id="ocorrenciaEncerrar" title="Encerrar atividade"> 
	
		<form action="./ocorrencia/encerrar" id="formEncerrarOcorrencia" method="post">
		
			<input type="hidden" id="idNaturezaEncerrar" name="ocorrencia.id" value="" />
			<div class="form">
				<div class="fields">
					
					<div class="field">
						<p><b id="logradouroNaturezaEncerrar"></b></p>
						<p id="resumoNaturezaEncerrar"></p>

						<div class="label label-dialog">
							<label>Natureza final: </label>
						</div>
						<div class="select">
							<select name="ocorrencia.natureza2.id" id="selectNaturezaEncerrar">

							</select>
						</div>
					</div>
					
					<div class="buttons buttons-dialog">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="encerrar atividade" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="closeOcorrenciaEncerrar();" />				
					</div>	
					
				</div>
			</div>		
			
		</form>
	
	</div>

	<div id="ocorrenciaAssumir" title="Assumir atividade"> 
	
		<form action="./ocorrencia/assumir" id="formAssumirOcorrencia" method="post">
		
			<div class="form">
				<div class="fields">
					
					<div class="field">

						<div class="label label-dialog">
							<p><label>Deseja assumir essa atividade ? </label></p>
							<p><b id="idOcorrenciaAssumir"></b></p>		
							<input id="idOcorrenciaAssumirInput" type="hidden" name="ocorrencia.id" value="" />					
						</div>
					</div>
					
					<div class="buttons buttons-dialog">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="assumir atividade" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="closeOcorrenciaAssumir();" />				
					</div>	
					
				</div>
			</div>		
			
		</form>
	
	</div>

	<div id="ocorrenciaComentario" title="Observações"> 
	
		<div id="comentarios" style="">
			<ul id="listaComentarios"></ul>
		</div>
		
		<form action="./ocorrencia/comentario" id="formComentarioOcorrencia" method="post">
		
			<input type="hidden" id="ocorrenciaComentarioId" name="id" value="" />
			<div class="form">
				<div class="fields">
					
					<div class="field">
						<b>Nova observação:</b>
						<textarea class="textarea-medium" name="comentario" style="width: 650px; height: 30px;" validate="vazio" id="ocorrenciaComentarioText"></textarea>
					</div>
										
					<div class="buttons buttons-dialog">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar comentário" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="fechar" onclick="closeOcorrenciaComentario();" />				
					</div>	
					
				</div>
			</div>		
			
		</form>	
	</div>
			
	<div id="chat" title="Chat">

		<ul class="tabs_chat" id="tabs_chat">

		</ul>
		
		<div id="mensagens" style="">
			<ul id="listaMensagens">			
			</ul>
		</div>
		
		<form action="" id="formChat">
							
			<input type="hidden" id="tokenChat" name="token" value="" />
			<div class="form">
				<div class="fields">
					
					<div class="field">
						<b>Mensagem:</b>
						<input class="input-medium" name="mensagemChat" style="width: 650px; height: 30px;" validate="vazio" id="mensagemChat"></textarea>
					</div>
										
					<div class="buttons buttons-dialog">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar comentário" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="fechar" onclick="closeChat();" />				
					</div>	
					
				</div>
			</div>		
			
		</form>	
	</div>
				
</jsp:attribute>

<jsp:body>

	<div id="fastview" class="fastview box">
		
		<div class="title">
			
			<ul class="links ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
				<li id="ocorrencias" class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active aba_link" rel="ocorrencias_tab"><a href="">Atividades</a></li>				
				<li id="veiculos" class="ui-state-default ui-corner-top aba_link" rel="veiculos_tab"><a href="">Veículos</a></li>
				<li id="agentes" class="ui-state-default ui-corner-top aba_link" rel="agentes_tab"><a href="">Agentes</a></li>					
			</ul>
		</div>
		
		<jsp:include page="../ocorrencia/tab_ocorrencia.jsp" flush="false"></jsp:include>			
		<jsp:include page="../ocorrencia/tab_veiculos.jsp" flush="false"></jsp:include>
		<jsp:include page="../ocorrencia/tab_agentes.jsp" flush="false"></jsp:include>
			
 	</div>
		
	<div id="mapa" style="height: 200px;">	</div>

</jsp:body>

</layout:pagina>
