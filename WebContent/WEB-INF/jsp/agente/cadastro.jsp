<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/style.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/ccpb.css" rel="stylesheet" />
<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script>
	<script type="text/javascript" src="../../../js/jquery.maskedinput.js"></script> 
	
</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">
		
		function showDialogVeiculos() {
		
			$.ajax({
		        type: "POST",
		        url: '${pageContext.request.contextPath}/veiculo/listaVeiculos',
		        success: function( resposta )
		        {
//		        	json = eval('(' + resposta + ')').result;        	        	
		        	json = resposta.result;
		        	$('#listaVeiculosAgente').html('');
		        	console.log(json);
		        	
		        	for(var i=0; i < json.length; i++) {        
						
		        		if (json[i].idOrgao == null || json[i].idOrgao == "") {
		        			json[i].idOrgao = "-";
		        		}
		        		$('#listaVeiculosAgente').append('<li><a href="javascript:;" onclick="setVeiculo(' + json[i].id + ', \'' + json[i].placa + '\');"><span>' + json[i].idOrgao + '</span>' + json[i].placa + '</a></li>');
		        	}    	
		        	
		        	$("#dialogVeiculos").dialog('open');
		        },
		        error:function(XMLHttpRequest, textStatus, errorThrown)
		        {
		        	console.log("algo deu errado");
		        }
			});		
		}
		
		function showDialogSenha() {
			
			$('#dialogSenha').dialog('open');			
		}

		function closeDialogSenha() {
			
			$('#dialogSenha').dialog('close');			
		}		
		
		function setVeiculo(id, veiculo) {
			
			$('#veiculoId').val(id);
			$('#veiculoTxt').html('<p>' + veiculo + ' <img style="margin-left: 20px; cursor: pointer;" onclick="zeraVeiculo();" src="../../../images/ico_excluir.gif" /></p>');
			$('#addVeiculo').hide();			
			$("#dialogVeiculos").dialog('close');
			
		}
		
		function inicio() {
			
			parent.location='../../../controle_geral/agentes/listar/';
		}

		function zeraVeiculo() {
			$('#veiculoTxt').html('');
			$('#veiculoId').val('');
			$('#addVeiculo').show();
		}
		
		$(document).ready( function() {
			
			$("#cpfAgente").mask("999.999.999-99");	
			
			$("#dialogVeiculos").dialog({ minWidth: 300, maxHeight: 400, autoOpen: false, resizable: false });	
			$("#dialogSenha").dialog({ minWidth: 350, maxHeight: 400, autoOpen: false, resizable: false });
			
			$('.ajuda').tipTip({maxWidth: "auto", edgeOffset: 10});

			$("#formAgente").jFotoForm(function () {
				return addRota(this);
			});

			$("#formUsuarioSenha").jFotoForm({ajax: true, divErro: 'mensagem-erro-senha'}, function (resposta) {
								
				if (resposta.status == 'error') {
					
					$('#mensagem-erro-senha').find('h6').html(resposta.msgErro);
					$('#mensagem-erro-senha').show();
				} else {

					$('#message-success-senha').find('h6').html(resposta.msgSucesso);
					$('#message-success-senha').show();	
					$('#mensagem-erro-senha').hide();	
					$('#dialogSenha').dialog('close');		

					$(window).trigger('resize');
				}
				
				return false;
			});

			$(window).trigger('resize');

		});
		
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

	<div id="dialogVeiculos" title="Associar veículo">
	
		<div  style="height: 400px; overflow-y: scroll;">
		<ul id="listaVeiculosAgente">
		
		</ul>
		</div>
	
		<div class="form" style="clear: both;">
			<div class="fields">	
				<div class="buttons" style="margin-left: 0;">
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="$('#dialogVeiculos').dialog('close');" />
				</div>
			</div>
		</div>
	</div>

	<div id="dialogSenha" title="Alterar senha">

		<div id="mensagem-erro-senha" class="messages" style="display: none;">
			<div class="message message-error">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/error.png" />
				</div>
				<div class="text">
					<h6 id="msgErrorSenha">Ocorreram os seguintes erros</h6>	
				</div>
			</div>
		</div>
	
		<form id="formUsuarioSenha" action="../../../controle_geral/usuarios/senha/" method="post">
		
			<input type="hidden" name="id" value="${agente.usuario.id}" />
			
			<div class="form">

			<div class="fields">	

				<div class="field">
					<div class="label">
						<label for="input-medium">Senha atual:</label>
					</div>
					<div class="input">
						<input id="senhaAtual" name="senhaAtual" type="password" value="" style="width: 310px;"/>							  		
					</div>  				
				</div>
		
				<div class="field">
					<div class="label">
						<label for="input-medium">Nova senha</label>
					</div>
					<div class="input">
						<input id="senha01" name="senha01" type="password" value="" style="width: 310px;"/>							  		
					</div>  				
				</div>
		
				<div class="field">
					<div class="label">
						<label for="input-medium">Nova senha confirmação</label>
					</div>
					<div class="input">
						<input id="senha02" name="senha02" type="password" value="" style="width: 310px;"/>							  		
					</div>  				
				</div>
							
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="alterar senha" />
					</div>				
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="closeDialogSenha();" />							
				</div>
			</div>
			</div>
		</form>			
	</div>
	
</jsp:attribute>

<jsp:body>

	<div id="conteudo" class="box box-inline">
		
		<div class="title">
			<h5>Cadastrar agente</h5>
		</div>

		<jsp:include page="../includes/mensagens.jsp" />

		<div class="messages">
			<div id="message-success-senha" class="message message-success" style="display: none;">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/success.png" />
				</div>
				<div class="text">
					<h6></h6>
				</div>
			</div>
		</div>
		
		<form id="formAgente" action="../../../controle_geral/agentes/adicionar/" method="post">
			
			<input type="hidden" name="agente.id" value="${agente.id}"/>
			
			<div class="form">
				<div class="fields">
				
					<div class="field">
						<div class="label">
							<label for="select">Matricula:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Matricula do agente">?</div></label>
						</div>
											
						<div class="input">
							<input class="medium${erroMatricula eq true ? ' error' : ''}" type="text" name="agente.usuario.matricula" value="${agente.usuario.matricula}" validate="vazio" />
						</div>				
					</div>
								
					<div class="field">
						<div class="label">
							<label for="input-medium">Nome:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Nome do agente">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroNome eq true ? ' error' : ''}" type="text" name="agente.usuario.nome" value="${agente.usuario.nome}" validate="vazio"/>
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="input-medium">Login:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Login">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroLogin eq true ? ' error' : ''}" type="text" name="agente.usuario.login" value="${agente.usuario.login}" validate="vazio"/>
						</div>
					</div>
<c:if test="${agente.id == null || agente.id <= 0}">
					<div class="field">
						<div class="label">
							<label for="input-medium">Senha:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Senha">?</div></label>
						</div>
						<div class="input">
							<input id="senha" class="medium${erroSenha eq true ? ' error' : ''}" type="password" name="agente.usuario.senha" value="${agente.usuario.senha}" validate="vazio" tmMinimo="5" />
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="input-medium">Senha Novamente:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Senha novamente">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroSenha eq true ? ' error' : ''}" type="password" name="senha_novamente" value="" validate="senha" campoConfere="senha" />
						</div>
					</div>
</c:if>								
					<div class="field">
						<div class="label">
							<label for="input-medium">Cpf:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Cpf do agente">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroCpf eq true ? ' error' : ''}" id="cpfAgente" type="text" name="agente.usuario.cpf" value="${agente.usuario.cpf}" validate="cpf"/>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="input-medium">Descricao:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroDescricao eq true ? ' error' : ''}" type="text" name="agente.usuario.descricao" value="${agente.usuario.descricao}" validate="vazio"/>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="select">Status:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Status do agente">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="agente.usuario.status.id">
								<c:forEach var="status" items="${listaStatus}">
									<option value="${status.id}" ${agente.usuario.status.id == status.id ? 'selected="selected"' : ''}>${status.valor}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="select">Grupo:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Grupo do agente">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="agente.usuario.grupo.id">
								<c:forEach var="grupo" items="${listaGrupos}">
									<option value="${grupo.id}" ${agente.usuario.grupo.id == grupo.id ? 'selected="selected"' : ''}>${grupo.nome}</option>
								</c:forEach>
							</select>
						</div>
					</div>
															
					<div class="field">
						<div class="label">
							<label for="select">Veículo associado:<div class="ajuda" title="Selecionar um veículo caso o agente esteja associado a algum.">?</div></label>
						</div>
						
						<div style="margin:10px 0 0 0px;">
							<div id="veiculoTxt">
								<c:if test="${agente.veiculo.id > 0}">
									<p>${agente.veiculo.placa} <img style="margin-left: 20px; cursor: pointer;" onclick="zeraVeiculo();" src="../../../images/ico_excluir.gif" /></p>
								</c:if>						
							</div>

							<div id="addVeiculo" style="margin:10px 0px 0 185px;<c:if test="${agente.veiculo.placa != null}"> display: none;</c:if>">												
								<a href="javascript:;" onclick="showDialogVeiculos();"><img src="../../../images/plus.png" /> associar a veículo</a>
								<input type="hidden" id="veiculoId" name="agente.veiculo.id" value="${agente.veiculo.id}" />									
							</div>						
						</div>
					</div>
													
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${agente.id == null ? 'adicionar' : 'alterar'}" />
						</div>
<c:if test="${agente.id != null || agente.id > 0}">						
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="alterar senha" onclick="showDialogSenha();" />				
</c:if>						
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
