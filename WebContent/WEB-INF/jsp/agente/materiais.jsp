<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">
	
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 
	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jTable.js"></script> 
	
</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">
		
	function inicio() {
		
		parent.location = '../../../agente/listar/';
	}
	
	function removeObjetoAssociado(id, nome) {
		
		var answer = confirm("Deseja realmente objeto " + nome + " desse Agente ?");
		
		if (answer) {				
			$.ajax({
		        type: "GET",
		        url: "../../../controle_geral/agentes/materiais/remover/",
		        data: {'objeto.id': id, 'agente.id': '${agente.id}'},
		        success: function( resposta )
		        {
//		        	json = resposta;
		        	json = eval('(' + resposta + ')'); 
		        	
		        	if (json.status == 'ok') {

		    			var id = $('#materiais').val();
		        		setTabelaAssociados();
		        		setTabelaDisponiveis(id);
		        	}
		        },
		        error:function(XMLHttpRequest, textStatus, errorThrown)
		        {
		            console.log('erro');
		        }
			});	
		}
	}
	
	function setTabelaDisponiveis(id) {
		
		$("#listaDisponiveis").jTable({
			url: '../../../objeto/listaObjetos/',
			cabecalho: [								
				{'titulo': 'ID Objeto', 'width': 'auto', 'align': 'left', 'campo': 'id' },
				{'titulo': 'Material', 'width': 'auto', 'align': 'left', 'campo': 'material' },
				{'titulo': 'Descricao', 'width': 'auto', 'align': 'left', 'campo': 'descricao' },
				{'titulo': 'Controle', 'width': 'auto', 'align': 'left', 'campo': 'controle' },					
				{'titulo': 'Estado', 'width': 'auto', 'align': 'left', 'campo': 'estado' },
				{'width': '20px', 'align': 'left', 'campo': 'id', 'tipo':'checkbox' }
			],				
			campoRef: 'id',
//			parametros: {'material.id': '${materialSelecionado.id}'},
			parametros: {'material.id': id},				
			campoTituloRef: 'descricao'
		});
	}
	
	function setTabelaAssociados() {

		$("#listaAssociados").jTable({
			url: '../../../agentes/materiais/listaObjetos/',				
			cabecalho: [								
				{'titulo': 'ID Objeto', 'width': 'auto', 'align': 'left', 'campo': 'id' },
				{'titulo': 'Descricao', 'width': 'auto', 'align': 'left', 'campo': 'descricao' },
				{'titulo': 'Material', 'width': 'auto', 'align': 'left', 'campo': 'material' },
				{'titulo': 'Controle', 'width': 'auto', 'align': 'left', 'campo': 'controle' },					
				{'titulo': 'Estado', 'width': 'auto', 'align': 'left', 'campo': 'estado' }
			],
			opcoesTitulo: 'Opções',
			parametros: {'agente.id': '${agente.id}'},
			campoRef: 'id',
			campoTituloRef: 'descricao',
			opcoes: [{'icon' : '../../../images/ico_excluir.png', 'link':'','funcao': 'removeObjetoAssociado', 
				      'ajax':'true', 'alt':'excluir'}]
		});
	}
		
	$(document).ready(function() {
  	  
		setTabelaDisponiveis('${materialSelecionado.id}');
		setTabelaAssociados();

		$("#funcionalidade").change(function() {
			
			$.ajax({
		         type: "POST",
		         url: "../../../controle_geral/materiais/listaMateriaisCombo/",
		         data: {funcionalidade: $("#funcionalidade").val()},
		         dataType: "json",
		         success: function(json){
		        	 
		        	json = json.result;
		            var options = "";
		            for(x in json) {
		            	
		            	options += '<option value="' + json[x].id + '">' + json[x].titulo + '</option>';
		            }

		            $("#materiais").html(options);
		            
		            if (json.length > 0) {
		            	
		            	setTabelaDisponiveis(json[0].id);
		            
		            }
		         }
		      });					
		});
  		
		$("#materiais").change(function() {

			var id = $(this).val();
//			$("#listaDisponiveis").setParametros({'material.id':id});
//			$("#listaDisponiveis").refresh();
			setTabelaDisponiveis(id);
		});		
		
		$('#formAssociarObjeto').jFotoForm({"ajax": true}, function() {
			
			var id = $('#materiais').val();
    		setTabelaDisponiveis(id);
			setTabelaAssociados();
		});			
	});
				
	</script>

</jsp:attribute>

<jsp:body>

	<div id="conteudo" class="box box-inline">
		
		<div class="title">
			<h5>Associar material ao agente</h5>
		    <ul class="links">
				<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
				<a href="/smc/controle_geral/agentes/materiais/${agente.id}">Materiais</a>
				</li>
		    </ul>
			<ul class="links">
				<li class="ui-state-active">
				<a href="/smc/controle_geral/agentes/editar/?id=${agente.id}">Geral</a>
				</li>
		    </ul>
		</div>

		<jsp:include page="../includes/mensagens.jsp" />
	
		<form id="formAssociarObjeto" action="../../../controle_geral/agentes/materiais/associar/" method="post">
			
			<input type="hidden" name="agente.id" value="${agente.id}"/>
			
			<div class="form">
				<div class="fields">
					
					<div class="field">
						<div class="label">
							<label for="select">Funcionalidade:<div class="ajuda" title="Funcionalidade">?</div></label>
						</div>						
						<div class="select">
							<select name="funcionalidade" id="funcionalidade">
								<c:forEach var="dominio" items="${listaFuncionalidade}">
									<option value="${dominio.id}" ${funcionalidade == dominio.id ? 'selected="selected"' : ''}>${dominio.valor}</option>
								</c:forEach>
								
							</select>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Material:<div class="ajuda" title="Material">?</div></label>
						</div>
						<div class="select">
							<select name="materiais" id="materiais"  >	
								<c:forEach var="dominio" items="${listaMateriais}">
									<option value="${dominio.id}" ${funcionalidade == dominio.id ? 'selected="selected"' : ''}>${dominio.titulo}</option>
								</c:forEach>													
							</select>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Objetos Disponiveis:<div class="ajuda" title="Material">?</div></label>
						</div>
						
						<div class="input">
							<div id="listaDisponiveis"></div>
						</div>		
					</div>
					
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="associar" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>

				</div>	
			</div>
		</form>

		<br /><br />

		<div class="title">
			<h5>Objetos associados ao agente</h5>
		</div>
		
		<br /><br />
		<div class="table">
			<div id="listaAssociados"></div>
		</div>			
		<br/><br/>

	</div>

</jsp:body>

</layout:controleGeral>
