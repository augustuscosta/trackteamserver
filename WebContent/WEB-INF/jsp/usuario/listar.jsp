<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	function removeUsuario(id, nome) {
		
		var answer = confirm("Deseja realmente excluir o Usuário " + nome + " ?");
		if (answer) {
			
			$.post( "../excluir/", { 'id': id },
			   		function( resposta ) {

						resposta = eval('(' + resposta + ')');       

						if (resposta.status == 'error') {
							
						} else {
						
							$('#message-success-senha').find('h6').html(resposta.msgSucesso);
							$('#message-success-senha').show();	
							$('#mensagem-erro-senha').hide();
							
							$('#lista').refresh();
							
							$(window).trigger('resize');							
						}
					});
			

		}
	}
	
	function criaTabelaUsuarios(){
		$("#lista").jTable({
			url: '../../../controle_geral/usuarios/listaUsuarios/',
			cabecalho: [
				{'titulo': 'Matricula', 'width': 'auto', 'align': 'left', 'campo': 'matricula' },        
				{'titulo': 'Nome', 'width': 'auto', 'align': 'left', 'campo': 'nome' },
				{'titulo': 'Login', 'width': 'auto', 'align': 'left', 'campo': 'login' },
				{'titulo': 'Status', 'width': 'auto', 'align': 'left', 'campo': 'status' },
				{'titulo': 'Grupo', 'width': 'auto', 'align': 'left', 'campo': 'grupo' }
			],
			opcoesTitulo: 'Opções',
			editar: true,
			remover: true,
			linkEditar: '../../../controle_geral/usuarios/editarUsuario/',
			funcaoRemover: 'removeUsuario',
			campoRef: 'id',
			campoTituloRef: 'nome',
			iconEditar: '../../../images/ico_alterar.png',
			iconExcluir: '../../../images/ico_excluir.png'
		});
	} 
	
	$(document).ready(function() {
/*
		<c:if test="${rowCounter.count > 1}">,</c:if>{"id": "${agente.id}",
			"matricula": "${agente.usuario.matricula}",
			"nome": "${agente.usuario.nome}",
			"login": "${agente.usuario.login}",
			"status": "${agente.usuario.status.valor}",	
			"grupo": "${agente.usuario.grupo.nome}"
			}
*/		
	criaTabelaUsuarios();
					
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Lista usuário</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		
		<div class="table">
<div id="lista"></div>
		</div>
	</div>
	
</jsp:body>

</layout:controleGeral>
