<%@page import="br.com.otgmobile.trackteam.modal.Regioes"%>
<%@page import="br.com.otgmobile.trackteam.modal.usuario.Usuario"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/style.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/ccpb.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />		

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>	
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script>	
	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 	
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script>
	<script type="text/javascript" src="../../../js/jquery.maskedinput.js"></script> 
	<script type="text/javascript" src="../../../js/gmap3.js"></script> 	
	 
	
	
	
</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">

	var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');
	
	var mapRegiao,mapAddRegiao,mapRef = "";
	
	var path = new google.maps.MVCArray;	
	
	var listaRegioes = [];
	var listaRegioesSelecionadas = [];
	
	var contador = 0;
	
	var regiaoPath = new google.maps.MVCArray;	
	
	var regiao = new google.maps.Polygon({
	    fillColor: '#FF0000',
	    strokeOpacity: 0.3,
	    strokeColor: "#FF0000",
	    strokeWeight: 2,
	    fillOpacity: 0.05
	});	
	
	
	$("#mapaRegiao").ready( function() { 
		
		$("#mapaRegiao").gmap3({ 
			action:'init',
			options:{
				center:[latlngMapa.lat(),latlngMapa.lng()],
				zoom: 14
			}	
		});
		
		mapRegiao = $('#mapaRegiao').gmap3('get');  	
		
		regiao.setMap(mapRegiao);
		regiao.setPaths(new google.maps.MVCArray([regiaoPath]));
	    
	 });
	
	function inicio() {
		
		parent.location='../../../controle_geral/usuarios/listar/';
		
	}
	
	function showRegiaoDialog() {			
		
		getRegioes();
			
		$("#dialogRegiao").dialog('open');
		google.maps.event.trigger(mapRegiao, 'resize');
		$('#adicionarRegião').html('<div style="margin: 10px 0 0 0px;" ><a href="javascript:;" onclick="addRegiao();"><img src="../../../images/plus.png" /> adicionar Região</a></div>');
		$(window).trigger('resize');
		
	}
	
	function getRegioes() {

		$.ajax({
			type: "POST",
			url: "../../regioes/listaRegiao",
			success: function( resposta ) {
				
	        	json = eval('(' + resposta + ')').list;       
	        	listaRegioes = json;			       	

				div = "";
				
				for(x in listaRegioes) {
					div += '<li id="Regiao-' + listaRegioes[x].id +'"><a href="javascript:;" onclick="exibeRegioes(' + listaRegioes[x].id + ')">' + listaRegioes[x].nome + '</li>';	
				}
				
				$('#listaRegioes').html(div);
				
				if (listaRegioes.length > 0) {					
					exibeRegioes(listaRegioes[0].id);
					
				}				
			}
		});			
	}
	
	function exibeRegioes(id) {
		
		
		$('#listaRegioes').find('li').removeClass('selecionado');
		$('#regioesSel').val(id);	
		
		regiaoPath.clear();
		var limites = new google.maps.LatLngBounds();
		
		 for (var i=0; i < listaRegioes.length; i++) {	 
			 
			if (listaRegioes[i].id == id) {
				 
				for (var f=0; f < listaRegioes[i].geoPontos.length; f++) {					
					var ponto = new google.maps.LatLng(listaRegioes[i].geoPontos[f].latitude,listaRegioes[i].geoPontos[f].longitude);
					regiaoPath.insertAt(regiaoPath.length, ponto);
					limites.extend(ponto);
				}
				break;
			}
		}
		
		mapRegiao.setCenter(limites.getCenter()); 
		
		$('#regioes-' + id).addClass('selecionado');
	}
	
	function selecionaRegioes(id) {		
		
		for (var i=0; i < listaRegioes.length; i++) {
			
			if (listaRegioes[i].id == id) {				
				break;
			}
		}		
			
		
		var conteudoDiv = $("#Regioes").text();		
		
		if(conteudoDiv == '')
		{
			$('#Regioes').html('<div id=regiaoAtribuida'+listaRegioes[i].id+' style="margin-top: 10px; display: inline-block; width: 100%;><input type="hidden" name="usuario.regioes[].id" value='+listaRegioes[i].id+'></input><input type="hidden" name="usuario.regioes[].id" value='+listaRegioes[i].id+'>'+listaRegioes[i].nome +'</input>' + '<img style="margin-left: 20px; cursor: pointer;" onclick="delRegiaoSelecionada('+listaRegioes[i].id+');" src="../../../images/ico_excluir.gif" /></div>');
			
		}
		else
		{		
			
			$('#Regioes').html($('#Regioes').html() + '<div id=regiaoAtribuida'+listaRegioes[i].id+' style="margin-top: 10px; display: inline-block; width: 100%;><input type="hidden" name="usuario.regioes[].id" value='+listaRegioes[i].id+'></input><input type="hidden" name="usuario.regioes[].id" value='+listaRegioes[i].id+'>'+listaRegioes[i].nome +'</input>' + '<img style="margin-left: 20px; cursor: pointer;" onclick="delRegiaoSelecionada('+listaRegioes[i].id+');" src="../../../images/ico_excluir.gif" /></div>');
			
		}	
		
		//listaRegioesSelecionadas[contador] = listaRegioes[i].id;
		$('#regiaoCampo').val(listaRegioes[i].id);
		$('#addRegiao').hide(300);	
		$(window).trigger('resize');	
		
		contador++;
		
	}
	
	function delRegiaoSelecionada(id) {
		
		$('#regiaoAtribuida' + id ).remove();
	}
	
	function zeraRegiao() {
		$('#Regioes').html('');
		$('#regiaoCampo').val('');
		$('#addRegiao').show();
		$('#adicionarRegião').html('');
		
	}
	
	function fechaRegiao() {
		
		$('#dialogRegiao').dialog('close');	
		$('#addRegiao').remove();
		
	}
	
	function filtraRegioes() {
		
		var txtBusca = $('#filtroRegiao').val().toUpperCase();
		var div = "";
		$('#listaRegioes').html('');

		for (x in listaRegioes) {
	
			if (listaRegioes[x].nome.toUpperCase().lastIndexOf(txtBusca) >= 0 ) {

				div += '<li id="regioes-' + listaRegioes[x].id +'"><a href="javascript:;" onclick="exibeCerca(' + listaRegioes[x].id + ')">' + listaRegioes[x].nome + '</li>';														
			}
		}
		
		$('#listaRegioes').html(div);
		
		if (listaRegioes.length > 0) {
			exibeRegioes(listaRegioes[0].id);
		}
	}
	
	function addRegiao(id, nome) {		
		showRegiaoDialog();
	}
	
	function addRegioes(id,nome){
		
		$('#Regioes').html($('#Regioes').html() + '<div id=regiaoAtribuida'+id+' style="margin-top: 10px; display: inline-block; width: 100%;><input type="hidden" name='+id+' value='+id+'></input><input type="hidden" name="usuario.regioes[].id" value='+id+'>'+nome +'</input>' + '<img style="margin-left: 20px; cursor: pointer;" onclick="delRegiaoSelecionada('+id+');" src="../../../images/ico_excluir.gif" /></div>');
		
	}
	
	function showDialogSenha() {
		
		$('#dialogSenha').dialog('open');			
	}
	
	function closeDialogSenha() {
		
		$('#dialogSenha').dialog('close');			
	}
	
	$(document).ready( function() {		
		
		$("#cpfUsuario").mask("999.999.999-99");	
		
		
		$('#formRegiao').submit(function() {
			selecionaRegioes(this.elements["regioesSel"].value);			
			fechaRegiao();		
			return false;
		});
		
		$("#dialogRegiao").dialog({ minWidth: 900, maxHeight: 500, autoOpen: false, resizable: false });	
		$("#dialogSenha").dialog({ minWidth: 350, maxHeight: 400, autoOpen: false, resizable: false });
		
		<c:forEach var="regiao" items="${usuario.regioes}">
		addRegioes('${regiao.id}', '${regiao.nome}');
		</c:forEach>
		
		$("#formUsuarioSenha").jFotoForm({ajax: true, divErro: 'mensagem-erro-senha'}, function (resposta) {
			
			if (resposta.status == 'error') {
				
				$('#mensagem-erro-senha').find('h6').html(resposta.msgErro);
				$('#mensagem-erro-senha').show();
			} else {

				$('#message-success-senha').find('h6').html(resposta.msgSucesso);
				$('#message-success-senha').show();	
				$('#mensagem-erro-senha').hide();	
				$('#dialogSenha').dialog('close');		

				$(window).trigger('resize');
			}
			
			return false;
		});
		
	});
	
		
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

	<div id="dialogRegiao" title="Selecionar região">
	
		<form id="formRegiao">
		
		<div class="form">
		
			<input type="hidden" id="regioesSel" name="regioesSel" value="" />
			<div class="fields">					
				<div class="field">						
					<div id="listaRegiaoDiv">		
					
						<input type="text" id="filtroRegiao" name="filtro" value="" style="width: 100%;" onkeyup="filtraRegioes();" />	
								
						<ul id="listaRegioes">
							
						</ul>
					</div>
												
					<div id="mapaDivRegiao" style="width: 100%;">
						<div id="mapaRegiao" style="height: 420px; padding: 7px;"></div>
					</div>			  				
				</div>
				
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="selecionar regiao" />
					</div>
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="fechaRegiao();" />
				</div>
				
			</div>
		</div>
		
		</form>
	</div>
	
		<div id="dialogSenha" title="Alterar senha">

		<div id="mensagem-erro-senha" class="messages" style="display: none;">
			<div class="message message-error">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/error.png" />
				</div>
				<div class="text">
					<h6 id="msgErrorSenha">Ocorreram os seguintes erros</h6>	
				</div>
			</div>
		</div>
	
		<form id="formUsuarioSenha" action="../../../controle_geral/usuarios/senha/" method="post">
		
			<input type="hidden" name="id" value="${usuario.id}" />
			
			<div class="form">

			<div class="fields">

				<div class="field">
					<div class="label">
						<label for="input-medium">Senha atual:</label>
					</div>
					<div class="input">
						<input id="senhaAtual" name="senhaAtual" type="password" value="" style="width: 310px;"/>							  		
					</div>  				
				</div>
		
				<div class="field">
					<div class="label">
						<label for="input-medium">Nova senha</label>
					</div>
					<div class="input">
						<input id="senha01" name="senha01" type="password" value="" style="width: 310px;"/>							  		
					</div>  				
				</div>
		
				<div class="field">
					<div class="label">
						<label for="input-medium">Nova senha confirmação</label>
					</div>
					<div class="input">
						<input id="senha02" name="senha02" type="password" value="" style="width: 310px;"/>							  		
					</div>  				
				</div>
							
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="alterar senha" />
					</div>				
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="closeDialogSenha();" />							
				</div>
			</div>
			</div>
		</form>			
	</div>
	
</jsp:attribute>

<jsp:body>

	<div id="conteudo" class="box box-inline">
		
		<div class="title">
			<h5>Cadastrar usuário</h5>
		</div>

		<jsp:include page="../includes/mensagens.jsp" />
	
		<form id="formUsuario" action="../../../controle_geral/usuarios/adicionar/" method="post" AUTOCOMPLETE="OFF">
			
			<input type="hidden" name="usuario.id" value="${usuario.id}"/>
			
			<div class="form">
				<div class="fields">
				
					<div class="field">
						<div class="label">
							<label for="select">Matricula:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Matricula do usuário">?</div></label>
						</div>
											
						<div class="input">
							<input class="medium${erroMatricula eq true ? ' error' : ''}" type="text" name="usuario.matricula" value="${usuario.matricula}" validate="vazio" />
						</div>				
					</div>
								
					<div class="field">
						<div class="label">
							<label for="input-medium">Nome:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Nome do usuário">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroNome eq true ? ' error' : ''}" type="text" name="usuario.nome" value="${usuario.nome}" validate="vazio"/>
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="input-medium">Login:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Login">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroLogin eq true ? ' error' : ''}" type="text" name="usuario.login" value="${usuario.login}" validate="vazio"/>
						</div>
					</div>
<c:if test="${usuario.id == null || usuario.id <= 0}">
					<div class="field">
						<div class="label">
							<label for="input-medium">Senha:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Senha">?</div></label>
						</div>
						<div class="input">
							<input id="senha" class="medium${erroSenha eq true ? ' error' : ''}" type="password" name="usuario.senha" value="${usuario.senha}" validate="vazio" tmMinimo="5" />
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="input-medium">Senha Novamente:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Senha novamente">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroSenha eq true ? ' error' : ''}" type="password" name="senha_novamente" value="" validate="senha" campoConfere="senha" />
						</div>
					</div>
</c:if>								
					<div class="field">
						<div class="label">
							<label for="input-medium">Cpf:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Cpf do usuário">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroCpf eq true ? ' error' : ''}" id="cpfUsuario" type="text" name="usuario.cpf" value="${usuario.cpf}" validate="cpf"/>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="input-medium">Descricao:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroDescricao eq true ? ' error' : ''}" type="text" name="usuario.descricao" value="${usuario.descricao}" validate="vazio"/>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="select">Status:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Status do usuário">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="usuario.status.id">
								<c:forEach var="status" items="${listaStatus}">
									<option value="${status.id}" ${usuario.status.id == status.id ? 'selected="selected"' : ''}>${status.valor}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="select">Grupo:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Grupo do usuário">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="usuario.grupo.id">
								<c:forEach var="grupo" items="${listaGrupos}">
									<option value="${grupo.id}" ${usuario.grupo.id == grupo.id ? 'selected="selected"' : ''}>${grupo.nome}</option>
								</c:forEach>
							</select>
						</div>
					</div>
										
					<div class="field">
						<div class="label">
							<label for="select">Região:<div class="ajuda" title="Selecionar região que o usuário ira monitorar">?</div></label>
						</div>
						<div class="select">				
							<div style="margin:10px 0 0 0px;">
								<input type="hidden" id="regiaoCampo" name="regiao.id" value="${regiao.id}"/>	
								<div id="Regioes" style="display: inline;"></div>								
								<div id="addRegiao" <c:if test="${regiao.id != null}"> style="display: none;"</c:if> style="margin: 10px 0 0 0px; ">
									<a href="javascript:;" onclick="showRegiaoDialog();">Selecionar Região</a><br /><br />									
								</div>
								<div id="adicionarRegião">
								
								</div>								
							</div>						
						</div>
					</div>					
					
													
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${usuario.id == null ? 'adicionar' : 'alterar'}" />
						</div>
<c:if test="${usuario.id != null || usuario.id > 0}">						
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="alterar senha" onclick="showDialogSenha();" />				
</c:if>						
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
