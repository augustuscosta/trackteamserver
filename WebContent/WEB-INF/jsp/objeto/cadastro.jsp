<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/style.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/ccpb.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">
	
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 
	
</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">
		
		
	function inicio() {
		
		parent.location='../listar/';
	}

	$(document).ready(function() {

		$("#formobjeto").jFotoForm();
		
	});
		
</script>

</jsp:attribute>

<jsp:body>

	<div id="conteudo" class="box box-inline">
		
		<div class="title">
			<h5>Cadastrar objeto</h5>
		</div>

		<jsp:include page="../includes/mensagens.jsp" />
	
		<form id="formobjeto" action="../../../controle_geral/objeto/adicionar/" method="post">
			
			<input type="hidden" name="objeto.id" value="${objeto.id}"/>
			
			<div class="form">
				<div class="fields">
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Descricao:<div class="ajuda" title="Descricao do objeto">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroDescricao eq true ? ' error' : ''}" type="text" name="objeto.descricao" value="${objeto.descricao}" />
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Controle:<div class="ajuda" title="Numero de controle interno do objeto">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroDescricao eq true ? ' error' : ''}" type="text" name="objeto.numero_controle_interno" value="${objeto.numero_controle_interno}" />
						</div>
					</div>
	
	                <div class="field">
						<div class="label">
							<label for="select">Material:<div class="ajuda" title="Material do objeto">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="objeto.materiais.id">
								<c:forEach var="materiais" items="${listaMateriaisObjeto}">
									<option value="${materiais.id}" ${objeto.materiais.id == materiais.id ? 'selected="selected"' : ''}>${materiais.titulo}</option>
								</c:forEach>
							</select>
						</div>
					</div>
	
					<div class="field">
						<div class="label">
							<label for="select">Estado:<div class="ajuda" title="Estado do objeto">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="objeto.estado.id">
								<c:forEach var="dominio" items="${listaEstadoObjeto}">
									<option value="${dominio.id}" ${objeto.estado.id == dominio.id ? 'selected="selected"' : ''}>${dominio.valor}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${objeto.id == null ? 'adicionar' : 'alterar'}" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
