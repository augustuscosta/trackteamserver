<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="${contexto}/css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="${contexto}/css/jquery-autocomplete.css" rel="stylesheet" />
	

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="${contexto}/js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="${contexto}/js/jFoto/jTable.js"></script> 
	<script type="text/javascript" src="${contexto}/js/jFoto/jDialogSelect/jDialogSelect.js"></script>  
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false"></script>	
	<script type="text/javascript" src="${contexto}/js/gmap3.js"></script> 
	<script type="text/javascript" src="${contexto}/js/jquery-autocomplete.js"></script> 	

</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	var latlngMapa = new google.maps.LatLng('${usuarioSession.regioes[0].centro.latitude}','${usuarioSession.regioes[0].centro.longitude}');
	var map = "";

	var listaViaturas = {};
	var listaAgentes = {};
	var marcador = "";
	var endereco = {};
	
	var agente;
	var veiculo;
	<c:if test="${agenteId != null}">agente = ${agenteId};</c:if>	
	<c:if test="${viaturaId != null}">veiculo = ${viaturaId};</c:if>
	

	endereco.latitude = latlngMapa.lat();
	endereco.longitude = latlngMapa.lng();
	
	var imageOcorrencia = new google.maps.MarkerImage(
		"${contexto}/images/icones_mapa/ocorrencia_amarelo.png",
		new google.maps.Size(78,80),
		new google.maps.Point(0, 0),
		new google.maps.Point(32,74)
	);
	
	$("#mapa").ready( function() { 
		
		$("#mapa").gmap3({ 
			action:'init',
			options:{
				center:[latlngMapa.lat(),latlngMapa.lng()],
				zoom: 14
			},
			events:{
	       		click: function(marker, event, data){
	       			atualizaMarcador(event.latLng);
	       		}
	       	}	
		});
		
		map = $('#mapa').gmap3('get');
		
		$('#endereco').autocomplete({
			source: function() {
			    $("#mapa").gmap3({
			    	action:'getAddress',
			      	address: $(this).val(),
			      	callback:function(results){
			        	if (!results) return;
			        	$('#endereco').autocomplete('display', results, false);
			      	}
			    });
			},
			cb:{
				cast: function(item){
				  return item.formatted_address;
				},
				select: function(item) {
					
					atualizaMarcador(item.geometry.location);
					map.setCenter(item.geometry.location);
					
				}
		 	}
		});
	});
	
	function atualizaMarcador(latLng) {
	
		if (marcador == null || marcador == "") {
			marcador = new google.maps.Marker({
			    position: latLng,
			    map: map,
			    icon: imageOcorrencia,
			    draggable: true
			});
			
			google.maps.event.addListener(marcador, 'dragend', function(event) {
				
				findAddress(event.latLng);		
			});
			
		} else {
			
			marcador.setPosition(latLng);
		}
		
		findAddress(latLng);
	}
	
	function findAddress(latLng) {
		
		var geocoder = new google.maps.Geocoder();
	    geocoder.geocode({latLng: latLng}, function(results, status) {
	    	if (status == google.maps.GeocoderStatus.OK) {
	        	if (results[0]) {
	          		$('#endereco').val(results[0].formatted_address); 
	          		
	          		endereco.numero = results[0].address_components[0].long_name;          		
	          		endereco.logradouro = results[0].address_components[1].long_name;
	          		endereco.bairro = results[0].address_components[2].long_name;    
	          		endereco.latitude = latLng.lat();
	          		endereco.longitude = latLng.lng();
	        	}
	    	}
	    });
	}
	
	function getViaturas() {
	
		$.ajax({
			type : "POST",
			url : "${url}/veiculo/listaVeiculos",
			success : function(resposta) {
	
				json = resposta.result;
				listaViaturasTemp = {};
				div = "";
				
				for (x in json) {
					
					listaViaturasTemp[json[x].id] = json[x].idOrgao+ ' - '+ json[x].placa; 
					listaViaturas[json[x].id] = json[x]; 
				}
								
				$('#selecionaViatura').jDialogSelect({
					lista: listaViaturasTemp,
					url: '${url}/js/jFoto/jDialogSelect'}, function(id) { selecionaViatura(id); });		

				if (veiculo != null) {
					selecionaViatura(veiculo);										
				}
			}
		});
	}

	function getAgentes() {
		
		$.ajax({
			type : "POST",
			url : "${url}/agente/listaAgentes",
			success : function(resposta) {
	
				json = resposta.result;
				listaAgentesTemp = {};
				div = "";
				
				for (x in json) {
					
					listaAgentesTemp[json[x].id] = json[x].matricula + ' - ' + json[x].nome; 
					listaAgentes[json[x].id] = json[x]; 
				}
								
				$('#selecionaAgente').jDialogSelect({
					lista: listaAgentesTemp,
					url: '${url}/js/jFoto/jDialogSelect'}, function(id) { selecionaAgente(id); });
				
				if (agente != null) {
					selecionaAgente(agente);										
				}
			}
		});
	}

	function selecionaViatura(id) {

		$('#ViaturaNome').html(listaViaturas[id].idOrgao + ' - ' + listaViaturas[id].modelo + '<img style="margin-left: 20px; cursor: pointer;" onclick="zeraSelecaoViaturas();" src="${contexto}/images/ico_excluir.gif" />');
		$('#viaturaId').val(id);
		$('#addviatura').hide(300);
		$('#agenteSel').hide(300);
		
		pesquisaMateriais('${contexto}/materiais/listaObjetos/veiculo/' + id);
	}

	function zeraSelecaoViaturas() {
		$('#ViaturaNome').html('');
		$('#viaturaCampo').val('');
		$('#addviatura').show();
		$('#agenteSel').show();

	}

	function selecionaAgente(id) {

		$('#agenteNome').html(listaAgentes[id].matricula + ' - ' + listaAgentes[id].nome + '<img style="margin-left: 20px; cursor: pointer;" onclick="zeraSelecaoAgentes();" src="${contexto}/images/ico_excluir.gif" />');
		$('#agenteId').val(id);
		$('#addAgente').hide(300);
		$('#viaturaSel').hide(300);

		pesquisaMateriais('${contexto}/materiais/listaObjetos/agente/' + id);		

	}

	function zeraSelecaoAgentes() {
		$('#agenteNome').html('');
		$('#agenteId').val('');
		$('#addAgente').show();
		$('#viaturaSel').show();

	}

	function addEndereco() {
		
		$('#enderecoGeo').val($('#endereco').val());
		
		$('#enderecoNumero').val(endereco.numero);
		$('#enderecoRua').val(endereco.logradouro);
		$('#enderecoLat').val(endereco.latitude);
		$('#enderecoLng').val(endereco.longitude);
		$('#enderecoBairro').val(endereco.bairro);
	  		
		$("#dialogMapa").dialog('close');	
		return false;
	}

	function showMap() {
		
		if ($('#enderecoLat').val() != "" && $('#enderecoLng').val() != "" && isNaN($('#enderecoLat').val()) && isNaN($('#enderecoLng').val()) && $('input[name="ocorrencia.id"]').val() != "") {
			
			var latLng = new google.maps.LatLng($('#enderecoLat').val(), $('#enderecoLng').val());
			console.log(latLng);
			
			if (marcador == null || marcador == "") {
				
				marcador = new google.maps.Marker({
				    position: latLng,
				    map: map,
				    icon: imageOcorrencia,
				    draggable: true
				});
				
				google.maps.event.addListener(marcador, 'dragend', function(event) {
					
					findAddress(event.latLng);		
				});
				
			} else {
				
				marcador.setPosition(latLng);
			}
	
			findAddress(latLng);
			
		} else {
			
			latLng = latlngMapa;
			
		}
		
		$("#dialogMapa").dialog('open');
		google.maps.event.trigger(map, 'resize');
		
		map.panTo(latLng);
//		map.panTo(new google.maps.LatLng(parseFloat($('#enderecoLat').val()), parseFloat($('#enderecoLng').val())));
		
	}
	
	function closeMap() {
	
		$("#dialogMapa").dialog('close');
	}
	
	function pesquisaMateriais(url) {
		
		$("#lista").jTable({
			url: url,
			cabecalho: [
				{'titulo': 'ID', 'width': 'auto', 'align': 'left', 'campo': 'id' },	
				{'titulo': 'Material', 'width': 'auto', 'align': 'left', 'campo': 'material' },
				{'titulo': 'Controle', 'width': 'auto', 'align': 'left', 'campo': 'controle' },				
				{'titulo': 'Descricao', 'width': 'auto', 'align': 'left', 'campo': 'descricao' },
				{'titulo': 'Estado', 'width': 'auto', 'align': 'left', 'campo': 'estado' },
				{'width': '20px', 'align': 'left', 'campo': 'id', 'tipo':'checkbox' }
			],
			opcoesTitulo: 'Opções',
			campoRef: 'id'
		});
		
	}
	
	$(document).ready(function() {

		pesquisaMateriais('${contexto}/materiais/listaObjetos/');		
		
		getViaturas();
		getAgentes();

		$("#dialogMapa").dialog({ minWidth: 900, autoOpen: false, resizable: false });	
		
		$('#enderecoGeo').focus( function() {			
			showMap();											
		});			
	
		$('#formAddEndereco').submit(function() {
			return false;
		});	
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

	<div id="dialogMapa" title="Selecionar endereço">
	
		<form id="formAddEndereco" action="">
	
			<div class="form">
				<div class="fields">
						
					<div class="field">
						<div class="label">
							<label for="input-medium">Endereço:</label>
						</div>
						<div class="input">
							<input id="endereco" type="text" style="background-color: #FFF;" />							  		
						</div>
						
						<div id="mapaDiv">
							<div id="mapa" style="height: 100%; padding: 7px;"></div>
						</div>			  				
	
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar" onclick="addEndereco();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="closeMap();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>
	
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Deixar material</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		
		<form id="formobjeto" action="${contexto}/controle_geral/objeto/ocorrencia/adicionar/" method="post">
			
			<input type="hidden" id="viaturaId" name="viaturaId" value=""/>
			<input type="hidden" id="agenteId" name="agenteId" value=""/>
				
			<div class="form">
				<div class="fields">
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Endereço:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Endereço da atividade">?</div></label>
						</div>
						<div class="input">
							<input id="enderecoGeo" class="medium${erroEndereco eq true ? ' error' : ''}" type="text" name="ocorrencia.endereco.endGeoref" value="${ocorrencia.endereco.endGeoref}" />
		    				<img class="bnt_mapa" src="${contexto}/images/icones/map.png" width="28px;" height="28px;" onclick="showMap();" />
		    				<input type="hidden" id="enderecoId" name="ocorrencia.endereco.id" value="${ocorrencia.endereco.id}" />
		    				<input type="hidden" id="enderecoNumero" name="ocorrencia.endereco.numero" value="${ocorrencia.endereco.numero}" />
		    				<input type="hidden" id="enderecoRua" name="ocorrencia.endereco.logradouro" value="${ocorrencia.endereco.logradouro}" validate="vazio" msgErro="endereço inválido" divDestaque="enderecoGeo" />
		    				<input type="hidden" id="enderecoLat" name="ocorrencia.endereco.latitude" value="${ocorrencia.endereco.latitude}" />
		    				<input type="hidden" id="enderecoLng" name="ocorrencia.endereco.longitude" value="${ocorrencia.endereco.longitude}" />	    				
		    				<input type="hidden" id="enderecoBairro" name="ocorrencia.endereco.bairro" value="${ocorrencia.endereco.bairro}" />
						</div>					
					</div>
					
					<div class="field" id="viaturaSel">
						<div class="label">
							<label for="select">Viatura:<div class="ajuda" title="Selecione a viatura que deverá ser criado o relatório">?</div></label>
						</div>
						<div class="select">				
							<div style="margin: 10px 0 0 0px;">
		
								<div id="ViaturaNome" style="display: inline;"></div>
								
								<div id="addviatura">
									<a href="javascript:;" id="selecionaViatura">selecione a Viatura</a><br /><br />
								</div>
							</div>						
						</div>
					</div>
	
					<div class="field" id="agenteSel">
						<div class="label">
							<label for="select">Agente:<div class="ajuda" title="Selecione o agente que deverá ser criado o relatório">?</div></label>
						</div>
						<div class="select">				
							<div style="margin: 10px 0 0 0px;">
		
								<div id="agenteNome" style="display: inline;"></div>
		
								<div id="addAgente">
									<a href="javascript:;" id="selecionaAgente">selecione o Agente</a><br /><br />
								</div>
							</div>						
						</div>
					</div>
						
					<div class="field">
						<div class="label">
							<label for="input-medium">Resumo:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Resumo da atividade. Será mostrado na aba das ocorrências.">?</div></label>
						</div>
						<div class="input">
							<input style="display: block;" class="medium" type="text" name="ocorrencia.resumo" value="${ocorrencia.resumo}" validate="vazio"/>
						</div>
					</div>
			
					<div class="field">
						<div class="label">
							<label for="input-medium">Detalhes:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Destalhes da atividade">?</div></label>
						</div>
						<div class="">
							<textarea class="textarea-medium" name="ocorrencia.descricao">${ocorrencia.descricao}</textarea>
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="input-medium">Materiais:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Materiais que foram deixados no local">?</div></label>
						</div>
						<div style="margin-left: 185px;">
							<div id="lista"></div>
						</div>
					</div>

					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="associar" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>
																								
				</div>
			</div>
		</form>
	</div>
	
</jsp:body>

</layout:controleGeral>
