<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/style.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/ccpb.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">
	
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 
	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jTable.js"></script> 
	
</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">
		
		
		function inicio() {
			
			parent.location='../../../controle_geral/objeto/listarAssociados/';
		}
		
		function removeObjetoAssociado(id, nome) {
			
			var answer = confirm("Deseja realmente excluir a associacao do objeto " + nome + "?");
			if (answer) {				
				alert('apagou');
			}
		}
		
		$(document).ready(function() {

			$("#lista").jTable({
				url: '../../../controle_geral/objeto/listaObjetosAssociados/',
				cabecalho: [
					{'titulo': 'ID', 'width': 'auto', 'align': 'left', 'campo': 'id' },	
					{'titulo': 'Material', 'width': 'auto', 'align': 'left', 'campo': 'material' },
					{'titulo': 'Controle', 'width': 'auto', 'align': 'left', 'campo': 'controle' },				
					{'titulo': 'Descricao', 'width': 'auto', 'align': 'left', 'campo': 'descricao' },
					{'titulo': 'Estado', 'width': 'auto', 'align': 'left', 'campo': 'estado' }					
				],
				opcoesTitulo: 'Opções',
				editar: false,
				remover: true,				
				funcaoRemover: 'removeObjeto',
				campoRef: 'id',
				campoTituloRef: 'descricao',				
				iconExcluir: '../../../images/ico_excluir.png'
			});
		});
				
	</script>

</jsp:attribute>

<jsp:body>

	<div id="conteudo" class="box box-inline">
		
		<div class="title">
			<h5>Associar objeto</h5>
		</div>

		<jsp:include page="../includes/mensagens.jsp" />
	
		<form id="formAssociar" action="../../../controle_geral/objeto/associar/" method="post">
			
			<input type="hidden" name="objeto.id" value="${objeto.id}"/>
			
			<div class="form">
				<div class="fields">
					
					<div class="field">
						<div class="label">
							<label for="select">Funcionalidade:<div class="ajuda" title="Funcionalidade">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="funcionalidade">
								<c:forEach var="dominio" items="${listaFuncionalidade}">
									<option value="${dominio.id}" ${funcionalidade == dominio.id ? 'selected="selected"' : ''}>${dominio.valor}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Material:<div class="ajuda" title="Material">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="materiais">
								<c:forEach var="arraymateriais" items="${listaMateriais}">
									<option value="${arraymateriais.id}" ${funcionalidade == arraymateriais.funcionalidade.id ? 'selected="selected"' : ''}>${arraymateriais.titulo}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Objeto:<div class="ajuda" title="Objeto">?</div></label>
						</div>
						<div class="select">
							<select id="select" name="objeto">
								<c:forEach var="arrayobjeto" items="${listaObjeto}">
									<option value="${arrayobjeto.id}" ${materiais == arrayobjeto.materiais.id ? 'selected="selected"' : ''}>${arrayobjeto.descricao}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="associar" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>

				</div>	
			</div>
		</form>
		
		<div class="table">
			<div id="lista"></div>
		</div>
				
	</div>

</jsp:body>

</layout:controleGeral>
