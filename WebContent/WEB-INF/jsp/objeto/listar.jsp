<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="${contexto}/css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="${contexto}/js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="${contexto}/js/jFoto/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	function removeObjeto(id, nome) {
		
		var answer = confirm("Deseja realmente excluir o objeto " + nome + " ?");
		if (answer) {
			
			alert('apagou');
		}
	}

	$(document).ready(function() {

		$("#lista").jTable({
			url: '${contexto}/controle_geral/objeto/listaObjeto/',
			cabecalho: [
				{'titulo': 'ID', 'width': 'auto', 'align': 'left', 'campo': 'id' },	
				{'titulo': 'Material', 'width': 'auto', 'align': 'left', 'campo': 'material' },
				{'titulo': 'Controle', 'width': 'auto', 'align': 'left', 'campo': 'controle' },				
				{'titulo': 'Descricao', 'width': 'auto', 'align': 'left', 'campo': 'descricao' },
				{'titulo': 'Estado', 'width': 'auto', 'align': 'left', 'campo': 'estado' }
			],
			opcoesTitulo: 'Opções',
			editar: true,
			remover: true,
			linkEditar: '${contexto}/controle_geral/objeto/editar/',
			funcaoRemover: 'removeObjeto',
			campoRef: 'id',
			campoTituloRef: 'descricao',
			iconEditar: '${contexto}/images/ico_alterar.png',
			iconExcluir: '${contexto}/images/ico_excluir.png'
		});
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Lista Objeto</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		
		<div class="table">
<div id="lista"></div>
		</div>
	</div>
	
</jsp:body>

</layout:controleGeral>
