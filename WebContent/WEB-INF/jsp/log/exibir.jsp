<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:pagina keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../js/jFoto/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">
		
		$(document).ready(function() {
					
		});
	
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Detalhes do Log</h5>
		    <ul class="links">
				<li class="ui-state-active">
					<a href="${contexto}/log/listar">listar</a>
				</li>
				<li class="ui-state-active ui-tabs-selected">
					<a href="${contexto}/log/exibir">exibir</a>
				</li>				
		    </ul>			
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		<br />

		<div class="form">
			<div class="fields">				

				<div class="field">
					<div class="label">
						<label for="select">Controller:</label>
					</div>
					<p style="margin-top: 0px;">${log.controller}</p>
				</div>

				<div class="field">
					<div class="label">
						<label for="select">Método:</label>
					</div>
					<p style="margin-top: 0px;">${log.tipo}</p>
				</div>

				<div class="field">
					<div class="label">
						<label for="select">Descrição:</label>
					</div>
					<p style="margin-top: 0px;">${log.descricao}</p>
				</div>				
<c:if test="${log.valorNovo != null}">
				<div class="field">
					<div class="label">
						<label for="select">Valores novos:</label>
					</div>
					<p style="margin-top: 0px; margin-left: 185px;">${log.valorNovo}</p>
				</div>
</c:if>
<c:if test="${log.valorAntigo != null}">
				<div class="field">
					<div class="label">
						<label for="select">Valores antigos:</label>
					</div>
					<p style="margin-top: 0px; margin-left: 185px;">${log.valorAntigo}</p>
				</div>
</c:if>					
				<div class="field">
					<div class="label">
						<label for="select">Data:</label>
					</div>
					<p style="margin-top: 0px;">${log.dataCriacao}</p>
				</div>
					
			</div>
		</div>	
	</div>
		
</jsp:body>

</layout:pagina>