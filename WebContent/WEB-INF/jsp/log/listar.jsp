<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:pagina keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../js/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">
		
		function criaTabela(){
			
			$("#lista").jTable({
				url: 'listarJSON/',
				cabecalho: [
					{'titulo': 'Id', 'width': 'auto', 'align': 'left', 'campo': 'id' },        
					{'titulo': 'Controller', 'width': 'auto', 'align': 'left', 'campo': 'controller' },
					{'titulo': 'Método', 'width': 'auto', 'align': 'left', 'campo': 'tipo' },
					{'titulo': 'Descrição', 'width': 'auto', 'align': 'left', 'campo': 'descricao' },
					{'titulo': 'Id usuário', 'width': 'auto', 'align': 'center', 'campo': 'usuario.id' },
					{'titulo': 'Data', 'width': 'auto', 'align': 'center', 'campo': 'dataCriacao' }
				],
				opcoesTitulo: 'Opções',
				campoRef: 'id',
				campoTituloRef: 'controller',
				opcoes: [{'icon' : '../images/icones/log_icon.png', 'link':'exibir/','funcao': '', 'alt':'editar', 'params': ['id']}]			
			});	
		} 
		
		$(document).ready(function() {
		
			parametrosDataPicker = {	dateFormat: 'dd-mm-yy',
				dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
				dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
				dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
				monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',
					'Outubro','Novembro','Dezembro'],
				monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
				nextText: 'Próximo',
				prevText: 'Anterior'
			}
			
			$("#inicio").datepicker(parametrosDataPicker);
			$("#fim").datepicker(parametrosDataPicker);
			
			$('#pesquisar').click(function() {
				
				var inicio = $('#inicio').val();
				var fim = $('#fim').val();
				var filtro = $('#filtro').val();
				
				var url = 'listarJSON';
				if (inicio != null && inicio != "") {
					
					url = url + "/i/" + inicio; 
					
					if (fim != null && fim != "") {
					
						url = url + "/f/" + fim;
					
					}
				}	
				
				if (filtro != null && filtro != "") {
					
					url = url + "/" + filtro;
				}
				
				var parametros = $("#lista").getParametros();
				parametros['url'] = url + "/";
				$("#lista").setParametros(parametros);
				$("#lista").refresh();
			});
			
			criaTabela();
						
		});
	
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Lista de logs</h5>
		    <ul class="links">
				<li class="ui-state-active ui-tabs-selected">
					<a href="listar">listar</a>
				</li>
		    </ul>			
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		<br />

		<div class="table">
		
			<form id="formUsuario" action="${url}/log/filtrar" method="post" style="margin-bottom: 30px;">
		
				<div class="inputSubDiv">
					<div style="display: block; margin-bottom: 5px;"><b>Data início</b></div>
					<input class="subDiv" id="inicio" name="inicio" type="text" style="width:100px; margin-top: 2px;" />
				</div>
				<div class="inputSubDiv">
					<div style="display: block; margin-bottom: 5px;"><b>Data fim</b></div>
					<input class="subDiv" id="fim" name="fim" style="width: 100px; margin-top: 2px;" type="text" />
				</div>
				<div class="inputSubDiv">
					<div style="display: block; margin-bottom: 5px;"><b>Filtro para pesquisa</b></div>
					<input class="subDiv" name="filtro" id="filtro" style="width: 400px; margin-top: 2px;" type="text" />
				</div>

				<div class="form" style="padding: 0; clear: none;">
					<div class="fields">
						<div class="buttons" style="margin: 23px 0 0 20px;">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" id="pesquisar" value="filtrar" />
						</div>										
					</div>
				</div>
										
			</form>
					
			<div id="lista" style="clear: both;"></div>
		</div>
	</div>
		
</jsp:body>

</layout:pagina>