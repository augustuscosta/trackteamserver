<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../js/jFoto/jDropDown.js"></script> 
	<script type="text/javascript" src="../../js/jFoto/jFotoForm.js"></script> 

	<script type="text/javascript" src="../../js/jQuery/sortable/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../../js/jQuery/sortable/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../../js/jQuery/sortable/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="../../js/jQuery/sortable/jquery.ui.draggable.js"></script>
	<script type="text/javascript" src="../../js/jQuery/sortable/jquery.ui.sortable.js"></script>
	
</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	function showAddMenu() {
	
		$('#formAddMenu')[0].reset();
		$("#dialogAddMenu").dialog('open');
	}
	
	function updateMenu(json) {
		
		var outTxt = "";
		outTxt = outTxt + '<li class="menuDrop">' + '\n';
		outTxt = outTxt + '	<a href="" class="menuNome">' + $('#formMenu_id').val() + '</a>' + '\n';
		outTxt = outTxt + '	<div class="menuBotoes">' + '\n';
		outTxt = outTxt + '		<a href="javascript:;"><img src="../../images/ico_alterar.png"></a>' + '\n';
		outTxt = outTxt + '		<a href="javascript:;" style="margin-left: 10px;"><img src="../../images/ico_excluir.png"></a>' + '\n';
		outTxt = outTxt + '	</div>' + '\n';
		outTxt = outTxt + '</li>' + '\n';
		
		$('#menuDrop').append(outTxt);
		$('#menuDrop').jDropDown();
		
	}

	$(document).ready(function() {

		$('#menuDrop').jDropDown();
		
		$("#dialogAddMenu").dialog({ minWidth: 810, maxHeight: 400, autoOpen: false, resizable: false });	

		$("#formAddMenu").jFotoForm({
			ajax: true,
			divErro: 'formAddMenuErroMsg'
		}, function(json) {
			
			updateMenu(json);
			return false;
		});		
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

	<div id="dialogAddMenu" title="Adicionar opção no menu">

		<div id="formAddMenuErroMsg" class="messages" style="display: none;">
			<div class="message message-error">
				<div class="image">
					<img height="32" alt="Error" src="../../images/icones/error.png" />
				</div>
				<div class="text">
					<h6>Ocorreram os seguintes erros</h6>	
				</div>
			</div>
		</div>
		
		<form id="formAddMenu" action="../../controle_geral/opcoes/menu/adicionar/" method="post">

		<div class="form" style="clear: both;">

			<div class="fields">
			
				<input class="medium" type="hidden" name="menu.id" value="${menu.id}" />
				
				<div class="field">
					<div class="label">
						<label for="select">Nome:</label>
					</div>
										
					<div class="input">
						<input id="formMenu_id" class="medium" type="text" name="menu.nome" value="${menu.nome}" validate="vazio" />
					</div>				
				</div>

				<div class="field">
					<div class="label">
						<label for="select">Link:</label>
					</div>
										
					<div class="input">
						<input class="medium" type="text" name="menu.link" value="${menu.link}" validate="vazio" />
					</div>				
				</div>

				<div class="field">
					<div class="label">
						<label for="select">Role:</label>
					</div>
										
					<div class="input">
						<input class="medium" type="text" name="menu.role" value="${menu.role}" validate="vazio" />
					</div>
							
				</div>

				<div class="field">
					<div class="label">
						<label for="select">Tag:</label>
					</div>
										
					<div class="input">
						<input class="medium" type="text" name="menu.tag" value="${menu.tag}" validate="vazio" />
					</div>				
				</div>

				<div class="field">
					<div class="label">
						<label for="select">Status:</label>
					</div>

					<div class="select">
						<select id="select" name="menu.ativo">
							<option value="1" ${agente.usuario.status.id == true ? 'selected="selected"' : ''}>Ativo</option>
							<option value="0" ${agente.usuario.status.id == false ? 'selected="selected"' : ''}>Inativo</option>
						</select>
					</div>
																			
				</div>
																										
				<div class="buttons" style="margin-left: 0;">
					<div class="highlight">
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar" />
					</div>
					<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="$('#dialogAddMenu').dialog('close');" />
				</div>
			</div>
		</div>

	</div>

</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Configuração de Menu</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		
		<ul id="menuDrop">
<c:forEach var="menu" items="${menuQuick}">
			<li class="menuDrop">
				<a href="" class="menuNome">${menu.nome}</a>
				<div class="menuBotoes">
					<a href="javascript:;"><img src="../../images/ico_alterar.png"></a>
					<a href="javascript:;" style="margin-left: 10px;"><img src="../../images/ico_excluir.png"></a>
				</div>
				
	<c:if test="${!empty menu.listaSubMenu}">			
				<ul id="submenu-${menu.id}" class="submenu">
		<c:forEach var="subMenu" items="${menu.listaSubMenu}">
					<li>
						<a href="">${subMenu.nome}</a>
						<div class="menuBotoes">
							<a href="javascript:;"><img src="../../images/ico_alterar.png"></a>
							<a href="javascript:;" style="margin-left: 10px;"><img src="../../images/ico_excluir.png"></a>
						</div>
											
			<c:if test="${!empty subMenu.listaSubMenu}">			
						<ul id="submenuIn-${subMenu.id}" class="submenuIn">
				<c:forEach var="subMenuIn" items="${subMenu.listaSubMenu}">
							<li>
								<a href="">${subMenuIn.nome}</a>
								<div class="menuBotoes">
									<a href="javascript:;"><img src="../../images/ico_alterar.png"></a>
									<a href="javascript:;" style="margin-left: 10px;"><img src="../../images/ico_excluir.png"></a>
								</div>
							</li>				
				</c:forEach>
						</ul>
			</c:if>					
					</li>
		</c:forEach>
				</ul>
	</c:if>
			</li>
</c:forEach>		

		</ul>
		
		<a class="addOpcMenu" href="javascript:;" onclick="showAddMenu();">adicionar opção de menu</a>
		
	</div>
	
</jsp:body>

</layout:controleGeral>
