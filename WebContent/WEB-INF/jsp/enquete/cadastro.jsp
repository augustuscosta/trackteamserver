<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false"></script>
	<script type="text/javascript" src="../../../js/gmap3.js"></script> 
	<script type="text/javascript" src="../../../js/jquery-autocomplete.js"></script> 
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script>
	<script type="text/javascript" src="../../../js/jquery.maskedinput.js"></script> 
	
</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">
	
		var idPergunta = 1;
	
	
		function inicio() {
			
			parent.location='../../../controle_geral/enquetes/listar/';
		}
		
		$(document).ready( function() {
			$('.ajuda').tipTip({maxWidth: "auto", edgeOffset: 10});

			$("#formEnquete").jFotoForm();
			

			$(window).trigger('resize');
			
			<c:if test="${empty enquete.perguntas and enquete.id == null}">
				addPergunta(" ", "Texto");
			</c:if>
		
			<c:forEach var="pergunta" items="${enquete.perguntas}">
				addPergunta('${pergunta.pergunta}', '${pergunta.tipoEntrada}');
			</c:forEach>
		});
		
		
		function addPergunta(perguntaTexto, tipoEntrada) {
			var id = idPergunta;
			$('#perguntas').append('<div id="pergunta' + idPergunta 
					+ '" style="margin-top: 10px; display: inline-block;">' 
					+ $('#perguntaModelo').html() + "</div>");				
			var pergunta = $('#pergunta' + idPergunta); 
			pergunta.find('img').click(function() { delPergunta(id); });	
			pergunta.find("input[name='enquete.perguntas[].pergunta']").val(perguntaTexto);	
			pergunta.find("input[name='enquete.perguntas[].tipoEntrada']").val(tipoEntrada);
			idPergunta++;
		}
		
		
		function delPergunta(id) {
			$('#pergunta' + id ).remove();
		}
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
</jsp:attribute>

<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Cadastrar enquete</h5>
		</div>

		<jsp:include page="../includes/mensagens.jsp"/>

		<div class="messages">
			<div id="message-success-senha" class="message message-success" style="display: none;">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/success.png" />
				</div>
				<div class="text">
					<h6></h6>
				</div>
			</div>
		</div>
		
		<div id="perguntaModelo" style="display: none;">
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>Pergunta</b></div>
				<input class="subDiv" name="enquete.perguntas[].pergunta" id="perguntaPergunta" type="text" style="width:500px;"/>
			</div>
			
			<div class="inputSubDiv">
				<div style="display: block; margin-bottom: 5px;"><b>Tipo de dado</b></div>
				<input class="subDiv" name="enquete.perguntas[].tipoEntrada" id="tipoEntradaPergunta" type="text" style="width:85px;"/>
			</div>
			
			<div class="inputSubDiv">
				<img style="margin: 25px 0 0 10px; cursor: pointer;" src="../../../images/ico_excluir.gif" />
			</div>
		</div>
		
		<form id="formEnquete" action="../../../controle_geral/enquetes/adicionar/" method="post">
			<input type="hidden" name="enquete.id" value="${enquete.id}"/>
			
			<div class="form">
				<div class="fields">
				
								
					<div class="field">
						<div class="label">
							<label for="input-medium">Nome:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Nome da enquete">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroNome eq true ? ' error' : ''}" type="text" name="enquete.nome" value="${enquete.nome}" validate="vazio"/>
						</div>
					</div>
					
					
					<div class="field">
						<div class="label">
							<label for="input-medium">Descrição:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Descrição da enquete">?</div></label>
						</div>
						<div class="">
							<textarea class="textarea-medium${erroObservacao eq true ? ' error' : ''}" name="enquete.descricao">${enquete.descricao}</textarea>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Perguntas:<div class="ajuda" title="Pergunta da enquete. Tipos de dado para resposta: texto, sim_nao, data, data_hora, número.">?</div></label>
						</div>									
						
						<div id="perguntas" class="input" style="margin-left: 185px;">
						
						</div>
						
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="addPergunta();"><img src="../../../images/plus.png" /> adicionar pergunta</a>
						</div>
					</div>
					
					
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${enquete.id == null ? 'adicionar' : 'alterar'}" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
