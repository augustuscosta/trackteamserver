<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jFoto/jTable.js"></script> 

</jsp:attribute>

<jsp:attribute name="scripts">

<script type="text/javascript">

	function removeEnquete(id, nome) {
		
		var answer = confirm("Deseja realmente excluir a Enquete " + nome + " ?");
		if (answer) {
			
			$.post( "../excluir/", { 'id': id },
			   		function( resposta ) {

						resposta = eval('(' + resposta + ')');       

						if (resposta.status == 'error') {
							
						} else {
						
							$('#message-success-senha').find('h6').html(resposta.msgSucesso);
							$('#message-success-senha').show();	
							$('#mensagem-erro-senha').hide();
							
							$('#lista').refresh();
							
							$(window).trigger('resize');							
						}
					});		

		}
	}

	$(document).ready(function() {
	
		$("#lista").jTable({
			url: '../../../controle_geral/enquetes/listaEnquetes/',
			cabecalho: [
						{'titulo': 'Nome', 'width': 'auto', 'align': 'left', 'campo': 'nome' }
					],
			opcoesTitulo: 'Opções',
			campoRef: 'id',
			campoTituloRef: 'nome',
			opcoes: [{'icon' : '../../../images/ico_alterar.png', 'link':'../../../controle_geral/enquetes/editar/','funcao': '', 'alt':'editar', 'params': ['id']},
			         {'icon' : '../../../images/ico_excluir.png', 'link':'','funcao': 'removeEnquete', 'ajax':'true', 'alt':'excluir'}]			
		});	
		
	});

</script>

</jsp:attribute>

<jsp:attribute name="dialogs">
</jsp:attribute>

<jsp:body>

	
	<div class="box box-inline">
	
		
		
		<div class="title">
			<h5>Lista enquetes</h5>
			<ul class="links">
				<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
				</li>
		    </ul>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		
		<div class="table">
			<div id="lista"></div>
		</div>
	</div>
	
</jsp:body>

</layout:controleGeral>
