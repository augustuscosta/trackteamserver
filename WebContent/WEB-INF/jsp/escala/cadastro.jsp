<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<layout:controleGeral keywords="" description="" title=".On The Go.">

<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/style.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/jquery-autocomplete.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/ccpb.css" rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

<jsp:attribute name="link_scripts">
	
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script>
	<script type="text/javascript" src="../../../js/jFoto/jFotoForm.js"></script> 
	<script type="text/javascript" src="../../../js/jQuery/jquery-ui-timepicker-addon.js"></script>
	
</jsp:attribute>

<jsp:attribute name="scripts">

	<script type="text/javascript">
	
		var ocorrencias = [];
		var ocorrenciasJSON = [];
		
		function showDialogOcorrencias() {
			getOcorrencias();
			$("#dialogAddOcorrencia").dialog('open');
		}
		
		function closeDialogOcorrencias() {
			
			$("#dialogAddOcorrencia").dialog('close');
		}
		
		function getOcorrencias() {       

			$('#loading').show();		
			
			// Envia o formulário via Ajax
			$.ajax({
			        type: "POST",
			        url: '../../../escala/ocorrencias',
			        success: function( resposta )
			        {
			        	ocorrenciasJSON = resposta.list;   
			        	
			        	plotaOcorrencias(ocorrenciasJSON);
			        	$('#loading').hide();
			        },
			        error:function(XMLHttpRequest, textStatus, errorThrown)
			        {
			        	console.log("algo deu errado");
			        	$('#loading').hide();

			        }
			});		
		};
		
		function filtraOcorrencias() {
			
			var txtBusca = $('#filtroOcorrencia').val().toUpperCase();
			var listaOcorrencias = [];
			
			$('#listaOcorrencias').html('');
			
			for (x in ocorrenciasJSON) {
		
				if(ocorrenciasJSON[x]){
					codigo = ocorrenciasJSON[x].codigo + "";
					if (codigo.toUpperCase().lastIndexOf(txtBusca) >= 0 || ocorrenciasJSON[x].resumo.toUpperCase().lastIndexOf(txtBusca) >= 0) {
						
						listaOcorrencias.push(ocorrenciasJSON[x]);														
					};
				}
			}
			
			plotaOcorrencias(listaOcorrencias);
		}
		
		function addRemoveOcorrenciaSel(id) {
			
			for (x in ocorrencias) {
				
				if (ocorrencias[x].id == id) {
					addRemoveOcorrencia(ocorrencias[x]);
					break;
				}
			}
		}
		
		function addRemoveOcorrencia(marcador) {
			
			if (marcador.selecionado) {
				
				resizeIcon(marcador);
				marcador.selecionado = false;
				$('#ocorrenciaM-' + marcador.id).removeClass("selecionado");
				
			} else {
				
				resizeIcon(marcador, 'big');
				marcador.selecionado = true;
				$('#ocorrenciaM-' + marcador.id).addClass("selecionado");
			}
		}
		
		function addOcorrencias() {
			
			var listaOcorrencias = "";
			
			for (var i=0; i< ocorrencias.length; i++) {
				
				if (ocorrencias[i].selecionado) {
		
					listaOcorrencias += '<li id="ocorrencia-' + ocorrencias[i].id + '">'+ ocorrencias[i].id + ' - ' + ocorrencias[i].resumo + '<img style="margin-left: 20px; cursor: pointer;" onclick="removeOcorrencia(' + ocorrencias[i].id + ');" src="../../../images/ico_excluir.gif" />' + 
						'<input type="hidden" name="escala.ocorrencias[].id" value="' + ocorrencias[i].id + '" /></li>';
				};
			}
			
			$('#listaOcorrenciasCadastro').html(listaOcorrencias);
			closeDialogOcorrencias();
		}
		
		function removeOcorrencia(id) {
			
			$('#ocorrencia-' + id ).remove();

			for (var i=0; i< ocorrencias.length; i++) {
				
				if (ocorrencias[i].id == id) {

					ocorrencias[i].selecionado = false;
				};
			};		
		}
		
		function plotaOcorrencias(listaOcorrencias) {

			var ocorrenciasOld = [];
			
			for (var i=0; i < ocorrencias.length; i++) {
							
				if (ocorrencias[i].selecionado) {	
					ocorrenciasOld.push(ocorrencias[i]);
				};
			}

			$('#listaOcorrencias').html('');	
			ocorrencias = [];
			
			var div = "";
			
			for(var i=0; i < listaOcorrencias.length; i++) {

				var classe = "";
				var selecionado = false;
				
				for (var f=0; f < ocorrenciasOld.length; f++) {
					
					if (ocorrenciasOld[f].id == listaOcorrencias[i].id) {
						
						selecionado = true;
						classe = ' class="selecionado"';
					}
				}
				
				var txt ="";		
				var classeEmAtendimento = "naoEmAtendimento";
				txt = '<div class="' + classeEmAtendimento + '"></div>' + listaOcorrencias[i].codigo + " - " + listaOcorrencias[i].resumo;
				marcador = new Object();
				if (selecionado) {	
					resizeIcon(marcador, 'big');	    			
				} else {	    			
					resizeIcon(marcador);
				};
				
				div += '<li id="ocorrenciaM-' + listaOcorrencias[i].id +'"' + classe + '><a href="javascript:;" onclick="addRemoveOcorrenciaSel(' + listaOcorrencias[i].id + ')">' + txt + '</li>';	
				
				
				marcador.id = listaOcorrencias[i].id;
				marcador.selecionado = selecionado;
				marcador.codigo = listaOcorrencias[i].codigo;
				
				ocorrencias.push(marcador);	
				
			}		

			$('#listaOcorrencias').html(div);
		}
		
		
		function inicio() {
			
			parent.location='../../../controle_geral/escala/listar/';
		}
				
		$(document).ready(function() {
		
			$.datepicker.regional['ru'] = {
					prevText : 'Anterior',
					nextText : 'Próximo',
					monthNames : [ 'Janeiro', 'Fevereiro', 'Março',
							'Abril', 'Maio', 'Junho', 'Julho',
							'Agosto', 'Setembro', 'Outubro',
							'Novembro', 'Dezembro' ],
					monthNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua',
							'Qui', 'Sex', 'Sáb', 'Dom' ],
					dayNames : [ 'Domingo', 'Segunda', 'Terça',
							'Quarta', 'Quinta', 'Sexta', 'Sábado',
							'Domingo' ],
					dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua',
							'Qui', 'Sex', 'Sáb', 'Dom' ],
					dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S',
							'S', 'D' ],
					dateFormat : 'dd/mm/yy'
				};
	
				$.datepicker.setDefaults($.datepicker.regional['ru']);
			
			$('.dataHora').datepicker({
				showSecond : false,
				showMillisec : false,
				timeFormat : 'hh:mm:ss',
				timeText : 'Horário',
				hourText : 'Hora',
				minuteText : 'Minuto',
				secondText : 'Segundo',
				currentText : 'Agora',
				closeText : 'Ok'
			});
			
			$("#dialogAddOcorrencia").dialog({ minWidth: 300, autoOpen: false, resizable: false });
			$('#formAddOcorrencia').submit(function() {
				return false;
			});
			
			<c:forEach var="ocorrencia" items="${escala.ocorrencias}">
				var ocorrencia = Object();
				ocorrencia.id = "${ocorrencia.id}";
				ocorrencia.resumo = "${ocorrencia.resumo}";
				ocorrencia.selecionado = true;
			
				ocorrencias.push(ocorrencia);
			</c:forEach>

			addOcorrencias();
		
		});
	</script>

</jsp:attribute>

<jsp:attribute name="dialogs">

	<div id="dialogAddOcorrencia" title="Adicionar atividades">
	
		<form id="formAddOcorrencia" action="">
	
			<div class="form">
			
				<div class="fields">						
					<div class="field">

						<div id="listaOcorrenciasDiv">		
						
							<input type="text" id="filtroOcorrencia" name="filtro" value="" style="width: 100%;" onkeyup="filtraOcorrencias();" />	
									
							<ul id="listaOcorrencias" class="listaDialogSelect">
								
							</ul>
						</div>
					</div>
					
					<div class="buttons" style="margin-left: 0;">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="adicionar ocorrencias" onclick="addOcorrencias();" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="cancelar" onclick="closeDialogOcorrencias();" />
					</div>
					
				</div>
			</div>	
		</form>		
	</div>
</jsp:attribute>

<jsp:body>

	<div id="loading">
    	<table style="width: 100%; height: 100%;">
    		<tr valign="middle">    			
    			<td align="center">Carregando...<br /><img src="../../../images/ajax-loader.gif" /></td>
    		</tr>
    	</table>    	
    </div>

	<div id="conteudo" class="box box-inline">
		
		<div class="title">
			<h5>Cadastrar escala</h5>
		</div>

		<jsp:include page="../includes/mensagens.jsp" />
	
		<form id="formMateriais" action="../../../controle_geral/escala/adicionar/" method="post">
			
			<input type="hidden" name="escala.id" value="${escala.id}"/>
			
			<div class="form">
				<div class="fields">
					<div class="field">
						<div class="label">
							<label for="input-medium">Nome:<b style="color: #FF0000; font-size: 18px;">*</b><div class="ajuda" title="Nome da escala">?</div></label>
						</div>
						<div class="input">
							<input class="medium${erroNome eq true ? ' error' : ''}" type="text" name="escala.nome" value="${escala.nome}" validate="vazio"/>
						</div>
					</div>
					<div class="field">
						<div class="label">
							<label for="select">Data/hora Entrada:<b style="color: #FF0000; font-size: 18px;">*</b>
						</div>
						<div class="input">
							<input style="display: block; width: 200px; margin-right: 10px;"
									class="medium${erroResumo eq true ? ' error' : ''} dataHora" type="text"
									name="escala.entrada" 
									value="${escala.entrada}" />
						</div>
					</div>
					<div class="field">
						<div class="label">
							<label for="select">Data/hora Saída:<b style="color: #FF0000; font-size: 18px;">*</b>
						</div>
						<div class="input">
							<input style="display: block; width: 200px; margin-right: 10px;"
									class="medium${erroResumo eq true ? ' error' : ''} dataHora" type="text"
									name="escala.saida" 
									value="${escala.saida}" />
						</div>
					</div>
					<div class="field">
						<div class="label">
							Ativo:
						</div>
						<div class="input">
							<input type="checkbox" name="escala.ativo"
								<c:if test="${escala.ativo}">checked</c:if> />
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							<label for="select">Atividades<b style="color: #FF0000; font-size: 18px;">*</b>:<div class="ajuda" title="Atividades da escala.">?</div></label>
						</div>
						
						<div class="input" style="margin:10px 0 0 185px;">
							<ul id="listaOcorrenciasCadastro">
							</ul>
						</div>
										
						<div style="margin:10px 0 0 185px;">
							<a href="javascript:;" onclick="showDialogOcorrencias();"><img src="../../../images/plus.png" /> adicionar atividade</a>
						</div>
					</div>
					
					<div class="field">
						<div class="label">
							Segunda:
						</div>
						<div class="input">
							<input type="checkbox" name="escala.segunda"
								<c:if test="${escala.segunda}">checked</c:if> />
						</div>
					</div>
					<div class="field">
						<div class="label">
							Terça:
						</div>
						<div class="input">
							<input type="checkbox" name="escala.terca"
								<c:if test="${escala.terca}">checked</c:if> />
						</div>
					</div>
					<div class="field">
						<div class="label">
							Quarta:
						</div>
						<div class="input">
							<input type="checkbox" name="escala.quarta"
								<c:if test="${escala.quarta}">checked</c:if> />
						</div>
					</div>
					<div class="field">
						<div class="label">
							Quinta:
						</div>
						<div class="input">
							<input type="checkbox" name="escala.quinta"
								<c:if test="${escala.quinta}">checked</c:if> />
						</div>
					</div>
					<div class="field">
						<div class="label">
							Sexta:
						</div>
						<div class="input">
							<input type="checkbox" name="escala.sexta"
								<c:if test="${escala.sexta}">checked</c:if> />
						</div>
					</div>
					<div class="field">
						<div class="label">
							Sábado:
						</div>
						<div class="input">
							<input type="checkbox" name="escala.sabado"
								<c:if test="${escala.sabado}">checked</c:if> />
						</div>
					</div>
					<div class="field">
						<div class="label">
							Domingo:
						</div>
						<div class="input">
							<input type="checkbox" name="escala.domingo"
								<c:if test="${escala.domingo}">checked</c:if> />
						</div>
					</div>
					<div class="field">
						<div class="label">
							Observação:
						</div>
						<div class="input">
							<div class="">
							<textarea
									class="textarea-medium${erroDescricao eq true ? ' error' : ''}"
									name="escala.observacoes">${escala.observacoes}</textarea>
						</div>
						</div>
					</div>
	
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${materiais.id == null ? 'adicionar' : 'alterar'}" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all" type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
