<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout"%>

<layout:controleGeral keywords="" description=""
	title=".On The Go.">

	<jsp:attribute name="link_css">

	<link type="text/css"
			href="../../../../../css/flick/jquery-ui-1.8.16.custom.css"
			rel="stylesheet" />

</jsp:attribute>

	<jsp:attribute name="link_scripts">

	<script type="text/javascript"
			src="../../../../../js/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="../../../../../js/jFoto/jTable.js"></script> 

</jsp:attribute>

	<jsp:attribute name="scripts">

<script type="text/javascript">
	function removeRegra(id, nome) {

		var answer = confirm("Deseja realmente excluir o Usuário " + nome + " ?");
		if (answer) {

			$.post( "${contexto}/controle_geral/opcoes/usuario/regra/excluir/", { 'id': id }, function( resposta ) {

				resposta = eval('(' + resposta + ')');

				if (resposta.status == 'error') {

				} else {

					$('#message-success-senha').find('h6').html(resposta.msgSucesso);
					$('#message-success-senha').show();
					$('#mensagem-erro-senha').hide();

					$('#lista').refresh();

					$(window).trigger('resize');
				}
			});

		}
	}

	function criaTabelaRegras() {
		$("#lista").jTable({
			url : '../../../../../controle_geral/usuarios/listaRegras/',
			cabecalho : 
				[{'titulo' : 'Id','width' : 'auto','align' : 'left','campo' : 'id'},
				 {'titulo' : 'Nome','width' : 'auto',	'align' : 'left','campo' : 'regra'} ],
			opcoesTitulo : 'Opções',
			campoRef : 'id',
			campoTituloRef : 'regra',
			opcoes : [
			{'icon' : '../../../../../images/ico_alterar.png','link' : '../../../../../controle_geral/opcoes/usuario/regras','funcao' : '','alt' : 'editar','params' : [ 'id' ]	},
			{'icon' : '../../../../../images/ico_excluir.png','link' : '','funcao' : 'removeRegra','ajax' : 'true','alt' : 'excluir'
			} ]
		});
	}
	
	function inicio() {
		parent.location = '../../../controle_geral/opcoes/usuario/regras/listar/';
	}
	
	$(document).ready(function() {

		criaTabelaRegras();

	});
</script>

</jsp:attribute>

	<jsp:attribute name="dialogs">
</jsp:attribute>

	<jsp:body>

	<div class="box box-inline">
		
		<div class="title">
			<h5>Lista de regras</h5>
		</div>
	
		<jsp:include page="../includes/mensagens.jsp" />
		
		<div class="table">
<div id="lista"></div>
		</div>
	</div>
	
</jsp:body>

</layout:controleGeral>
