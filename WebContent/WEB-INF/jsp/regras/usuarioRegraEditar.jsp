<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<layout:controleGeral keywords="" description=""
	title=".On The Go.">

	<jsp:attribute name="link_css">

	<link type="text/css" href="../../../css/jquery-autocomplete.css"
			rel="stylesheet" />
	<link type="text/css"
			href="../../../css/flick/jquery-ui-1.8.16.custom.css"
			rel="stylesheet" />
	<link type="text/css" href="../../../css/tipTip.css" rel="stylesheet" />

</jsp:attribute>

	<jsp:attribute name="link_scripts">

	<script type="text/javascript" src="../../../js/jQuery/jquery-ui-1.8.18.custom.min.js"></script> 
	<script type="text/javascript" src="../../../js/jquery.tipTip.js"></script> 

</jsp:attribute>

	<jsp:attribute name="scripts">

<script type="text/javascript">
	function inicio() {
		parent.location = '../../../controle_geral/opcoes/usuario/regras/listar/';
	}

	$(document).ready(function() {

		$("#formRegras").jFotoForm();

	});
</script>

</jsp:attribute>

	<jsp:attribute name="dialogs">	
			
</jsp:attribute>


	<jsp:body>

    <div id="loading">
    	<table style="width: 100%; height: 100%;">
    		<tr valign="middle">    			
    			<td align="center">Carregando...<br /><img
						src="../../../images/ajax-loader.gif" /></td>
    		</tr>
    	</table>    	
    </div>
    
	<div class="box box-inline">
		
		<div class="title">
			<h5>${regra.id == null ? 'Cadastrar nova' : 'Editar'} regra</h5>
		</div>
	
	<c:if test="${not empty errors}">
	
		<br />
		<div class="messages">
			<div id="message-error" class="message message-error">
				<div class="image">
					<img height="32" alt="Error" src="../../../images/icones/error.png" />
				</div>
				<div class="text">
					<h6>Ocorrerão os seguintes erros</h6>
				    <c:forEach var="error" items="${errors}">   
				          <span>${error.message}</span>
				    </c:forEach>  		
				</div>
				<div class="dismiss">
					<a href="#message-error"></a>
				</div>
			</div>
		</div>
	
	</c:if>
													
		<form id="formRegras" action="./regras" method="post">
			
			<div class="form">
			
				<input name="regra.id" type="hidden" value="${regra.id}" />
			
				<div class="fields">
												
					<div class="field">
						<div class="label">
							<label for="input-medium">Nome:<b
									style="color: #FF0000; font-size: 18px;">*</b>
								<div class="ajuda" title="Nome da regra.">?</div></label>
						</div>
						<div class="input">
							<input style="display: block;" class="medium" type="text"
									name="regra.role" value="${regra.role}" validate="vazio" />
						</div>
					</div>

					<div class="field">
						<div class="label">
							<label for="input-medium">Páginas:<div class="ajuda"
										title="Páginas que terão o acesso permitido para essa regra.">?</div></label>
						</div>
						
						<div class="checkboxes">
						
						<c:forEach var="pagina" items="${paginas}">
						
<c:set var="paginaMarcada" value="false" />
<c:forEach var="paginasMarcadas" items="${regra.role}">
	<c:if test="${paginasMarcadas.role == pagina.role}">
		<c:set var="paginaMarcada" value="true" />
	</c:if>
</c:forEach>
							
							<div class="checkbox" style="width: 500px; margin-bottom: 2px;">
								<input type="checkbox" id="checkbox" name="regra.listaPaginas[].id" value="${pagina.id}" ${paginaMarcada ? 'CHECKED' : ''}>
								<label for="checkbox-1" style="margin: 1px 0 0 10px;">${pagina.descricao}</label>
							</div>
						</c:forEach>						
						</div>
					</div>
								
					<div class="buttons">
						<div class="highlight">
							<input class="ui-button ui-widget ui-state-default ui-corner-all"
									type="submit"
									value="${ocorrencia.id == null ? 'adicionar' : 'alterar'}" />
						</div>
						<input class="ui-button ui-widget ui-state-default ui-corner-all"
								type="button" value="cancelar" onclick="inicio();" />				
					</div>															
				</div>	
			</div>
		</form>
	</div>

</jsp:body>

</layout:controleGeral>
