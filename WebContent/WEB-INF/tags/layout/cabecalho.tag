<%@ tag body-content="empty" description="Header tag file" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="alertaIE" title="Alerta" style="font: bold 14px Arial; text-align: center;"></div>

<script type="text/javascript">
<!--
//verifica se o navegador � Internet Explorer vers�o 6 ou anterior
	var browser = navigator.appName
	var ver = navigator.appVersion
	var thestart = parseFloat(ver.indexOf("MSIE"))+1
	var brow_ver = parseFloat(ver.substring(thestart+4,thestart+7))
	if ((browser=="Microsoft Internet Explorer") && (brow_ver < 8))
	{

		$('#alertaIE').append('<br />Essa aplica��o pode n�o funcionar corretamente no Internet Explorer. <br /><br />Por favor, utilize outro navegador.');
		$('#alertaIE').dialog({ autoOpen: true, modal: false, closeOnEscape: false });
	}
	
//-->
</script>

<div id="header">
	
	<div class="menuTopo">
		<c:if test="${usuarioSession.id > 0}">
			${usuarioSession.nome} | 
			<a href="<%=request.getContextPath()%>/logout">
				<img src="<%=request.getContextPath()%>/images/icones/logout.png" title="Sair" alt="Sair" style="float:right; padding-left:5px;" />
			</a>
		</c:if>
	</div>

	 <div id="header-inner">

	 	<a title="Home" href="<%=request.getContextPath()%>"><div class="logo"><img src="<%=request.getContextPath()%>/images/logo.png"></div></a>
		
		<ul id="quick">
			
			<c:forEach var="menu" items="${menuQuick}">
			<li>
				<a href="<c:if test="${menu.link == null}">javascript:;</c:if><c:if test="${menu.link != null}"><%=request.getContextPath()%>/${menu.link}</c:if>" title="${menu.nome}">
					<span class="icon">
						<img src="<%=request.getContextPath()%>/${menu.icon}" alt="${menu.nome}" />
					</span>
					<span>${menu.nome}</span>
				</a>	
				
				<c:if test="${fn:length(menu.subMenu) > 0}">				
				<ul style="">
					<c:forEach var="subMenu" items="${menu.subMenu}"> 
					<li>
						<a href="<%=request.getContextPath()%>/${subMenu.link}">${subMenu.nome}</a>
					</li>
					</c:forEach>		
				</ul>				
				</c:if>					
			</li>
			</c:forEach>									
		</ul>
	</div>
</div>