<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ tag description="Page layout" %>
 
<%@ attribute name="title"       required="true" description="Page title" %>
<%@ attribute name="keywords"    required="true" description="Page keywords to improve SEO" %>
<%@ attribute name="description" required="true" description="Page description" %>

<%@ attribute name="link_css" fragment="true" description="Os links para CSS ficar�o aqui" %>
<%@ attribute name="link_scripts" fragment="true" description="Os links para os scripts ficar�o aqui" %> 
<%@ attribute name="dialogs" fragment="true" description="Os dialogos dever�o ficar aqui" %>
<%@ attribute name="scripts" fragment="true" description="Os scripts dever�o ficar aqui" %>
 
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta name="keywords"    content="${keywords}" />
	<meta name="description" content="${description}" />
	        
	<title>${title}</title>
	
<jsp:invoke fragment="link_css"/>

	<link type="text/css" href="<%=request.getContextPath()%>/css/style.css" rel="stylesheet" />
	<link type="text/css" href="<%=request.getContextPath()%>/css/ccpb.css" rel="stylesheet" />
	<link type="text/css" href="<%=request.getContextPath()%>/css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	
	<script src="<%=request.getContextPath()%>/js/jquery-1.7.1.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script> 	
	<script src="<%=request.getContextPath()%>/js/util.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/js/jFoto/jTelaCheia.js" type="text/javascript"></script> 
	
<jsp:invoke fragment="link_scripts"/>

</head>

<body>

	<script type="text/javascript">

		function openSubMenu(id) {
			
			if ($('#menu-'+id).hasClass('selected')) {
				
				$('#menu-'+id).removeClass('selected');
			} else {
				
				$('#menu-'+id).addClass('selected');
			}
			
			$('#submenu-'+id).toggle(500, function() {
				$(window).trigger('resize');
			});
		}

		function openSubSubMenu(id, elem) {
						
			if ($(elem).hasClass('plus')) {
				
				$(elem).removeClass('plus');
				$(elem).addClass('minus');
				
			} else {

				$(elem).addClass('plus');
				$(elem).removeClass('minus');				
			}
			
			$('#subSub-' + id).toggle(500, function() {
				$(window).trigger('resize');
			});
			
		}
				
		$('#content').ready( function() {
			
			$('#content').jTelaCheia({
				cabecalho: 165,
				rodape: 110,
				divs: ['left', 'conteudo']
			});
		});
	</script>
		
<jsp:invoke fragment="scripts"/>

<layout:cabecalho />
            
<jsp:invoke fragment="dialogs"/>
        
<div id="content">

	<div id="left">
		<div id="menu">
		
<c:forEach var="menu" items="${listaSubMenu}">

	<c:if test="${menu.ativo}">
			<h6 id="menu-${menu.id}">
			<c:if test="${not empty menu.listaSubMenu}"><a href="javascript:;" onclick="openSubMenu('${menu.id}');"></c:if>
			<c:if test="${empty menu.listaSubMenu}"><a href="<%=request.getContextPath()%>/${menu.link}"></c:if>
				<span>${menu.nome}</span>
				</a>
			</h6>

			<ul id="submenu-${menu.id}" class="opened" style="display: none;">
			<c:forEach var="menuSub" items="${menu.listaSubMenu}">
				<c:if test="${menuSub.ativo}">
				<li<c:if test="${not empty menuSub.listaSubMenu}"> class="collapsible"</c:if>>
					<c:if test="${not empty menuSub.listaSubMenu}"><a href="javascript:;" class="collapsible plus" onclick="openSubSubMenu('${menuSub.id}', this);"></c:if>
					<c:if test="${empty menuSub.listaSubMenu}"><a href="<%=request.getContextPath()%>/${menuSub.link}"></c:if>
					${menuSub.nome}</a>

					<ul id="subSub-${menuSub.id}" class="expanded" style="display: none;">
					<c:forEach var="menuSubSub" items="${menuSub.listaSubMenu}">			
						<li>
							<a href="<%=request.getContextPath()%>/${menuSubSub.link}">${menuSubSub.nome}</a>
						</li>
					</c:forEach>
					</ul>
												
				</li>
				</c:if>								
			</c:forEach>
			</ul>	

		<c:if test="${menu.id == subMenu.submenuId}">
			<script type="text/javascript">
				openSubMenu('${menu.id}');		
			</script>			
		</c:if>
	</c:if>
</c:forEach>	
			
		</div>
	</div>
	
<jsp:doBody />

</div>



</body>
</html>