<%@ tag description="Page layout" %>
 
<%@ attribute name="title"       required="true" description="Page title" %>
<%@ attribute name="keywords"    required="true" description="Page keywords to improve SEO" %>
<%@ attribute name="description" required="true" description="Page description" %>

<%@ attribute name="link_css" fragment="true" description="Os links para CSS ficar�o aqui" %>
<%@ attribute name="link_scripts" fragment="true" description="Os links para os scripts ficar�o aqui" %> 
<%@ attribute name="dialogs" fragment="true" description="Os dialogos dever�o ficar aqui" %>
<%@ attribute name="scripts" fragment="true" description="Os scripts dever�o ficar aqui" %>
 
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta name="keywords"    content="${keywords}" />
	<meta name="description" content="${description}" />
	        
	<title>${title}</title>
	
<jsp:invoke fragment="link_css"/>

	<link type="text/css" href="<%=request.getContextPath()%>/css/style.css" rel="stylesheet" />
	<link type="text/css" href="<%=request.getContextPath()%>/css/ccpb.css" rel="stylesheet" />
	
	<script src="<%=request.getContextPath()%>/js/jquery-1.7.1.js" type="text/javascript"></script>

<jsp:invoke fragment="link_scripts"/>

</head>

<body>

<script>

var urlApp = '${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.localPort}${pageContext.request.contextPath}/';

</script>

<jsp:invoke fragment="scripts"/>

<layout:cabecalho />
            
<jsp:invoke fragment="dialogs"/>
        
<div id="content">

<jsp:doBody/>

</div>


</body>
</html>