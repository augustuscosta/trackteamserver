package test;

import org.junit.Test;

import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Materiais;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.comunicacao.CObjeto;

public class TestObjetos {

	private Objeto objeto;
	
	public TestObjetos() {
		
		Dominio estado = new Dominio();
		Materiais material = new Materiais();
		
		objeto = new Objeto();
		objeto.setId(10);
		objeto.setDescricao("descricao");
		objeto.setEstado(estado);
		objeto.setMateriais(material);
		objeto.setNumero_controle_interno("100");
		
	}
	
	@Test
	public void conversaoDeObjetoParaCObjeto() {

		CObjeto cObjeto = new CObjeto();
		cObjeto.parseFrom(objeto);
		
		Objeto obj = new Objeto();
		obj = cObjeto.parseTo();
	
	}

	@Test
	public void conversaoDeCObjetoParaObjeto() {

		CObjeto cObjeto = new CObjeto();
		
		Objeto obj = new Objeto();
		obj = cObjeto.parseTo();

		cObjeto.parseFrom(objeto);
		obj = cObjeto.parseTo();
	}

}
