package test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.caelum.vraptor.util.test.MockResult;
import br.com.otgmobile.trackteam.dao.OcorrenciaDao;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Endereco;
import br.com.otgmobile.trackteam.modal.comunicacao.COcorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.NiveisEmergenciaService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.tasks.SocketIOTask;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class TesteOcorrencias extends TestCase {

	@Mock private OcorrenciaDao ocorrenciaDao;
	@Mock private SocketIOTask socketIOTask;
	@Mock private NiveisEmergenciaService niveisEmergenciaService;
	@Mock private TokenService tokenService;
	@Mock private VeiculoService veiculoService;
	@Mock private HttpServletResponse response;
	@Mock private AgenteService agenteService;

	private OcorrenciaService ocorrenciaService;
	private Ocorrencia ocorrencia;
	private NiveisEmergencia niveisEmergencia;

	public TesteOcorrencias() {
	
		ocorrencia = new Ocorrencia();
		
		Endereco endereco = new Endereco();
		endereco.setLatitude("200");
		endereco.setLongitude("300");
		endereco.setEndGeoref("R. Manoel Assunção, 148-436 - Eusébio - Ceará, 61760-000, Brazil");
		
		Dominio dominio = new Dominio();
		dominio.setId(10);
		
		niveisEmergencia = new NiveisEmergencia();
		niveisEmergencia.setId(10);
		
		ocorrencia.setDescricao("descricao");
		ocorrencia.setEndereco(endereco);
		ocorrencia.setNatureza1(dominio);
		ocorrencia.setOcorrenciasStatus(dominio);
		ocorrencia.setNiveisEmergencia(niveisEmergencia);
		ocorrencia.setResumo("resumo");
		
		MockitoAnnotations.initMocks(this);				

		when(ocorrenciaDao.insert(any(Ocorrencia.class))).thenReturn(ocorrencia);
		when(ocorrenciaDao.cancelaOcorrencia(any(Ocorrencia.class))).thenReturn(ocorrencia);
		when(ocorrenciaDao.findOcorrencia(any(Ocorrencia.class))).thenReturn(ocorrencia);			
		when(niveisEmergenciaService.find(any(NiveisEmergencia.class))).thenReturn(new NiveisEmergencia());
				
		ocorrenciaService = new OcorrenciaService(ocorrenciaDao, socketIOTask, niveisEmergenciaService, veiculoService, tokenService);		
	}
	
	@Test
	public void testeListarTodasAsOcorrencias() {

		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(20);
				
		List<Ocorrencia> lista = ocorrenciaService.findOcorrencias();

		assertNotNull(lista);		
	}
	
	@Test
	public void testeInclusao() {

		Ocorrencia ocorrencia = new Ocorrencia();

		ocorrencia = ocorrenciaService.insert(ocorrencia);
		assertNull(ocorrencia);

		assertNotNull(ocorrenciaService.insert(this.ocorrencia));

	}	
	
	@Test
	public void testeAlteracao() {

		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia = ocorrenciaService.insert(ocorrencia);
		
		assertNull(ocorrencia);
		assertNotNull(ocorrenciaService.insert(this.ocorrencia));

	}		
	
	@Test
	public void testeCancelar() {
					
		assertEquals(ocorrencia, ocorrenciaService.cancelaOcorrencia(ocorrencia));
		assertEquals(null, ocorrenciaService.cancelaOcorrencia(null));

	}

	@Test
	public void testeEncerrar() {
			
		Boolean erro = false;
	
		assertEquals(ocorrencia, ocorrenciaService.encerrarOcorrencia(ocorrencia));
		assertEquals(null, ocorrenciaService.encerrarOcorrencia(null));
		
	}
	
	@Test
	public void testeAdicionarViaJSON() {

		String jsonStr = "{\"_id\":232," +
				"\"vitimas\":[{\"_id\":8,\"descricao\":\"Vitimologia\",\"id\":0,\"idade\":12,\"ocorrenciaID\":232}]," + 
				"\"descricao\":\"Under the prefere\"," +
				"\"endereco\":{\"endGeoref\":\"R. Júlio Abreu, 124-238 - Varjota Fortaleza - CE 60160-240 \",\"latitude\":-3.7359684,\"longitude\":-38.487915}," +
				"\"estado\":8,\"estadoObj\":{\"_id\":12,\"id\":8,\"valor\":\"Em atendimento\"}," +
				"\"materiais\":[{\"_id\":2,\"codigo\":\"555\",\"descricao\":\"RADAR MOVEL TIPO 1\",\"id\":325}, {\"_id\":1,\"codigo\":\"62\",\"descricao\":\"CAPACETE VIATURA 1\",\"id\":302}, {\"_id\":3,\"codigo\":\"38\",\"descricao\":\"CONE DANIFICADO\",\"id\":697}]," +
				"\"natureza\":11,\"naturezaObj\":{\"_id\":8,\"id\":11,\"valor\":\"Assalto\"}," + 
				"\"nivelEmergencia\":{\"_id\":8,\"cor\":\"#ffde5b\",\"descricao\":\"Importante\",\"id\":2}," + 
				"\"nivelEmergenciaID\":2," +
				"\"resumo\":\"Mistake null pointer\"," +
				"\"solicitantes\":[{\"_id\":2,\"cpf\":\"4625415441\",\"id\":0,\"nome\":\"Titio\",\"telefone1\":\"1225896624\",\"telefone2\":\"1255665339\"}, {\"_id\":1,\"cpf\":\"Que foi\",\"id\":0,\"nome\":\"Apagou abalou\",\"telefone1\":\"Qual era\",\"telefone2\":\"Poos n ao o e\"}]," +
				"\"enviado\":false," +
				"\"cancelado\":false}";
		
			Gson gson = new Gson();
			COcorrencia cOcorrencia = gson.fromJson(jsonStr, COcorrencia.class);
			Ocorrencia novaOcorrencia = cOcorrencia.parseTo();
			
			assertEquals(ocorrencia, ocorrenciaService.insert(novaOcorrencia));
			
	}
	
	@Test
	public void testeAlterarCOcorrenciaController() {
		
		MockResult result = new MockResult();
		
		Ocorrencia ocorrenciaTemp = ocorrencia;
		
		ocorrenciaTemp.setInicio(null);
		ocorrenciaTemp.setFim(new Timestamp(0));
		ocorrenciaTemp.setInicioProgramado(null);
		ocorrenciaTemp.setFimProgramado(null);
		ocorrenciaTemp.setHoraChegada(new Timestamp(0));
		ocorrenciaTemp.setHoraSaida(new Timestamp(0));
		ocorrenciaTemp.setRecebimento(new Timestamp(0));
		
		Gson gson = new Gson();	
		COcorrencia cOcorrencia = new COcorrencia();
		cOcorrencia.parseFrom(ocorrencia);
		
		JsonElement je = gson.toJsonTree(cOcorrencia);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);
		
//		COcorrenciaController cOcorrenciaController = new COcorrenciaController(result, ocorrenciaService, tokenService, veiculoService, response, agenteService);
//		cOcorrenciaController.alterar(jo.toString(), "7");
		
	}
	
	@Test
	public void testeAssociarObjetosOcorrencia() {
		
		
		
	}
}
