package test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.otgmobile.trackteam.modal.GeoPontoRota;
import br.com.otgmobile.trackteam.modal.Rota;
import br.com.otgmobile.trackteam.modal.comunicacao.CRota;

public class TestRotas {

	private Rota rota;
	
	public TestRotas() {
		
		List<GeoPontoRota> geoPonto = new ArrayList<GeoPontoRota>();
		GeoPontoRota geoPontoRota = new GeoPontoRota();
		
		geoPontoRota.setId(1);
		geoPontoRota.setLatitude("3");
		geoPontoRota.setLongitude("4");
		
		rota = new Rota();
		rota.setId(10);
		rota.setNome("rota teste");
		rota.setGeoPonto(geoPonto);
	}
	
	@Test
	public void conversaoDeRotaParaCRota() {
		
		CRota cRota = new CRota();
		cRota.parseFrom(rota);			
	}

}
