package test;

import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.otgmobile.trackteam.dao.EnderecoDao;
import br.com.otgmobile.trackteam.dao.OcorrenciaDao;
import br.com.otgmobile.trackteam.modal.comunicacao.COcorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.EnderecoService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class TesteGson {

	public static void listaObjetos(JSONObject jObject) {

    	try {
//			JSONObject jObject = new JSONObject(json);
	
	        Iterator<?> keys = jObject.keys();
	        
	        while( keys.hasNext() ){
	            String key = (String)keys.next();
          
	            System.out.println(jObject.get(key).getClass());
	            
				if( jObject.get(key) instanceof JSONObject ){
										
		            listaObjetos((JSONObject) jObject.get(key));
		            
				} else {

					System.out.println(key);
					System.out.println("-" + jObject.get(key));

				}				
	       }
	       
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    	
	}
	
	public static void main(String[] args) {

		final EntityManagerFactory factory;
		final EntityManager entityManager;

    	factory = Persistence.createEntityManagerFactory("default");
    	entityManager = factory.createEntityManager();
    	
		EnderecoService enderecoService = new EnderecoService(new EnderecoDao(entityManager));
		OcorrenciaDao ocorrenciaDao = new OcorrenciaDao(entityManager, enderecoService);
		
		Ocorrencia ocorrencia = ocorrenciaDao.findOcorrencia(new Ocorrencia(730377));
		COcorrencia cOcorrencia = new COcorrencia(ocorrencia);
		
		Gson gson = new Gson();
		
		JsonElement je = gson.toJsonTree(cOcorrencia);		
	    JsonObject jo = new JsonObject();	    
    	jo = je.getAsJsonObject();

		JSONObject jObject;
		try {
			jObject = new JSONObject(jo.toString());
			listaObjetos(jObject);   
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
