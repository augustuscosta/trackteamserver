package br.com.otgmobile.trackteam.controller;

import java.io.File;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;

@Resource
public class ImagemController {

	private final Result result;
	@SuppressWarnings("unused")
	private final HttpServletResponse response;
	private final String pastaArquivos;

	public ImagemController(Result result, HttpServletResponse response) {

		this.result = result;
		this.response = response;
		ResourceBundle resourceConfig = java.util.ResourceBundle.getBundle("config");			
		pastaArquivos = resourceConfig.getString("salvarImgVideo");

	}

	@SuppressWarnings("unused")
	@Get
	@MonkeySecurity(role="all")
	@Path("/ocorrencia/imagem/{ocorrenciaId}/{imagemNome}.jpg")
	public File getImagem(String ocorrenciaId, String imagemNome) {

		File file = new File(pastaArquivos + ocorrenciaId + "/" + imagemNome);

		if (file != null) {
			
			return file;

		} else {
			result.use(Results.logic()).redirectTo(ImagemController.class).retornaVazio();
		}

		return null;
	}
	
	
	@SuppressWarnings("unused")
	@Get
	@MonkeySecurity(role="all")
	@Path("/ocorrencia/assinatura/{ocorrenciaId}/{imagemNome}.jpg")
	public File getAssinatura(String ocorrenciaId, String imagemNome) {

		File file = new File(pastaArquivos + ocorrenciaId + "/assinaturas/" + imagemNome);

		if (file != null) {
			
			return file;

		} else {
			result.use(Results.logic()).redirectTo(ImagemController.class).retornaVazio();
		}

		return null;
	}

	@SuppressWarnings("unused")
	@Get
	@MonkeySecurity(role="all")
	@Path("/agente/imagem/{agente}.jpg")
	public File getImagemAgente(String agente) {

		File file = new File(pastaArquivos + "snapShot" + "/" + agente);

		if (file != null) {
			
			return file;

		} else {
			result.use(Results.logic()).redirectTo(ImagemController.class).retornaVazio();
		}

		return null;
	}
	
	@SuppressWarnings("unused")
	@Get
	@MonkeySecurity(role="all")
	@Path("/ocorrencia/video/{ocorrenciaId}/{videoNome}")
	public File getVideos(String ocorrenciaId, String videoNome, String formato) {
		
		File file = new File(pastaArquivos + ocorrenciaId + "/" + videoNome );

		if (file != null) {
			System.out.println(file);
			return file;

		} else {
			result.use(Results.logic()).redirectTo(ImagemController.class).retornaVazio();
		}

		return null;
	}

	public void retornaVazio() {

		result.use(Results.http()).setStatusCode(200);
		result.use(Results.http()).body("");
	}
}
