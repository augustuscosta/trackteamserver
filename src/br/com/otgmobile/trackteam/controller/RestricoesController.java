package br.com.otgmobile.trackteam.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.modal.restricoes.PessoaRestricao;
import br.com.otgmobile.trackteam.modal.restricoes.VeiculoRestricao;
import br.com.otgmobile.trackteam.service.RestricaoService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.util.Util;

@Resource
public class RestricoesController {

	private Result result;
	private final HttpServletResponse response;
	private final RestricaoService restricaoService;
	private final TokenService tokenService;
	
	public RestricoesController(
			Result result,
			HttpServletResponse response,
			RestricaoService restricaoService,
			TokenService tokenService) {
		
		this.result = result;
		this.response = response;
		this.restricaoService = restricaoService;
		this.tokenService = tokenService;
	}
	
	@Get
	@Path("/restricoes/consulta/veiculo/")
	public void consultaVeiculo(String placa, String chassi, String renavam) {

		List<VeiculoRestricao> listaVeiculos = new ArrayList<VeiculoRestricao>(); 
		
		if (placa == null) {
			
			placa = "";
			
		} else if (chassi == null) {
			
			chassi = "";
			
		} else if (renavam == null) {
			
			renavam = "";			
		} 

		listaVeiculos = restricaoService.findVeiculo(placa, chassi, renavam);
					
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Util.toJSON(listaVeiculos));
	}
	
	@Get
	@Path("/restricoes/consulta/pessoa/{token}/{query}")
	public void consultaPessoa(String token, String query) {
		
		List<PessoaRestricao> listaPessoas = new ArrayList<PessoaRestricao>();
		List<PessoaRestricao> listaPessoasToJSON = new ArrayList<PessoaRestricao>(); 

		if (token != null && tokenService.isValido(token) && query != null) {
		
			listaPessoas = restricaoService.findPessoa(query);
			
			for (PessoaRestricao pessoa : listaPessoas) {
				
				listaPessoasToJSON.add(pessoa);			
			}
				
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(Util.toJSON(listaPessoasToJSON));
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);			
		}
	}
		

	@Post
	@Path("/restricoes/cadastro/veiculo/")
	public void cadastraVeiculoRestricao(String json) {

		List<VeiculoRestricao> listaVeiculos = Util.fromGsonArray(VeiculoRestricao.class, json);
		
		for (VeiculoRestricao veiculo : listaVeiculos) {
			
			restricaoService.insertVeiculo(veiculo);
		}
	}
	
	@Post
	@Path("/restricoes/cadastro/pessoa/")
	public void cadastraPessoaRestricao(String json) {

		List<PessoaRestricao> listaPessoas = Util.fromGsonArray(PessoaRestricao.class, json);
		
		for (PessoaRestricao pessoa : listaPessoas) {
			
			restricaoService.insertPessoa(pessoa);
		}
	}
	
}
