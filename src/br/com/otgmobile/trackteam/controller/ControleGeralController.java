package br.com.otgmobile.trackteam.controller;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.IndexController;

@Resource
public class ControleGeralController {
	
	private Result result;
	private MenuService menuService;
	
	public ControleGeralController(Result result, MenuService menuService) {
		
		this.result = result;
		this.menuService = menuService;
	}
	
	@Path("/controle_geral/{menu_id}")
	@MonkeySecurity(role="all")
	public void opcoes(String menu_id) {

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
		Menu menu = new Menu();
		menu.setTag(menu_id);
		
		menu = menuService.findByTag(menu); 
		
		if (menu != null && menu.getId() != null && menu.getId() > 0) {		
			
			List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(menu);
			result.include("listaSubMenu", listaSubMenu);
		} else {
			
			result.use(Results.logic()).forwardTo(IndexController.class).index();	
		}
	}
}
