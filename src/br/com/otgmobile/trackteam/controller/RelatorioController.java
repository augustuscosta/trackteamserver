package br.com.otgmobile.trackteam.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.dao.Conexao;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CAgente;
import br.com.otgmobile.trackteam.modal.comunicacao.CNivelEmergencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.HistoricoService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.RelatoriosService;
import br.com.otgmobile.trackteam.service.VeiculoService;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class RelatorioController {

	public enum Formato {
		PDF, EXCEL
	}
	
	private HttpServletResponse response;
	private ServletContext sc;
	private Result result;
	private final Validator validator;	
	private final VeiculoService veiculoService;
	private final AgenteService agenteService;
	private final OcorrenciaService ocorrenciaService;
	private final RelatoriosService relatoriosService;
	private final HistoricoService historicoService;
	private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private final String prefeituraNome = "Prefeitura de São Jose dos Campos";
	
	public RelatorioController(Result result, 
			Validator validator,	
			HttpServletResponse response, 
			ServletContext sc,
			VeiculoService veiculoService,
			OcorrenciaService ocorrenciaService,
			RelatoriosService relatoriosService,
			HistoricoService historicoService,
			AgenteService agenteService) {
		
		super();
		this.response = response;
		this.sc = sc;
		this.result = result;
		this.veiculoService = veiculoService;
		this.ocorrenciaService = ocorrenciaService;
		this.relatoriosService = relatoriosService;
		this.historicoService = historicoService;		
		this.validator = validator;
		this.agenteService = agenteService;
	}

	@Get
	@Path("/estatistica/relatorios/agente/")
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorios-agente")
	public void ocorrenciasAgente() {

	}

	@Get
	@Path("/estatistica/relatorios/despache/")
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorios-agente")
	public void ocorrenciasDespache() {

	}
	
	@Get
	@Path("/estatistica/relatorios/atendente/")
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorios-atendente")
	public void ocorrenciasAtendente() {

	}

	@Get
	@Path("/estatistica/relatorios/viatura/")
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorios-agente")
	public void ocorrenciasViatura() {
		
	}

	@Get
	@Path("/estatistica/relatorios/ait/")
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorios-ait")
	public void aitTalaoEletronico() {

	}

	@Get
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorio-veiculo")
	@Path("/relatorios/viatura/estado/")
	public void viaturaEstadoAtual() {
		
	}
	
	@Get
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorio-mapacalor")
	@Path("/estatistica/relatorios/mapa/")
	public void mapaCalor() {
		
	}
	
	@Post
	@Path("/estatistica/relatorios/mapa/calor")
	@MonkeySecurity(role = "relatorio")
	public void gerarMapaDeCalor(){
		result.notify();
	}
	
	@SuppressWarnings("unchecked")
	@Post
	@Path("/estatistica/relatorios/agente/relatorio")
	@MonkeySecurity(role = "relatorio")
	public void gerarOcorrenciasAgenteRelatorio(String dataInicio, String dataFim, String formato, int nivelEmergenciaId, String status, final String agenteId) {


		Agente agente = null;

		if (agenteId == null || agenteId.equals("")) {

			validator.add(new ValidationMessage("Agente inválido", "error"));
			
		} else {
			
			agente = agenteService.find(new Agente(Integer.parseInt(agenteId)));
			
			if (agente == null) {
				
				validator.add(new ValidationMessage("Veículo inválido", "error"));
			}
		}
		
		result.include("dataInicio", dataInicio);
		result.include("dataFim", dataFim);
		result.include("formato", formato);
		result.include("agenteId", agenteId);
		validator.onErrorUse(Results.logic()).redirectTo(RelatorioController.class).ocorrenciasAgente();
		
		Date dataInicial = formataDataInicio(dataInicio);
		Date dataFinal = formataDataFim(dataFim);
		
		List<Ocorrencia>listaOcorrencias = new ArrayList<Ocorrencia>(); 
		
		
		
		listaOcorrencias = ocorrenciaService.findOcorrenciaByAgente(agente, dataInicial, dataFinal, status, nivelEmergenciaId);	
		
		HashMap parametros = new HashMap(); 
		parametros.put("data", sdf.format(dataInicial) + " - " + sdf.format(dataFinal));
		parametros.put("idNome", agente.getId()+" - "+agente.getUsuario().getNome());
		if (formato.equals("pdf")) {
			
			relatoriosService.geraRelatorio("total-ocorrencias-agente", parametros, listaOcorrencias);
			
		} else if (formato.equals("excel")) {
			
			relatoriosService.geraRelatorioEcxel("total-ocorrencias-agente", parametros, listaOcorrencias);
		}

		result.nothing();
	}

	@SuppressWarnings("unchecked")
	@Post
	@Path("/estatistica/relatorios/despache/relatorio")
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorios-despache")
	public void gerarOcorrenciasDespacheRelatorio(String dataInicio, String dataFim, String formato, Integer despachanteID) {
		result.include("dataInicio", dataInicio);
		result.include("dataFim", dataFim);
		result.include("formato", formato);
		result.include("despachanteID", despachanteID);
		validator.onErrorUse(Results.logic()).redirectTo(RelatorioController.class).ocorrenciasViatura();
		
		
		
		Date dataInicial = formataDataInicio(dataInicio);
		Date dataFinal = formataDataFim(dataFim);
		
		HashMap parametros = new HashMap(); 
		parametros.put("data", sdf.format(dataInicial) + " - " + sdf.format(dataFinal));
		
		Connection conn = new Conexao().getConnection();
		HashMap parameters = new HashMap();
		try {
			JasperPrint impressao;
						
			InputStream ipStream  = this.getClass().getClassLoader().getResourceAsStream("/relatorios/total-ocorrencias-despachante.jasper");
						
			impressao = JasperFillManager.fillReport(ipStream, parameters,conn);
			
			if (formato.equals("pdf")) {

				response.setContentType("application/pdf");
				response.addHeader("Content-disposition","attachment; filename=\"OcorrenciasDespacheRelatorio.pdf\"");

				JRPdfExporter exporterPdf = new JRPdfExporter();
				exporterPdf.setParameter(JRPdfExporterParameter.JASPER_PRINT,impressao);
				exporterPdf.setParameter(JRPdfExporterParameter.OUTPUT_STREAM,response.getOutputStream());
				exporterPdf.exportReport();
			}
			if (formato.equals("excel")) {
				response.setContentType("application/vnd.ms-excel");
				response.addHeader("Content-disposition","attachment; filename=\"OcorrenciasDespacheRelatorio.xls\"");
				JRXlsExporter exporterXLS = new JRXlsExporter();
				exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,impressao);
				exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,response.getOutputStream());
				exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.TRUE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
				exporterXLS.exportReport();
			}
		} catch (JRException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			result.nothing();
		}
	}

	@SuppressWarnings("unchecked")
	@Post
	@Path("/estatistica/relatorios/viatura/relatorio")
	@MonkeySecurity(role = "relatorio")
	@MenuTag(tag = "relatorios-viatura")
	public void gerarOcorrenciasViaturaRelatorio(String formato,Integer viaturaCampo) {
		Connection conn = new Conexao().getConnection();
		String relatorio = sc.getRealPath("/relatorios/");
		HashMap parameters = new HashMap();
		parameters.put("AGENTE_ID", viaturaCampo);
		try {
			JasperPrint impressao;
						
			InputStream ipStream  = this.getClass().getClassLoader().getResourceAsStream("/relatorios/total-ocorrencias-viatura.jasper");
			
			impressao = JasperFillManager.fillReport(ipStream, parameters,conn);
			
			if (formato.equals("pdf")) {

				response.setContentType("application/pdf");
				response.addHeader("Content-disposition","attachment; filename=\"OcorrenciasViaturaRelatorio.pdf\"");

				JRPdfExporter exporterPdf = new JRPdfExporter();
				exporterPdf.setParameter(JRPdfExporterParameter.JASPER_PRINT,impressao);
				exporterPdf.setParameter(JRPdfExporterParameter.OUTPUT_STREAM,response.getOutputStream());
				exporterPdf.exportReport();
			}
			if (formato.equals("excel")) {
				response.setContentType("application/vnd.ms-excel");
				response.addHeader("Content-disposition","attachment; filename=\"OcorrenciasViaturaRelatorio.xls\"");
				JRXlsExporter exporterXLS = new JRXlsExporter();
				exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,impressao);
				exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,response.getOutputStream());
				exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.TRUE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
				exporterXLS.exportReport();
			}
		} catch (JRException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			result.nothing();
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Post
	@MonkeySecurity(role = "relatorio")
	@Path("/estatistica/relatorios/atendente/relatorio")
	public void gerarOcorrenciasAtendenteRelatorio(String dataInicio, String dataFim, String formato, int nivelEmergenciaId,
			String status, final String atendenteId) {
		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Post
	@MonkeySecurity(role = "relatorio")
	@Path("/relatorio/ocorrencia/veiculo")
	public void geraRelatorioOcorrenciaAtendidaViatura(String dataInicio, String dataFim, String formato, int nivelEmergenciaId, String status, final String viaturaId) {
		
		Veiculo veiculo = null;

		if (viaturaId == null || viaturaId.equals("")) {

			validator.add(new ValidationMessage("Veículo inválido", "error"));
			
		} else {
			
			veiculo = veiculoService.find(new Veiculo(Integer.parseInt(viaturaId)));
			
			if (veiculo == null) {
				
				validator.add(new ValidationMessage("Veículo inválido", "error"));
			}
		}
		
		result.include("dataInicio", dataInicio);
		result.include("dataFim", dataFim);
		result.include("formato", formato);
		result.include("viaturaId", viaturaId);
		validator.onErrorUse(Results.logic()).redirectTo(RelatorioController.class).ocorrenciasViatura();
				
		Date dataInicial = formataDataInicio(dataInicio);
		Date dataFinal = formataDataFim(dataFim);
		
		List<Ocorrencia> listaOcorrencias = null;
		
		if(status.equals("todos")){
			listaOcorrencias = ocorrenciaService.findOcorrenciaByVeiculo(veiculo, dataInicial, dataFinal, nivelEmergenciaId);
		}else{
			listaOcorrencias = ocorrenciaService.findOcorrenciaAbertasFechadasByVeiculo(veiculo, dataInicial, dataFinal, status, nivelEmergenciaId);
		}
		
		HashMap parametros = new HashMap(); 
		parametros.put("prefeitura", prefeituraNome);
		parametros.put("data", sdf.format(dataInicial) + " - " + sdf.format(dataFinal));
		parametros.put("logo", "/var/www/Imagens/outras/logo-orgao.jpg");
		parametros.put("placa", veiculo.getPlaca());

		if (formato.equals("pdf")) {
			
			relatoriosService.geraRelatorio("ocorrencia_atendida_viatura", parametros, listaOcorrencias);
			
		} else if (formato.equals("excel")) {
			
			relatoriosService.geraRelatorioEcxel("ocorrencia_atendida_viatura", parametros, listaOcorrencias);
		}

		result.nothing();
	
	}
	

	
	@Post
	@MonkeySecurity(role = "relatorio")
	@Path("/relatorio/ocorrencia/veiculo/listaEmergencia")
	public void listaNiveisEmergencia() {
		
		List<NiveisEmergencia> listaNiveisEmergencia = ocorrenciaService.findNivelEmergencia();
		List<CNivelEmergencia> clistaNiveisEmergencia = new ArrayList<CNivelEmergencia>();
		
		for (NiveisEmergencia nivelEmergencia : listaNiveisEmergencia) {
			
			CNivelEmergencia cNivelEmergencia = new CNivelEmergencia();
			cNivelEmergencia.parseFrom(nivelEmergencia);
			
			clistaNiveisEmergencia.add(cNivelEmergencia);
		}

		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(clistaNiveisEmergencia);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
		
	}
	
	@Post
	@MonkeySecurity(role = "relatorio")
	@Path("/relatorio/ocorrencia/agente/listaAgentes")
	public void listaAgentes() {
	
		List<Agente> listaAgentes = agenteService.findAll();
		List<CAgente> cListaAgentes = new ArrayList<CAgente>();
		
		for(Agente agente : listaAgentes) {
			
			CAgente cAgente = new CAgente();
			cAgente.parseFrom(agente);
			cListaAgentes.add(cAgente);
		}	
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cListaAgentes);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
		
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Post
	@MonkeySecurity(role = "relatorio")
	@Path("/relatorios/viatura/estado")
	public void geraRelatorioViaturaEstadoAtual(String formato) {

		List<Veiculo> listaVeiculos = veiculoService.findAll();
		
		for (Veiculo veiculo : listaVeiculos) {
			
			veiculo.setHistorico(historicoService.getUltimoHistorico(veiculo));
		}
		
		HashMap parametros = new HashMap(); 
		parametros.put("prefeitura", prefeituraNome);
		parametros.put("logo", "/var/www/Imagens/outras/logo-orgao.jpg");
		
//		relatoriosService.geraRelatorio("viatura_estado_atual", parametros, listaVeiculos);	
		if (formato.equals("pdf")) {
			
			relatoriosService.geraRelatorio("viatura_estado_atual", parametros, listaVeiculos);
			
		} else if (formato.equals("excel")) {
			
			relatoriosService.geraRelatorioEcxel("viatura_estado_atual", parametros, listaVeiculos);
		}		
	}
	
	
	public Date formataDataInicio(String data) {
	
		Date dataInicial = new Date();
		
		
			
			try {
				dataInicial = sdf.parse(data);
			}catch (ParseException e) {
				dataInicial.setTime(dataInicial.getTime() - 24*60*60*1000);
//				e.printStackTrace();
			} catch (NullPointerException ex) {
				dataInicial.setTime(dataInicial.getTime() - 24*60*60*1000);
			}
			
		
		
		return dataInicial;
	}
	
	public Date formataDataFim(String data) {
		
		Date dataFinal =  new Date();
		
		try {
	
			dataFinal = sdf.parse(data);
			
		} catch (ParseException e) {
			dataFinal.setTime(dataFinal.getTime() - 24*60*60*1000);
//			e.printStackTrace();
		} catch (NullPointerException ex) {
			dataFinal.setTime(dataFinal.getTime() - 24*60*60*1000);
		}
		
		return dataFinal;
	}
}
