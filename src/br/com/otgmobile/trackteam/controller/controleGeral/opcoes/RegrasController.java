package br.com.otgmobile.trackteam.controller.controleGeral.opcoes;


import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioPagina;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioRole;
import br.com.otgmobile.trackteam.service.UsuarioService;

@Resource
public class RegrasController {
	
	private Result result;
	private Validator validator;
	private UsuarioService usuarioService;
	private Integer quantidade = 10;
	
	public RegrasController(Result result, Validator validator, UsuarioService usuarioService) {

		this.result = result;
		this.validator = validator;
		this.usuarioService = usuarioService;
	}


	@Get
	@Path("/controle_geral/opcoes/usuario/regras/listar/")
	@MenuTag(tag = "usuario_regras")
	@MonkeySecurity(role="usuario")
	public void usuarioRegraListar() {
		
	}

	@Path("/controle_geral/usuarios/listaRegras/")
	@MonkeySecurity(role="all")
	public void listaUsuariosTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<UsuarioRole> listaRegras = usuarioService.findAllPaginaRegras(quantidade, pagina);
		result.include("listaRegras", listaRegras);			
		
		Integer qnt = usuarioService.findTotalPaginasRegras();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
				
		result.forwardTo("/json/regraTabela.jsp"); 
	}
	
	@Get
	@Path({"/controle_geral/opcoes/usuario/regras", "/controle_geral/opcoes/usuario/regras/{regra}"})
	@MenuTag(tag = "usuario_regras")
	@MonkeySecurity(role="usuario")
	public void usuarioRegraEditar(UsuarioRole regra, Integer id) {
		
		
		if (id != null) {
			regra = new UsuarioRole(id);
		}
		
		if (regra != null) {
			regra = usuarioService.findRole(regra);
		}
		
		List<UsuarioPagina> paginas = usuarioService.findPaginas();
		
		result.include("regra", regra);
		result.include("paginas", paginas);
	}
	
	@Post
	@Path("/controle_geral/opcoes/usuario/regras")
	@MonkeySecurity(role="usuario")
	public void usuarioRegraSalvar(UsuarioRole regra) {
		
		UsuarioRole pesquisaUsuarioRole = null;
		
		if(regra.getRole() == null || "".equals(regra.getRole())){		
			validator.add(new ValidationMessage("Nome inválido", "error"));
		}
		
		pesquisaUsuarioRole = usuarioService.findRole(regra);
		
		if (regra != null && regra.getId() != null && regra.getId() > 0) {
			
			
			if (pesquisaUsuarioRole.getRole() != null && pesquisaUsuarioRole.getRole().equals(regra.getRole())) {
				
				validator.add(new ValidationMessage("Nome "+regra.getRole() + " já utilizado para outro Regra", "error"));
				
			} else {
				usuarioService.updateRole(regra);

				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Regra alterado com sucesso");
				result.include("avisos", mensagens);
				
				result.use(Results.logic()).redirectTo(RegrasController.class).usuarioRegraListar();
			}
			
		} else 
			if(pesquisaUsuarioRole != null ){
				validator.add(new ValidationMessage("Nome "+regra.getRole() + " já utilizado para outro Regra", "error"));			
		} else {
			
			usuarioService.insertRegra(regra);
			
			Mensagem mensagens = new Mensagem();
			mensagens.setTitulo("Regra adicionado com sucesso");
			result.include("avisos", mensagens);
			
			result.use(Results.logic()).redirectTo(RegrasController.class).usuarioRegraListar();
			
		}
		
		validator.onErrorUse(Results.logic()).redirectTo(RegrasController.class).usuarioRegraEditar(regra, null);
		
	}
	
	@Post
	@Path("/controle_geral/opcoes/usuario/regra/excluir/")
	@MonkeySecurity(role="usuario")
	public void usuarioRegraExcluir(Integer id) {
		
		if (id != null) {
			
			usuarioService.delete(id);

			result.include("status", "ok");		
			result.include("id", id);
			result.include("msgSucesso", "Regra excluido com sucesso");			
			result.forwardTo("/json/resposta.jsp");
			
		} else {

			result.include("status", "error");		
			result.include("id", id);
			result.include("msgSucesso", "Erro ao excluir o grupo");			
			result.forwardTo("/json/resposta.jsp");
		}
	}
	/*
	private List<UsuarioPagina> criaLista(UsuarioRole role) {
					
		UsuarioRole rolePg = grupoService.FindPagina(role);
		return rolePg.getListaPaginas();

	}
	*/
}
