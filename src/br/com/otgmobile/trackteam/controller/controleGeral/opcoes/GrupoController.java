package br.com.otgmobile.trackteam.controller.controleGeral.opcoes;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioGrupo;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioPagina;
import br.com.otgmobile.trackteam.service.GrupoService;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.UsuarioService;

@Resource
public class GrupoController {

	private Result result;
	private Validator validator;
	private GrupoService grupoService;
	private UsuarioService usuarioService;
	private Integer quantidade = 10;
	private final LogService logService;

	public GrupoController(Result result, Validator validator,
			GrupoService grupoService,
			UsuarioService usuarioService,
			LogService logService) {

		this.result = result;
		this.validator = validator;
		this.grupoService = grupoService;
		this.usuarioService = usuarioService;
		this.logService = logService;
	}

	@Get
	@Path({ "/controle_geral/opcoes/usuario/grupos",
			"/controle_geral/opcoes/usuario/grupos/{usuarioGrupo}" })
	@MenuTag(tag = "usuario-grupo")
	@MonkeySecurity(role = "usuario")
	public void usuarioGrupoEditar(UsuarioGrupo usuarioGrupo, Integer id) {

		if (id != null) {
			
			usuarioGrupo = new UsuarioGrupo(id);
		}
		
		if (usuarioGrupo != null) {
			
			usuarioGrupo = grupoService.find(usuarioGrupo);
		}
		
		List<UsuarioPagina> paginas = usuarioService.findPaginas();
		
		result.include("usuarioGrupo", usuarioGrupo);
		result.include("paginas", paginas);
	}

	@Post
	@Path("/controle_geral/opcoes/usuario/grupos")
	@MonkeySecurity(role="usuario")
	public void UsuarioGrupoSalvar(UsuarioGrupo usuarioGrupo) {		
		
		UsuarioGrupo pesquisarUsuarioGrupo = grupoService.findNome(usuarioGrupo);

		if(usuarioGrupo.getNome() == null || "".equals(usuarioGrupo.getNome())){		
			
			validator.add(new ValidationMessage("Nome inválido", "error"));
		
		}
			
		List<UsuarioPagina> paginas = new ArrayList<UsuarioPagina>();
		
		if(usuarioGrupo.getPaginas() != null){
			for (UsuarioPagina pagina : usuarioGrupo.getPaginas()) {
				
				pagina = usuarioService.findPagina(pagina);
				paginas.add(pagina);
			}	
		}
		
					

		if (usuarioGrupo != null && usuarioGrupo.getId() != null && usuarioGrupo.getId() > 0) {
			
			UsuarioGrupo usuarioGrupoBD = grupoService.find(usuarioGrupo);
			
			if (pesquisarUsuarioGrupo.getNome() == null && !pesquisarUsuarioGrupo.getNome().equals(usuarioGrupoBD.getNome())) {
				
				validator.add(new ValidationMessage("Nome "+usuarioGrupo.getNome() + " já utilizado para outro Grupo", "error"));
				
			} else {
				
				usuarioGrupoBD.setNome(usuarioGrupo.getNome());
				usuarioGrupoBD.setGrupoAtivo(usuarioGrupo.getGrupoAtivo());
				usuarioGrupoBD.setPaginas(paginas);
				
				grupoService.update(usuarioGrupoBD);
				logService.gravaLog("Grupo", "change", "Alterado grupo id -> " + usuarioGrupoBD.getId(), usuarioGrupoBD, usuarioGrupo);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Grupo alterado com sucesso");
				result.include("avisos", mensagens);
				
				result.use(Results.logic()).redirectTo(GrupoController.class).usuarioGrupoListar();
			}
			
		} else if (pesquisarUsuarioGrupo.getNome() == null) {
			
			if (usuarioGrupo.getGrupoAtivo() == null) {
			
				usuarioGrupo.setGrupoAtivo(false);
			}

			usuarioGrupo.setPaginas(paginas);
			usuarioGrupo = grupoService.Salvar(usuarioGrupo);
			logService.gravaLog("Grupo", "add", "Adicionado no grupo id -> " + usuarioGrupo.getId(), usuarioGrupo, null);
			
			Mensagem mensagens = new Mensagem();
			mensagens.setTitulo("Grupo adicionado com sucesso");
			result.include("avisos", mensagens);

			result.use(Results.logic()).redirectTo(GrupoController.class).usuarioGrupoListar();
			
		} else {
			
			validator.add(new ValidationMessage("Nome "+usuarioGrupo.getNome() + " já utilizado para outro Grupo", "error"));			
		}
		
		validator.onErrorUse(Results.logic()).redirectTo(GrupoController.class).usuarioGrupoEditar(usuarioGrupo, null);
		
	}

	@Get
	@Path("/controle_geral/opcoes/usuario/grupos/listar/")
	@MenuTag(tag = "usuario-grupo")
	@MonkeySecurity(role="usuario")
	public void usuarioGrupoListar() {
		
	}
	
	@Path("/controle_geral/usuarios/listaGrupos/")
	@MonkeySecurity(role="all")
	public void listaGruposTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<UsuarioGrupo> listaGrupos = grupoService.findAllPaginaGrupos(quantidade, pagina);
		result.include("listaGrupos", listaGrupos);			
		
		Integer qnt = grupoService.findTotalPaginasGrupos();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
				
		result.forwardTo("/json/grupoTabela.jsp");		
	}

	@Post
	@Path("/controle_geral/usuarios/grupo/excluir/")
	@MonkeySecurity(role="ocorrencia")	
	public void excluir(Integer id) {
		
		if (id != null) {
			
			grupoService.delete(new UsuarioGrupo(id));
			logService.gravaLog("Grupo", "excluir", "Excluido grupo id = " + id + " com sucesso");

			result.include("status", "ok");		
			result.include("id", id);
			result.include("msgSucesso", "Grupo excluido com sucesso");			
			result.forwardTo("/json/resposta.jsp");
			
		} else {

			result.include("status", "error");		
			result.include("id", id);
			result.include("msgSucesso", "Erro ao excluir o grupo");			
			result.forwardTo("/json/resposta.jsp");
		}
	}	
}
