package br.com.otgmobile.trackteam.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.GeoPontoCerca;
import br.com.otgmobile.trackteam.modal.GeoPontoRota;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Local;
import br.com.otgmobile.trackteam.modal.Materiais;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.Resposta;
import br.com.otgmobile.trackteam.modal.Rota;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CCerca;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.modal.comunicacao.CObjeto;
import br.com.otgmobile.trackteam.modal.comunicacao.CRota;
import br.com.otgmobile.trackteam.modal.comunicacao.CVeiculo;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.CercaService;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MateriaisService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.ObjetoService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.RotaService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.util.Json;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class VeiculoController {

	private Result result;
	private VeiculoService veiculoService;
	private Validator validator;
	private OcorrenciaService ocorrenciaService;
	private MateriaisService materiaisService;
	private ObjetoService objetoService;
	private CercaService cercaService;
	private RotaService rotaService;
	private MenuService menuService;
	private Integer quantidade;
	private HttpServletResponse response;
	public final ServletRequest request;
	public final LogService logService;
	
	public VeiculoController(Result result, VeiculoService veiculoService, Validator validator, 
			OcorrenciaService ocorrenciaService, MateriaisService materiaisService, 
			ObjetoService objetoService,CercaService cercaService, RotaService rotaService, 
			MenuService menuService, HttpServletResponse response,
			ServletRequest request,
			LogService logService) {
		
		this.result = result;
		this.veiculoService = veiculoService;
		this.validator = validator;
		this.ocorrenciaService = ocorrenciaService;
		this.materiaisService = materiaisService;
		this.objetoService = objetoService;
		this.cercaService = cercaService;
		this.rotaService= rotaService;
		this.menuService= menuService;
		this.quantidade = 10;
		this.response = response;
		this.request = request;
		this.logService = logService;
	}	
	
	@MonkeySecurity(role="all")
	public void detalhes(Integer id) {
		
		Veiculo veiculo = new Veiculo();
		veiculo.setId(id);
		veiculo = veiculoService.find(veiculo);
		List<Ocorrencia> ocorrencias = ocorrenciaService.findOcorrenciaByVeiculo(veiculo);

		result.include("veiculo", veiculo);
		result.include("ocorrencias", ocorrencias);		
	}	

	@Get
	@Path("/controle_geral/veiculo/adicionar/")
	@MonkeySecurity(role="veiculo")
	public void cadastro(Veiculo veiculo) {
		
		SubMenu subMenu = new SubMenu();
		subMenu.setTag("cadastro-veiculo");
		subMenu = menuService.findByTag(subMenu);
		
		List<Dominio> listaTipo = veiculoService.findTipo();
		List<Local> listaLocal = veiculoService.findLocal();
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
		result.include("listaTipo", listaTipo);
		result.include("listaLocal", listaLocal);
		result.include("veiculo", veiculo);
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);	
	}

	@Post
	@Path("/controle_geral/veiculo/adicionar/")	
	@MonkeySecurity(role="veiculo")
	public void adicionar(Veiculo veiculo) {
		
		if (veiculo == null) {
			
			result.use(Results.logic()).redirectTo(VeiculoController.class).cadastro(veiculo);
		
		} else {
		
			if (veiculo.getPlaca() == null || "".equals(veiculo.getPlaca())) {
				
				result.include("erroPlaca", true);
				validator.add(new ValidationMessage("Placa inválida", "error"));
			}

			if (veiculo.getModelo() == null || "".equals(veiculo.getModelo())) {
				
				result.include("erroModelo", true);
				validator.add(new ValidationMessage("Modelo inválido", "error"));
			}			

			if (veiculo.getAno() == null || "".equals(veiculo.getAno())) {
				
				result.include("erroAno", true);
				validator.add(new ValidationMessage("Ano inválido", "error"));
			}			

			if (veiculo.getCor() == null || "".equals(veiculo.getCor())) {
				
				result.include("erroCor", true);
				validator.add(new ValidationMessage("Cor inválida", "error"));
			}			

			if (veiculo.getMarca() == null || "".equals(veiculo.getMarca())) {
				
				result.include("erroMarca", true);
				validator.add(new ValidationMessage("Marca inválida", "error"));
			}			

			if (veiculo.getRenavam() == null || "".equals(veiculo.getRenavam())) {
				
				result.include("erroRenavam", true);
				validator.add(new ValidationMessage("Renavam inválido", "error"));
			}			
						
			if (veiculo.getRota().getId() == null || veiculo.getRota().getId() <= 0) {
				
				veiculo.setRota(null);
			}

			if (veiculo.getCerca().getId() == null || veiculo.getCerca().getId() <= 0) {
				
				veiculo.setCerca(null);
			}

			
			validator.onErrorUse(Results.logic()).redirectTo(VeiculoController.class).cadastro(veiculo);
	
			if (veiculo.getId() == null || veiculo.getId() <= 0) {
				
				veiculo = veiculoService.insert(veiculo);
				logService.gravaLog("Veiculo", "add", "Adicionado veículo id -> " + veiculo.getId(), new CVeiculo(veiculo), null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Veículo adicionado com sucesso");
				result.include("avisos", mensagens);

			} else {
				
				veiculo = veiculoService.update(veiculo);
				logService.gravaLog("Veiculo", "change", "Alterado veículo id -> " + veiculo.getId(), new CVeiculo(veiculo), null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Veículo alterado com sucesso");
				result.include("avisos", mensagens);
				
			}
/*				
			String json = Util.formataJSON("10", "Afff", agente.toJSON());
			Util.excutePost("http://192.168.0.198:8000/add/", "json=" + json);
*/
			result.use(Results.logic()).redirectTo(VeiculoController.class).listar(0);	
		}
	}

	@Path("/controle_geral/veiculo/editar/")	
	@MonkeySecurity(role="veiculo")
	public void editar(Integer id) {
		
		Veiculo veiculo = new Veiculo();

		if (id != null && id > 0) {
			
			veiculo.setId(id);	
			veiculo = veiculoService.find(veiculo);
			
		} 

		result.use(Results.logic()).forwardTo(VeiculoController.class).cadastro(veiculo);
	}

	@Path("/controle_geral/veiculo/excluir/")
	@Post
	@MonkeySecurity(role = "veiculo")
	public void excluir(Integer id) {

		Veiculo veiculo = new Veiculo(id);

		veiculo = veiculoService.excluirVeiculo(veiculo);
		logService.gravaLog("Veiculo", "delete", "Removido veiculo - id -> " + veiculo.getId());

		result.include("status", "ok");
		result.include("id", id);
		result.include("msgSucesso", "Veículo excluido com sucesso");
		result.forwardTo("/json/resposta.jsp");

	}

	@Path("/controle_geral/veiculo/cerca/excluir/")
	@Post
	@MonkeySecurity(role = "veiculo")
	public void excluirCerca(Integer id) {

		Cerca cerca = new Cerca(id);

		cerca = veiculoService.excluirCerca(cerca);
		logService.gravaLog("Veiculo", "delete", "Removida cerca - id -> " + cerca.getId());

		Resposta resposta = new Resposta();
		resposta.setStatus("sucesso");
		resposta.setSucesso("Cerca excluida com sucesso");
		
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export(resposta));		
	}

	@Path("/controle_geral/veiculo/rota/excluir/")
	@Post
	@MonkeySecurity(role = "veiculo")
	public void excluirRota(Integer id) {

		Rota rota = new Rota(id);

		rota = veiculoService.excluirRota(rota);
		logService.gravaLog("Veiculo", "delete", "Removida rota - id -> " + rota.getId());

		Resposta resposta = new Resposta();
		resposta.setStatus("sucesso");
		resposta.setSucesso("Rota excluida com sucesso");
		
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export(resposta));		
	}
	
	@Path("/controle_geral/veiculo/listar/")
	@MonkeySecurity(role="veiculo")
	public void listar(Integer pagina) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-veiculo");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
		if (pagina == null || pagina <= 0) {
			pagina=1;
		}
		
		List<Veiculo> listaVeiculos = veiculoService.findAllPagina(quantidade, pagina);
		result.include("listaVeiculos", listaVeiculos);	
	}
	
	@Path("/controle_geral/veiculo/listaVeiculos/")
	@MonkeySecurity(role="all")
	public void listaVeiculosTabela(Integer pagina) {
		
		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<Veiculo> listaVeiculos = veiculoService.findAllPagina(quantidade, pagina);
		result.include("listaVeiculos", listaVeiculos);			
		
		Integer qnt = veiculoService.findTotal();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
				
		result.forwardTo("/json/veiculoTabela.jsp"); 
	}
	
	@MonkeySecurity(role="all")	
	public void listaVeiculos() {
	
		List<Veiculo> listaVeiculos = veiculoService.findAll();
		List<CVeiculo> cListaVeiculo = new ArrayList<CVeiculo>();
		
		for(Veiculo veiculo : listaVeiculos) {
			
			CVeiculo cVeiculo = new CVeiculo();
			cVeiculo.parseFrom(veiculo);
			cListaVeiculo.add(cVeiculo);
		}	
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cListaVeiculo);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");
		result.use(Results.http()).body(jo.toString());
	}

	@MonkeySecurity(role="all")
	public void listaVeiculosHistorico() {
		
		List<Historico> listaHistorico = veiculoService.findAllPosicao();
		List<CHistorico> cListaHistorico = new ArrayList<CHistorico>();
		
		for(Historico historico : listaHistorico) {
			
			CHistorico cHistorico = new CHistorico();
			cHistorico.parseFrom(historico);
			cListaHistorico.add(cHistorico);
		}	
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cListaHistorico);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
	}

	@Post
	@Path("/controle_geral/veiculo/rota/salvar/")
	@MonkeySecurity(role="veiculo")
	public void adicionarRota(Rota rota, List<String> pontos, Integer cerca_id) {
		
		Rota rota_view = new Rota();
		
		if (rota.getId() != null && rota.getId() > 0) {
			
			rota_view.setId(rota.getId());
			rota_view = rotaService.find(rota_view);
			
			if (pontos != null && pontos.size() > 0) {
				
				rota_view = rotaService.excluiGeoPontos(rota_view);
			}
		}
		
		Cerca cerca = new Cerca();
		cerca.setId(cerca_id);
		cerca = cercaService.find(cerca);
		
		rota_view.setNome(rota.getNome());
		rota_view.setCerca(cerca);

		List<GeoPontoRota> geoPontos = new ArrayList<GeoPontoRota>();					

		if (pontos != null && pontos.size() > 0) {
			
			for(String ponto : pontos) {
				
				GeoPontoRota geoPonto = new GeoPontoRota();
				
				String[] geoPontoTmp = ponto.split(";");
				
				if (geoPontoTmp.length >= 2) {
					geoPonto.setLatitude(geoPontoTmp[0]);
					geoPonto.setLongitude(geoPontoTmp[1]);
					geoPonto.setRota(rota_view);
					geoPontos.add(geoPonto);
				}		
			}
			
			rota_view.setGeoPonto(geoPontos);
		}

		Mensagem mensagens = new Mensagem();
				
		if (rota.getId() != null && rota.getId() > 0) {

			rota_view = rotaService.update(rota_view);
			logService.gravaLog("Veiculo", "changeRota", "Alterada rota id -> " + rota_view.getId(), new CRota(rota_view), null);

			mensagens.setTitulo("Rota alterada com sucesso");

		} else {
			
			rota_view = rotaService.insert(rota_view);
			logService.gravaLog("Veiculo", "addRota", "Adicionada rota id -> " + rota_view.getId(), new CRota(rota_view), null);
			
			mensagens.setTitulo("Rota adicionada com sucesso");
		}
		    	
		result.include("avisos", mensagens);
		result.use(Results.logic()).redirectTo(VeiculoController.class).listarRotas();
		
	}
	
	@MonkeySecurity(role="all")
	public void listaCercas() {
		
		List<Cerca> listaCercas = cercaService.findAll();	
		result.include("listaCercas", listaCercas);		
		result.forwardTo("/json/cerca.jsp"); 
	}	

	@Path("/controle_geral/veiculo/cerca/listar/")
	@MonkeySecurity(role="all")
	@MenuTag(tag="cerca")
	public void listarCercas() {
		
	}	


	@MenuTag(tag="cerca")	
	@MonkeySecurity(role="veiculo")
	@Path("/controle_geral/veiculo/cerca/editar/")	
	public void adicionarCerca(String nome, List<String> pontos, Integer id) {
					
		Cerca cerca = new Cerca();
		cerca.setNome(nome);

		if (nome != null && pontos.size() > 2) {
							
				List<GeoPontoCerca> geoPontos = new ArrayList<GeoPontoCerca>();
				
				for(String ponto : pontos) {
					
					GeoPontoCerca geoPonto = new GeoPontoCerca();
					
					String[] geoPontoTmp = ponto.split(",");
					
					if (geoPontoTmp.length >= 2) {
						geoPonto.setLatitude(geoPontoTmp[0]);
						geoPonto.setLongitude(geoPontoTmp[1]);
						geoPonto.setCerca(cerca);
						geoPontos.add(geoPonto);
					}		
				}

				if (id != null && id > 0) {
	
					Cerca cercaBD = new Cerca();
					CCerca cercaOld = new CCerca(cercaBD);
					
					cercaBD.setId(id);
					cercaBD = cercaService.find(cercaBD);
					cercaBD.setNome(nome);
					cercaBD.setGeoPonto(geoPontos);
					
					cercaService.update(cercaBD);					
					logService.gravaLog("Veiculo", "changeCerca", "Alterada cerca id -> " + cercaBD.getId(), new CCerca(cercaBD), cercaOld);
					
					Mensagem mensagens = new Mensagem();
					mensagens.setTitulo("Cerca alterada com sucesso");
					result.include("avisos", mensagens);
					
				} else {
					
					if (!cercaService.existe(cerca)) {
						
						cerca.setGeoPonto(geoPontos);		
						cercaService.insert(cerca);
						logService.gravaLog("Veiculo", "addCerca", "Adicionada cerca id -> " + cerca.getId(), new CCerca(cerca), null);
						
						Mensagem mensagens = new Mensagem();
						mensagens.setTitulo("Cerca adicionada com sucesso");
						result.include("avisos", mensagens);
						
					} else {
						
						result.include("cerca", cerca);
						validator.add(new ValidationMessage("O nome escolhido já está associado a outra cerca", "error"));						
					}
				}
							
		} else {
			
			if (nome != null && pontos.size() > 2) {
				
				result.include("cerca", cerca);
				validator.add(new ValidationMessage("Nome inválido", "error"));			
			} 

			if (nome != null && pontos.size() > 2) {
				
				result.include("cerca", cerca);
				validator.add(new ValidationMessage("Cerca inválida", "error"));			
			} 

		}		
			
		validator.onErrorUse(Results.logic()).forwardTo(VeiculoController.class).editarCerca(null);
	
		result.use(Results.logic()).forwardTo(VeiculoController.class).listarCercas();
	}
	
	@Path("/controle_geral/veiculo/cerca/adicionar/")
	@MonkeySecurity(role="veiculo")
	@MenuTag(tag="cerca")
	public void editarCerca(Integer id) {
		
		if (id != null) {
			
			Cerca cerca = new Cerca();
			cerca.setId(id);
			cerca = cercaService.find(cerca);
			
			result.include("cerca", cerca);			
		}				
	}	

	@Path("/controle_geral/veiculo/listaCercasTabela/")
	@MonkeySecurity(role="all")
	public void listaCercasTabela(Integer pagina) {
		
		if (pagina == null) {

			pagina = 0;
		}

		Integer total = 0;
					
		List<Cerca> listaCercas = cercaService.findAllPagina(quantidade, pagina);
		
		Integer qnt = cercaService.findTotal();
		total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}	
		
		result.include("listaCercas", listaCercas);
		result.include("total", total);
		result.forwardTo("/json/cercaTabela.jsp");
		
	}	
	
	@Post
	@MonkeySecurity(role="veiculo")
	public void cerca(Integer id) {
		
		List<Cerca> listaCercas = new ArrayList<Cerca>();

		Veiculo veiculo = new Veiculo();
		veiculo.setId(id);
		veiculo = veiculoService.find(veiculo);
		
		listaCercas.add(veiculo.getCerca());
		
		result.include("listaCercas", listaCercas);		
		result.forwardTo("/json/cerca.jsp"); 
		
	}

	@Post
	@Path("/controle_geral/veiculo/cerca/")
	@MonkeySecurity(role="veiculo")
	public void listaCerca(Integer id) {
		
		Cerca cerca = new Cerca();
		cerca.setId(id);
		cerca = cercaService.find(cerca);
		
		CCerca cCerca = new CCerca();
		cCerca.parseFrom(cerca);		
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cCerca);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);
		
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(jo.toString());
		
	}
	
	@Path("/controle_geral/veiculo/rota/listar/")
	@MonkeySecurity(role="veiculo")
	@MenuTag(tag="cerca")
	public void listarRotas() {
		
	}

	@Path("/controle_geral/veiculo/listaRotasTabela/")
	@MonkeySecurity(role="all")
	public void listaRotasTabela(Integer pagina) {
		
		if (pagina == null) {

			pagina = 0;
		}

		Integer total = 0;
					
		List<Rota> listaRotas = rotaService.findAllPagina(quantidade, pagina);
		
		Integer qnt = rotaService.findTotal();
		total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}	
		
		result.include("listaRotas", listaRotas);
		result.include("total", total);
		result.forwardTo("/json/rotaTabela.jsp");
		
	}

	@Path("/controle_geral/veiculo/rota/adicionar/")
	@MonkeySecurity(role="veiculo")
	@MenuTag(tag="cerca")
	public void editarRota(Integer id) {
		
		List<Cerca> listaCercas = cercaService.findAll();
		result.include("listaCercas", listaCercas);					
		
		Rota rota = new Rota();

		if (id != null) {
			
			rota.setId(id);
			rota = rotaService.find(rota);
			
			result.include("totalGeoPontos", rota.getGeoPonto().size() - 1);	
		} else {
			
			rota.setCerca(listaCercas.get(0));
		}
		
		result.include("rota", rota);
	}	
	
	@MonkeySecurity(role="veiculo")
	public void rota(Integer id) {
		
		List<Rota> listaRotas = new ArrayList<Rota>();

		Veiculo veiculo = new Veiculo();
		veiculo.setId(id);
		veiculo = veiculoService.find(veiculo);

		listaRotas.add(veiculo.getRota());

		result.include("listaRotas", listaRotas);		
		result.forwardTo("/json/rota.jsp"); 
		
	}
	
	
	@MonkeySecurity(role="all")
	public void listaRotas(Integer cerca_id) {
		
		List<Rota> listaRotas;
		System.out.println(cerca_id);
		
		if (cerca_id == null || cerca_id == 0) {
		
			listaRotas = rotaService.findAll();
			
		} else {
			
			Cerca cerca = new Cerca();
			cerca.setId(cerca_id);
			
			listaRotas = rotaService.findByCerca(cerca);
		}
		
		result.include("listaRotas", listaRotas);		
		result.forwardTo("/json/rota.jsp"); 
		
//      result.use(json()).from(listaCercas).include("geoPonto").serialize();        
//	    result.use(Results.http()).body("alguma coisa");  	    	
//		result.nothing();
	}	
	
	

	@Path({"/controle_geral/veiculo/materiais/{veiculo.id}", "/controle_geral/veiculo/materiais/"})
	@MonkeySecurity(role="veiculo")
	public void editarMateriais(Veiculo veiculo, Integer id) {

		if (veiculo == null && id != null) {
			
			veiculo = new Veiculo();
			veiculo.setId(id);
			
		}
		
		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-veiculo");
		subMenu = menuService.findByTag(subMenu);
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		List<Menu> listaMenu = menuService.listMenu();			
		
		List<Dominio> listaFuncionalidade = materiaisService.findFuncionalidade();			
		List<Materiais> listaMateriais = objetoService.findMateriais(listaFuncionalidade.get(0));		
				
		result.include("listaFuncionalidade", listaFuncionalidade);	
		result.include("listaMateriais", listaMateriais);		
		result.include("veiculo", veiculo);
		result.include("materialSelecionado", listaMateriais.get(0));
		
		result.include("menuQuick", listaMenu);	
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);
	}
	
	@Post
	@Path("/controle_geral/veiculo/materiais/associar/")
	@MonkeySecurity(role="veiculo")
	public void associarMateriais(Veiculo veiculo, List<Integer> id) {
	
		if (id == null || id.size() <= 0) {
			
			result.include("msgSucesso", "Nenhum material associado");
			result.include("status", "error");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		
			
		} else if (veiculo == null) {
			
			result.use(Results.logic()).redirectTo(VeiculoController.class).listar(0);
		
		} else {
			
			veiculo = veiculoService.find(veiculo);
			List<Objeto> listaObjetos = veiculo.getListaObjetos();
			List<CObjeto> cListaObjetos = new ArrayList<CObjeto>();
			
			for (Integer idTmp : id) {
				
				Objeto objeto = new Objeto();
				objeto.setId(idTmp);
				objeto = objetoService.find(objeto);
				
				listaObjetos.add(objeto);
				
			}
			
			for (Objeto objeto : listaObjetos) {
				
				cListaObjetos.add(new CObjeto(objeto));
			}
									
			veiculo.setListaObjetos(listaObjetos);
			veiculo = veiculoService.update(veiculo);
			logService.gravaLog("Veiculo", "changeMateriais", "Associados materiais ao veículo id -> " + veiculo.getId(), cListaObjetos, null);
			
/*
			Mensagem mensagens = new Mensagem();
			mensagens.setTitulo("Objeto(s) associado(s) com sucesso");
			result.include("avisos", mensagens);
*/
			
			result.include("msgSucesso", "Materiais associados com sucesso");
			result.include("status", "ok");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		
		}
	}

	@Get
	@Path("/controle_geral/veiculo/materiais/remover/")
	@MonkeySecurity(role="veiculo")
	public void removerMateriais(Veiculo veiculo, Objeto objeto) {
	
        if (veiculo == null || objeto == null) {
			
			result.include("status", "erro");		
			result.forwardTo("/json/resposta.jsp"); 		
		
		} else {
			
			veiculo = veiculoService.find(veiculo);
			List<Objeto> listaObjetos = veiculo.getListaObjetos();
			List<Objeto> listaObjetosRemovidos = new ArrayList<Objeto>();
			List<CObjeto> cListaObjetos = new ArrayList<CObjeto>();
			
			for (Objeto objetoLista : listaObjetos) {
				
				if (objetoLista.getId().equals(objeto.getId())) {
					
					listaObjetosRemovidos.add(objetoLista);
				}				
			}
				
			for (Objeto objetoLista : listaObjetosRemovidos) {
				
				listaObjetos.remove(objetoLista);
			}

			for (Objeto objetoLista : listaObjetos) {
				
				cListaObjetos.add(new CObjeto(objeto));
			}
			
			veiculo.setListaObjetos(listaObjetos);
			veiculo = veiculoService.update(veiculo);
			logService.gravaLog("Veiculo", "changeMateriais", "Associados materiais ao veículo id -> " + veiculo.getId(), cListaObjetos, null);

			result.include("status", "ok");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		}
	}
	
	@MonkeySecurity(role="all")
	@Path("/veiculo/materiais/listaObjetos/")
	public void listaObjetosAssociadosTabela(Integer pagina, Veiculo veiculo) {

		if (pagina == null) {

			pagina = 0;
		}

		Integer total = 0;
		List<Objeto> listaObjetosAssociados = null;
		
		if (veiculo != null && veiculo.getId() != null) {
			
			listaObjetosAssociados = veiculoService.findAllObjetosAssociadosPagina(quantidade, pagina, veiculo);
			
			Integer qnt = veiculoService.findTotalObjetosAssociados(veiculo);
			total = qnt / quantidade;
			
			if ((qnt % quantidade) > 0 ) {
				
				total++;
			}
			
		}
		
		result.include("listaObjetos", listaObjetosAssociados);
		result.include("total", total);
		result.forwardTo("/json/objetoTabela.jsp");
	}

	@Get
	@Path("/veiculo/gps/")
	public void adicionarHistoricoGps(String id, String latitude, String longitude, String velocidade, String temperatura, String bateria, String rpm) {
		
		Veiculo veiculo = veiculoService.findByIdGps(id);
			
		if (veiculo != null && veiculo.getId() > 0) {				
			
			Historico historico = new Historico();
			historico.setDataHora(new Timestamp(new Date().getTime()));
			historico.setLatitude(latitude);
			historico.setLongitude(longitude);
			historico.setRpm(rpm);
			historico.setTemperatura(temperatura);
			historico.setTensao_bateria(bateria);
			historico.setVelocidade(velocidade);
			historico.setVeiculo_id(veiculo.getId());
			
			veiculoService.insertHistorico(historico);
			
			if (!historico.getLatitude().equals("0.0") || !historico.getLongitude().equals("0.0")) {
				
				veiculo.setLatitude(historico.getLatitude());
				veiculo.setLongitude(historico.getLongitude());
				veiculoService.update(veiculo);
			}
		}
		
		result.nothing();
//		GET http://187.58.67.82:8096/php/compbordo.php?id=NQX6748&latitude=-3.966453&longitude=-38.707092&velocidade=250&temperatura=400&bateria=14&rpm=7200\r\n
//		GET http://187.58.67.83:8080/smc/veiculo/gps/?id=NQX6748&latitude=-3.966453&longitude=-38.707092&velocidade=250&temperatura=400&bateria=14&rpm=7200		
	}
	
}
