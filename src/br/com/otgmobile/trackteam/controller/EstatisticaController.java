package br.com.otgmobile.trackteam.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.service.HistoricoService;
import br.com.otgmobile.trackteam.service.VeiculoService;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class EstatisticaController {

	private final Result result;
	private final HttpServletResponse response;
	private final VeiculoService veiculoService;
	private final HistoricoService historicoService;
	
	public EstatisticaController(Result result, 
			HttpServletResponse response,
			HistoricoService historicoService,
			VeiculoService veiculoService) {
		
		this.result = result;
		this.response = response;
		this.veiculoService = veiculoService;
		this.historicoService = historicoService;
	}	
	
	@Get
	@Path("/estatistica/veiculo/historico/{veiculo_id}")
	@MonkeySecurity(role="estatistica")
	public void historicoVeiculo(Integer veiculo_id) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, -24);
		
		result.include("dataInicio", format.format(c.getTime()));
		result.include("veiculo_id", veiculo_id);			
	}
		
	@Post
	@Path("/estatistica/veiculo/historico/")
	@MonkeySecurity(role="estatistica")
	public void historicoVeiculo(Integer veiculo_id, Integer agente_id, String dataInicio, String dataFim) {

		Date inicio;
		Date fim;
		List<Historico> listaHistorico = new ArrayList<Historico>();
		
		if (dataInicio != null && !"".equals(dataInicio)) {
					
			try {
				
				if (dataInicio != null && !"".equals(dataInicio)) {			
					
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
					inicio = new Date(format.parse(dataInicio).getTime());
					
				} else {					
					inicio = new Date();
				}
			    
				if (dataFim != null && !"".equals(dataFim)) {			
										
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
					fim = new Date(format.parse(dataFim).getTime());
					
				} else {					
					fim = new Date();
				}
				
				if (veiculo_id != null && veiculo_id > 0) {
					
					Veiculo veiculo = new Veiculo(veiculo_id);
					listaHistorico = historicoService.getHistorico(veiculo, inicio, fim);
				}
				
				List<CHistorico> cListaHistorico = new ArrayList<CHistorico>();
				
				for (Historico historico : listaHistorico) {
					
					CHistorico cHistorico = new CHistorico();
					cHistorico.parseFrom(historico);
					
					cListaHistorico.add(cHistorico);
				}
				
				Gson gson = new Gson();
				JsonElement je = gson.toJsonTree(cListaHistorico);
			    JsonObject jo = new JsonObject();
			    jo.add("result", je);

		        response.setContentType("application/json; charset=UTF-8");  
				result.use(Results.http()).body(jo.toString());
				
			} catch (ParseException e) {
				
				System.out.println("Erro ao converter data");
			}
		}
	}	
}
