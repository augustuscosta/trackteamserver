package br.com.otgmobile.trackteam.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Materiais;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.comunicacao.CAgente;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioGrupo;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MateriaisService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.ObjetoService;
import br.com.otgmobile.trackteam.service.UsuarioService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.util.Util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class AgenteController {

	private Result result;
	private Validator validator;
	private VeiculoService veiculoService;
	private AgenteService agenteService;
	private UsuarioService usuarioService;	
	private MenuService menuService;
	private MateriaisService materiaisService;
	private ObjetoService objetoService;
	private Integer quantidade;
	private HttpServletResponse response;
	private final LogService logService;

	public AgenteController(Result result, 
			Validator validator, 
			VeiculoService veiculoService, 
			AgenteService agenteService, 
			MenuService menuService,MateriaisService materiaisService,
			ObjetoService objetoService,
			UsuarioService usuarioService,
			HttpServletResponse response,
			LogService logService) {

		this.result = result;
		this.validator = validator;
		this.veiculoService = veiculoService;
		this.agenteService = agenteService;
		this.menuService= menuService;
		this.materiaisService = materiaisService;
		this.objetoService = objetoService;
		this.usuarioService= usuarioService;
		this.response = response;
		this.quantidade = 10;
		this.logService = logService;
	}

	@Get
	@Path("/controle_geral/agentes/adicionar/")
	@MonkeySecurity(role="agente")
	public void cadastro(Agente agente) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("cadastro-agente");
		subMenu = menuService.findByTag(subMenu);
		
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);		

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	

		List<UsuarioGrupo> listaGrupos = usuarioService.findGrupos();
		result.include("listaGrupos", listaGrupos);	
		
		List<Dominio> listaStatus = agenteService.findStatus();
		result.include("listaStatus", listaStatus);	
		result.include("agente", agente);
	}

	@Post 
	@Path("/controle_geral/agentes/adicionar/")
	@MonkeySecurity(role="agente")
	public void adicionar(Agente agente) {
		
		if (agente == null) {
			
			result.use(Results.logic()).redirectTo(AgenteController.class).cadastro(agente);
		
		} else {
		
			if (agente.getUsuario().getNome() == null || "".equals(agente.getUsuario().getNome())) {
				
				result.include("erroNome", true);
				validator.add(new ValidationMessage("Nome inválido", "error"));
			}

			if (agente.getUsuario().getCpf() == null || "".equals(agente.getUsuario().getCpf())) {
				
				result.include("erroCpf", true);
				validator.add(new ValidationMessage("Cpf inválido", "error"));
			}

			if (agente.getUsuario().getDescricao() == null || "".equals(agente.getUsuario().getDescricao())) {
				
				result.include("erroDescricao", true);
				validator.add(new ValidationMessage("Descrição inválida", "error"));
			}			
			
			if (agente.getUsuario().getMatricula() == null || "".equals(agente.getUsuario().getMatricula())) {
				
				result.include("erroMatricula", true);
				validator.add(new ValidationMessage("Matricula inválida", "error"));
			}				
			
			if (agente.getUsuario().getLogin() == null || "".equals(agente.getUsuario().getLogin())) {
				
				result.include("erroLogin", true);
				validator.add(new ValidationMessage("Login inválido", "error"));
			}	

			if ((agente.getUsuario().getSenha() == null || agente.getUsuario().getSenha().length() < 5) && (agente.getId() == null || agente.getId() <=0)) {
				
				result.include("erroSenha", true);
				validator.add(new ValidationMessage("A senha precisa ter pelo menos 5 caracteres", "error"));
			}	

			validator.onErrorUse(Results.logic()).redirectTo(AgenteController.class).cadastro(agente);

			if (agente.getVeiculo() != null && agente.getVeiculo().getId() != null && agente.getVeiculo().getId() > 0) {
				
				agente.setVeiculo(veiculoService.find(agente.getVeiculo()));
			} else {
				
				agente.setVeiculo(null);
			}
		
			if (agente.getId() == null || agente.getId() <= 0) {
				
				Usuario usuario = agente.getUsuario();
				usuario.setSenha(Util.md5(agente.getUsuario().getSenha()));
				agente.setUsuario(usuario);
				
				agente = agenteService.insert(agente);
				logService.gravaLog("Agente", "add", "Adicionado novo agente", new CAgente(agente), null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Agente adicionado com sucesso");
				result.include("avisos", mensagens);
				
			} else {
				
				Agente agenteBD = agenteService.find(agente);
				agenteBD.setVeiculo(agente.getVeiculo());
				
				Usuario usuario = agenteBD.getUsuario();
				usuario.setCpf(agente.getUsuario().getCpf());
				usuario.setDescricao(agente.getUsuario().getDescricao());
				usuario.setGrupo(agente.getUsuario().getGrupo());
				usuario.setLogin(agente.getUsuario().getLogin());
				usuario.setMatricula(agente.getUsuario().getMatricula());
				usuario.setNome(agente.getUsuario().getNome());
				usuario.setStatus(agente.getUsuario().getStatus());
				
				agente = agenteService.update(agenteBD);
				logService.gravaLog("Agente", "change", "Alterado agente id -> " + agenteBD.getId(), new CAgente(agenteBD), new CAgente(agente));
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Agente alterado com sucesso");
				result.include("avisos", mensagens);				
			}
/*				
			String json = Util.formataJSON("10", "Afff", agente.toJSON());
			Util.excutePost("http://192.168.0.198:8000/add/", "json=" + json);
*/
			result.use(Results.logic()).redirectTo(AgenteController.class).listar(0);	
		}
	}

	@Path("/controle_geral/agentes/editar/")
	@MonkeySecurity(role="agente")
	public void editar(Integer id) {
		
		Agente agente = new Agente();

		if (id != null && id > 0) {
			
			agente.setId(id);	
			agente = agenteService.find(agente);
			
		} 

		result.use(Results.logic()).forwardTo(AgenteController.class).cadastro(agente);	
	}

	@Path("/controle_geral/agentes/listar/")
	@MonkeySecurity(role="agente")
	public void listar(Integer pagina) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-agente");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
	}
	
	@Path("/controle_geral/agentes/listaAgentes/")
	@MonkeySecurity(role="agente")
	public void listaAgentesTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<Agente> listaAgentes = agenteService.findAllPagina(quantidade, pagina);
		result.include("listaAgentes", listaAgentes);			
		
		Integer qnt = agenteService.findTotal();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
		result.forwardTo("/json/agenteTabela.jsp"); 
	}
	
	@MonkeySecurity(role="all")
	public void listaAgentes() {
	
		List<Agente> listaAgentes = agenteService.findAll();
		List<CAgente> cListaAgentes = new ArrayList<CAgente>();
		
		for(Agente agente : listaAgentes) {
			
			CAgente cAgente = new CAgente();
			cAgente.parseFrom(agente);
			cListaAgentes.add(cAgente);
		}	
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cListaAgentes);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
		
	}
	
	@MonkeySecurity(role="all")
	public void listaAgentesHistorico() {
		
		List<Historico> listaHistorico = agenteService.findAllPosicao();
		List<CHistorico> cListaHistorico = new ArrayList<CHistorico>();
		
		for(Historico historico : listaHistorico) {
			
			CHistorico cHistorico = new CHistorico();
			cHistorico.parseFrom(historico);
			cListaHistorico.add(cHistorico);
		}	
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cListaHistorico);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
	}	
	
	
	@Path("/controle_geral/agentes/excluir/")
	@Post
	@MonkeySecurity(role="agente")	
	public void excluir(Integer id) {
		
		Agente agente = new Agente();
		agente.setId(id);
		
		agente = agenteService.findDeletarAgente(agente);
		logService.gravaLog("Agente", "delete", "Deletado agente id -> " + agente.getId());		

		result.include("status", "ok");		
		result.include("id", id);
		result.include("msgSucesso", "Agente excluido com sucesso");			
		result.forwardTo("/json/resposta.jsp");
		
	}
	
	@Path({"/controle_geral/agentes/materiais/{agente.id}", "/controle_geral/agentes/materiais/"})
	@MonkeySecurity(role="all")
	public void materiais(Agente agente, Integer id) {

		if (agente == null && id != null) {
			
			agente = new Agente();
			agente.setId(id);
			
		}
		
		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-agente");
		subMenu = menuService.findByTag(subMenu);
		
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		List<Menu> listaMenu = menuService.listMenu();			
		
		List<Dominio> listaFuncionalidade = materiaisService.findFuncionalidade();			
		List<Materiais> listaMateriais = objetoService.findMateriais(listaFuncionalidade.get(0));		
				
		result.include("listaFuncionalidade", listaFuncionalidade);	
		result.include("listaMateriais", listaMateriais);		
		result.include("agente", agente);
		result.include("materialSelecionado", listaMateriais.get(0));
		
		result.include("menuQuick", listaMenu);	
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);
	}
	
	@Post
	@Path("/controle_geral/agentes/materiais/associar/")
	@MonkeySecurity(role="all")
	public void associarMateriais(Agente agente, List<Integer> id) {
	
        if (agente == null) {
			
			result.use(Results.logic()).redirectTo(AgenteController.class).listar(0);
		
		} else {
			
			agente = agenteService.find(agente);
			List<Objeto> listaObjetos = agente.getListaObjetos();
			
			for (Integer idTmp : id) {
				
				Objeto objeto = new Objeto();
				objeto.setId(idTmp);
				objeto = objetoService.find(objeto);
				
				listaObjetos.add(objeto);
				
			}
									
			agente.setListaObjetos(listaObjetos);
			agente = agenteService.update(agente);
			logService.gravaLog("Agente", "addMateriais", "Adicionados materiais ao agente id -> " + agente.getId(), listaObjetos, null);
			
			result.include("status", "ok");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		
		}
	}
	
	@MonkeySecurity(role="all")
	@Path("/agentes/materiais/listaObjetos/")
	public void listaObjetosAssociadosTabela(Integer pagina, Agente agente) {

		if (pagina == null) {

			pagina = 0;
		}

		Integer total = 0;
		List<Objeto> listaObjetosAssociados = null;
		
		if (agente != null && agente.getId() != null) {
			
			listaObjetosAssociados = agenteService.findAllObjetosAssociadosPagina(quantidade, pagina, agente);
			
			Integer qnt = agenteService.findTotalObjetosAssociados(agente);
			total = qnt / quantidade;
			
			if ((qnt % quantidade) > 0 ) {
				
				total++;
			}
			
		}
		
		result.include("listaObjetos", listaObjetosAssociados);
		result.include("total", total);
		result.forwardTo("/json/objetoTabela.jsp");
	}
	
	@Get
	@Path("/controle_geral/agentes/materiais/remover/")
	@MonkeySecurity(role="all")
	public void removerMateriais(Agente agente, Objeto objeto) {
	
        if (agente == null || objeto == null) {
			
			result.include("status", "erro");		
			result.forwardTo("/json/resposta.jsp"); 		
		
		} else {
			
			agente = agenteService.find(agente);
			List<Objeto> listaObjetos = agente.getListaObjetos();
			List<Objeto> listaObjetosRemovidos = new ArrayList<Objeto>();
			
			for (Objeto objetoLista : listaObjetos) {
				
				if (objetoLista.getId().equals(objeto.getId())) {
					
					listaObjetosRemovidos.add(objetoLista);
				}				
			}
				
			for (Objeto objetoLista : listaObjetosRemovidos) {
				
				listaObjetos.remove(objetoLista);
			}
			
			agente.setListaObjetos(listaObjetos);
			agente = agenteService.update(agente);
			logService.gravaLog("Agente", "deleteMateriais", "Removidos objetos do agente id -> " + agente.getId(), listaObjetos, null);

			result.include("status", "ok");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		}
	}
}
