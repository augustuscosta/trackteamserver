package br.com.otgmobile.trackteam.controller;

import static br.com.caelum.vraptor.view.Results.json;

import java.util.List;

import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackteam.modal.ViaturaRota;
import br.com.otgmobile.trackteam.service.ViaturaRotaService;

@Resource
public class ViaturaRotaController {
	
	private ViaturaRotaService viaturaRotaService;
	private Result result;
	
	public ViaturaRotaController(ViaturaRotaService service, Result result) {
		
		this.viaturaRotaService = service;
		this.result = result;
	}

	public void add(ViaturaRota viaturaRota) {
		
		viaturaRotaService.salvar(viaturaRota);
//		result.include("viaturaRota", viaturaRota);

		System.out.println(viaturaRota.getViatura_id() + ":" + viaturaRota.getLatitude() + "," + viaturaRota.getLongitude());
		result.use(json()).from(viaturaRota).serialize();

	}
	
	public void simular() {
		
	}
	
	public void get() {
		
		List<ViaturaRota> lista = viaturaRotaService.findAll();
		result.use(json()).from(lista).serialize();
		
	}
}
