package br.com.otgmobile.trackteam.controller;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Materiais;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MateriaisService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.ObjetoService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.service.VeiculoService;

@Resource
public class ObjetoController {

	private final Integer QUANTIDADE = 10;
	private final Result result;
	private final MateriaisService materiaisService;
	private final ObjetoService objetoService;
	private final Validator validator;	
	private final MenuService menuService;
	private final LogService logService;
	private final AgenteService agenteService;
	private final VeiculoService veiculoService;
	private final TokenService tokenService;
	private final OcorrenciaService ocorrenciaService;
	
	public ObjetoController(Result result, 
			MateriaisService materiaisService,
			ObjetoService objetoService, 
			Validator validator,
			MenuService menuService,
			LogService logService,
			AgenteService agenteService,
			VeiculoService veiculoService,
			TokenService tokenService,
			OcorrenciaService ocorrenciaService) {
		
		this.result = result;
		this.materiaisService = materiaisService;
		this.objetoService = objetoService;
		this.validator = validator;		
		this.menuService= menuService;
		this.logService = logService;
		this.agenteService = agenteService;
		this.veiculoService = veiculoService;
		this.tokenService = tokenService;
		this.ocorrenciaService = ocorrenciaService;
	}
    
	@MonkeySecurity(role="all")
	@Path("/controle_geral/objeto/cadastro/")
	@MenuTag(tag="objeto")	
	public void cadastro(Objeto objeto) {
	
		List<Dominio> listaFuncionalidade = materiaisService.findFuncionalidade();
		List<Objeto> listaObjeto = objetoService.findObjeto();
		List<Dominio> listaEstadoObjeto = objetoService.findEstado();
		List<Materiais> listaMateriaisObjeto = objetoService.findMateriais();
		
		result.include("listaFuncionalidade", listaFuncionalidade);		
		result.include("listaObjeto", listaObjeto);		
		result.include("listaEstadoObjeto", listaEstadoObjeto);
		result.include("listaMateriaisObjeto", listaMateriaisObjeto);
		result.include("objeto", objeto);
	}

	@Get
	@MonkeySecurity(role="all")
	@Path({"/controle_geral/objeto/ocorrencia/", "/controle_geral/objeto/ocorrencia/adicionar/"})
	@MenuTag(tag="objeto")	
	public void criaOcorrenciaObjeto() {
	
	}
	
	@Path("/controle_geral/objeto/adicionar/")
	@MonkeySecurity(role="all")
	public void adicionar(Objeto objeto) {
		
		if (objeto == null) {
			
			result.use(Results.logic()).redirectTo(ObjetoController.class).cadastro(objeto);
		
		} else {
		
			if (objeto.getMateriais() == null || "".equals(objeto.getMateriais())) {
				
				result.include("erroFuncionalidade", true);
				validator.add(new ValidationMessage("Material invalido", "error"));
			}

			if (objeto.getEstado() == null || "".equals(objeto.getEstado())) {
				
				result.include("erroFuncionalidade", true);
				validator.add(new ValidationMessage("Estado invalido", "error"));
			}			
			
			validator.onErrorUse(Results.logic()).redirectTo(ObjetoController.class).cadastro(objeto);
					    
			if (objeto.getId() == null || objeto.getId() <= 0) {

				objeto = objetoService.insert(objeto);
				logService.gravaLog("Objeto", "add", "Adicionado novo objeto", objeto, null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Objeto adicionado com sucesso");
				result.include("avisos", mensagens);
			
			} else {
						
				objetoService.update(objeto);
				logService.gravaLog("Objeto", "change", "Alterado objeto com id -> " + objeto.getId(), objeto, null);				
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Objeto alterado com sucesso");
				result.include("avisos", mensagens);
			
			}
			
			result.use(Results.logic()).redirectTo(ObjetoController.class).listar(0);

		}
	}

	@MonkeySecurity(role="all")
	@Path("/controle_geral/objeto/editar/")	
	public void editar(Integer id) {
		
		Objeto objeto = new Objeto();

		if (id >= 0) {
			
			objeto.setId(id);
			objeto = objetoService.find(objeto);
			result.use(Results.logic()).forwardTo(ObjetoController.class).cadastro(objeto);
			
		} else {

			objeto.setId(id);		
			result.use(Results.logic()).forwardTo(ObjetoController.class).cadastro(objeto);
		}		
	}
	
	@MonkeySecurity(role="all")
	public void remover(Objeto objeto) {
		objeto = objetoService.delete(objeto);
		logService.gravaLog("Objeto", "delete", "Removido objeto com id -> " + objeto.getId(), null, null);
	}
	
	@MonkeySecurity(role="all")
	@Path("/controle_geral/objeto/listar/")	
	@MenuTag(tag="objeto")		
	public void listar(Integer pagina) {
				
		if (pagina == null || pagina <= 0) {
			pagina=1;
		}
		
		List<Objeto> listaObjeto = objetoService.findAllPagina(QUANTIDADE, pagina);
		result.include("listaObjeto", listaObjeto);

	}
	
	@MonkeySecurity(role="all")
	@Path("/controle_geral/objeto/listaObjeto/")	
	public void listaObjetoTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<Objeto> listaObjeto = objetoService.findAllPagina(QUANTIDADE, pagina);
		result.include("listaObjetos", listaObjeto);
		
		Integer qnt = objetoService.findTotal();
		Integer total = qnt / QUANTIDADE;

		if ((qnt % QUANTIDADE) > 0 ) {

			total++;
		}

		result.include("total", total);

		result.forwardTo("/json/objetoTabela.jsp");
	}
	
	@MonkeySecurity(role="all")
	@Path("/controle_geral/objeto/listaMateriais/")	
	public void listaMateriaisTabela(Integer id) {
		
		List<Materiais> listaObjetoMateriais = objetoService.findMateriais(id);
		result.include("listaObjetoMateriais", listaObjetoMateriais);
		
		result.forwardTo("/json/objetoMateriaisTabela.jsp");
	}
	
	@MonkeySecurity(role="all")
	@Path("/objeto/listaObjetos/")
	public void listaObjetosDisponiveisTabela(Integer pagina, Materiais material) {

		if (pagina == null) {

			pagina = 0;
		}

		Integer total = 0;
		List<Objeto> listaObjeto = null;
		
		if (material != null && material.getId() != null) {
			
			listaObjeto = objetoService.findAllObjetosDisponiveisPagina(QUANTIDADE, pagina, material);
			
			Integer qnt = objetoService.findTotalObjetosDisponiveis(material);
			total = qnt / QUANTIDADE;

			if ((qnt % QUANTIDADE) > 0 ) {
				
				total++;
			}
		}
		
		result.include("listaObjetos", listaObjeto);
		result.include("total", total);

		result.forwardTo("/json/objetoTabela.jsp");
	}
	
	@MonkeySecurity(role="all")
	@Path({"/materiais/listaObjetos/agente/{agente.id}", "/materiais/listaObjetos/veiculo/{veiculo.id}", "/materiais/listaObjetos/"})
	public void listaObjetosAssociadosTabela(Integer pagina, Agente agente, Veiculo veiculo) {

		if (pagina == null) {

			pagina = 0;
		}

		Integer total = 0;
		List<Objeto> listaObjetosAssociados = null;
		
		if (agente != null && agente.getId() != null) {
			
			listaObjetosAssociados = agenteService.findAllObjetosAssociadosPagina(9999, pagina, agente);
			
			Integer qnt = agenteService.findTotalObjetosAssociados(agente);
			total = 0;			
		}

		if (veiculo != null && veiculo.getId() != null) {
			
			listaObjetosAssociados = veiculoService.findAllObjetosAssociadosPagina(9999, pagina, veiculo);
			
			Integer qnt = veiculoService.findTotalObjetosAssociados(veiculo);
			total = 0;		
		}
		
		result.include("listaObjetos", listaObjetosAssociados);
		result.include("total", total);
		result.forwardTo("/json/objetoTabela.jsp");
	}

	@Post
	@Path("/controle_geral/objeto/ocorrencia/adicionar/")
	@MonkeySecurity(role="all")
	@MenuTag(tag="objeto")	
	public void salvaOcorrenciaObjeto(Integer[] id, Ocorrencia ocorrencia, Integer viaturaId, Integer agenteId) {

		if (ocorrencia == null) {
			
			validator.add(new ValidationMessage("Não foi possóvel criar a atividade", "error"));
			
		} else {
			
			if (ocorrencia.getResumo() == null || ocorrencia.getResumo().equals("")) {
				
				validator.add(new ValidationMessage("O resumo não pode ser nulo", "error"));
			}
			
			if (id == null || id.length <= 0) {
				
				validator.add(new ValidationMessage("Nenhum objeto selecionado", "error"));
			}	
			
			if (viaturaId == null && agenteId == null) {
				
				validator.add(new ValidationMessage("Nenhum agente ou veículo selecionado", "error"));
			}	

			if (ocorrencia.getEndereco() == null || ocorrencia.getEndereco().equals("") ||
					ocorrencia.getEndereco().getEndGeoref() == null || ocorrencia.getEndereco().getEndGeoref().equals("") ||
					ocorrencia.getEndereco().getLatitude() == null ||  ocorrencia.getEndereco().getLatitude().equals("") || 
					ocorrencia.getEndereco().getLongitude() == null || ocorrencia.getEndereco().getLongitude().equals("")) {
				
				validator.add(new ValidationMessage("Endereço inválido", "error"));
			}			
		}

		validator.onErrorUse(Results.logic()).forwardTo(ObjetoController.class).criaOcorrenciaObjeto();
		
		if (ocorrencia != null && (ocorrencia.getId() == null || ocorrencia.getId() <= 0)) {
			
			Token token;
			
			if (viaturaId != null) {
				
				token = tokenService.findByVeiculo_id(viaturaId);
				
			} else {
				
				token = tokenService.findByAgente_id(agenteId);
			}
			
			ocorrencia.setUsuarioCriacao(agenteService.find(new Agente(token.getAgente_id())).getUsuario());
			
			List<Objeto> objetos = new ArrayList<Objeto>();
			
			for (Integer itemId : id) {
				
				Objeto objeto = objetoService.find(new Objeto(itemId));
				objetos.add(objeto);
			}		

			ocorrencia.setFlag(200);			
			ocorrencia.setHoraCriacao(new Timestamp(new Date().getTime()));
			
			ocorrenciaService.update(ocorrencia);
			
			result.use(Results.logic()).forwardTo(ObjetoController.class).listar(0);

		}	
	}
}
