package br.com.otgmobile.trackteam.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Teste;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.modal.comunicacao.COcorrencia;
import br.com.otgmobile.trackteam.modal.comunicacao.Operador;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.tasks.SocketIOTask;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class TesteController {

	public SocketIOTask socketIOTask;
	public OcorrenciaService ocorrenciaService;
	public Result result;
	private VeiculoService veiculoService;
	private TokenService tokenService;
	private EntityManager entityManager;
	
	public TesteController(SocketIOTask socketIOTask, 
			OcorrenciaService ocorrenciaService, 
			Result result, 
			VeiculoService veiculoService,
			TokenService tokenService,
			EntityManager entityManager) {
		
		this.socketIOTask = socketIOTask;
		this.ocorrenciaService = ocorrenciaService;
		this.result = result;
		this.veiculoService = veiculoService;
		this.tokenService = tokenService;
		this.entityManager = entityManager;
	}
	
	@Path("/teste1")
	public void teste(List<Teste> lista) {
/*
		select u from User u
		inner join u.attributeList a1
		inner join u.attributeList a2
		where a1.value = 'green house street' and a1.key = 'address' 
		and a2.key ='phone' and a2.value = '234324'
*/
		
		StringBuilder sql = new StringBuilder("select * from agente");
		Query query = this.entityManager.createQuery(sql.toString());
	
		List<Object> resultado = query.getResultList(); 
//		Object resultado = query.getSingleResult();
		
		System.out.println();

/*		
		System.out.println("opa");
		for(Teste elem : lista) {
			
			System.out.println(elem.getVar01() + "-" + elem.getVar02() + "-" + elem.getVar03());
		}
*/
//		Util.notificacao("nova_ocorrencia", 72);
/*		
		Class<TesteController> classe = TesteController.class;
		Annotation annotation = classe.getAnnotation(FotoSecurity.class);

		if(annotation instanceof FotoSecurity){
			FotoSecurity myAnnotation = (FotoSecurity) annotation;
		    System.out.println("name: " + myAnnotation.role());
		}
*/	
/*		
		String param = "Email=fotodesenvolvimento@gmail.com" +
				"&Passwd=foto@123" +
				"&accountType=GOOGLE" +
				"&source=MyLittleExample" + 
				"&service=ac2dm";
		
		System.out.println("opo");
		String line = Util.excutePost("https://www.google.com/accounts/ClientLogin", param);		
		String[] linhas = line.split("\r");
				
		String auth_key = null;
		
		for(String linha : linhas) {
			
			if (linha.startsWith("Auth=")) {
				auth_key = linha.substring(5);
			}			
		}
		
		String urlParameters = "registration_id=APA91bHsTd-atcB07xwZSdZ21kUDZ6TmwwMoKkybKCflz0a9gsYe-sZzByKPOJ1p7kw7OE7Q79FXlgwgNXGC5p1QJua4-lZ6kxUxTaaGhlugey0t6rbGUTj7vi2bNMkoMy-6uSSfO01kwkBWOTJAmvE3phq7f_Gcbw" +
				"&collapse_key=0" +
				"&data.payload=qwert";
			
		String targetURL = "http://android.clients.google.com/c2dm/send";
		
		URL url;
		HttpURLConnection connection = null;  
		
		try {
			//Create connection
			url = new URL(targetURL);	
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
					
			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "pt-BR");  
			connection.setRequestProperty("Authorization", "GoogleLogin auth=" + auth_key);
			
			connection.setUseCaches (false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
		
			//	Send request
			DataOutputStream wr = new DataOutputStream (
            connection.getOutputStream ());
			wr.writeBytes (urlParameters);
			wr.flush ();
			wr.close ();

			//	Get Response	
			
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			StringBuffer response = new StringBuffer();
			
			while((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			
			System.out.println(response);
			rd.close();

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			if(connection != null) {
				connection.disconnect(); 
			}
		}	
*/		
	}
	
	@Path("/test/")
	public void teste() {
		

		Gson gson = new Gson();		

		Ocorrencia ocorrenciaObj = new Ocorrencia();
		ocorrenciaObj.setId(72);
		
		ocorrenciaObj = ocorrenciaService.findOcorrencia(ocorrenciaObj);
		
		COcorrencia cOcorrencia = new COcorrencia();
		cOcorrencia.parseFrom(ocorrenciaObj);
		
//		entityManager.detach(ocorrenciaObj);
//		entityManager.clear();

//		String ocorrencia = gson.toJson(ocorrenciaObj);
		
		JsonElement je = gson.toJsonTree(cOcorrencia);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);
	    
	    System.out.println(jo.toString());
/*	    
		if (novaOcorrencia == null 
				|| novaOcorrencia.getNiveisEmergencia() == null 
				|| novaOcorrencia.getNiveisEmergencia().getId() == null
				|| novaOcorrencia.getNiveisEmergencia().getId() <= 0
				|| novaOcorrencia.getOcorrenciasStatus() == null
				|| novaOcorrencia.getNatureza1() == null) {
			
			System.out.println("falta emergencia");		
			
		} else {		
		
//			novaOcorrencia = ocorrenciaService.insert(novaOcorrencia);
		}
*/	
	}
	
	@Path("/testNode/")
	public void testeNode() throws JSONException {
		
		List<Operador> listaOperador = new ArrayList<Operador>();
		
		Operador operador = new Operador();
		operador.setName("Fabio");
		operador.setToken("opopo");
		
		listaOperador.add(operador);
 
		operador = new Operador();
		operador.setName("Fabio1");
		operador.setToken("opopo3");
		
		listaOperador.add(operador);
		
		operador = new Operador();
		operador.setName("Fabio2");
		operador.setToken("opopo1");
		
		listaOperador.add(operador);
		
	    
		JSONObject jsonNode;
		JSONObject jsonResult = new JSONObject();
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(listaOperador);	    
		jsonNode = new JSONObject("{\"result\": " + je.toString() + "}");
//		jsonResult.put("result", je);
		
		
	    socketIOTask.enviaMsg("testeChat", jsonNode);
	    result.nothing();

		
	}

	@Path("/test/get/historico/")
	public void testeGetHistorico() {
		
		List<Historico> listaHistorico = veiculoService.findAllPosicao();
		
		for (Historico historico : listaHistorico) {
			
			System.out.println(historico.getLatitude() + "-" + historico.getLongitude());
		}
	}
	
	@Path("/teste/historico/")
	public void testeHistorico(String lat, String lng, String token) throws JSONException {
		
		Token tokenObj = new Token();
		tokenObj.setToken(token);
		tokenObj = tokenService.find(tokenObj);		
		
		CHistorico cHistorico = new CHistorico();
/*
		cHistorico.setLatitude("-3.745913");
		cHistorico.setLongitude("-38.494623");
		cHistorico.setVeiculo_id(68);
*/
		Date today = new java.util.Date();

		cHistorico.setLatitude(lat);
		cHistorico.setLongitude(lng);
		cHistorico.setVeiculo_id(tokenObj.getVeiculo_id());
		cHistorico.setDataHora(new Timestamp(today.getTime()).getTime());

		Historico novoHistorico = cHistorico.parseTo();
		novoHistorico = veiculoService.insertHistorico(novoHistorico);
    
		result.nothing();
	}	
	
	@Path("/teste/ocorrencia/add")
	public void testeOcorrenciaAdd() throws JSONException {

		String jsonStr = "{\"_id\":232," +
			"\"vitimas\":[{\"_id\":8,\"descricao\":\"Vitimologia\",\"id\":0,\"idade\":12,\"ocorrenciaID\":232}]," + 
			"\"descricao\":\"Under the prefere\"," +
			"\"endereco\":{\"endGeoref\":\"R. Júlio Abreu, 124-238 - Varjota Fortaleza - CE 60160-240 \",\"latitude\":-3.7359684,\"longitude\":-38.487915}," +
			"\"estado\":8,\"estadoObj\":{\"_id\":12,\"id\":8,\"valor\":\"Em atendimento\"}," +
			"\"materiais\":[{\"_id\":2,\"codigo\":\"555\",\"descricao\":\"RADAR MOVEL TIPO 1\",\"id\":325}, {\"_id\":1,\"codigo\":\"62\",\"descricao\":\"CAPACETE VIATURA 1\",\"id\":302}, {\"_id\":3,\"codigo\":\"38\",\"descricao\":\"CONE DANIFICADO\",\"id\":697}]," +
			"\"natureza\":11,\"naturezaObj\":{\"_id\":8,\"id\":11,\"valor\":\"Assalto\"}," + 
			"\"nivelEmergencia\":{\"_id\":8,\"cor\":\"#ffde5b\",\"descricao\":\"Importante\",\"id\":2}," + 
			"\"nivelEmergenciaID\":2," +
			"\"resumo\":\"Mistake null pointer\"," +
			"\"solicitantes\":[{\"_id\":2,\"cpf\":\"4625415441\",\"nome\":\"Titio\",\"telefone1\":\"1225896624\",\"telefone2\":\"1255665339\"}, {\"_id\":1,\"cpf\":\"Que foi\",\"nome\":\"Apagou abalou\",\"telefone1\":\"Qual era\",\"telefone2\":\"Poos n ao o e\"}]," +
			"\"enviado\":false," +
			"\"cancelado\":false}";

		Gson gson = new Gson();
		COcorrencia cOcorrencia = gson.fromJson(jsonStr, COcorrencia.class);
		Ocorrencia novaOcorrencia = cOcorrencia.parseTo();
		
		novaOcorrencia = ocorrenciaService.insert(novaOcorrencia);	
/*
		Ocorrencia ocorrencia = ocorrenciaService.findOcorrencia(novaOcorrencia);
	
		JSONObject json;
		JSONObject jsonResult = new JSONObject();
		json = new JSONObject(ocorrencia);
		jsonResult.put("result", json);
		
		System.out.println("--" + jsonResult.toString());
		socketIOTask.enviaMsg("ocorrencia/add", jsonResult);
*/	    
		result.nothing();
	}	
	
	@Path("/teste/ocorrencia/change")
	public void testeOcorrenciaChange() throws JSONException {
		
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(188);
		ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
		
//		ocorrencia.getNiveisEmergencia().setCor("#00FFF");
		
		COcorrencia cOcorrencia = new COcorrencia();
		cOcorrencia.parseFrom(ocorrencia);
				
		JSONObject json;
		JSONObject jsonResult = new JSONObject();
		json = new JSONObject(cOcorrencia);
		jsonResult.put("result", json);
		
		socketIOTask.enviaMsg("ocorrencia/change", jsonResult);
	    
		result.nothing();
	}	
	
	@Path("/teste/ocorrencia/delete")
	public void testeOcorrenciaDelete() throws JSONException {
		
		COcorrencia cOcorrencia = new COcorrencia();
		cOcorrencia.setId(1018);
		
		JSONObject json;
		JSONObject jsonResult = new JSONObject();
		json = new JSONObject(cOcorrencia);
		jsonResult.put("result", json);
		
		socketIOTask.enviaMsg("ocorrencia/delete", jsonResult);
	    
		result.nothing();
	}
	
}
