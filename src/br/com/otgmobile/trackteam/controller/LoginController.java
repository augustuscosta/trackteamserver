package br.com.otgmobile.trackteam.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.IndexController;
import br.com.otgmobile.trackteam.modal.GeoPontoRegioes;
import br.com.otgmobile.trackteam.modal.Regioes;
import br.com.otgmobile.trackteam.modal.comunicacao.CGeoPonto;
import br.com.otgmobile.trackteam.modal.comunicacao.CRegiao;
import br.com.otgmobile.trackteam.modal.comunicacao.CUsuario;
import br.com.otgmobile.trackteam.modal.session.UsuarioSession;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioPagina;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.UsuarioService;
import br.com.otgmobile.trackteam.util.Util;

@Resource
public class LoginController {
	
	private final Result result;
	private final HttpServletResponse response;
	private final UsuarioService usuarioService;
	private final Validator validator;
	private final UsuarioSession usuarioSession;
	private final LogService logService;
	
	public LoginController(Result result,
			HttpServletResponse response,
			UsuarioService usuarioService, 
			Validator validator, 
			UsuarioSession usuarioSession,
			LogService logService) {
		
		this.result = result;
		this.usuarioService = usuarioService;
		this.validator = validator;
		this.usuarioSession = usuarioSession;
		this.response = response;
		this.logService = logService;
	}

	@Get
	@Path("/verifica_login/")
	public void verificaLogin() {
		
		CUsuario usuario = new CUsuario();
		usuario.setId(usuarioSession.getId());
		usuario.setNome(usuarioSession.getNome());
		
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Util.toJSON(usuario));				
	}	
	
	@Post
	@Path("/login")
	public void loginPost(String login, String senha) {

		Usuario usuario = new Usuario();
		usuario.setLogin(login);
		usuario.setSenha(senha);
		
		if (login == null || senha == null) {
			
			result.include("login", login);		
			result.use(Results.logic()).redirectTo(LoginController.class).login();
		
		} else {
			
			if (login == null || "".equals(login)) {
				
				result.include("erroLogin", true);
				result.include("erroSenha", true);
				validator.add(new ValidationMessage("Os campos precisam ser preenchidos corretamente", "error"));
				
			} else 	if (senha == null || "".equals(senha)) {
				
				result.include("erroLogin", true);
				result.include("erroSenha", true);
				validator.add(new ValidationMessage("Os campos precisam ser preenchidos corretamente", "error"));
			}
			
			senha = Util.md5(senha);
			
			result.include("login", login);
			validator.onErrorUse(Results.logic()).redirectTo(LoginController.class).login();

			usuario = usuarioService.findByLogin(usuario);
			
			if (usuario == null || !senha.equals(usuario.getSenha())) {
							
				validator.add(new ValidationMessage("Login ou senha inválidos", "error"));				
				logService.gravaLog("Login", "login", "Login, erro ao efetuar login com usuario -> " + login);
			}
			
			validator.onErrorUse(Results.logic()).redirectTo(LoginController.class).login();
		}
		
		List<CRegiao> regioes = new ArrayList<CRegiao>();
		
		if (usuario.getRegioes().size() > 0) {
					
			for (Regioes regiao : usuario.getRegioes()) {
			
				Integer cont = 0;
				Float x = 0f;
				Float y = 0f;
				
				for (GeoPontoRegioes geoPonto : regiao.getGeoPonto()) {
					
					x += Float.parseFloat(geoPonto.getLatitude());
					y += Float.parseFloat(geoPonto.getLongitude());
					
					cont++;
				}
				
				CRegiao regiaoDados = new CRegiao();
				regiaoDados.setNome(regiao.getNome());	
				regiaoDados.setCentro(new CGeoPonto(Float.toString(x/cont), Float.toString(y/cont)));
				
				regioes.add(regiaoDados);
			}
			
		} else {
			
			CRegiao regiaoDados = new CRegiao();
			regiaoDados.setNome("Geral");
			regiaoDados.setCentro(new CGeoPonto("-3.739646" , "-38.486738"));
			
			regioes.add(regiaoDados);
		}
		
		
		usuarioSession.setNome(usuario.getNome());
		usuarioSession.setMatricula(usuario.getMatricula());
		usuarioSession.setId(usuario.getId());
		usuarioSession.setCpf(usuario.getCpf());
		usuarioSession.setDescricao(usuario.getDescricao());
		usuarioSession.setRole(new ArrayList<String>());
		usuarioSession.setRegioes(regioes);
			
		for(UsuarioPagina pagina : usuario.getGrupo().getPaginas()) {
			
			usuarioSession.getRole().add(pagina.getPagina());
		}			

		logService.gravaLog("Login", "login", "Login efetuado com sucesso -> " + usuario.getMatricula() + " - " + usuario.getNome() );
		result.redirectTo(IndexController.class).index();
	}

	@Get
	@Path("/login")
	public void login() {
		
	}

	@Get("/logout")
	public void logout() {
		
		if (usuarioSession != null && usuarioSession.getId() != null) {
			logService.gravaLog("Login", "logout", "Logout efetuado com sucesso -> " + usuarioSession.getMatricula() + " - " + usuarioSession.getNome() );			
		}
		usuarioSession.logout();
		
		result.redirectTo(this).login();
	}
	
}
