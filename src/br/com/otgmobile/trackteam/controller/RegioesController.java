package br.com.otgmobile.trackteam.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.modal.GeoPontoRegioes;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.Regioes;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.comunicacao.CRegiao;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.RegioesService;

@Resource
public class RegioesController {
	
	private Result result;
	private RegioesService regioesService;
	private MenuService menuService;
	private Integer quantidade;
	private Validator validator;
	private final LogService logService;
	
	
	public RegioesController(Result result, RegioesService regioesService,
			MenuService menuService,Validator validator,
			LogService logService) {
		
		this.result = result;
		this.regioesService = regioesService;
		this.menuService = menuService;
		this.quantidade = 10;
		this.validator = validator;
		this.logService = logService;
	}	
	
	@Get
	@Path("controle_geral/regioes/adicionar/")
	@MonkeySecurity(role = "regioes")
	@MenuTag(tag="regiao")
	public void cadastro(Regioes regioes){			
		result.include("regioes",regioes);
	}
	
	@Post
	@MenuTag(tag="regiao")	
	@MonkeySecurity(role="regioes")
	@Path("/controle_geral/regioes/editar/")	
	public void adicionarRegiao(String nome, List<String> pontos, Integer id) {
		Regioes regioes = new Regioes();
		regioes.setNome(nome);

		if (nome != null && pontos.size() > 2) {
							
				List<GeoPontoRegioes> geoPontos = new ArrayList<GeoPontoRegioes>();
				
				for(String ponto : pontos) {
					
					GeoPontoRegioes geoPonto = new GeoPontoRegioes();
					
					String[] geoPontoTmp = ponto.split(",");
					
					if (geoPontoTmp.length >= 2) {
						geoPonto.setLatitude(geoPontoTmp[0]);
						geoPonto.setLongitude(geoPontoTmp[1]);
//						geoPonto.setRegioes(regioes);
						geoPontos.add(geoPonto);
					}		
				}

				if (id != null && id > 0) {
	
					Regioes regioesBD = new Regioes();
					regioesBD.setId(id);
					regioesBD = regioesService.find(regioesBD);
									
					regioesBD.setNome(nome);
					regioesBD.setGeoPonto(geoPontos);
					
					regioesBD = regioesService.update(regioesBD);					
					logService.gravaLog("Regiao", "change", "Região alterada com sucesso - id -> " + regioesBD.getId(), regioesBD, null);				
					
					Mensagem mensagens = new Mensagem();
					mensagens.setTitulo("Região alterada com sucesso");
					result.include("avisos", mensagens);
					
				} else {
					
					if (!regioesService.existe(regioes)) {
						
						regioes.setGeoPonto(geoPontos);		
						regioes = regioesService.insert(regioes);
						
						logService.gravaLog("Regiao", "add", "Adicionada região com sucesso - id -> " + regioes.getId(), regioes, null);
						Mensagem mensagens = new Mensagem();
						mensagens.setTitulo("Região adicionada com sucesso");
						result.include("avisos", mensagens);
						
					} else {
						
						result.include("cerca", regioes);
						validator.add(new ValidationMessage("O nome escolhido já está associado a outra Região", "error"));						
					}
				}
							
		} else {
			
			if (nome != null && pontos.size() > 2) {
				
				result.include("regioes", regioes);
				validator.add(new ValidationMessage("Nome inválido", "error"));			
			} 

			if (nome != null && pontos.size() > 2) {
				
				result.include("regioes", regioes);
				validator.add(new ValidationMessage("Região inválida", "error"));			
			} 

		}		
			
		validator.onErrorUse(Results.logic()).forwardTo(RegioesController.class).editarRegiao(null);
	
		result.use(Results.logic()).forwardTo(RegioesController.class).listar(quantidade);				
	}	
	
	@Path("/controle_geral/regioes/listar/")
	@MonkeySecurity(role="all")
	public void listar(Integer pagina) {
		
		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-usuario");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);			
	}
	
	@Path("/controle_geral/regiao/listaRegioes/")
	@MonkeySecurity(role="all")
	public void listaRegioesTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<Regioes> listaRegioes = regioesService.findAllPagina(quantidade, pagina);
		result.include("listaRegioes", listaRegioes);		
		
		Integer qnt = regioesService.findTotal();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
				
		result.forwardTo("/json/regiaoTabela.jsp"); 
	}
	
	@Path("/controle_geral/regioes/listaRegiao")
	public void listaRegiao(){
		List<Regioes> listaRegioes = regioesService.findAll();
		result.include("listaRegioes", listaRegioes);		
		result.forwardTo("/json/regioes.jsp"); 
	}
	
	@Post
	@Path("/controle_geral/regioes/excluir/")	
	@MonkeySecurity(role="regioes")	
	public void excluir(Integer id) {
		
		Regioes regioes = new Regioes();
		regioes.setId(id);
		
		regioes = regioesService.findRegioesExcluir(regioes);
		logService.gravaLog("Regiao", "delete", "Removida região - id -> " + regioes.getId());						

		result.include("status", "ok");		
		result.include("id", id);
		result.include("msgSucesso", "Região excluida com sucesso");			
		result.forwardTo("/json/resposta.jsp");
	}	
	
	@Path("/controle_geral/regioes/editarRegiao/")	
	@MonkeySecurity(role="regioes")
	@MenuTag(tag="regiao")
	public void editarRegiao(Integer id){
		Regioes regiao = new Regioes();
		
		if (id != null) {
			
			regiao = regioesService.findRegioesId(id);			
			result.include("regiao", regiao);	
			
		}
		result.use(Results.logic()).forwardTo(RegioesController.class).cadastro(regiao);		
	}

}
