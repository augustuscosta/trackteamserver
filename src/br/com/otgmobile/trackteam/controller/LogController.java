package br.com.otgmobile.trackteam.controller;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Log;
import br.com.otgmobile.trackteam.modal.Tabela;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.util.Json;

@Resource
public class LogController {

	private final Result result;
	private final HttpServletResponse response;
	private final LogService logService;

	public LogController(Result result,
			HttpServletResponse response,
			LogService logService) {
		
		this.result = result;
		this.response = response;	
		this.logService = logService;
	}
	
	@Get
	@Path("/log/listar")
	@MonkeySecurity(role="logs")	
	public void listar() {
		
	}
	
	@Get
	@Path({"/log/listarJSON/", 
		"/log/listarJSON/i/{inicio}/", 
		"/log/listarJSON/i/{inicio}/f/{fim}/", 
		"/log/listarJSON/i/{inicio}/f/{fim}/{filtro}/",
		"/log/listarJSON/i/{inicio}/{filtro}/",
		"/log/listarJSON/{filtro}/",})
	@MonkeySecurity(role="all")	
	public void listarJSON(String inicio, String fim, String filtro, Integer pagina) {

		if (pagina == null) {
			pagina = 0;
		}
		
		Tabela<Log> listaElementos = logService.findPagina(pagina, inicio, fim, filtro);	
				
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export("", listaElementos));
	}	
	
	@Get
	@Path("/log/exibir/")
	@MonkeySecurity(role="logs")		
	public void exibir(Integer id) {
		
		Log log = logService.find(new Log(id));
		result.include("log", log);		
	}	
}
