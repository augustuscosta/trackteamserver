package br.com.otgmobile.trackteam.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Escala;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.comunicacao.COcorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.EscalaService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.util.Json;

@Resource
public class EscalaController {

	private final Result result;
	private final EscalaService escalaService;
	private final OcorrenciaService ocorrenciaService;
	private final Validator validator;
	private final MenuService menuService;
	private final Integer quantidade;
	private HttpServletResponse response;

	public EscalaController(Result result, EscalaService escalaService, OcorrenciaService ocorrenciaService,
			Validator validator, MenuService menuService,
			HttpServletResponse response) {

		this.result = result;
		this.escalaService = escalaService;
		this.ocorrenciaService = ocorrenciaService;
		this.validator = validator;
		this.menuService = menuService;
		this.quantidade = 10;
		this.setResponse(response);
	}

	@MonkeySecurity(role = "all")
	@Path("/controle_geral/escala/cadastro/")
	public void cadastro(Escala escala) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("cadastro-escala");
		subMenu = menuService.findByTag(subMenu);

		List<SubMenu> listaSubMenu = menuService
				.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu", listaSubMenu);
		result.include("subMenu", subMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);

		result.include("escala", escala);
	}

	@Path("/controle_geral/escala/adicionar/")
	@MonkeySecurity(role = "all")
	public void adicionar(Escala escala) {

		if (escala == null) {

			result.use(Results.logic()).redirectTo(EscalaController.class)
					.cadastro(escala);

		} else {

			validator.onErrorUse(Results.logic())
					.redirectTo(EscalaController.class).cadastro(escala);
			
			List<Ocorrencia> ocorrencias = new ArrayList<Ocorrencia>();
			if(escala.getOcorrencias() != null){
				for(Ocorrencia ocorrencia:escala.getOcorrencias()){
					ocorrencias.add(ocorrenciaService.findOcorrencia(ocorrencia));
				}
				escala.setOcorrencias(ocorrencias);
			}
			

			if (escala.getId() == null || escala.getId() <= 0) {
				escala = escalaService.insert(escala);
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Escala adicionada com sucesso");
				result.include("avisos", mensagens);

			} else {

				escalaService.update(escala);
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Escala alterada com sucesso");
				result.include("avisos", mensagens);

			}

			result.use(Results.logic()).redirectTo(EscalaController.class)
					.listar(0);

		}
	}

	@MonkeySecurity(role = "all")
	@Path("/controle_geral/escala/editar/")
	public void editar(Integer id) {

		Escala escala = new Escala();

		if (id >= 0) {

			escala.setId(id);
			escala = escalaService.find(escala);
			result.use(Results.logic()).forwardTo(EscalaController.class)
					.cadastro(escala);

		} else {

			escala.setId(id);
			result.use(Results.logic()).forwardTo(EscalaController.class)
					.cadastro(escala);
		}
	}

	@MonkeySecurity(role = "all")
	public void remover(Escala escala) {
		escala = escalaService.delete(escala);
	}

	@MonkeySecurity(role = "all")
	public void escala() {

		List<Escala> listaEscala = escalaService.findEscala();
		result.include("listaEscala", listaEscala);
		result.forwardTo("/json/escala.jsp");

	}

	@MonkeySecurity(role = "all")
	@Path("/controle_geral/escala/listar/")
	public void listar(Integer pagina) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-escala");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService
				.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu", listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);

		if (pagina == null || pagina <= 0) {
			pagina = 1;
		}

		List<Escala> listaEscala = escalaService.findAllPagina(quantidade,
				pagina);
		result.include("listaEscala", listaEscala);

	}

	@MonkeySecurity(role = "all")
	@Path("/controle_geral/escala/listaEscala/")
	public void listaEscalasTabela(Integer pagina) {

		if (pagina == null) {

			pagina = 0;
		}

		List<Escala> listaEscala = escalaService.findAllPagina(quantidade,
				pagina);
		result.include("listaEscala", listaEscala);

		Integer qnt = escalaService.findTotal();
		Integer total = qnt / quantidade;

		if ((qnt % quantidade) > 0) {

			total++;
		}

		result.include("total", total);

		result.forwardTo("/json/escalaTabela.jsp");
	}
	
	@Post
	@MonkeySecurity(role="all")
	public void ocorrencias() {
		
		List<Ocorrencia> listaOcorrencias = ocorrenciaService.findOcorrenciasAtivas();
		List<COcorrencia> cListaOcorrencia = new ArrayList<COcorrencia>();
		
		for (Ocorrencia ocorrencia : listaOcorrencias) {
			
			
			COcorrencia cOcorrencia = new COcorrencia(ocorrencia);
			cListaOcorrencia.add(cOcorrencia);
		}

        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export(cListaOcorrencia));
	}
	
	@Path("/controle_geral/escala/excluir/")
	@Post
	@MonkeySecurity(role="all")	
	public void excluir(Integer id) {
		
		Escala escala = new Escala();
		escala.setId(id);
		
		escala = escalaService.find(escala);
		escalaService.delete(escala);
		
		result.include("status", "ok");		
		result.include("id", id);
		result.include("msgSucesso", "Escala com sucesso");			
		result.forwardTo("/json/resposta.jsp");
		
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

}
