package br.com.otgmobile.trackteam.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Endereco;
import br.com.otgmobile.trackteam.modal.Enquete;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.Pergunta;
import br.com.otgmobile.trackteam.modal.PontoDeInteresse;
import br.com.otgmobile.trackteam.modal.RespostaEnquete;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CAgente;
import br.com.otgmobile.trackteam.modal.comunicacao.CComentario;
import br.com.otgmobile.trackteam.modal.comunicacao.CEnquete;
import br.com.otgmobile.trackteam.modal.comunicacao.CNivelEmergencia;
import br.com.otgmobile.trackteam.modal.comunicacao.COcorrencia;
import br.com.otgmobile.trackteam.modal.comunicacao.CPontoDeInteresse;
import br.com.otgmobile.trackteam.modal.comunicacao.CUsuario;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaAgente;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaApoio;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaEnquete;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaPontoDeInteresse;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciasVeiculo;
import br.com.otgmobile.trackteam.modal.ocorrencia.Solicitante;
import br.com.otgmobile.trackteam.modal.session.UsuarioSession;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.EnqueteService;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.NiveisEmergenciaService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.PontoDeInteresseService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.service.UsuarioService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.util.Json;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class OcorrenciaController {

	private final Result result;
	private final OcorrenciaService ocorrenciaService;
	private final Validator validator;
	private final VeiculoService veiculoService;
	private final NiveisEmergenciaService niveisEmergenciaService;
	private final UsuarioSession usuarioSession;
	private final MenuService menuService;
	private final Integer quantidade;
	private final HttpServletResponse response;
	private final UsuarioService usuarioService;
	private final AgenteService agenteService;
	private final PontoDeInteresseService pontoDeInteresseService;
	private final LogService logService;
	private final EnqueteService enqueteService;
	
	public OcorrenciaController(Result result, 
			OcorrenciaService ocorrenciaService, Validator validator, VeiculoService veiculoService, NiveisEmergenciaService niveEmergenciaService, 
			UsuarioSession usuarioSession,	MenuService menuService,	HttpServletResponse response, UsuarioService usuarioService,
			TokenService tokenService,
			AgenteService agenteService,
			LogService logService,
			PontoDeInteresseService pontoDeInteresseService,
			EnqueteService enqueteService) {
		
		this.result = result;
		this.ocorrenciaService = ocorrenciaService;
		this.validator = validator;
		this.veiculoService = veiculoService;
		this.niveisEmergenciaService = niveEmergenciaService;
		this.usuarioSession = usuarioSession;
		this.menuService= menuService;
		this.quantidade = 10;
		this.response = response;
		this.usuarioService = usuarioService;
		this.agenteService = agenteService;
		this.logService = logService;
		this.pontoDeInteresseService = pontoDeInteresseService;
		this.enqueteService = enqueteService;
	}

	@MonkeySecurity(role="ocorrencia")
	@Path("/controle_geral/ocorrencias/cadastro/")
	public void cadastro(Ocorrencia ocorrencia) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("cadastro-ocorrencia");
		subMenu = menuService.findByTag(subMenu);
		
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);		

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
				
		List<Dominio> listaNatureza = ocorrenciaService.findNatureza();
		List<NiveisEmergencia> listaNiveisEmergencias = ocorrenciaService.findNivelEmergencia();
		List<Dominio> listaGravidade = ocorrenciaService.findGravidade();
		List<Dominio> listaVitimaCondicao = ocorrenciaService.findVitimaTipo();
		List<Dominio> listaStatus = ocorrenciaService.findStatus();
		List<Dominio> listaMeios = ocorrenciaService.findMeioCadastro();
		
		if (ocorrencia == null) {
			
			Endereco endereco = new Endereco();
			endereco.setLatitude(usuarioSession.getRegioes().get(0).getCentro().getLatitude());
			endereco.setLongitude(usuarioSession.getRegioes().get(0).getCentro().getLongitude());
			
			ocorrencia = new Ocorrencia();
			ocorrencia.setEndereco(endereco);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");			

		if (ocorrencia.getInicioProgramado() == null) {
			
			result.include("dataInicioProgramado", sdf.format(new Date()));
			
		} else {
			
			result.include("dataInicioProgramado", sdf.format(ocorrencia.getInicioProgramado()));			
		}

		if (ocorrencia.getFimProgramado() != null) {

			result.include("dataFimProgramado", sdf.format(ocorrencia.getFimProgramado()));			
		}
		
		result.include("listaNatureza", listaNatureza);
		result.include("listaNiveisEmergencias", listaNiveisEmergencias);
		result.include("listaGravidade", listaGravidade);
		result.include("listaVitimaCondicao", listaVitimaCondicao);		
		result.include("listaStatus", listaStatus);
		result.include("listaMeios", listaMeios);
		result.include("ocorrencia", ocorrencia);
		
		Usuario usuario = usuarioService.find(new Usuario(usuarioSession.getId()));
		result.include("regioes", Json.export(usuario.getRegioes()));					
	}
	
	@MonkeySecurity(role="ocorrencia")	
	public void adicionar(Ocorrencia ocorrencia, String dataInicioProgramado, String dataFimProgramado) {
		
		if (ocorrencia == null) {
			
			result.use(Results.logic()).redirectTo(OcorrenciaController.class).cadastro(ocorrencia);
		
		} else {
		
			if (ocorrencia.getDescricao() == null || "".equals(ocorrencia.getDescricao())) {
				
				result.include("erroDescricao", true);
				validator.add(new ValidationMessage("Descrição inválida", "error"));
			}
	
			if (ocorrencia.getResumo() == null || "".equals(ocorrencia.getResumo())) {
				
				result.include("erroResumo", true);
				validator.add(new ValidationMessage("Resumo inválido", "error"));
			}
	
			if (ocorrencia.getEndereco() == null || "".equals(ocorrencia.getEndereco())) {

				result.include("erroEndereco", true);
				validator.add(new ValidationMessage("Endereço inválido", "error"));
			}
					
			List<Solicitante> listaSolicitantes = new ArrayList<Solicitante>();
			 
			if (ocorrencia.getSolicitantes() != null) {
				
				for(Solicitante solicitante : ocorrencia.getSolicitantes()) {
										
					if(!(solicitante.getCpf() == "" && solicitante.getNome() == "")) {
						
						listaSolicitantes.add(solicitante);
					}					
				}
			}

			ocorrencia.setSolicitantes(listaSolicitantes);
						
			validator.onErrorUse(Results.logic()).redirectTo(OcorrenciaController.class).cadastro(ocorrencia);
		
			Usuario usuario = new Usuario();
			usuario.setId(usuarioSession.getId());
						
			ocorrencia.setUsuario(usuario);
			ocorrencia.setFlag(100);
			
			Date today = new java.util.Date();
		    
			try {
				if (dataInicioProgramado != null && !"".equals(dataInicioProgramado)) {			
					
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
					Date data = new Date(format.parse(dataInicioProgramado).getTime());
					ocorrencia.setInicioProgramado(new Timestamp(data.getTime())); 					
				}
			    
				if (dataFimProgramado != null && !"".equals(dataFimProgramado)) {			
										
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
					Date data = new Date(format.parse(dataFimProgramado).getTime());
					ocorrencia.setFimProgramado(new Timestamp(data.getTime()));					
				}			
			} catch (ParseException e) {
				
				System.out.println("Erro ao converter data");
			}
						
			if (ocorrencia.getOcorrenciasVeiculos() != null) {
				
				for(OcorrenciasVeiculo ocorrenciaVeiculo : ocorrencia.getOcorrenciasVeiculos()) {
										
					if (ocorrenciaVeiculo.getId() == null || ocorrenciaVeiculo.getId() == 0) {
						
						ocorrenciaVeiculo.setDataHora(new Timestamp(today.getTime()));
					}
				}
			}

			if (ocorrencia.getOcorrenciaAgentes() != null) {
				
				for(OcorrenciaAgente ocorrenciaAgente : ocorrencia.getOcorrenciaAgentes()) {
					
					ocorrenciaAgente.setAgente(agenteService.find(ocorrenciaAgente.getAgente()));
					
					if (ocorrenciaAgente.getId() == null || ocorrenciaAgente.getId() == 0) {
						
						ocorrenciaAgente.setDataHora(new Timestamp(today.getTime()));
					}
				}
			}
			
			if (ocorrencia.getOcorrenciaPontosDeInteresse() != null) {
				
				for(OcorrenciaPontoDeInteresse ocorrenciaPonto : ocorrencia.getOcorrenciaPontosDeInteresse()) {
					ocorrenciaPonto.setPontoDeinteresse(pontoDeInteresseService.find(ocorrenciaPonto.getPontoDeinteresse()));
				}
			}
			
			if (ocorrencia.getId() == null || ocorrencia.getId() <= 0) {

				ocorrencia.setUsuarioCriacao(new Usuario(usuarioSession.getId()));
				ocorrencia = ocorrenciaService.insert(ocorrencia);
				ocorrencia.setNiveisEmergencia(niveisEmergenciaService.find(ocorrencia.getNiveisEmergencia()));			
				
				logService.gravaLog("Atividade", "add", "Adicionado nova atividade", new COcorrencia(ocorrencia), null);				
				ocorrenciaService.notificaAlteracaoOcorrencia(ocorrencia, new Ocorrencia());
				ocorrenciaService.enviaNotificacao(ocorrencia, "ocorrencia/browser/add", null);

				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Atividade adicionada com sucesso");
				result.include("avisos", mensagens);				
			
			} else {
			
				Ocorrencia ocorrenciaBD = ocorrenciaService.findOcorrencia(ocorrencia);
				COcorrencia cOcorrenciaOld = new COcorrencia(ocorrenciaBD);
				
				ocorrencia.setFim(ocorrenciaBD.getFim());
				ocorrencia.setHoraChegada(ocorrenciaBD.getHoraChegada());
				ocorrencia.setHoraCriacao(ocorrenciaBD.getHoraCriacao());
				ocorrencia.setHoraEncerramento(ocorrenciaBD.getHoraEncerramento());
				ocorrencia.setHoraSaida(ocorrenciaBD.getHoraSaida());
				ocorrencia.setInicio(ocorrenciaBD.getInicio());
				ocorrencia.setRecebimento(ocorrenciaBD.getRecebimento());
				ocorrencia.setListaImagens(ocorrenciaBD.getListaImagens());
				ocorrencia.setListaObjetos(ocorrenciaBD.getListaObjetos());
				ocorrencia.setListaVideos(ocorrenciaBD.getListaVideos());
				ocorrencia.setMeioCadastro(ocorrenciaBD.getMeioCadastro());
//				ocorrencia.setUsuario(ocorrenciaBD.getUsuario());
				ocorrencia.setUsuarioCriacao(ocorrenciaBD.getUsuarioCriacao());
				
				Ocorrencia ocorrenciaAntiga = new Ocorrencia();
				ocorrenciaAntiga.setFlag(ocorrencia.getFlag());
				
				if (ocorrenciaBD.getOcorrenciasVeiculos() != null && ocorrenciaBD.getOcorrenciasVeiculos().size() > 0) {
					ocorrenciaAntiga.setOcorrenciasVeiculos(ocorrenciaBD.getOcorrenciasVeiculos());					
				} else {
					ocorrenciaAntiga.setOcorrenciasVeiculos(null);
				}

				if (ocorrenciaBD.getOcorrenciaAgentes() != null && ocorrenciaBD.getOcorrenciaAgentes().size() > 0) {
					ocorrenciaAntiga.setOcorrenciaAgentes(ocorrenciaBD.getOcorrenciaAgentes());					
				} else {
					ocorrenciaAntiga.setOcorrenciaAgentes(null);
				}

				ocorrencia.setNiveisEmergencia(niveisEmergenciaService.find(ocorrencia.getNiveisEmergencia()));
				ocorrenciaService.update(ocorrencia);

				logService.gravaLog("Atividade", "change", "Alterada uma atividade id -> " + ocorrencia.getId(), new COcorrencia(ocorrencia), cOcorrenciaOld);								
				ocorrenciaService.notificaAlteracaoOcorrencia(ocorrencia, ocorrenciaAntiga);
				ocorrenciaService.enviaNotificacao(ocorrencia, "ocorrencia/browser/change", null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Atividade alterada com sucesso");
				result.include("avisos", mensagens);				
			}
			
			result.use(Results.logic()).redirectTo(OcorrenciaController.class).listar(0);

		}
	}	

	@MonkeySecurity(role="ocorrencia")
	@Path("/controle_geral/ocorrencias/editar/")	
	public void editar(Integer id) {
		
		Ocorrencia ocorrencia = new Ocorrencia();

		if (id >= 0) {
			
			ocorrencia.setId(id);		
			ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
			result.use(Results.logic()).forwardTo(OcorrenciaController.class).cadastro(ocorrencia);
			
		} else {

			ocorrencia.setId(id);		
			result.use(Results.logic()).forwardTo(OcorrenciaController.class).cadastro(ocorrencia);
		}		
	}

	@MonkeySecurity(role="ocorrencia")	
	public void encerrar(Ocorrencia ocorrencia) {
		
		Ocorrencia ocorrenciaNew = ocorrenciaService.findOcorrencia(ocorrencia);
		ocorrenciaNew.setNatureza2(ocorrencia.getNatureza2());
		
		Date today = new java.util.Date();				
		ocorrenciaNew.setHoraEncerramento(new Timestamp(today.getTime()));

		logService.gravaLog("Atividade", "delete", "Encerrada uma atividade id -> " + ocorrenciaNew.getId());								
		ocorrenciaService.enviaNotificacao(ocorrenciaNew, "ocorrencia/browser/delete", null);
		ocorrenciaService.encerrarOcorrencia(ocorrenciaNew);

		result.include("status", "ok");		
		result.include("id", "10");
		result.forwardTo("/json/resposta.jsp"); 
	}

	@Path("/controle_geral/ocorrencias/excluir/")
	@Post
	@MonkeySecurity(role="ocorrencia")	
	public void excluir(Integer id) {
		
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(id);
		
		ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
				
		Date today = new java.util.Date();				
		ocorrencia.setHoraEncerramento(new Timestamp(today.getTime()));
	
		logService.gravaLog("Atividade", "delete", "Encerrada uma atividade id -> " + ocorrencia.getId());								
		ocorrenciaService.enviaNotificacao(ocorrencia, "ocorrencia/browser/delete", null);
		ocorrenciaService.encerrarOcorrencia(ocorrencia);

		result.include("status", "ok");		
		result.include("id", id);
		result.include("msgSucesso", "Atividade excluida com sucesso");			
		result.forwardTo("/json/resposta.jsp");
	}
	
	@MonkeySecurity(role="ocorrencia")	
	public void assumir(Ocorrencia ocorrencia) {
		
		
		Ocorrencia ocorrenciaNew = ocorrenciaService.findOcorrencia(ocorrencia);
				
		Usuario usuario = new Usuario();
		usuario.setId(usuarioSession.getId());
		
		ocorrenciaNew.setUsuario(usuario);
	
		logService.gravaLog("Atividade", "assumir", "Atividade assumida com sucesso - id -> " + ocorrenciaNew.getId());								
		ocorrenciaService.update(ocorrenciaNew);
	 
		ocorrenciaService.enviaNotificacao(ocorrenciaNew, "ocorrencia/browser/change", null);

		result.include("status", "ok");		
		result.include("id", "10");
		result.forwardTo("/json/resposta.jsp"); 
	}
	
	@MonkeySecurity(role="all")
	public void ocorrencias() {
		
		List<Ocorrencia> listaOcorrencia = ocorrenciaService.findOcorrenciasAtivas();
		
		List<COcorrencia> cListaOcorrencia = new ArrayList<COcorrencia>();
		
		for(Ocorrencia ocorrencia : listaOcorrencia) {

			COcorrencia cOcorrencia = new COcorrencia();
			cOcorrencia.parseFrom(ocorrencia);
			cListaOcorrencia.add(cOcorrencia);
		}	
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cListaOcorrencia);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);
		
        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());

	}
	
	@MonkeySecurity(role="all")
	public void listaNatureza() {
		
		List<Dominio> listaNatureza = ocorrenciaService.findNatureza();
		result.include("listaNatureza", listaNatureza);		
		result.forwardTo("/json/natureza.jsp"); 		
	}
	
	@MonkeySecurity(role="all")
	public void listaNiveisEmergencia() {
		
		List<NiveisEmergencia> listaNiveisEmergencia = ocorrenciaService.findNivelEmergencia();
		List<CNivelEmergencia> clistaNiveisEmergencia = new ArrayList<CNivelEmergencia>();
		
		for (NiveisEmergencia nivelEmergencia : listaNiveisEmergencia) {
			
			CNivelEmergencia cNivelEmergencia = new CNivelEmergencia();
			cNivelEmergencia.parseFrom(nivelEmergencia);
			
			clistaNiveisEmergencia.add(cNivelEmergencia);
		}

		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(clistaNiveisEmergencia);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
		
	}
	
	@Post
	@MonkeySecurity(role="all")
	public void listaUsuario() {
		
		List<Usuario> listaUsuarios = ocorrenciaService.findUsuarios();
		List<CUsuario> clistaUsuarios = new ArrayList<CUsuario>();
		Integer ultimoId = 0;
		
		for (Usuario usuario : listaUsuarios) {
		
			if (usuario.getId() != ultimoId) {
				
				CUsuario cUsuario = new CUsuario();
				cUsuario.parseFrom(usuario);
				ultimoId = usuario.getId();
				
				clistaUsuarios.add(cUsuario);
			}
		}

		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(clistaUsuarios);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
		
	}	
	
	@MonkeySecurity(role="all")
	public void ocorrencia(Integer id) {
				
		List<Ocorrencia> listaOcorrencia = new ArrayList<Ocorrencia>();
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(id);
		ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
		
		listaOcorrencia.add(ocorrencia);
		
		result.include("listaOcorrencia", listaOcorrencia);		
		result.forwardTo("/json/ocorrencia.jsp"); 

	}	

	@Post
	@Path("/ocorrencia/{id}")
	@MonkeySecurity(role="all")
	public void ocorrenciaId(Integer id) {
				
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(id);
		ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
		
		COcorrencia cOcorrencia = new COcorrencia(ocorrencia);
		
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export(cOcorrencia));		
	}	
	
	@MonkeySecurity(role="all")
	public void veiculosProximos(String latitude, String longitude) {
		
		List<Veiculo> listaVeiculos = veiculoService.findAllComHistorico();
		
		for (Veiculo veiculo : listaVeiculos) {
			
			veiculo.setEmAtendimento(false);
			veiculo.setListaOcorrencia(ocorrenciaService.findOcorrenciaByVeiculo(veiculo));
			veiculo.setQntOcorrencias(veiculo.getListaObjetos().size());
							
			for (Ocorrencia ocorrencia : veiculo.getListaOcorrencia()) {
				
				if (ocorrencia.getInicio() != null) {
					
					veiculo.setEmAtendimento(true);
				}
			}			
		}
		
		result.include("listaVeiculos", listaVeiculos);		
	}

	@Post
	@MonkeySecurity(role="all")
	public void agentesProximos(String latitude, String longitude) {
		
		List<Agente> listaAgentes = agenteService.findNaCercaComHistorico(latitude, longitude);
		List<CAgente> cListaAgentes = new ArrayList<CAgente>();
		
		for (Agente agente : listaAgentes) {
			
			agente.setEmAtendimento(false);
			List<Ocorrencia> listaOcorrencias = ocorrenciaService.findOcorrenciaByAgente(agente);
			
			agente.setQntOcorrencias(listaOcorrencias.size());
			
			for (Ocorrencia ocorrencia : listaOcorrencias) {
				
				if (ocorrencia.getInicio() != null) {
					
					agente.setEmAtendimento(true);
				}
			}
			
			CAgente cAgente = new CAgente(agente);
			cListaAgentes.add(cAgente);
		}

        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export(cListaAgentes));
	}
	
	@Post
	@MonkeySecurity(role="all")
	public void pontosDeInteresseProximos(String latitude, String longitude) {
		
		List<PontoDeInteresse> listaPontosDeInteresse = pontoDeInteresseService.findAll();
		List<CPontoDeInteresse> cListaPontos = new ArrayList<CPontoDeInteresse>();
		
		for (PontoDeInteresse ponto : listaPontosDeInteresse) {
			CPontoDeInteresse cPonto = new CPontoDeInteresse(ponto);
			cListaPontos.add(cPonto);
		}

        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export(cListaPontos));
	}
	
	@Post
	@MonkeySecurity(role="all")
	public void enquetes() {
		
		List<Enquete> listaEnquetes = enqueteService.findAll();
		List<CEnquete> cListaEnquetes = new ArrayList<CEnquete>();
		
		for (Enquete enquete : listaEnquetes) {
			CEnquete cEnquete = new CEnquete(enquete);
			cListaEnquetes.add(cEnquete);
		}

        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export(cListaEnquetes));
	}
	
	@MonkeySecurity(role="all")
	public void detalhes(Integer id) {
		
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(id);
		ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
		limparRespostas(ocorrencia);
		result.include("ocorrencia", ocorrencia);		
	}	

	@Post
	@Path("/ocorrencia/exibeDetalhes")
	@MonkeySecurity(role="all")
	public void exibeDetalhes(Integer id) {
		
		if (id != null && id > 0) {
			Ocorrencia ocorrencia = new Ocorrencia();
			ocorrencia.setId(id);
			ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
			limparRespostas(ocorrencia);
			result.include("ocorrencia", ocorrencia);
		} else {
			
			result.nothing();
		}
	}
	
	private void limparRespostas(Ocorrencia ocorrencia){
		if(ocorrencia.getOcorrenciaEnquetes() != null){
			for(OcorrenciaEnquete ocorrenciaEnquete:ocorrencia.getOcorrenciaEnquetes()){
				if(ocorrenciaEnquete.getEnquete().getPerguntas() != null){
					List<RespostaEnquete> respostas = null;
					for(Pergunta pergunta:ocorrenciaEnquete.getEnquete().getPerguntas()){
						respostas = new ArrayList<RespostaEnquete>();
						if(pergunta.getRespostas() != null){
							for(RespostaEnquete resposta:pergunta.getRespostas()){
								if(ocorrencia.getId().equals(resposta.getOcorrencia().getId())){
									respostas.add(resposta);
								}
							}
						}
						pergunta.setRespostas(respostas);
					}
				}
			}
		}
	}

	@Path("/controle_geral/ocorrencias/listaOcorrencias/")
	@MonkeySecurity(role="all")
	public void listaOcorrenciaTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<Ocorrencia> listaOcorrencias = ocorrenciaService.findAllPagina(quantidade, pagina);
		result.include("listaOcorrencias", listaOcorrencias);			
		
		Integer qnt = ocorrenciaService.findTotal();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
				
		result.forwardTo("/json/ocorrenciaTabela.jsp"); 
	}
	
	@Path("/controle_geral/ocorrencias/listaOcorrenciasAtivas/")
	@MonkeySecurity(role="all")
	public void listaOcorrenciaAtivaTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<Ocorrencia> listaOcorrencias = ocorrenciaService.findAllAtivaPagina(quantidade, pagina);
		result.include("listaOcorrencias", listaOcorrencias);			
		
		Integer qnt = ocorrenciaService.findTotalAtiva();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
				
		result.forwardTo("/json/ocorrenciaTabela.jsp"); 
	}

	@Path("/controle_geral/ocorrencias/listar/")
	@MonkeySecurity(role="ocorrencia")
	public void listar(Integer pagina) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-ocorrencia");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
	}

	@Get
	@Path("/controle_geral/ocorrencias/listar_todas/")
	@MonkeySecurity(role="ocorrencia")
	@MenuTag(tag = "listar-ocorrencia")
	public void listarTodas(Integer pagina) {
	
	}
	
	@Get
	@Path("/ocorrencia/listaComentarios")
	@MonkeySecurity(role="ocorrencia")
	public void listarComentarios(Integer id) {
		
		if (id != null && !"".equals(id) && id > 0) {
			
			Ocorrencia ocorrencia = new Ocorrencia();
			ocorrencia.setId(id);
			ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
			
			List<CComentario> listaComentarios = new ArrayList<CComentario>();
			CComentario cComentario;
			Usuario usuario;
			
			for (OcorrenciaApoio comentario : ocorrencia.getComentarios()) {
			
				usuario = usuarioService.findById(comentario.getUsuario_id());
				
				cComentario = new CComentario();
				cComentario.parseFrom(comentario);
				cComentario.setUsuario(usuario.getLogin());
				
				listaComentarios.add(cComentario);
			}

			Gson gson = new Gson();

			JsonElement je = gson.toJsonTree(listaComentarios);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			
			result.use(Results.http()).setStatusCode(404);			
			
		}
	}
	
	@Post
	@Path("/ocorrencia/comentario")
	@MonkeySecurity(role="all")
	public void adicionarComentario(Integer id, String comentario) {

		if (id != null && id > 0) {
			
			Ocorrencia ocorrencia = new Ocorrencia();
			ocorrencia.setId(id);
			ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
	
			OcorrenciaApoio ocorrenciaApoio = new OcorrenciaApoio();
			ocorrenciaApoio.setData(new Timestamp(new Date().getTime()));
			ocorrenciaApoio.setTexto(comentario);
			ocorrenciaApoio.setUsuario_id(usuarioSession.getId());
			
			List<OcorrenciaApoio> listaComentarios = ocorrencia.getComentarios();					
			listaComentarios.add(ocorrenciaApoio);
	
			logService.gravaLog("Atividade", "comentario", "Novo comentário adicionado á atividade id -> " + ocorrencia.getId(), Json.export(listaComentarios), null);	
			
			ocorrencia = ocorrenciaService.update(ocorrencia);
			ocorrenciaService.enviaNotificacao(ocorrencia, "ocorrencia/browser/change", null);

			result.include("status", "ok");		
			result.include("id", ocorrencia.getId());
			result.forwardTo("/json/resposta.jsp"); 
			
		} else {
			
			result.use(Results.http()).setStatusCode(404);			
		}		
	}

	@Post
	@Path("/ocorrencia/validar/")
	@MonkeySecurity(role="all")
	public void validar(Integer id) {

		if (id != null && id > 0) {
			
			Usuario usuario = new Usuario();
			usuario.setId(usuarioSession.getId());			
			
			Ocorrencia ocorrencia = new Ocorrencia(id);
			ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
			
			ocorrencia.setUsuario(usuario);		
			ocorrencia.setFlag(100);
				
			ocorrencia = ocorrenciaService.update(ocorrencia);
			
			logService.gravaLog("Atividade", "validar", "Atividade validada com sucesso - id -> " + ocorrencia.getId());								
			ocorrenciaService.notificaAlteracaoOcorrencia(ocorrencia, null);			
			ocorrenciaService.enviaNotificacao(ocorrencia, "ocorrencia/browser/change", null);
			
//			ocorrenciaService.enviaNotificacao(ocorrencia, "ocorrencia/addDevice");
		}
		
		result.nothing();
	}
	
	@Post
	@Path("/controle_geral/ocorrencias/importar")
	@MonkeySecurity(role="all")
	public void importar(String lista) {
	
        if (lista == null) {
			
			result.use(Results.logic()).redirectTo(OcorrenciaController.class).listar(0);
		
		} else {
			
			List<Ocorrencia> ocorrencias = new ArrayList<Ocorrencia>();
			String[] objectString = lista.split("\n");
			for(String value: objectString){
				ocorrencias.add(getOcorrenciaFromString(value));
			}
			for(Ocorrencia ocorrencia:ocorrencias){
				ocorrenciaService.insert(ocorrencia);
			}
			
			logService.gravaLog("Atividades", "importar", "Importados " + ocorrencias.size() + " atividades", lista, null);
			
			result.include("status", "ok");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		
		}
	}
	
	
	private Ocorrencia getOcorrenciaFromString(String value){
		String[] values = value.split(";");
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setResumo("Atividade");
		ocorrencia.setDescricao("Atividade");
		
		Agente agente = new Agente();
		Usuario usuario = new Usuario();
		usuario.setMatricula(values[0]);
		agente.setUsuario(usuario);
		agente = agenteService.findByMatricula(agente);
		OcorrenciaAgente ocorrenciaAgente = new OcorrenciaAgente();
		ocorrenciaAgente.setAgente(agente);
		ocorrencia.setOcorrenciaAgentes(new ArrayList<OcorrenciaAgente>());
		ocorrencia.getOcorrenciaAgentes().add(ocorrenciaAgente);
		
		PontoDeInteresse ponto = new PontoDeInteresse();
		ponto.setCodigo(values[1]);
		ponto = pontoDeInteresseService.findByCodigo(ponto);
		OcorrenciaPontoDeInteresse ocorrenciaPontoDeInteresse = new OcorrenciaPontoDeInteresse();
		ocorrenciaPontoDeInteresse.setPontoDeinteresse(ponto);
		ocorrencia.setOcorrenciaPontosDeInteresse(new ArrayList<OcorrenciaPontoDeInteresse>());
		ocorrencia.getOcorrenciaPontosDeInteresse().add(ocorrenciaPontoDeInteresse);
		
		Endereco endereco = new Endereco();
		endereco.setBairro(ponto.getEndereco().getBairro());
		endereco.setLogradouro(ponto.getEndereco().getLogradouro());
		endereco.setEndGeoref(ponto.getEndereco().getEndGeoref());
		endereco.setNumero(ponto.getEndereco().getNumero());
		endereco.setLatitude(ponto.getEndereco().getLatitude());
		endereco.setLongitude(ponto.getEndereco().getLongitude());
		ocorrencia.setEndereco(endereco);
		
		
		
		List<Dominio> listaStatus = ocorrenciaService.findStatus();
		List<Dominio> listaMeios = ocorrenciaService.findMeioCadastro();
		List<Dominio> listaNatureza = ocorrenciaService.findNatureza();
		List<NiveisEmergencia> listaNiveisEmergencias = ocorrenciaService.findNivelEmergencia();
		
		ocorrencia.setNatureza1(listaNatureza.get(0));
		ocorrencia.setNiveisEmergencia(listaNiveisEmergencias.get(0));
		ocorrencia.setMeioCadastro(listaMeios.get(0));
		ocorrencia.setOcorrenciasStatus(listaStatus.get(0));
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = sdf.parse(values[3]);
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(date);
			if("D".equals(values[2])){
				calendar.set(Calendar.HOUR_OF_DAY, 8);
			}else if("N".equals(values[2])){
				calendar.set(Calendar.HOUR_OF_DAY, 19);
			}
			
			Timestamp time = new Timestamp(calendar.getTimeInMillis());
			ocorrencia.setInicioProgramado(time);
			ocorrencia.setFimProgramado(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		List<Enquete> enquetes = enqueteService.findAll();
		if(enquetes != null){
			ocorrencia.setOcorrenciaEnquetes(new ArrayList<OcorrenciaEnquete>());
			OcorrenciaEnquete ocorrenciaEnquete;
			for(Enquete enquete:enquetes){
				ocorrenciaEnquete = new OcorrenciaEnquete();
				ocorrenciaEnquete.setEnquete(enquete);
				ocorrencia.getOcorrenciaEnquetes().add(ocorrenciaEnquete);
			}
		}
		
		usuario = new Usuario();
		usuario.setId(usuarioSession.getId());
		ocorrencia.setUsuario(usuario);
		ocorrencia.setFlag(100);
		ocorrencia.setUsuarioCriacao(usuario);
		
		return ocorrencia;
	}
}
