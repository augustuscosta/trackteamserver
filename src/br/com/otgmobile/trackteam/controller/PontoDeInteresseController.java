package br.com.otgmobile.trackteam.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Endereco;
import br.com.otgmobile.trackteam.modal.Materiais;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.PontoDeInteresse;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.comunicacao.CPontoDeInteresse;
import br.com.otgmobile.trackteam.modal.session.UsuarioSession;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MateriaisService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.ObjetoService;
import br.com.otgmobile.trackteam.service.PontoDeInteresseService;
import br.com.otgmobile.trackteam.service.UsuarioService;
import br.com.otgmobile.trackteam.util.Json;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class PontoDeInteresseController {

	private Result result;
	private Validator validator;
	private PontoDeInteresseService pontoDeInteresseService;
	private MenuService menuService;
	private Integer quantidade;
	private HttpServletResponse response;
	private final LogService logService;
	private final UsuarioSession usuarioSession;
	private final UsuarioService usuarioService;
	private final MateriaisService materiaisService;
	private final ObjetoService objetoService;

	public PontoDeInteresseController(Result result, 
			Validator validator, 
			PontoDeInteresseService pontoDeInteresseService, 
			MenuService menuService,MateriaisService materiaisService,
			UsuarioService usuarioService,
			HttpServletResponse response,
			LogService logService, UsuarioSession usuarioSession, ObjetoService objetoService) {

		this.result = result;
		this.validator = validator;
		this.pontoDeInteresseService = pontoDeInteresseService;
		this.menuService= menuService;
		this.response = response;
		this.quantidade = 10;
		this.logService = logService;
		this.usuarioSession = usuarioSession;
		this.usuarioService = usuarioService;
		this.materiaisService = materiaisService;
		this.objetoService = objetoService;
	}

	@Get
	@Path("/controle_geral/pontos_de_interesse/adicionar/")
	@MonkeySecurity(role="ponto-de-interesse")
	public void cadastro(PontoDeInteresse ponto) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("cadastro-ponto-de-interesse");
		subMenu = menuService.findByTag(subMenu);
		
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);		

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
		if (ponto == null) {
			
			Endereco endereco = new Endereco();
			endereco.setLatitude(usuarioSession.getRegioes().get(0).getCentro().getLatitude());
			endereco.setLongitude(usuarioSession.getRegioes().get(0).getCentro().getLongitude());
			
			ponto = new PontoDeInteresse();
			ponto.setEndereco(endereco);
		}
		
		result.include("ponto", ponto);
		Usuario usuario = usuarioService.find(new Usuario(usuarioSession.getId()));
		result.include("regioes", Json.export(usuario.getRegioes()));
	}

	@Post 
	@Path("/controle_geral/pontos_de_interesse/adicionar/")
	@MonkeySecurity(role="ponto-de-interesse")
	public void adicionar(PontoDeInteresse ponto) {
		
		if (ponto == null) {
			
			result.use(Results.logic()).redirectTo(PontoDeInteresseController.class).cadastro(ponto);
		
		} else {
		
			if (ponto.getNome() == null || "".equals(ponto.getNome())) {
				
				result.include("erroNome", true);
				validator.add(new ValidationMessage("Nome inválido", "error"));
			}


			validator.onErrorUse(Results.logic()).redirectTo(PontoDeInteresseController.class).cadastro(ponto);

		
			if (ponto.getId() == null || ponto.getId() <= 0) {
				
				ponto = pontoDeInteresseService.insert(ponto);
				logService.gravaLog("Ponto de interesse", "add", "Adicionado novo ponto de interesse", new CPontoDeInteresse(ponto), null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Ponto de interesse adicionado com sucesso");
				result.include("avisos", mensagens);
				
			} else {
				
				PontoDeInteresse pontoDB = pontoDeInteresseService.find(ponto);
				ponto = pontoDeInteresseService.update(pontoDB);
				logService.gravaLog("Ponto de interesse", "change", "Alterado ponto de interesse id -> " + pontoDB.getId(), new CPontoDeInteresse(pontoDB), new CPontoDeInteresse(ponto));
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Ponto de interesse alterado com sucesso");
				result.include("avisos", mensagens);				
			}
			result.use(Results.logic()).redirectTo(PontoDeInteresseController.class).listar(0);	
		}
	}

	@Path("/controle_geral/pontos_de_interesse/editar/")
	@MonkeySecurity(role="ponto-de-interesse")
	public void editar(Integer id) {
		
		PontoDeInteresse ponto = new PontoDeInteresse();

		if (id != null && id > 0) {
			
			ponto.setId(id);	
			ponto = pontoDeInteresseService.find(ponto);
			
		} 

		result.use(Results.logic()).forwardTo(PontoDeInteresseController.class).cadastro(ponto);	
	}

	@Path("/controle_geral/pontos_de_interesse/listar/")
	@MonkeySecurity(role="ponto-de-interesse")
	public void listar(Integer pagina) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-ponto-de-interesse");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
	}
	
	@Path("/controle_geral/pontos_de_interesse/listaPontos/")
	@MonkeySecurity(role="ponto-de-interesse")
	public void listaPontosTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<PontoDeInteresse> listaPontoDeInteresse = pontoDeInteresseService.findAllPagina(quantidade, pagina);
		result.include("listaPontoDeInteresse", listaPontoDeInteresse);			
		
		Integer qnt = pontoDeInteresseService.findTotal();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
		result.forwardTo("/json/pontoDeInteresseTabela.jsp"); 
	}
	
	@MonkeySecurity(role="all")
	public void listaPontosDeInteresse() {
	
		List<PontoDeInteresse> listaPontoDeInteresse = pontoDeInteresseService.findAll();
		List<CPontoDeInteresse> cListaPontoDeInteresse = new ArrayList<CPontoDeInteresse>();
		
		for(PontoDeInteresse ponto : listaPontoDeInteresse) {
			
			CPontoDeInteresse cPonto = new CPontoDeInteresse();
			cPonto.parseFrom(ponto);
			cListaPontoDeInteresse.add(cPonto);
		}	
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cListaPontoDeInteresse);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
		
	}
	
	@Path("/controle_geral/pontos_de_interesse/excluir/")
	@Post
	@MonkeySecurity(role="ponto-de-interesse")	
	public void excluir(Integer id) {
		
		PontoDeInteresse ponto = new PontoDeInteresse();
		ponto.setId(id);
		
		ponto = pontoDeInteresseService.findDeletarAgente(ponto);
		logService.gravaLog("Ponto", "delete", "Deletado ponto id -> " + ponto.getId());		

		result.include("status", "ok");		
		result.include("id", id);
		result.include("msgSucesso", "Ponto excluido com sucesso");			
		result.forwardTo("/json/resposta.jsp");
		
	}
	
	@MonkeySecurity(role="all")
	@Path("/pontos_de_interesse/materiais/listaObjetos/")
	public void listaObjetosAssociadosTabela(Integer pagina, PontoDeInteresse ponto) {

		if (pagina == null) {

			pagina = 0;
		}

		Integer total = 0;
		List<Objeto> listaObjetosAssociados = null;
		
		if (ponto != null && ponto.getId() != null) {
			
			listaObjetosAssociados = pontoDeInteresseService.findAllObjetosAssociadosPagina(quantidade, pagina, ponto);
			
			Integer qnt = pontoDeInteresseService.findTotalObjetosAssociados(ponto);
			total = qnt / quantidade;
			
			if ((qnt % quantidade) > 0 ) {
				
				total++;
			}
			
		}
		
		result.include("listaObjetos", listaObjetosAssociados);
		result.include("total", total);
		result.forwardTo("/json/objetoTabela.jsp");
	}
	
	@Get
	@Path("/controle_geral/pontos_de_interesse/materiais/remover/")
	@MonkeySecurity(role="all")
	public void removerMateriais(PontoDeInteresse ponto, Objeto objeto) {
	
        if (ponto == null || objeto == null) {
			
			result.include("status", "erro");		
			result.forwardTo("/json/resposta.jsp"); 		
		
		} else {
			
			ponto = pontoDeInteresseService.find(ponto);
			List<Objeto> listaObjetos = ponto.getListaObjetos();
			List<Objeto> listaObjetosRemovidos = new ArrayList<Objeto>();
			
			for (Objeto objetoLista : listaObjetos) {
				
				if (objetoLista.getId().equals(objeto.getId())) {
					
					listaObjetosRemovidos.add(objetoLista);
				}				
			}
				
			for (Objeto objetoLista : listaObjetosRemovidos) {
				
				listaObjetos.remove(objetoLista);
			}
			
			ponto.setListaObjetos(listaObjetos);
			ponto = pontoDeInteresseService.update(ponto);
			logService.gravaLog("Ponto de interesse", "deleteMateriais", "Removidos objetos do ponto id -> " + ponto.getId(), listaObjetos, null);

			result.include("status", "ok");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		}
	}
	
	@Path({"/controle_geral/pontos_de_interesse/materiais/{ponto.id}", "/controle_geral/pontos_de_interesse/materiais/"})
	@MonkeySecurity(role="all")
	public void materiais(PontoDeInteresse ponto, Integer id) {

		if (ponto == null && id != null) {
			
			ponto = new PontoDeInteresse();
			ponto.setId(id);
			
		}
		
		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-ponto-de-interesse");
		subMenu = menuService.findByTag(subMenu);
		
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		List<Menu> listaMenu = menuService.listMenu();			
		
		List<Dominio> listaFuncionalidade = materiaisService.findFuncionalidade();			
		List<Materiais> listaMateriais = objetoService.findMateriais(listaFuncionalidade.get(0));		
				
		result.include("listaFuncionalidade", listaFuncionalidade);	
		result.include("listaMateriais", listaMateriais);		
		result.include("ponto", ponto);
		result.include("materialSelecionado", listaMateriais.get(0));
		
		result.include("menuQuick", listaMenu);	
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);
	}
	
	@Post
	@Path("/controle_geral/pontos_de_interesse/materiais/associar/")
	@MonkeySecurity(role="all")
	public void associarMateriais(PontoDeInteresse ponto, List<Integer> id) {
	
        if (ponto == null) {
			
			result.use(Results.logic()).redirectTo(PontoDeInteresseController.class).listar(0);
		
		} else {
			
			ponto = pontoDeInteresseService.find(ponto);
			List<Objeto> listaObjetos = ponto.getListaObjetos();
			
			for (Integer idTmp : id) {
				
				Objeto objeto = new Objeto();
				objeto.setId(idTmp);
				objeto = objetoService.find(objeto);
				
				listaObjetos.add(objeto);
				
			}
									
			ponto.setListaObjetos(listaObjetos);
			ponto = pontoDeInteresseService.update(ponto);
			logService.gravaLog("Ponto de interesse", "addMateriais", "Adicionados materiais ao ponto de interesse id -> " + ponto.getId(), listaObjetos, null);
			
			result.include("status", "ok");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		
		}
	}
	
	@Post
	@Path("/controle_geral/pontos_de_interesse/{id}")
	@MonkeySecurity(role="all")
	public void pontoDeInteresseId(Integer id) {
				
		PontoDeInteresse ponto = new PontoDeInteresse();
		ponto.setId(id);
		ponto = pontoDeInteresseService.find(ponto);
		
		CPontoDeInteresse cPonto = new CPontoDeInteresse(ponto);
		
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export(cPonto));		
	}	
	
	@Post
	@Path("/controle_geral/pontos_de_interesse/importar")
	@MonkeySecurity(role="all")
	public void importar(String lista) {
	
        if (lista == null) {
			
			result.use(Results.logic()).redirectTo(PontoDeInteresseController.class).listar(0);
		
		} else {
			
			List<PontoDeInteresse> pontos = new ArrayList<PontoDeInteresse>();
			String[] objectString = lista.split("\n");
			for(String value: objectString){
				pontos.add(getPontoDeInteresseFromString(value));
			}
			for(PontoDeInteresse ponto:pontos){
				pontoDeInteresseService.insert(ponto);
			}
			
			logService.gravaLog("Ponto de interesse", "importar", "Importados " + pontos.size() + " pontos de interesse", lista, null);
			
			result.include("status", "ok");		
			result.include("id", "10");
			result.forwardTo("/json/resposta.jsp"); 		
		}
	}

	private PontoDeInteresse getPontoDeInteresseFromString(String value) {
		String[] values = value.split(";");
		
		PontoDeInteresse ponto = new PontoDeInteresse();
		ponto.setNome(values[0]);
		
		Endereco endereco = new Endereco();
		endereco.setLatitude(values[1]);
		endereco.setLongitude(values[2]);
		endereco.setLogradouro(values[3]);
		endereco.setBairro(values[4]);
		endereco.setNumero(values[5]);
		endereco.setEndGeoref(values[6]);
		
		ponto.setEndereco(endereco);
		
		ponto.setTelefone(values[7]);
		ponto.setEmail(values[8]);
		ponto.setContato(values[9]);
		ponto.setObservacao(values[10]);
		ponto.setCodigo(values[11]);
		
		
		return ponto;
	}
	
}
