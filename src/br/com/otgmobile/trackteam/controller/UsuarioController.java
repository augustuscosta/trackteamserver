package br.com.otgmobile.trackteam.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.Regioes;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.comunicacao.CUsuario;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioGrupo;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.RegioesService;
import br.com.otgmobile.trackteam.service.UsuarioService;
import br.com.otgmobile.trackteam.util.Util;

@Resource
public class UsuarioController {

	private Result result;
	private MenuService menuService;
	private UsuarioService usuarioService;
	private RegioesService regioesService;
	private Integer quantidade;
	private Validator validator;
	private final LogService logService;

	public UsuarioController(Result result, MenuService menuService,
			UsuarioService usuarioService, Validator validator,
			RegioesService regioesService,
			LogService logService) {

		this.result = result;
		this.menuService = menuService;
		this.usuarioService = usuarioService;
		this.quantidade = 10;
		this.validator = validator;
		this.regioesService = regioesService;
		this.logService = logService;
	}

	@Get
	@Path("/controle_geral/usuarios/adicionar/")
	@MonkeySecurity(role = "usuario")
	public void cadastro(Usuario usuario) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("cadastro-usuario");
		subMenu = menuService.findByTag(subMenu);

		List<SubMenu> listaSubMenu = menuService
				.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu", listaSubMenu);
		result.include("subMenu", subMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);

		List<UsuarioGrupo> listaGrupos = usuarioService.findGrupos();
		result.include("listaGrupos", listaGrupos);

		List<Dominio> listaStatus = usuarioService.findStatus();
		result.include("listaStatus", listaStatus);
		result.include("usuario", usuario);

	}

	@Post
	@Path("/controle_geral/usuarios/adicionar/")
	@MonkeySecurity(role = "usuario")
	public void adicionar(Usuario usuario) {
		if (usuario == null) {

			result.use(Results.logic()).redirectTo(UsuarioController.class)
					.cadastro(usuario);

		} else if (usuario.getId() == null) {

			Usuario verificarLoginMatricula = usuarioService
					.findByMatricula(usuario);
			if (usuario.getMatricula() == null
					|| "".equals(usuario.getMatricula().trim())) {

				result.include("erroMatricula", true);
				validator.add(new ValidationMessage("Matricula inválida",
						"error"));

			} else if (verificarLoginMatricula.getMatricula() != null) {

				if (verificarLoginMatricula.getMatricula().toLowerCase()
						.equals(usuario.getMatricula().toLowerCase())) {

					result.include("erroMatricula", true);
					validator.add(new ValidationMessage("Matricula "
							+ usuario.getMatricula() + " já Cadastrada!!",
							"error"));
				}
			}

			if (usuario.getNome() == null
					|| "".equals(usuario.getNome().trim())) {

				result.include("erroNome", true);
				validator.add(new ValidationMessage("Nome inválido", "error"));

			}

			if (usuario.getNome() == null
					|| "".equals(usuario.getNome().trim())) {

				result.include("erroNome", true);
				validator.add(new ValidationMessage("Nome inválido", "error"));

			}

			Usuario verificarLoginUsuario = usuarioService.findByLogin(usuario);
			if (usuario.getLogin() == null
					|| "".equals(usuario.getLogin().trim())) {

				result.include("erroLogin", true);
				validator.add(new ValidationMessage("Login inválido", "error"));

			} else if (verificarLoginUsuario.getLogin() != null) {

				if (verificarLoginUsuario.getLogin().toLowerCase()
						.equals(usuario.getLogin().toLowerCase())) {

					result.include("erroLogin", true);
					validator
							.add(new ValidationMessage("Login "
									+ usuario.getLogin() + " já Cadastrado!!",
									"error"));
				}
			}

			if ((usuario.getSenha() == null || usuario.getSenha().length() < 5)
					&& (usuario.getId() == null || usuario.getId() <= 0)) {

				result.include("erroSenha", true);
				validator
						.add(new ValidationMessage(
								"A senha precisa ter pelo menos 5 caracteres",
								"error"));
			}

			Usuario verificarLoginCPF = usuarioService.findByCpf(usuario);
			if (usuario.getCpf() == null || "".equals(usuario.getCpf().trim())
					|| usuario.getCpf().length() < 11) {

				result.include("erroCpf", true);
				validator.add(new ValidationMessage("Cpf inválido", "error"));
			} else if (verificarLoginCPF.getCpf() != null) {

				if (verificarLoginCPF.getCpf().equals(usuario.getCpf())) {

					result.include("erroCpf", true);
					validator.add(new ValidationMessage("CPF "
							+ usuario.getCpf() + " já Cadastrado!!", "error"));
				}
			}

			validator.onErrorUse(Results.logic())
					.redirectTo(UsuarioController.class).cadastro(usuario);

			if (usuario.getDescricao() == null
					|| "".equals(usuario.getDescricao().trim())) {

				result.include("erroDescricao", true);
				validator.add(new ValidationMessage("Descrição inválida",
						"error"));
			}

			List<Regioes> ListaUsuarioRegioes = new ArrayList<Regioes>();
			if (usuario.getRegioes() != null) {
				for (Regioes regioes : usuario.getRegioes()) {
					if (!(regioes.getId() == null && regioes.getNome() == null)) {
						ListaUsuarioRegioes.add(regioes);
					}
				}
			}

			usuario.setRegioes(ListaUsuarioRegioes);

			if (usuario.getId() == null || usuario.getId() <= 0) {

				usuario.setSenha(Util.md5(usuario.getSenha()));

				usuario = usuarioService.insert(usuario);
				logService.gravaLog("Usuario", "add", "Adicionado novo usuario - id -> " + usuario.getId(), new CUsuario(usuario), null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Usuario adicionado com sucesso");
				result.include("avisos", mensagens);

			}

		} else {

			Usuario usuarioInserir = usuarioService.find(usuario);
			CUsuario cUsuario = new CUsuario(usuarioInserir);
			
			usuarioInserir.setMatricula(usuario.getMatricula());
			usuarioInserir.setNome(usuario.getNome());
			usuarioInserir.setLogin(usuario.getLogin());
			usuarioInserir.setCpf(usuario.getCpf());
			usuarioInserir.setDescricao(usuario.getDescricao());
			usuarioInserir.setStatus(usuario.getStatus());
			usuarioInserir.setGrupo(usuario.getGrupo());

			List<Regioes> listaRegioes = new ArrayList<Regioes>();

			if (usuario.getRegioes() != null) {

				for (Regioes regioes : usuario.getRegioes()) {
					listaRegioes.add(regioesService.find(regioes));
				}
			}

			usuarioInserir.setRegioes(listaRegioes);

			usuario = usuarioService.update(usuarioInserir);
			logService.gravaLog("Usuario", "change", "Alterado usuario - id -> " + usuario.getId(), new CUsuario(usuarioInserir), cUsuario);

			Mensagem mensagens = new Mensagem();
			mensagens.setTitulo("Usuario alterado com sucesso");
			result.include("avisos", mensagens);

		}

		validator.onErrorUse(Results.logic())
				.forwardTo(UsuarioController.class).editarUsuario(null);
		result.use(Results.logic()).redirectTo(UsuarioController.class)
				.listar(0);

	}

	@Post
	@Path("/controle_geral/usuarios/senha/")
	@MonkeySecurity(role = "all")
	public void alterarSenha(Integer id, String senhaAtual, String senha01,	String senha02) {

		Usuario usuario = null;
		String msgError = "";

		if (id != null && id > 0) {

			usuario = usuarioService.findById(id);
			senhaAtual = Util.md5(senhaAtual);
			senha01 = Util.md5(senha01);
			senha02 = Util.md5(senha02);

		} else {

			msgError = "Usuário inválido";
		}

		if (senha01 != null && senha02 != null && senha01.equals(senha02)
				&& usuario != null && usuario.getSenha().equals(senhaAtual)) {

			usuario.setSenha(senha01);
			usuarioService.update(usuario);
			logService.gravaLog("Usuario", "changeSenha", "Alterada senha do usuario - id -> " + usuario.getId());
			
			result.include("status", "ok");
			result.include("id", id);
			result.include("msgSucesso", "Senha alterada com sucesso");
			result.forwardTo("/json/resposta.jsp");

		} else {

			if (msgError.equals("") && usuario != null
					&& !usuario.getSenha().equals(senhaAtual)) {

				msgError = "Senha não confere";
			}

			if (senha01 == null || senha02 == null || !senha01.equals(senha02)) {

				msgError = "Senha nova não confere";
			}

			logService.gravaLog("Usuario", "changeSenha", "Erro ao alterar senha do usuario - id -> " + usuario.getId());
			
			result.include("status", "error");
			result.include("id", id);
			result.include("msgErro", msgError);
			result.forwardTo("/json/resposta.jsp");
		}
	}

	@Path("/controle_geral/usuarios/listar/")
	@MonkeySecurity(role = "usuario")
	public void listar(Integer pagina) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-usuario");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService
				.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu", listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);

	}

	@Path("/controle_geral/usuarios/listaUsuarios/")
	@MonkeySecurity(role = "all")
	public void listaUsuariosTabela(Integer pagina) {

		if (pagina == null) {

			pagina = 0;
		}

		List<Usuario> listaUsuarios = usuarioService.findAllPagina(quantidade,
				pagina);
		result.include("listaUsuarios", listaUsuarios);

		Integer qnt = usuarioService.findTotal();
		Integer total = qnt / quantidade;

		if ((qnt % quantidade) > 0) {

			total++;
		}

		result.include("total", total);

		result.forwardTo("/json/usuarioTabela.jsp");
	}

	@Path("/controle_geral/usuarios/excluir/")
	@Post
	@MonkeySecurity(role = "usuario")
	public void excluir(Integer id) {

		Usuario usuario = new Usuario();
		usuario.setId(id);

		usuario = usuarioService.findDeletarUsuarios(usuario);
		logService.gravaLog("Usuario", "delete", "Removido usuario - id -> " + usuario.getId());

		result.include("status", "ok");
		result.include("id", id);
		result.include("msgSucesso", "Usuário excluido com sucesso");
		result.forwardTo("/json/resposta.jsp");

	}

	@Path("/controle_geral/usuarios/editarUsuario/")
	@MonkeySecurity(role = "ocorrencia")
	@MenuTag(tag = "usuario")
	public void editarUsuario(Integer id) {
		Usuario usuario = new Usuario();

		if (id != null) {
			usuario = usuarioService.findById(id);
			result.include("usuario", usuario);
		}

		result.use(Results.logic()).forwardTo(UsuarioController.class)
				.cadastro(usuario);
	}
}
