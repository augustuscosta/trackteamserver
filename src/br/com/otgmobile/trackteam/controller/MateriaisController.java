package br.com.otgmobile.trackteam.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Materiais;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MateriaisService;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.util.Util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class MateriaisController {

	private final Result result;
	private final MateriaisService materiaisService;
	private final Validator validator;	
	private final MenuService menuService;
	private final Integer quantidade;
	private final LogService logService;
	private HttpServletResponse response;
	
	public MateriaisController(Result result, 
			MateriaisService materiaisService, 
			Validator validator,
			MenuService menuService,
			HttpServletResponse response,
			LogService logService) {
		
		this.result = result;
		this.materiaisService = materiaisService;
		this.validator = validator;		
		this.menuService= menuService;
		this.quantidade = 10;
		this.response = response;
		this.logService = logService;
	}
    
	@MonkeySecurity(role="materiais")
	@Path("/controle_geral/materiais/cadastro/")
	public void cadastro(Materiais materiais) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("cadastro-materiais");
		subMenu = menuService.findByTag(subMenu);
		
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);
		
		List<Dominio> listaFuncionalidadeMateriais = materiaisService.findFuncionalidade();		

		result.include("listaFuncionalidadeMateriais", listaFuncionalidadeMateriais);
		result.include("materiais", materiais);
	}

	@Path("/controle_geral/materiais/adicionar/")
	@MonkeySecurity(role="materiais")
	public void adicionar(Materiais materiais) {
		
		if (materiais == null) {
			
			result.use(Results.logic()).redirectTo(MateriaisController.class).cadastro(materiais);
		
		} else {
		
			if (materiais.getTitulo() == null || "".equals(materiais.getTitulo())) {
				
				result.include("erroTitulo", true);
				validator.add(new ValidationMessage("Titulo invalido", "error"));
			}
	
			if (materiais.getFuncionalidade() == null || "".equals(materiais.getFuncionalidade())) {
				
				result.include("erroFuncionalidade", true);
				validator.add(new ValidationMessage("Funcionalidade invalida", "error"));
			}
					
			validator.onErrorUse(Results.logic()).redirectTo(MateriaisController.class).cadastro(materiais);
					    
			if (materiais.getId() == null || materiais.getId() <= 0) {

				materiais = materiaisService.insert(materiais);
				logService.gravaLog("Materiais", "add", "Adicionado novo material", materiais, null);
				
				final ResourceBundle resourceConfig = java.util.ResourceBundle.getBundle("config");			
				String socketUrl = resourceConfig.getString("socket_url");
				
//				Util.notificacao("novo_material", materiais.getId(), socketUrl);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Material adicionado com sucesso");
				result.include("avisos", mensagens);
			
			} else {
						
				materiaisService.update(materiais);
				logService.gravaLog("Materiais", "change", "Alterado material id -> " + materiais.getId(), materiais, null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Material alterado com sucesso");
				result.include("avisos", mensagens);
			
			}
			
			result.use(Results.logic()).redirectTo(MateriaisController.class).listar(0);

		}
	}

	@MonkeySecurity(role="materiais")
	@Path("/controle_geral/materiais/editar/")	
	public void editar(Integer id) {
		
		Materiais materiais = new Materiais();

		if (id >= 0) {
			
			materiais.setId(id);
			materiais = materiaisService.findMaterial(materiais);
			result.use(Results.logic()).forwardTo(MateriaisController.class).cadastro(materiais);
			
		} else {

			materiais.setId(id);		
			result.use(Results.logic()).forwardTo(MateriaisController.class).cadastro(materiais);
		}		
	}

	
	@MonkeySecurity(role="materiais")
	public void remover(Materiais materiais) {
		
		materiais = materiaisService.delete(materiais);
		logService.gravaLog("Materiais", "delete", "Removido material id -> " + materiais.getId(), materiais, null);
	}
	
	
	@MonkeySecurity(role="materiais")
	public void materiais() {
		
		List<Materiais> listaMateriais = materiaisService.findMateriais();
		result.include("listaMateriais", listaMateriais);
		result.forwardTo("/json/materiais.jsp"); 

	}
	
	@MonkeySecurity(role="materiais")
	public void materiais(Integer id) {
				
		List<Materiais> listaMateriais = new ArrayList<Materiais>();
		Materiais materiais = new Materiais();
		materiais.setId(id);
		materiais = materiaisService.findMaterial(materiais);
		
		listaMateriais.add(materiais);
		
		result.include("listaMateriais", listaMateriais);		
		result.forwardTo("/json/materiais.jsp");
	}
	
	@MonkeySecurity(role="all")
	@Path("/controle_geral/materiais/listar/")	
	public void listar(Integer pagina) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-materiais");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
		if (pagina == null || pagina <= 0) {
			pagina=1;
		}
		
		List<Materiais> listaMateriais = materiaisService.findAllPagina(quantidade, pagina);
		result.include("listaMateriais", listaMateriais);	
		
	}
	
	@MonkeySecurity(role="all")
	@Path("/controle_geral/materiais/listaMateriais/")	
	public void listaMateriaisTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<Materiais> listaMateriais = materiaisService.findAllPagina(quantidade, pagina);
		result.include("listaMateriais", listaMateriais);			
		
		Integer qnt = materiaisService.findTotal();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
				
		result.forwardTo("/json/materiaisTabela.jsp");
	}

	@MonkeySecurity(role="all")
	@Path("/controle_geral/materiais/listaMateriaisCombo/")	
	public void listaMateriaisCombo(Integer funcionalidade) {
		
		Dominio func = new Dominio();
		func.setId(funcionalidade);
		
		List<Materiais> listaMateriais = materiaisService.findByFuncionalidade(func);

		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(listaMateriais);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");
		result.use(Results.http()).body(jo.toString());
	}
	
}
