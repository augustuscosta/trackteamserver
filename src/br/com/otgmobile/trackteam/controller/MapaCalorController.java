package br.com.otgmobile.trackteam.controller;

import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.wikinova.heatmaps.KernelMap;
import br.com.wikinova.heatmaps.renderer.DiscreteRainbowKMR;
import br.com.wikinova.heatmaps.renderer.KernelMapRenderer;
import br.com.wikinova.heatmaps.util.PontoLatLng;

@Resource
public class MapaCalorController {

	private final Result result;
	private final OcorrenciaService ocorrenciaService;
	private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public MapaCalorController(Result result, OcorrenciaService ocorrenciaService) {
		
		this.result = result;
		this.ocorrenciaService = ocorrenciaService;
	}
	
	@Get
	@MonkeySecurity(role="all")
    @Path("/kernel/{norte}/{sul}/{leste}/{oeste}/{zoom}")
    public InputStream kernel(
    		final Float norte, 
    		final Float sul, 
    		final Float leste, 
    		final Float oeste, 
    		Integer zoom) throws Exception {
		
        HashMap<String, Object> filtros = new HashMap<String, Object>();

        filtros.put("norte", norte);
        filtros.put("sul", sul);
        filtros.put("leste", leste);
        filtros.put("oeste", oeste);
       
        List<Ocorrencia> listaOcorrencias = ocorrenciaService.findOcorrenciasAtivas();
        List<Point> pontos = new ArrayList<Point>();
       
        for (Ocorrencia ocorrencia : listaOcorrencias) {
        	
        	if (ocorrencia.getEndereco().getLatitude() != null && 
        			ocorrencia.getEndereco().getLongitude() != null && 
        			!"".equals(ocorrencia.getEndereco().getLatitude()) && 
        			!"".equals(ocorrencia.getEndereco().getLongitude())) {
        		
        		PontoLatLng latLng = new PontoLatLng(Float.parseFloat(ocorrencia.getEndereco().getLatitude()),Float.parseFloat(ocorrencia.getEndereco().getLongitude()));          
        		pontos.add(latLng.toPixel(zoom));
        	}
        }
       
        int north = new PontoLatLng(norte, 0.0).toPixel(zoom).y;
        int south = new PontoLatLng(sul, 0.0).toPixel(zoom).y;
        int east = new PontoLatLng(0.0,leste).toPixel(zoom).x;
        int west = new PontoLatLng(0.0,oeste).toPixel(zoom).x;
        int maxX = new PontoLatLng(0,180).toPixel(zoom).x; //x correspondente a 180 graus de longitude
       
        if(west > east)
            west = west-maxX;
       
        int width = east - west;
        int height = south - north;
        Rectangle limitesPixel = new Rectangle(west, north, width, height);
           

        KernelMap map = new KernelMap(1,14,limitesPixel,pontos);
       
        //cria imagem
        KernelMapRenderer renderer = new DiscreteRainbowKMR(map,0.75f);
       
        Image img = renderer.renderImage();
       
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(((BufferedImage)img), "png", os);
        InputStream is = new ByteArrayInputStream(os.toByteArray());
       
        return is;
    }
/*	
//	@Get
//	@FotoSecurity(role="all")
//	@Path("/kernel2/{norte}/{sul}/{leste}/{oeste}/{zoom}")
	@Get
	@FotoSecurity(role="all")
	@Path("/kernel2/{norte}/{sul}/{leste}/{oeste}/{zoom}/")
    public InputStream kernel2(
    		final Float norte, 
    		final Float sul, 
    		final Float leste, 
    		final Float oeste, 
    		Integer zoom,
    		String json) throws Exception {

		
		HashMap<String, Object> filtros = new HashMap<String, Object>();

        filtros.put("norte", norte);
        filtros.put("sul", sul);
        filtros.put("leste", leste);
        filtros.put("oeste", oeste);
		Date hData = formataDataFim("01/08/2012");
		Date hDatafim = formataDataFim("24/08/2012");
        List<Ocorrencia> listaOcorrencias = ocorrenciaService.findOcorrenciaAll(hData, hDatafim);
        List<Point> pontos = new ArrayList<Point>();
        System.out.println(json);
        List<Date> listaJson = Util.fromGsonArray(Date.class, json);
        System.out.println("===========>"+listaJson.size());
        //List<Ocorrencia> listaOcorrencias = ocorrenciaService.findOcorrenciasAtivas();
        //listaOcorrencias = ocorrenciaService.findOcorrenciaAll(dataInicial,dataFinal,abertafechada);
        for (Ocorrencia ocorrencia : listaOcorrencias) {
        	
        	if (ocorrencia.getEndereco().getLatitude() != null && 
        			ocorrencia.getEndereco().getLongitude() != null && 
        			!"".equals(ocorrencia.getEndereco().getLatitude()) && 
        			!"".equals(ocorrencia.getEndereco().getLongitude())) {
        		
        		PontoLatLng latLng = new PontoLatLng(Float.parseFloat(ocorrencia.getEndereco().getLatitude()),Float.parseFloat(ocorrencia.getEndereco().getLongitude()));          
        		pontos.add(latLng.toPixel(zoom));
        	}
        }
       
        int north = new PontoLatLng(norte, 0.0).toPixel(zoom).y;
        int south = new PontoLatLng(sul, 0.0).toPixel(zoom).y;
        int east = new PontoLatLng(0.0,leste).toPixel(zoom).x;
        int west = new PontoLatLng(0.0,oeste).toPixel(zoom).x;
        int maxX = new PontoLatLng(0,180).toPixel(zoom).x; //x correspondente a 180 graus de longitude
       
        if(west > east)
            west = west-maxX;
       
        int width = east - west;
        int height = south - north;
        Rectangle limitesPixel = new Rectangle(west, north, width, height);
           

        KernelMap map = new KernelMap(1,14,limitesPixel,pontos);
       
        //cria imagem
        KernelMapRenderer renderer = new DiscreteRainbowKMR(map,0.75f);
       
        Image img = renderer.renderImage();
       
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(((BufferedImage)img), "png", os);
        InputStream is = new ByteArrayInputStream(os.toByteArray());
       
        return is;
		
	}
*/	
	public Date formataDataInicio(String data) {
		
		Date dataInicial = new Date();
		
		
			
			try {
				dataInicial = sdf.parse(data);
			}catch (ParseException e) {
				dataInicial.setTime(dataInicial.getTime() - 24*60*60*1000);
//				e.printStackTrace();
			} catch (NullPointerException ex) {
				dataInicial.setTime(dataInicial.getTime() - 24*60*60*1000);
			}
			
		
		
		return dataInicial;
	}
	
	public Date formataDataFim(String data) {
		
		Date dataFinal =  new Date();
		
		try {
	
			dataFinal = sdf.parse(data);
			sdf.setLenient(false);
		} catch (ParseException e) {
			dataFinal.setTime(dataFinal.getTime() - 24*60*60*1000);
//			e.printStackTrace();
		} catch (NullPointerException ex) {
			dataFinal.setTime(dataFinal.getTime() - 24*60*60*1000);
		}
		
		return dataFinal;
	}
}
