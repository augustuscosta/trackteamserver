package br.com.otgmobile.trackteam.controller.comunicacao;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.comunicacao.CAgente;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.util.Util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class CLoginController {

	final private Result result;
	private final HttpServletResponse response;
	private final TokenService tokenService;
	private final AgenteService agenteService;
	private final LogService logService;
	
	public CLoginController(Result result,
			HttpServletResponse response,
			TokenService tokenService,
			AgenteService agenteService,
			LogService logService) {

		this.result = result;
		this.tokenService = tokenService;
		this.agenteService = agenteService;		
		this.response = response;
		this.logService = logService;
	}
	
	
	@Post
	@Path("/comunicacao/session")
	public void logar(String login, String senha, String dispositivo, String versao) {
			
		Usuario usuario = new Usuario();
		usuario.setLogin(login);
		
		Agente agente = new Agente();
		agente.setUsuario(usuario);
		
		agente = agenteService.findByLogin(agente);
				
		if (agente != null && Util.md5(senha).equals(agente.getUsuario().getSenha())) {
					
			Token token = new Token();
			
			token.setToken(Util.geraToken(agente.getUsuario()));
			token.setAgente_id(agente.getId());
			token.setInativo(false);
			token.setVersao(versao);
			
			if (agente.getVeiculo() != null) {				
				token.setVeiculo_id(agente.getVeiculo().getId());
			}

			tokenService.limpa(token);
			tokenService.insert(token);
			
			logService.gravaLog("Login", "loginDevice", "Login efetuado com sucesso no device, pelo usuario -> " + agente.getUsuario().getMatricula() + " - " + agente.getUsuario().getNome());
			
			CAgente cAgente = new CAgente();
			cAgente.parseFrom(agente);
			cAgente.setToken(token.getToken());
			
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(cAgente);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
		    
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {

			logService.gravaLog("Login", "loginDevice", "Erro ao efetuar login. Usuário ou senha inválidos -> " + login);
			result.use(Results.http()).setStatusCode(401);
		}
	}
}
