package br.com.otgmobile.trackteam.controller.comunicacao;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.modal.Pergunta;
import br.com.otgmobile.trackteam.modal.RespostaEnquete;
import br.com.otgmobile.trackteam.modal.comunicacao.CRespostaEnquete;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.service.ObjetoService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.PerguntaService;
import br.com.otgmobile.trackteam.service.RespostaService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.util.Json;

@Resource
public class CEnqueteController {

	final private Result result;
	private final TokenService tokenService;
	private final HttpServletResponse response;
	private final RespostaService respostaService;
	private final OcorrenciaService ocorrenciaService;
	private final PerguntaService perguntaService;
	
	public CEnqueteController(Result result, ObjetoService objetoService, TokenService tokenService, RespostaService respostaService, OcorrenciaService ocorrenciaService, PerguntaService perguntaService, HttpServletResponse response) {

		this.result = result;
		this.tokenService = tokenService;
		this.respostaService = respostaService;
		this.ocorrenciaService = ocorrenciaService;
		this.perguntaService = perguntaService;
		this.response = response;
	}
	

	@Post
	@Path("/comunicacao/respostas")
	public void adicionaListaRespostas(String token, String respostasJson) {
		
		
		if (token != null && tokenService.isValido(token)) {
			
			List<CRespostaEnquete> cListaRespostas = Json.importaArray(CRespostaEnquete.class, respostasJson);
			for(CRespostaEnquete resposta:cListaRespostas){
				
				RespostaEnquete respostaEnquete = new RespostaEnquete();
				respostaEnquete.setResposta(resposta.getResposta());
				
				Pergunta pregunta = new Pergunta();
				pregunta.setId(resposta.getPergunta_id());
				pregunta = perguntaService.find(pregunta);
				
				respostaEnquete.setPergunta(pregunta);
				
				Ocorrencia ocorrencia = new Ocorrencia();
				ocorrencia.setId(resposta.getOcorrencia_id());
				ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
				
				respostaEnquete.setOcorrencia(ocorrencia);
				
				respostaService.insert(respostaEnquete);
			}
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(Json.export("result", cListaRespostas));
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);			
		}
	}	
	
	@Post
	@Path("/comunicacao/resposta")
	public void adicionaListaResposta(String token, String respostaJson) {
		
		
		if (token != null && tokenService.isValido(token)) {
			
			CRespostaEnquete resposta = (CRespostaEnquete) Json.importa(
					CRespostaEnquete.class, respostaJson);

			RespostaEnquete respostaEnquete = new RespostaEnquete();
			respostaEnquete.setResposta(resposta.getResposta());

			Pergunta pregunta = new Pergunta();
			pregunta.setId(resposta.getPergunta_id());
			pregunta = perguntaService.find(pregunta);

			respostaEnquete.setPergunta(pregunta);

			Ocorrencia ocorrencia = new Ocorrencia();
			ocorrencia.setId(resposta.getOcorrencia_id());
			ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);

			respostaEnquete.setOcorrencia(ocorrencia);

			respostaService.insert(respostaEnquete);

			response.setContentType("application/json; charset=UTF-8");
			result.use(Results.http()).body(Json.export("result", resposta));
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);			
		}
	}
}