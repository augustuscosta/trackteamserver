package br.com.otgmobile.trackteam.controller.comunicacao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CLocalizaMaterial;
import br.com.otgmobile.trackteam.modal.comunicacao.CObjeto;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.ObjetoService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.util.Json;
import br.com.otgmobile.trackteam.util.Util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class CObjetoController {

	final private Result result;
	private final TokenService tokenService;
	private ObjetoService objetoService;
	private final VeiculoService veiculoService;
	private final AgenteService agenteService;
	private final HttpServletResponse response;
	
	public CObjetoController(Result result, ObjetoService objetoService, TokenService tokenService,	VeiculoService veiculoService,
			AgenteService agenteService, HttpServletResponse response) {

		this.result = result;
		this.tokenService = tokenService;
		this.objetoService = objetoService;
		this.veiculoService = veiculoService;
		this.agenteService = agenteService;		
		this.response = response;
	}
	
	@Get
	@Path("/comunicacao/materiais/{token}")
	public void listarTodos(String token) {

		if (tokenService.isValido(token)) {

			List<Objeto> listaObjetos = new ArrayList<Objeto>();
			
			Token tokenMovel = new Token(token);
			tokenMovel = tokenService.find(tokenMovel);
			
			if (tokenMovel.getVeiculo_id() != null && tokenMovel.getVeiculo_id() > 0) {
				
				Veiculo veiculo = new Veiculo();
				veiculo.setId(tokenMovel.getVeiculo_id());
				veiculo = veiculoService.find(veiculo);
				listaObjetos = veiculo.getListaObjetos();
				
			} else if (tokenMovel.getAgente_id() != null && tokenMovel.getAgente_id() > 0) {
				
				Agente agente = new Agente();
				agente.setId(tokenMovel.getAgente_id());
				agente = agenteService.find(agente);
				listaObjetos = agente.getListaObjetos();								
			}
			
			List<Objeto> listaObjetosPontos = objetoService.findDisponiveisDosPontosDeInteresseByToken(tokenMovel);
			
			List<CObjeto> listaCObjetos = new ArrayList<CObjeto>();
			
			for(Objeto objeto : listaObjetos) {
				
				CObjeto cObjeto = new CObjeto();
				cObjeto.parseFrom(objeto);
				
				String titulo = "";
				
				if (objeto.getMateriais() != null && objeto.getMateriais().getTitulo() != null) {
					titulo += objeto.getMateriais().getTitulo();
				}
				
				if (cObjeto.getDescricao() != null ) {
					titulo += " - " + cObjeto.getDescricao();
				}
				
				cObjeto.setDescricao( titulo );
				
				listaCObjetos.add(cObjeto);
				
			}
			
			for(Objeto objeto : listaObjetosPontos) {
					
					CObjeto cObjeto = new CObjeto();
					cObjeto.parseFrom(objeto);
					
					String titulo = "";
					
					if (objeto.getMateriais() != null && objeto.getMateriais().getTitulo() != null) {
						titulo += objeto.getMateriais().getTitulo();
					}
					
					if (cObjeto.getDescricao() != null ) {
						titulo += " - " + cObjeto.getDescricao();
					}
					
					cObjeto.setDescricao( titulo );
					
					listaCObjetos.add(cObjeto);
					
				}
			
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(listaCObjetos);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());		
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Get
	@Path("/comunicacao/materiais/disponiveis/{token}/{query}")
	public void consultaObjetos(String token, String query) {
		
		if (token != null && tokenService.isValido(token) && query != null) {

			List<Objeto> listaObjetos = objetoService.findDisponiveisByQuery(query);
			List<CObjeto> cListaObjetos = new ArrayList<CObjeto>();
			
			for (Objeto objeto : listaObjetos) {
				
				CObjeto cObjeto = new CObjeto(objeto);				
				cListaObjetos.add(cObjeto);
			}
				
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(Util.toJSON(cListaObjetos));
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);			
		}
	}	

	@Post
	@Path("/comunicacao/materiais")
	public void adicionaObjeto(String token, String checkList) {
		
//		System.out.println(checkList);
		
		if (token != null && tokenService.isValido(token)) {
			
			Token tokenAgente = tokenService.find(new Token(token));			
			List<CObjeto> cListaObjeto = Json.importaArray(CObjeto.class, checkList);
			List<Objeto> listaObjetoAgente = null;

			Veiculo veiculo = new Veiculo();
			Agente agente = new Agente();
			
			if (tokenAgente.getVeiculo_id() != null && tokenAgente.getVeiculo_id() > 0 ) {
				
				veiculo = veiculoService.find(new Veiculo(tokenAgente.getAgente_id()));
				listaObjetoAgente = veiculo.getListaObjetos();
				
			} else if (tokenAgente.getAgente_id() != null && tokenAgente.getAgente_id() > 0 ) {
				
				agente = agenteService.find(new Agente(tokenAgente.getAgente_id()));
				listaObjetoAgente = agente.getListaObjetos();
			}
			
			Boolean registrado = false;
			
			if(listaObjetoAgente == null){
				listaObjetoAgente = new ArrayList<Objeto>();
			}
			
			for (CObjeto cObjeto : cListaObjeto) {
												
				for (Objeto objeto : listaObjetoAgente) {
					
					if (objeto.getId().equals(cObjeto.getId())) {
						registrado = true;
					}
				}
				if (!registrado) {					
					listaObjetoAgente.add(objetoService.find(new Objeto(cObjeto.getId())));
				}
			}

			if (tokenAgente.getVeiculo_id() != null && tokenAgente.getVeiculo_id() > 0 ) {
				
				veiculo.setListaObjetos(listaObjetoAgente);
				veiculoService.update(veiculo);
				
			} else if (tokenAgente.getAgente_id() != null && tokenAgente.getAgente_id() > 0 ) {
				
				agente.setListaObjetos(listaObjetoAgente);
				agenteService.update(agente);
			}
			
			cListaObjeto = new ArrayList<CObjeto>();
			
			for (Objeto objeto : listaObjetoAgente) {
				
				cListaObjeto.add(new CObjeto(objeto));
			}
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(Json.export("result", cListaObjeto));
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);			
		}
	}		
	
	@Post
	@Path("/comunicacao/localizamaterial")
	public void registrarMaterialOcorrencia(String token, String localizaMaterial) {
		
		System.out.println(localizaMaterial);
		Token tokenAgente = tokenService.find(new Token(token));
		CLocalizaMaterial ocorrenciaMateriais = (CLocalizaMaterial) Json.importa(CLocalizaMaterial.class, localizaMaterial);

		List<Objeto> listaObjetoAgente = getListaMateriaisToken(tokenAgente);
		
		for (CObjeto cObjeto : ocorrenciaMateriais.getMateriais()) {

			for (int i = 0; i < listaObjetoAgente.size(); i++) {
				
				if (listaObjetoAgente.get(i).getId().equals(cObjeto.getId())) {
					listaObjetoAgente.remove(i);
					break;
				}
			}
		}

		setListaMateriaisToken(tokenAgente, listaObjetoAgente);
		
		if (token != null && tokenService.isValido(token)) {

			result.use(Results.http()).setStatusCode(200);			

		} else {
			
			result.use(Results.http()).setStatusCode(401);			
		}
	}
	
	private List<Objeto> getListaMateriaisToken(Token token) {
		
		List<Objeto> listaObjetoAgente = null;
		
		Veiculo veiculo = new Veiculo();
		Agente agente = new Agente();
		
		if (token.getVeiculo_id() != null && token.getVeiculo_id() > 0 ) {
			
			veiculo = veiculoService.find(new Veiculo(token.getAgente_id()));
			listaObjetoAgente = veiculo.getListaObjetos();
			
		} else if (token.getAgente_id() != null && token.getAgente_id() > 0 ) {
			
			agente = agenteService.find(new Agente(token.getAgente_id()));
			listaObjetoAgente = agente.getListaObjetos();
		}
				
		if(listaObjetoAgente == null ){
			listaObjetoAgente = new ArrayList<Objeto>();
		}
		
		return listaObjetoAgente;
	}

	private void setListaMateriaisToken(Token token, List<Objeto> listaObjetoAgente) {

		Veiculo veiculo = new Veiculo();
		Agente agente = new Agente();
		
		if (token.getVeiculo_id() != null && token.getVeiculo_id() > 0 ) {
			
			veiculo = veiculoService.find(new Veiculo(token.getAgente_id()));
			veiculo.setListaObjetos(listaObjetoAgente);
			veiculoService.update(veiculo);
			
		} else if (token.getAgente_id() != null && token.getAgente_id() > 0 ) {
			
			agente = agenteService.find(new Agente(token.getAgente_id()));
			agente.setListaObjetos(listaObjetoAgente);
			agenteService.update(agente);
		}
	}
}