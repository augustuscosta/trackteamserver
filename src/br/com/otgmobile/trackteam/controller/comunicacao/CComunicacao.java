package br.com.otgmobile.trackteam.controller.comunicacao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.exception.SalvarArquivoException;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaAssinatura;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaImagem;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaVideo;
import br.com.otgmobile.trackteam.modal.poligono.Point2Df;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.HistoricoService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.tasks.SocketIOTask;
import br.com.otgmobile.trackteam.util.ConversaoFfmpeg;
import br.com.otgmobile.trackteam.util.GeoReferencial;
import br.com.otgmobile.trackteam.util.Util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class CComunicacao {

	private String pastaArquivos = null;
	private final Integer RAIO = 100;
	
	final private Result result;
	private final HttpServletResponse response;
	private final TokenService tokenService;
	private final HistoricoService historicoService;
	private final VeiculoService veiculoService;
	private final OcorrenciaService ocorrenciaService;
	private final AgenteService agenteService;
	private final SocketIOTask socketIOTask;
	
	public CComunicacao(Result result, HttpServletResponse response,
			TokenService tokenService, HistoricoService historicoService,
			VeiculoService veiculoService,
			OcorrenciaService ocorrenciaService,
			AgenteService agenteService,
			SocketIOTask socketIOTask) {

		this.result = result;
		this.tokenService = tokenService;
		this.response = response;
		this.historicoService = historicoService;
		this.veiculoService = veiculoService;
		this.ocorrenciaService = ocorrenciaService;
		this.agenteService = agenteService;
		this.socketIOTask = socketIOTask;
		ResourceBundle resourceConfig = java.util.ResourceBundle.getBundle("config");			
		pastaArquivos = resourceConfig.getString("salvarImgVideo");
	}

	@Post
	@Path("/comunicacao/historico")
	public void adicionarHistorico(String historico, String token) {

		if (historico != null && !"".equals(historico)
				&& tokenService.isValido(token)) {

			// Antigo funcional - salva no banco assim que recebe

			Token tokenBD = new Token(token);
			tokenBD.setToken(token);
			tokenBD = tokenService.find(tokenBD);

			Gson gson = new Gson();
			CHistorico cHistorico = gson.fromJson(historico, CHistorico.class);

			Historico novoHistorico = cHistorico.parseTo();

			if (tokenBD.getVeiculo_id() != null) {

				novoHistorico.setVeiculo_id(tokenBD.getVeiculo_id());
				Veiculo veiculo = veiculoService.find(new Veiculo(tokenBD.getVeiculo_id()));
				veiculo.setLatitude(novoHistorico.getLatitude());
				veiculo.setLongitude(novoHistorico.getLongitude());
				veiculoService.update(veiculo);

			}

			if (tokenBD.getAgente_id() != null) {

				novoHistorico.setAgente_id(tokenBD.getAgente_id());
			}

			novoHistorico = historicoService.insert(novoHistorico);

			result.use(Results.http()).body(historico);

		} else {

			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Post
	@Path("/comunicacao/historicos")
	public void adicionarHistoricos(String historico, String token) {
		
//		System.out.println(historico);
		
		if (historico != null && tokenService.isValido(token)) {

			Token tokenBD = new Token(token);
			tokenBD.setToken(token);
			tokenBD = tokenService.find(tokenBD);

			List<CHistorico> ch = Util.fromGsonArray(CHistorico.class, historico); 
			CHistorico cHistorico = new CHistorico();

			// Processa toda a lista e formata um cHistorico para ser atualizado via SocketIO
			
			for (CHistorico hist : ch) {
				
				if (!hist.getLatitude().equals("0.0") && !hist.getLongitude().equals("0.0")) {
					
					cHistorico.setLatitude(hist.getLatitude());
					cHistorico.setLongitude(hist.getLongitude());		
					cHistorico.setDataHora(hist.getDataHora());
				}					
				
				if (hist.getRpm() != null && !hist.getRpm().equals("")) {
					
					cHistorico.setRpm(hist.getRpm());
				}
				
				if (hist.getTemperatura() != null && !hist.getTemperatura().equals("")) {
					
					cHistorico.setTemperatura(hist.getTemperatura());
				}
				
				if (hist.getTensao_bateria() != null && !hist.getTensao_bateria().equals("")) {
					
					cHistorico.setTensao_bateria(hist.getTensao_bateria());
				}

				if (hist.getVelocidade() != null && !hist.getVelocidade().equals("")) {
					
					cHistorico.setVelocidade(hist.getVelocidade());
				}
			}
							
			if(cHistorico.getLatitude() != null && cHistorico.getLongitude() != null &&
					!cHistorico.getLatitude().equals("0.0") && !cHistorico.getLongitude().equals("0.0")){			

				if (tokenBD.getVeiculo_id() != null) {
					cHistorico.setVeiculo_id(tokenBD.getVeiculo_id());
					Veiculo veiculo = veiculoService.find(new Veiculo(tokenBD.getVeiculo_id()));
					veiculo.setLatitude(cHistorico.getLatitude());
					veiculo.setLongitude(cHistorico.getLongitude());
					veiculoService.update(veiculo);
				}
				
				if (tokenBD.getAgente_id() != null) {
					
					cHistorico.setAgente_id(tokenBD.getAgente_id());
					Agente agente = agenteService.find(new Agente(tokenBD.getAgente_id()));
					agente.setLatitude(cHistorico.getLatitude());
					agente.setLongitude(cHistorico.getLongitude());
					agente.setUltimaAtualizacao(new Timestamp(cHistorico.getDataHora()));
					agenteService.update(agente);
				}
				
				historicoService.enviaPosicaoSocketIO(cHistorico);
			}		
		
			// Persiste no banco todo o histórico
			
			Ocorrencia ocorrencia = ocorrenciaService.findOcorrenciaEmAtendimentoByToken(tokenBD);
			Point2Df centro = new Point2Df();

			if (ocorrencia != null) {
				
				centro.x = Float.parseFloat(ocorrencia.getEndereco().getLatitude());
				centro.y = Float.parseFloat(ocorrencia.getEndereco().getLongitude());
			}
			
			Boolean chegou = false;

			for (CHistorico hist : ch) {
				
				Historico novoHistorico = hist.parseTo();

				if (tokenBD.getVeiculo_id() != null) {
					novoHistorico.setVeiculo_id(tokenBD.getVeiculo_id());
				}

				if (tokenBD.getAgente_id() != null) {
					novoHistorico.setAgente_id(tokenBD.getAgente_id());
				}

				novoHistorico = historicoService.insert(novoHistorico);
				
				if(!novoHistorico.getLatitude().equals("0.0") && !novoHistorico.getLongitude().equals("0.0") && ocorrencia != null && !chegou){			

					Point2Df ponto = new Point2Df();
					ponto.x = Float.parseFloat(novoHistorico.getLatitude());
					ponto.y = Float.parseFloat(novoHistorico.getLongitude());
					
					if (GeoReferencial.estaNoRaio(centro, RAIO, ponto)) {
						
						ocorrencia.setHoraChegada(novoHistorico.getDataHora());
						ocorrenciaService.update(ocorrencia);
						chegou = true;
					}
				}
			}
			
			result.use(Results.http()).body("");
			result.use(Results.http()).setStatusCode(200);
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Post
	@Path("/comunicacao/token/veiculo/")
	public void adicionarHistorico(Integer veiculo_id) {

		Token token = tokenService.findByVeiculo_id(veiculo_id);

		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(token);
		JsonObject jo = new JsonObject();
		jo.add("result", je);

		response.setContentType("application/json; charset=UTF-8");
		result.use(Results.http()).body(jo.toString());
	}

	@Post
	@Path("/comunicacao/token/agente/")
	public void recuperaTokenAgente(Integer agente_id) {

		Token token = tokenService.findByAgente_id(agente_id);

		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(token);
		JsonObject jo = new JsonObject();
		jo.add("result", je);

		response.setContentType("application/json; charset=UTF-8");
		result.use(Results.http()).body(jo.toString());
	}
	
	@Post
	@Path("comunicacao/ocorrencia/fotos")
	public void uploadImagens(String ocorrencia, String token, UploadedFile foto) {
		//response.setContentType("application/json; charset=UTF-8");
		Date today = new Date();
		Long nomeDoArquivo = today.getTime();
		
		try {
			
			Util.salvarImagemVideo(ocorrencia, pastaArquivos, nomeDoArquivo.toString(), foto);
			
			Ocorrencia ocorrenciaDB = ocorrenciaService.findOcorrencia(new Ocorrencia(Integer.parseInt(ocorrencia)));			
			OcorrenciaImagem imagem = new OcorrenciaImagem();
			imagem.setNome(nomeDoArquivo.toString());
			ocorrenciaDB.getListaImagens().add(imagem);
			ocorrenciaService.update(ocorrenciaDB);
//			ocorrenciaService.enviaNotificacao(ocorrenciaDB, "ocorrencia/change");

			
			result.use(Results.http()).body("");
			result.use(Results.http()).setStatusCode(200);
			
		} catch (SalvarArquivoException e) {

			result.use(Results.http()).body("");
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	
	@Post
	@Path("comunicacao/ocorrencia/assinaturas")
	public void uploadImagensAssinaturas(String ocorrencia, String token, UploadedFile sign) {
		//response.setContentType("application/json; charset=UTF-8");
		Date today = new Date();
		Long nomeDoArquivo = today.getTime();
		
		try {
			
			Util.salvarAssinatura(ocorrencia, pastaArquivos, nomeDoArquivo.toString(), sign);
			
			Ocorrencia ocorrenciaDB = ocorrenciaService.findOcorrencia(new Ocorrencia(Integer.parseInt(ocorrencia)));			
			OcorrenciaAssinatura imagem = new OcorrenciaAssinatura();
			imagem.setNome(nomeDoArquivo.toString());
			ocorrenciaDB.getListaAssinaturas().add(imagem);
			ocorrenciaService.update(ocorrenciaDB);
//			ocorrenciaService.enviaNotificacao(ocorrenciaDB, "ocorrencia/change");

			
			result.use(Results.http()).body("");
			result.use(Results.http()).setStatusCode(200);
			
		} catch (SalvarArquivoException e) {

			result.use(Results.http()).body("");
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	
//	private static Logger logger = Logger.getLogger("name");
	@Post
	@Path("comunicacao/ocorrencia/filmagens")
	public void uploadVideos(String ocorrencia, String token, UploadedFile filmagem) {
		//response.setContentType("application/json; charset=UTF-8");
		Date today = new Date();
		Long nomeDoArquivo = today.getTime();
				
		try {
			
			Util.salvarImagemVideo(ocorrencia, pastaArquivos, nomeDoArquivo.toString(), filmagem);
			
			Ocorrencia ocorrenciaDB = ocorrenciaService.findOcorrencia(new Ocorrencia(Integer.parseInt(ocorrencia)));			
			OcorrenciaVideo video = new OcorrenciaVideo();
			video.setNome(nomeDoArquivo.toString());
			ocorrenciaDB.getListaVideos().add(video);
			ocorrenciaService.update(ocorrenciaDB);
//			ocorrenciaService.enviaNotificacao(ocorrenciaDB, "ocorrencia/change");
			
			//código exec para gerar o webm
			
			
			StringBuilder stb = new StringBuilder("ffmpeg -i ");
			stb.append(pastaArquivos);
			stb.append(ocorrencia);
			stb.append("/");
			stb.append(nomeDoArquivo.toString());
			stb.append(" -s 720x480 -aspect 4:3 -vb 500000 ");
			stb.append(pastaArquivos);
			stb.append(ocorrencia);
			stb.append("/");
			stb.append(nomeDoArquivo.toString());
			stb.append(".webm");
			
			System.out.println(stb.toString());
			
			//Boolean isDeucerto =
					ConversaoFfmpeg.executaComandoExec(stb.toString());
			
			/*
			 * cria o thumbler
			 * mas por enquanto não é preciso
			if (isDeucerto){
				StringBuilder stb2 = new StringBuilder("ffmpeg -i ");
				stb2.append(pastaArquivos);
				stb2.append(ocorrencia);
				stb2.append("/");
				stb2.append(nomeDoArquivo.toString());
				stb2.append(" -an -ss 00:00:04 -an -r 1 -s qcif -vframes 1 -y ");
				stb2.append(pastaArquivos);
				stb2.append(ocorrencia);
				stb2.append("/thb_");
				stb2.append(nomeDoArquivo.toString());
				stb2.append(".jpg");
				ConversãoFfmpeg.executaComandoExec(stb2.toString());
				System.out.println(stb2.toString());
			}
			*/
			
			result.use(Results.http()).setStatusCode(200);
			result.use(Results.http()).body("");
			
		} catch (SalvarArquivoException e) {

			result.use(Results.http()).setStatusCode(401);
			result.use(Results.http()).body("");
//			e.printStackTrace();
		}
	}

	@Post
	@Path("/comunicacao/token/")
	public void adicionarHistorico(String token) {

		Token tokenBD = new Token(token);
		tokenBD = tokenService.find(tokenBD);

		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(tokenBD);
		JsonObject jo = new JsonObject();
		jo.add("result", je);

		response.setContentType("application/json; charset=UTF-8");
		result.use(Results.http()).body(jo.toString());
	}

	@Post
	@Path("/device/camera/foto")
	public void tirarFotoDevice(String token, UploadedFile foto) {
	
		if (token == null) {

			result.use(Results.http()).setStatusCode(401);

		} else {
			
			Token tokenBD = new Token(token);
			tokenBD = tokenService.find(tokenBD);
			
			try {
				
				Util.salvarImagemVideo("snapShot", pastaArquivos, tokenBD.getAgente_id().toString(), foto);
				socketIOTask.enviaMsg("camera/show/browser", tokenBD);
				
			} catch (SalvarArquivoException e) {

			}

			response.setContentType("application/json; charset=UTF-8");
			result.use(Results.http()).body("");
		}
	}
}
