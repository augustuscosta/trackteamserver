package br.com.otgmobile.trackteam.controller.comunicacao;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackteam.service.TokenService;

@Resource
public class CVideoController {
	

	private Result result;
	private TokenService tokenService;
	public CVideoController(Result result, TokenService tokenService) {

		this.result = result;
		this.tokenService = tokenService;		
	}
	
	
	@Post
	@Path("comunicacao/video/params")
	public void open(String ocorrencia, String token){
		result.nothing();
	}

}
