package br.com.otgmobile.trackteam.controller.comunicacao;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CDominio;
import br.com.otgmobile.trackteam.modal.comunicacao.CNivelEmergencia;
import br.com.otgmobile.trackteam.modal.comunicacao.CObjeto;
import br.com.otgmobile.trackteam.modal.comunicacao.COcorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaAgente;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaApoio;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.ObjetoService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.util.Util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

@Resource
public class COcorrenciaController {

	private Result result;
	private OcorrenciaService ocorrenciaService;
	private TokenService tokenService;
	private final VeiculoService veiculoService;
	private final HttpServletResponse response;
	private final AgenteService agenteService;
	private final LogService logService;	
	private final ObjetoService objetoService;

	public COcorrenciaController(Result result, 
			OcorrenciaService ocorrenciaService, 
			TokenService tokenService,
			VeiculoService veiculoService,
			HttpServletResponse response,
			AgenteService agenteService,
			LogService logService,
			ObjetoService objetoService) {
		
		this.result = result;
		this.ocorrenciaService = ocorrenciaService;
		this.tokenService = tokenService;
		this.veiculoService = veiculoService;
		this.response = response;
		this.agenteService = agenteService;
		this.logService = logService;
		this.objetoService = objetoService;
	}
	
	@Get
	@Path("/comunicacao/ocorrencia/{id}/{token}")
	public void listar(Integer id, String token) {
		
		if (id != null && id > 0 && token != null && tokenService.isValido(token)) {
			
			Ocorrencia ocorrencia = new Ocorrencia();
			ocorrencia.setId(id);
			
			ocorrencia = ocorrenciaService.findOcorrencia(ocorrencia);
			COcorrencia cOcorrencia = new COcorrencia();
			cOcorrencia.parseFrom(ocorrencia);
			
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(cOcorrencia);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Get
	@Path("/comunicacao/ocorrencia/{token}")
	public void listarTodos(String token) {
		
		if (token != null && tokenService.isValido(token)) {

			Token tokenDispositivo = new Token(token); 
			tokenDispositivo = tokenService.find(tokenDispositivo);
			
			List<Ocorrencia> listaOcorrencia = ocorrenciaService.findOcorrenciasAtivasByToken(tokenDispositivo);
			List<COcorrencia> clistaOcorrencia = new ArrayList<COcorrencia>();
			
			for (Ocorrencia ocorrencia : listaOcorrencia) {
				
				COcorrencia cOcorrencia = new COcorrencia();
				cOcorrencia.parseFrom(ocorrencia);
				
				cOcorrencia.setAgentes(null);
				cOcorrencia.setVeiculos(null);
				
				clistaOcorrencia.add(cOcorrencia);
			}

			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(clistaOcorrencia);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Get
	@Path("/comunicacao/ocorrencia/niveisemergencia/{token}")
	public void listarNiveisEmergencia(String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			List<NiveisEmergencia> listaNiveisEmergencia = ocorrenciaService.findNivelEmergencia();
			List<CNivelEmergencia> clistaNiveisEmergencia = new ArrayList<CNivelEmergencia>();
			
			for (NiveisEmergencia nivelEmergencia : listaNiveisEmergencia) {
				
				CNivelEmergencia cNivelEmergencia = new CNivelEmergencia();
				cNivelEmergencia.parseFrom(nivelEmergencia);
				
				clistaNiveisEmergencia.add(cNivelEmergencia);
			}

			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(clistaNiveisEmergencia);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Get
	@Path("/comunicacao/ocorrencia/status/{token}")
	public void listarStatus(String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			List<Dominio> listaStatus = ocorrenciaService.findStatus();
			List<CDominio> cListaStatus = new ArrayList<CDominio>();
			
			for (Dominio status : listaStatus) {
				
				CDominio cStatus = new CDominio();
				cStatus.parseFrom(status);
				
				cListaStatus.add(cStatus);
			}

			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(cListaStatus);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	@Get
	@Path("/comunicacao/ocorrencia/natureza/{token}")
	public void listarNatureza(String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			List<Dominio> listaNatureza = ocorrenciaService.findNatureza();
			List<CDominio> cListaNatureza = new ArrayList<CDominio>();
			
			for (Dominio natureza : listaNatureza) {
				
				CDominio cStatus = new CDominio();
				cStatus.parseFrom(natureza);
				
				cListaNatureza.add(cStatus);
			}

			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(cListaNatureza);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	private Boolean isOcorrenciaValida(Ocorrencia ocorrencia) {
		
		if (ocorrencia == null 
				|| ocorrencia.getNiveisEmergencia() == null 
				|| ocorrencia.getNiveisEmergencia().getId() == null
				|| ocorrencia.getNiveisEmergencia().getId() <= 0
				|| ocorrencia.getOcorrenciasStatus() == null
				|| ocorrencia.getNatureza1() == null) {
			
			return false;
			
		} else {
			
			return true;	
		}				
	}

	@Post
	@Path("/comunicacao/ocorrencia/observacao")
	public void observacao(Integer ocorrencia, String texto, String token) {
		
		if (ocorrencia != null && ocorrencia > 0 && texto != null && !"".equals(texto) && tokenService.isValido(token)) {
			
			Token tokenDispositivo = new Token(token); 
			tokenDispositivo = tokenService.find(tokenDispositivo);
			
			Ocorrencia ocorrenciaBD = new Ocorrencia();
			ocorrenciaBD = ocorrenciaService.findOcorrencia(ocorrenciaBD);
			
			OcorrenciaApoio ocorrenciaApoio = new OcorrenciaApoio();
			ocorrenciaApoio.setData(new Timestamp(new Date().getTime()));
			ocorrenciaApoio.setUsuario_id(tokenDispositivo.getAgente_id());
			ocorrenciaApoio.setTexto(texto);
			
			List<OcorrenciaApoio> listaApoio = ocorrenciaBD.getComentarios();
			
			listaApoio.add(ocorrenciaApoio);
			
			ocorrenciaService.update(ocorrenciaBD);		
			ocorrenciaService.enviaNotificacao(ocorrenciaBD, "ocorrencia/change");
			
			logService.gravaLog("Atividade", "updateDevice", "Adicionado comentário a atividade no. " + ocorrencia + " pelo agente id " + tokenDispositivo.getAgente_id(), ocorrenciaBD, null);
			
			result.use(Results.http()).setStatusCode(200);
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
			
		}
	}
	
	@Post
	@Path("/comunicacao/ocorrencia/materiais")
	public void associarObjetos(String objetos, String ocorrencia, String token) {
			
		if (objetos != null && !"".equals(objetos) &&
				ocorrencia != null && !"".equals(ocorrencia) &&
				token != null && tokenService.isValido(token)) {
		
			Token tokenObj = new Token(token);
			tokenObj = tokenService.find(tokenObj);
						
			Veiculo veiculo = new Veiculo();
			veiculo.setId(tokenObj.getVeiculo_id());
			veiculo = veiculoService.find(veiculo);
			
			Gson gson = new Gson();
			
			Type collectionType = new TypeToken<List<CObjeto>>(){}.getType();
			List<CObjeto> listaCObjetos = gson.fromJson(objetos, collectionType);
			List<Objeto> listaObjetos = new ArrayList<Objeto>();
	
			COcorrencia cOcorrencia = gson.fromJson(ocorrencia, COcorrencia.class);
			Ocorrencia ocorrenciaObj = cOcorrencia.parseTo();
			ocorrenciaObj = ocorrenciaService.findOcorrencia(ocorrenciaObj);
			
			for(CObjeto cObjeto : listaCObjetos) {
				
				for (Objeto objeto : veiculo.getListaObjetos()) {
					
					if (objeto.getId().equals(cObjeto.getId())) {
						
						listaObjetos.add(objeto);				
					}
				}
			}
			
			List<Objeto> listaObjetosOcorrencia = ocorrenciaObj.getListaObjetos();
			List<Objeto> listaObjetosVeiculo = veiculo.getListaObjetos();
			
			for (Objeto objeto : listaObjetos) {
				
				listaObjetosOcorrencia.add(objeto);
				listaObjetosVeiculo.remove(objeto);				
			}
			
			veiculo.setListaObjetos(listaObjetosVeiculo);
			ocorrenciaObj.setListaObjetos(listaObjetosOcorrencia);
			
			veiculoService.update(veiculo);
			ocorrenciaService.update(ocorrenciaObj);
			ocorrenciaService.enviaNotificacao(ocorrenciaObj, "ocorrencia/change");


			cOcorrencia.parseFrom(ocorrenciaObj);
			JsonElement je = gson.toJsonTree(cOcorrencia);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
		}
	}
	
	@Post
	@Path("/comunicacao/ocorrencia")
	public void adicionar(String ocorrencia, String token) {
		
		if (ocorrencia != null && !"".equals(ocorrencia) && token != null && tokenService.isValido(token)) {
						
			Gson gson = new Gson();
			COcorrencia cOcorrencia = gson.fromJson(ocorrencia, COcorrencia.class);
			Ocorrencia novaOcorrencia = cOcorrencia.parseTo();
			
			if (isOcorrenciaValida(novaOcorrencia)) {
				
				novaOcorrencia = ocorrenciaService.insert(novaOcorrencia);

				ocorrenciaService.enviaNotificacao(novaOcorrencia, "ocorrencia/browser/add", null);
				logService.gravaLog("Atividade", "novaOcorrenciaDevice", "Adicionada nova atividade pelo agente id " + novaOcorrencia.getId(), new COcorrencia(novaOcorrencia), null);

				cOcorrencia = new COcorrencia();
				cOcorrencia.parseFrom(novaOcorrencia);
				
				JsonElement je = gson.toJsonTree(cOcorrencia);
			    JsonObject jo = new JsonObject();
			    jo.add("result", je);
				
		        response.setContentType("application/json; charset=UTF-8");  			
				result.use(Results.http()).body(jo.toString());
				
			} else {
			
				result.use(Results.http()).setStatusCode(400);
			}
			
		} else {
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Put
	@Path("/comunicacao/ocorrencia")
	public void alterar(String ocorrencia, String token) {
		
		if (ocorrencia != null && !"".equals(ocorrencia) && tokenService.isValido(token)) {
		    			
			Token tokenObj = new Token(token);
			tokenObj = tokenService.find(tokenObj);

			Agente agente = new Agente();
			agente.setId(tokenObj.getAgente_id());
			agente = agenteService.find(agente);
			
			ocorrencia = Util.decodeFromDevice(ocorrencia);			
		    
			Gson gson = new Gson();
			COcorrencia cOcorrencia = gson.fromJson(ocorrencia, COcorrencia.class);
			Ocorrencia ocorrenciaTablet = cOcorrencia.parseTo();
			
			if (ocorrenciaTablet.getId() == null || ocorrenciaTablet.getId() < 0) {
			
				if (isOcorrenciaValida(ocorrenciaTablet)) {
					
					ocorrenciaTablet.setUsuarioCriacao(agente.getUsuario());										
					
					if (tokenObj.getAgente_id() != null && tokenObj.getAgente_id() > 0) {
				
						OcorrenciaAgente ocorrenciaAgente = new OcorrenciaAgente();
						ocorrenciaAgente.setAgente(agente);
						
						List<OcorrenciaAgente> listaAgentes = new ArrayList<OcorrenciaAgente>();
						listaAgentes.add(ocorrenciaAgente);					
						ocorrenciaTablet.setOcorrenciaAgentes(listaAgentes);
					}
					
					List<Objeto> listaObjetos = new ArrayList<Objeto>();
					
					for (Objeto objeto : ocorrenciaTablet.getListaObjetos()) {
						
						listaObjetos.add(objetoService.find(objeto));
					}
					
					ocorrenciaTablet.setListaObjetos(listaObjetos);
					Ocorrencia novaOcorrencia = ocorrenciaService.insert(ocorrenciaTablet);
					
					ocorrenciaService.enviaNotificacao(novaOcorrencia, "ocorrencia/browser/add", null);
					logService.gravaLog("Atividade", "novaOcorrenciaDevice", "Adicionada nova atividade pelo agente id " + novaOcorrencia.getId(), new COcorrencia(novaOcorrencia), null);
					
					cOcorrencia = new COcorrencia();
					cOcorrencia.parseFrom(novaOcorrencia);
					cOcorrencia.setValidada(false);
					
					JsonElement je = gson.toJsonTree(cOcorrencia);
				    JsonObject jo = new JsonObject();
				    jo.add("result", je);
					
			        response.setContentType("application/json; charset=UTF-8");
					result.use(Results.http()).body(jo.toString());					
				} else {
				
					result.use(Results.http()).setStatusCode(400);
				}
			
//			} else if (ocorrenciaTablet.getId() != null && ocorrenciaTablet.getRecebimento() != null && !ocorrenciaTablet.getRecebimento().equals(0)) {
			} else if (ocorrenciaTablet.getId() != null) {				
							
				Ocorrencia ocorrenciaBanco = ocorrenciaService.findOcorrencia(ocorrenciaTablet);
				
				COcorrencia ocorrenciaOld = new COcorrencia(ocorrenciaBanco);
				
				Ocorrencia ocorrenciaAntiga = new Ocorrencia();
				ocorrenciaAntiga.setOcorrenciasVeiculos(ocorrenciaBanco.getOcorrenciasVeiculos());
				ocorrenciaAntiga.setOcorrenciaAgentes(ocorrenciaBanco.getOcorrenciaAgentes());
				ocorrenciaAntiga.setFlag(ocorrenciaBanco.getFlag());
				
				List<OcorrenciaApoio> listaComentarios = ocorrenciaBanco.getComentarios();										
				
				for (OcorrenciaApoio comentario : ocorrenciaTablet.getComentarios()) {
					
					if (comentario.getId() == null || comentario.getId() <= 0) {						
						
						comentario.setUsuario_id(agente.getUsuario().getId());
						comentario.setData(new Timestamp(new Date().getTime()));
						listaComentarios.add(comentario);
					}
				}

				List<Objeto> listaObjetos = new ArrayList<Objeto>();
				
				for (Objeto objeto : ocorrenciaTablet.getListaObjetos()) {
					
					listaObjetos.add(objetoService.find(objeto));
				}
				
				ocorrenciaBanco.setListaObjetos(listaObjetos);
				ocorrenciaBanco.setComentarios(listaComentarios);
				ocorrenciaBanco.setListaVeiculosEnvolvidos(ocorrenciaTablet.getListaVeiculosEnvolvidos());
				ocorrenciaBanco.setInicioProgramado(ocorrenciaTablet.getInicioProgramado());
				ocorrenciaBanco.setFimProgramado(ocorrenciaTablet.getFimProgramado());
				ocorrenciaBanco.setRecebimento(ocorrenciaTablet.getRecebimento());
				ocorrenciaBanco.setHoraSaida(ocorrenciaTablet.getHoraSaida());
				ocorrenciaBanco.setInicio(ocorrenciaTablet.getInicio());
				ocorrenciaBanco.setFim(ocorrenciaTablet.getFim());

				ocorrenciaBanco = ocorrenciaService.update(ocorrenciaBanco);

				ocorrenciaService.notificaAlteracaoOcorrencia(ocorrenciaBanco, ocorrenciaAntiga);
				ocorrenciaService.enviaNotificacao(ocorrenciaBanco, "ocorrencia/browser/change", null);
				
				cOcorrencia = new COcorrencia();
				cOcorrencia.parseFrom(ocorrenciaBanco);
				
				logService.gravaLog("Atividade", "changeOcorrenciaDevice", "Alterada atividade " + ocorrenciaBanco.getId() + " pelo agente id " + agente.getId(), cOcorrencia, ocorrenciaOld);
				
				JsonElement je = gson.toJsonTree(cOcorrencia);
			    JsonObject jo = new JsonObject();
			    jo.add("result", je);
			    		
		        response.setContentType("application/json; charset=UTF-8");  			
				result.use(Results.http()).body(jo.toString());
						
			} 
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}
}
