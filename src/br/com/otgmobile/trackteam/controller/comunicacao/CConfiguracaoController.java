package br.com.otgmobile.trackteam.controller.comunicacao;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.modal.Configuracao;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.service.ConfiguracaoService;
import br.com.otgmobile.trackteam.service.TokenService;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class CConfiguracaoController {

	final private Result result;
	private final TokenService tokenService;
	private final ConfiguracaoService configuracaoService;
	private final HttpServletResponse response;

	public CConfiguracaoController(Result result, 
			TokenService tokenService,
			ConfiguracaoService configuracaoService,
			HttpServletResponse response) {

		this.result = result;
		this.tokenService = tokenService;
		this.configuracaoService = configuracaoService;
		this.response = response;
	}
	
	@Get 
	@Path("/comunicacao/configuracao/{token}")
	public void listar(String token) {
		
		if (tokenService.isValido(token)) {
					
			Token tokenBD = new Token();
			tokenBD.setToken(token);
			tokenBD = tokenService.find(tokenBD);

			List<Configuracao> listaConfiguracao = configuracaoService.findAll(tokenBD);
			
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(listaConfiguracao);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
		    System.out.println(jo.toString());
		    
			response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());			
		}
	}
}
