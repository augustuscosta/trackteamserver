package br.com.otgmobile.trackteam.controller.comunicacao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.comunicacao.CAgente;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.service.AgenteService;
import br.com.otgmobile.trackteam.service.TokenService;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class CAgenteController {

	final private Result result;
	final private AgenteService agenteService;
	private final TokenService tokenService;
	private final HttpServletResponse response;
	
	public CAgenteController(Result result, 
			AgenteService agenteService, 
			TokenService tokenService,
			HttpServletResponse response) {

		this.result = result;
		this.agenteService = agenteService;
		this.tokenService = tokenService;
		this.response = response;
		
	}

	@Get
	@Path("/comunicacao/agente/{token}")
	public void listarTodos(String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			Token tokenDispositivo = new Token(token); 
			tokenDispositivo = tokenService.find(tokenDispositivo);
			
			List<Agente> listaAgente = agenteService.findAllByToken(tokenDispositivo);
			List<CAgente> clistaAgente = new ArrayList<CAgente>();
			
			for (Agente agente : listaAgente) {
				
				CAgente cAgente = new CAgente();
				cAgente.parseFrom(agente);
				
				clistaAgente.add(cAgente);
			}
			
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(clistaAgente);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());			
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	@Post
	@Path("/comunicacao/agente/historico")
	public void adicionarHistorico(String historico, String token) {
		
		if (historico != null && !"".equals(historico) && token != null && tokenService.isValido(token)) {
			
			Gson gson = new Gson();
			CHistorico cHistorico = gson.fromJson(historico, CHistorico.class);
			Historico novoHistorico = cHistorico.parseTo();
//			novoHistorico = agenteService.insertHistorico(novoHistorico);

			JsonElement je = gson.toJsonTree(novoHistorico);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
						
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}	
}
