package br.com.otgmobile.trackteam.controller.comunicacao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Rota;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CCerca;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.modal.comunicacao.CRota;
import br.com.otgmobile.trackteam.modal.comunicacao.CVeiculo;
import br.com.otgmobile.trackteam.modal.ocorrencia.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.service.CercaService;
import br.com.otgmobile.trackteam.service.RotaService;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.service.VeiculoEnvolvidoService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.tasks.SocketIOTask;
import br.com.otgmobile.trackteam.util.Json;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class CVeiculoController {

	private Result result;
	private VeiculoService veiculoService;
	private CercaService cercaService;
	private RotaService rotaService;
	private TokenService tokenService;
	private final VeiculoEnvolvidoService veiculoEnvolvidoService;
	private final HttpServletResponse response;
	
	public CVeiculoController(Result result, 
			VeiculoService veiculoService, 
			CercaService cercaService, 
			RotaService rotaService,
			TokenService tokenService,
			VeiculoEnvolvidoService veiculoEnvolvidoService,
			HttpServletResponse response,
			SocketIOTask socketIOTask) {
		
		this.result = result;
		this.veiculoService = veiculoService;
		this.cercaService = cercaService;
		this.rotaService = rotaService;
		this.tokenService = tokenService;
		this.veiculoEnvolvidoService = veiculoEnvolvidoService;
		this.response = response;

	}

	@Get
	@Path("/comunicacao/veiculo/{id}/{token}")
	public void listar(Integer id, String token) {
		
		if (id != null && id > 0 && token != null && tokenService.isValido(token)) {
						
			Veiculo veiculo = new Veiculo();
			veiculo.setId(id);
			
			veiculo = veiculoService.find(veiculo);

			CVeiculo cVeiculo = new CVeiculo();
			cVeiculo.parseFrom(veiculo);

			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(cVeiculo);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Get
	@Path("/comunicacao/veiculo/{token}")
	public void listarTodos(String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			Token tokenDispositivo = new Token(token); 
			tokenDispositivo = tokenService.find(tokenDispositivo);
			
			List<Veiculo> listaVeiculo = veiculoService.findAllByToken(tokenDispositivo);
			List<CVeiculo> clistaVeiculo = new ArrayList<CVeiculo>();
			
			for (Veiculo veiculo : listaVeiculo) {
				
				CVeiculo cVeiculo = new CVeiculo();
				cVeiculo.parseFrom(veiculo);
				
				clistaVeiculo.add(cVeiculo);
			}
			
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(clistaVeiculo);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());			
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Get
	@Path({"/comunicacao/veiculo/historico/{token}", "/comunicacao/historico/{token}"})
	public void listaVeiculosHistorico(String token) {

		if (token != null && tokenService.isValido(token)) {
			
			Token tokenDispositivo = new Token(token); 
			tokenDispositivo = tokenService.find(tokenDispositivo);
			
			List<Historico> listaHistorico = veiculoService.findAllPosicaoByToken(tokenDispositivo);
			List<CHistorico> cListaHistorico = new ArrayList<CHistorico>();
			
			for(Historico historico : listaHistorico) {
				
				if (historico.getLatitude() != null &&
						!"".equals(historico.getLatitude()) &&
						historico.getLongitude() != null &&
						!"".equals(historico.getLongitude()) &&
						historico.getVeiculo_id() != null) {
					
					CHistorico cHistorico = new CHistorico();
					cHistorico.parseFrom(historico);
					cListaHistorico.add(cHistorico);
				}
			}	
			
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(cListaHistorico);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
	
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
			result.use(Results.http()).setStatusCode(200);

		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	@Get
	@Path("/comunicacao/veiculo/cerca/{id}/{token}")
	public void listarCerca(Integer id, String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			Veiculo veiculo = new Veiculo();
			veiculo.setId(id);
			
			veiculo = veiculoService.find(veiculo);

			if (veiculo == null || veiculo.getId() == null || veiculo.getId() <= 0) {

				result.use(Results.http()).setStatusCode(400);

			} else {			
			
				CCerca cCerca = new CCerca();
				cCerca.parseFrom(veiculo.getCerca());
				
				Gson gson = new Gson();
				JsonElement je = gson.toJsonTree(cCerca);
			    JsonObject jo = new JsonObject();
			    jo.add("result", je);
				
		        response.setContentType("application/json; charset=UTF-8");  			
				result.use(Results.http()).body(jo.toString());			
			}
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	@Get
	@Path("/comunicacao/veiculo/cerca/{token}")
	public void listarCercas(Integer id, String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			Token tokenDispositivo = new Token(token); 
			tokenDispositivo = tokenService.find(tokenDispositivo);
			
			List<Cerca> cercas = cercaService.findAllByToken(tokenDispositivo);
			Gson gson = new Gson();
			JsonObject jo = new JsonObject();
			JsonElement je;
			
			if (cercas.size() > 0 && cercas.get(0) != null) {
				
				List<CCerca> toReturn = new ArrayList<CCerca>();
				
				for(Cerca cerca : cercas){
					CCerca cCerca = new CCerca();
					cCerca.parseFrom(cerca);
					toReturn.add(cCerca);
				}			
				
				je = gson.toJsonTree(toReturn);
				
			} else {
				
				je = gson.toJsonTree(new ArrayList<CCerca>());
			}
			
			jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	@Get
	@Path("/comunicacao/veiculo/rota/{token}")
	public void listarRotas(Integer id, String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			Token tokenDispositivo = new Token(token); 
			tokenDispositivo = tokenService.find(tokenDispositivo);
			
			List<Rota> rotas = rotaService.findAllByToken(tokenDispositivo);
			
			List<CRota> toReturn = new ArrayList<CRota>();
			
			for(Rota rota : rotas){
				CRota cRota = new CRota();
				cRota.parseFrom(rota);
				toReturn.add(cRota);
			}
		
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(toReturn);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Get
	@Path("/comunicacao/veiculo/rota/{id}/{token}")
	public void listarRota(Integer id, String token) {
		
		if (token != null && tokenService.isValido(token)) {
			
			Veiculo veiculo = new Veiculo();
			veiculo.setId(id);
			
			veiculo = veiculoService.find(veiculo);
						
			if (veiculo == null || veiculo.getId() == null || veiculo.getId() <= 0) {

				result.use(Results.http()).setStatusCode(400);

			} else {
			
				CRota cRota = new CRota();
				cRota.parseFrom(veiculo.getRota());
				
				Gson gson = new Gson();
				JsonElement je = gson.toJsonTree(cRota);
			    JsonObject jo = new JsonObject();
			    jo.add("result", je);
				
		        response.setContentType("application/json; charset=UTF-8");  			
				result.use(Results.http()).body(jo.toString());			
			
			}
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Get
	@Path("/comunicacao/veiculoEnvolvido/{token}")
	public void listarTodosVeiculosEnvolvidos(String token) {
/*		
		if (token != null && tokenService.isValido(token)) {
			
			List<VeiculoEnvolvido> listaVeiculo = veiculoEnvolvidoService.findAll();
			List<CVeiculo> clistaVeiculo = new ArrayList<CVeiculo>();
			
			for (VeiculoEnvolvido veiculo : listaVeiculo) {
				
				CVeiculo cVeiculo = new CVeiculo();
				cVeiculo.parseFrom(veiculo);
				
				clistaVeiculo.add(cVeiculo);
			}
		
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(clistaVeiculo);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());			
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
*/
        response.setContentType("application/json; charset=UTF-8");  			
		result.use(Results.http()).body(Json.export("result", new ArrayList<VeiculoEnvolvido>()));	
	}
	
	@Deprecated
	@Post
	@Path("/comunicacao/veiculo")
	public void adicionar(String veiculo, String token) {
		
		if (veiculo != null && !"".equals(veiculo) && token != null && tokenService.isValido(token)) {
			
			Gson gson = new Gson();
			CVeiculo cVeiculo = gson.fromJson(veiculo, CVeiculo.class);
			Veiculo novoVeiculo = cVeiculo.parseTo();
			novoVeiculo = veiculoService.insert(novoVeiculo);

			JsonElement je = gson.toJsonTree(novoVeiculo);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}

	@Deprecated
	@Post
	@Path("/comunicacao/veiculo/historico")
	public void adicionarHistorico(String historico, String token) {

		if (historico != null && !"".equals(historico) && tokenService.isValido(token)) {

// Antigo funcional
  			
			Token tokenBD = new Token(token);
			tokenBD.setToken(token);
			tokenBD = tokenService.find(tokenBD);
						
			Gson gson = new Gson();
			CHistorico cHistorico = gson.fromJson(historico, CHistorico.class);

			Historico novoHistorico = cHistorico.parseTo();			
			novoHistorico.setVeiculo_id(tokenBD.getVeiculo_id());
			
			novoHistorico = veiculoService.insertHistorico(novoHistorico);

			JsonElement je = gson.toJsonTree(novoHistorico);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
		
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	@Put
	@Path("/comunicacao/veiculo")
	public void alterar(String veiculo, String token) {
		
		if (veiculo != null && !"".equals(veiculo) && token != null && tokenService.isValido(token)) {
			
			Gson gson = new Gson();
			CVeiculo cVeiculo = gson.fromJson(veiculo, CVeiculo.class);
			Veiculo novoVeiculo = cVeiculo.parseTo();
			novoVeiculo = veiculoService.update(novoVeiculo);

			JsonElement je = gson.toJsonTree(novoVeiculo);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			
	        response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
						
		} else {
			
			result.use(Results.http()).setStatusCode(401);
		}
	}
	
	@Get
	@Path("/comunicacao/veiculoEnvolvido/{token}/{query}")
	public void listarByPlacaRenavam(String token, String query){
		
		if (token != null && tokenService.isValido(token)) {
			List<Veiculo> veiculos = veiculoService.findByPlacaRenavam(query != null ? query : "" );
			if(veiculos != null) {
			List<CVeiculo> clistaVeiculo = new ArrayList<CVeiculo>();
			
			for (Veiculo veiculo : veiculos) {
				
				CVeiculo cVeiculo = new CVeiculo();
				cVeiculo.parseFrom(veiculo);
				
				cVeiculo.setCerca(null);
				
				clistaVeiculo.add(cVeiculo);
			}
			
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(clistaVeiculo);
		    JsonObject jo = new JsonObject();
		    jo.add("result", je);
			response.setContentType("application/json; charset=UTF-8");  			
			result.use(Results.http()).body(jo.toString());
			
			System.out.println(jo.toString());
			
			}else{
				result.use(Results.http()).setStatusCode(401);
			}
		}
	}	
	
	@Delete
	@Path("/comunicacao/veiculo/{id}")
	public void delete(Integer id, String token) {
		
		if (id != null && id > 0 && token != null && tokenService.isValido(token)) {
			
			Veiculo veiculo = new Veiculo();
			veiculo.setId(id);
			
			veiculoService.excluirVeiculo(veiculo);

			result.nothing();
			
		} else {
			result.use(Results.http()).setStatusCode(401);
		}
	}
}
