package br.com.otgmobile.trackteam.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Enquete;
import br.com.otgmobile.trackteam.modal.Mensagem;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.comunicacao.CEnquete;
import br.com.otgmobile.trackteam.service.EnqueteService;
import br.com.otgmobile.trackteam.service.LogService;
import br.com.otgmobile.trackteam.service.MenuService;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Resource
public class EnqueteController {

	private Result result;
	private Validator validator;
	private EnqueteService enqueteService;
	private MenuService menuService;
	private Integer quantidade;
	private HttpServletResponse response;
	private final LogService logService;

	public EnqueteController(Result result, 
			Validator validator, 
			EnqueteService enqueteService, 
			MenuService menuService,
			HttpServletResponse response,
			LogService logService) {

		this.result = result;
		this.validator = validator;
		this.enqueteService = enqueteService;
		this.menuService= menuService;
		this.response = response;
		this.quantidade = 10;
		this.logService = logService;
	}

	@Get
	@Path("/controle_geral/enquetes/adicionar/")
	@MonkeySecurity(role="enquete")
	public void cadastro(Enquete enquete) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("cadastro-enquete");
		subMenu = menuService.findByTag(subMenu);
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);		
		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		result.include("enquete", enquete);
	}

	@Post 
	@Path("/controle_geral/enquetes/adicionar/")
	@MonkeySecurity(role="enquete")
	public void adicionar(Enquete enquete) {
		
		if (enquete == null) {
			
			result.use(Results.logic()).redirectTo(EnqueteController.class).cadastro(enquete);
		
		} else {
		
			if (enquete.getNome() == null || "".equals(enquete.getNome())) {
				
				result.include("erroNome", true);
				validator.add(new ValidationMessage("Nome inválido", "error"));
			}


			validator.onErrorUse(Results.logic()).redirectTo(EnqueteController.class).cadastro(enquete);
			
			if (enquete.getId() == null || enquete.getId() <= 0) {
				
				enquete = enqueteService.insert(enquete);
				logService.gravaLog("Enquete", "add", "Adicionado novo enquete", new CEnquete(enquete), null);
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Enquete adicionado com sucesso");
				result.include("avisos", mensagens);
				
			} else {
				
				Enquete enqueteDB = enqueteService.find(enquete);
				enqueteDB.setPerguntas(enquete.getPerguntas());
				enqueteDB.setNome(enquete.getNome());
				enqueteDB.setDescricao(enquete.getDescricao());
				enquete = enqueteService.update(enqueteDB);
				logService.gravaLog("Enquete", "change", "Alterado enquete id -> " + enqueteDB.getId(), new CEnquete(enqueteDB), new CEnquete(enquete));
				
				Mensagem mensagens = new Mensagem();
				mensagens.setTitulo("Enquete alterado com sucesso");
				result.include("avisos", mensagens);				
			}
			result.use(Results.logic()).redirectTo(EnqueteController.class).listar(0);	
		}
	}

	@Path("/controle_geral/enquetes/editar/")
	@MonkeySecurity(role="enquete")
	public void editar(Integer id) {
		
		Enquete enquete = new Enquete();

		if (id != null && id > 0) {
			
			enquete.setId(id);	
			enquete = enqueteService.find(enquete);
			
		} 

		result.use(Results.logic()).forwardTo(EnqueteController.class).cadastro(enquete);	
	}

	@Path("/controle_geral/enquetes/listar/")
	@MonkeySecurity(role="enquete")
	public void listar(Integer pagina) {

		SubMenu subMenu = new SubMenu();
		subMenu.setTag("listar-enquete");
		subMenu = menuService.findByTag(subMenu);
		result.include("subMenu", subMenu);

		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);	
		
	}
	
	@Path("/controle_geral/enquetes/listaEnquetes/")
	@MonkeySecurity(role="enquete")
	public void listaEnquetesTabela(Integer pagina) {

		if (pagina == null) {
			
			pagina = 0;
		}
		
		List<Enquete> listaEnquete = enqueteService.findAllPagina(quantidade, pagina);
		result.include("listaEnquete", listaEnquete);			
		
		Integer qnt = enqueteService.findTotal();
		Integer total = qnt / quantidade;
		
		if ((qnt % quantidade) > 0 ) {
			
			total++;
		}
		
		result.include("total", total);
		result.forwardTo("/json/enqueteTabela.jsp"); 
	}
	
	@MonkeySecurity(role="all")
	public void listaEnquetes() {
	
		List<Enquete> listaEnquete = enqueteService.findAll();
		List<CEnquete> cListaEnquete = new ArrayList<CEnquete>();
		
		for(Enquete enquete : listaEnquete) {
			
			CEnquete cPonto = new CEnquete();
			cPonto.parseFrom(enquete);
			cListaEnquete.add(cPonto);
		}	
		
		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(cListaEnquete);
	    JsonObject jo = new JsonObject();
	    jo.add("result", je);

        response.setContentType("application/json; charset=UTF-8");  
		result.use(Results.http()).body(jo.toString());
		
	}
	
	@Path("/controle_geral/enquetes/excluir/")
	@Post
	@MonkeySecurity(role="enquete")	
	public void excluir(Integer id) {
		
		Enquete enquete = new Enquete();
		enquete.setId(id);
		
		enquete = enqueteService.findDeletarAgente(enquete);
		logService.gravaLog("Enquete", "delete", "Deletado enquete id -> " + enquete.getId());		

		result.include("status", "ok");		
		result.include("id", id);
		result.include("msgSucesso", "Enquete excluido com sucesso");			
		result.forwardTo("/json/resposta.jsp");
		
	}
	

	
}
