package br.com.otgmobile.trackteam.controller;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioRole;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.UsuarioService;

@Resource
public class ControleOpcoesController {

	private Result result;
	private MenuService menuService;
	private UsuarioService usuarioService;
	
	public ControleOpcoesController(Result result, MenuService menuService, UsuarioService usuarioService) {

		this.result = result;
		this.menuService= menuService;	
		this.usuarioService = usuarioService;
	}
	
	@Path("/controle_geral/opcoes/menu")
	@MonkeySecurity(role="menu")
	public void menu() {
		
		SubMenu subMenu = new SubMenu();
		subMenu.setTag("menu");
		subMenu = menuService.findByTag(subMenu);
				
		List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
		result.include("listaSubMenu",listaSubMenu);
		result.include("subMenu", subMenu);		

		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);			

		List<UsuarioRole> listaRoles = usuarioService.findRoles();
		result.include("listaRoles", listaRoles);
		
	}
	
	@Path("/controle_geral/opcoes/menu/adicionar/")
	@MonkeySecurity(role="menu")
	public void menuAdicionar(Menu menu) {
		
		menu.setIcon("images/icones/edit.png");
	
		result.include("status", "ok");
		result.include("id", 10);		
		result.forwardTo("/json/resposta.jsp"); 
	}
}
