package br.com.otgmobile.trackteam.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import br.com.otgmobile.trackteam.modal.MailJava;
import br.com.otgmobile.trackteam.service.MailJavaService;
 
public class RecuperarSenhaController {
	
    public static void main(String[] args) {
        RecuperarSenhaController rs = new RecuperarSenhaController();
        rs.enviaEmail("teste@gmail.com", "loginTeste", "senhaTeste");
    }
    
    public void enviaEmail(String email, String login, String senha){
    	MailJava mj = new MailJava();

        mj.setSmtpHostMail("smtp.gmail.com");
        mj.setSmtpPortMail("587");
        mj.setSmtpAuth("true");
        mj.setSmtpStarttls("true");
        mj.setUserMail("teste@gmail.com");
        mj.setFromNameMail("Teste");
        mj.setPassMail("teste123");
        mj.setCharsetMail("UTF-8");
        mj.setSubjectMail("Solicitação para reenvio de senha.");
        mj.setBodyMail(htmlMessage(login, senha));
        mj.setTypeTextMail(MailJava.TYPE_TEXT_HTML);
 
        Map<String, String> map = new HashMap<String, String>();
        map.put(email, email);
        mj.setToMailsUsers(map);
 
        List<String> files = new ArrayList<String>();
        mj.setFileMails(files);
 
        try {
            new MailJavaService().senderMail(mj);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace(); 
        }
    }
 
 
	private static String htmlMessage(String login, String senha) {
        return 
			"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"+
			"<html>"+
			"	<head> </head>"+
			"	<body>"+
			"	<table align='center' width='500' cellpadding='0' cellspacing='0' >"+
			"		<tr>"+
			"			<td>"+ 
			"				<div style=\"width:242px; height:52px; margin: 50px auto 40px;\">"+
			"					<p style='padding: 8px 0 0 70px; margin: 0;'>"+login+"</p>"+
			"					<p style='padding: 6px 0 7px 70px; margin: 0;'>"+senha+"</p>"+
			"				</div>"+
			"			</td>"+
			"		</tr>"+
			"	</table>"+
			"	</body>"+
			"</html>";
    }
}
