package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.RegioesDAO;
import br.com.otgmobile.trackteam.modal.Regioes;
@Component
public class RegioesService {
	
	private RegioesDAO regioesDAO;
	
	
	public RegioesService(RegioesDAO regioesDAO) {		
		this.regioesDAO = regioesDAO;
	}


	public List<Regioes> findAllPagina(Integer quantidade, Integer pagina) {

		return regioesDAO.findAllPagina(quantidade, pagina);
	}


	public Integer findTotal() {
		
		return regioesDAO.findTotal();
	}


	public Regioes find(Regioes regioes) {
		
		return regioesDAO.find(regioes);
	}
	
	public List<Regioes> findAll() {
		
		return regioesDAO.findAll();
	}


	public Regioes update(Regioes regioes) {
		return regioesDAO.update(regioes);
		
	}	

	public boolean existe(Regioes regioes) {
		
		return regioesDAO.existe(regioes);
	}


	public Regioes insert(Regioes regioes) {		
		return regioesDAO.insert(regioes);
	}


	public Regioes findRegioesExcluir(Regioes regioes) {
		
		return regioesDAO.findRegioesExcluir(regioes);
	}


	public Regioes findRegioesId(Integer id) {
		
		return regioesDAO.findRegioesId(id);
	}
	
	public Regioes findRegioesNome(Regioes regioes){
		
		return regioesDAO.findRegioesNome(regioes);
		
	}

}
