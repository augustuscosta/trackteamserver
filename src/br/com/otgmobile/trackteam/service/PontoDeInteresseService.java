package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.PontoDeInteresseDAO;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.PontoDeInteresse;

@Component
public class PontoDeInteresseService {

	private PontoDeInteresseDAO pontoDeInteresseDAO;

	public PontoDeInteresseService(PontoDeInteresseDAO agenteDAO) {
		this.pontoDeInteresseDAO = agenteDAO;
	}

	public List<PontoDeInteresse> findAll() {
		return pontoDeInteresseDAO.findAll();
	}

	public PontoDeInteresse find(PontoDeInteresse ponto) {
		return pontoDeInteresseDAO.find(ponto);
	}
	
	public PontoDeInteresse findByCodigo(PontoDeInteresse ponto) {
		return pontoDeInteresseDAO.findByCodigo(ponto);
	}

	public List<PontoDeInteresse> findAllPagina(Integer quantidade,
			Integer pagina) {

		return pontoDeInteresseDAO.findAllPagina(quantidade, pagina);
	}

	public Integer findTotal() {

		return pontoDeInteresseDAO.findTotal();
	}

	public PontoDeInteresse update(PontoDeInteresse ponto) {
		ponto = pontoDeInteresseDAO.update(ponto);
		return ponto;
	}

	public PontoDeInteresse insert(PontoDeInteresse ponto) {

		return pontoDeInteresseDAO.insert(ponto);
	}

	public PontoDeInteresse findDeletarAgente(PontoDeInteresse ponto) {
		return pontoDeInteresseDAO.findDeletarPontoDeInteresse(ponto);

	}
	
	public List<Objeto> findAllObjetosAssociadosPagina(Integer quantidade,
			Integer pagina, PontoDeInteresse ponto) {
		
		return pontoDeInteresseDAO.findAllObjetosAssociadosPagina(quantidade,pagina,ponto);
	}

	public Integer findTotalObjetosAssociados(PontoDeInteresse ponto) {
		
		return pontoDeInteresseDAO.findTotalObjetosAssociados(ponto);
	}

}
