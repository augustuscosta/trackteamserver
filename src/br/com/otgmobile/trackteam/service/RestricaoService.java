package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.RestricaoDAO;
import br.com.otgmobile.trackteam.modal.restricoes.PessoaRestricao;
import br.com.otgmobile.trackteam.modal.restricoes.VeiculoRestricao;

@Component
public class RestricaoService {

	private final RestricaoDAO restricaoDAO;
	
	public RestricaoService(RestricaoDAO restricaoDAO) {
		
		this.restricaoDAO = restricaoDAO;
	}
	
	public VeiculoRestricao insertVeiculo(VeiculoRestricao veiculo) {
		
		return restricaoDAO.insertVeiculo(veiculo);
	}	
	
	public PessoaRestricao insertPessoa(PessoaRestricao pessoa) {
		
		return restricaoDAO.insertPessoa(pessoa);
	}	
	
	public List<PessoaRestricao> findPessoa(String query) {
		
		return restricaoDAO.findPessoa(query);
	}

	public List<VeiculoRestricao> findVeiculo(String placa, String chassi, String renavam) {

		return restricaoDAO.findVeiculo(placa, chassi, renavam);
	}	
}
