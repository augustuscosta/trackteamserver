package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.TokenDAO;
import br.com.otgmobile.trackteam.modal.Token;

@Component
public class TokenService {

	private TokenDAO tokenDAO;
	
	public TokenService(TokenDAO tokenDAO) {
		
		this.tokenDAO = tokenDAO;
	}
	
	public Token getId(Token token) {
		
		token = tokenDAO.find(token);
		
		return token;
	}

	public Token find(Token token) {
		
		return tokenDAO.find(token);
	}
	
	public Boolean isValido(String token) {
	
		if (token != null) {
			
			Token tokenBD = new Token(token);	
			tokenBD = this.find(tokenBD);
			
			if (tokenBD != null && 
					tokenBD.getId() != null && 
					tokenBD.getId() > 0 && 
					(tokenBD.isInativo() == null || 
					!tokenBD.isInativo())) {
				
				return true;
				
			} 
		}
		
		return false;
	}

	/**
	 * Remove um token do banco
	 * 
	 * @param token
	 * @return
	 */
	public void limpa(Token token) {
		
		tokenDAO.limpa(token);
	}
	
	/**
	 * Insere um token no banco
	 * 
	 * @param token
	 * @return
	 */
	public Token insert(Token token) {
		
		return tokenDAO.insert(token);
	}

	public Token findByVeiculo_id(Integer id) {
		
		return tokenDAO.findByVeiculo_id(id);
	}

	public Token findByAgente_id(Integer id) {

		return tokenDAO.findByAgente_id(id);
	}

	public Token update(Token token) {
		
		return tokenDAO.update(token);
	}

	public List<Token> findAll() {
		
		return tokenDAO.findAll(new Token());
	}
}
