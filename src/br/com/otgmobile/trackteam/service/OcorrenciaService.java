package br.com.otgmobile.trackteam.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.OcorrenciaDao;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Endereco;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.COcorrencia;
import br.com.otgmobile.trackteam.modal.comunicacao.CVeiculo;
import br.com.otgmobile.trackteam.modal.node.Node;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaAgente;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciasVeiculo;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.tasks.SocketIOTask;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

@Component
public class OcorrenciaService {

	private OcorrenciaDao ocorrenciaDao;
	private SocketIOTask socketIOTask;
	private NiveisEmergenciaService niveisEmergenciaService;
	private VeiculoService veiculoService;
	private final TokenService tokenService;
	
	public OcorrenciaService(OcorrenciaDao ocorrenciaDao, SocketIOTask socketIOTask, NiveisEmergenciaService niveisEmergenciaService, 
			VeiculoService veiculoService,	TokenService tokenService) {
		
		this.ocorrenciaDao = ocorrenciaDao;
		this.socketIOTask = socketIOTask;
		this.niveisEmergenciaService = niveisEmergenciaService;
		this.veiculoService = veiculoService;
		this.tokenService = tokenService;

	}
	
	public List<Ocorrencia> findAllPagina(Integer quantidade, Integer pagina) {

		return ocorrenciaDao.findAllPagina(quantidade, pagina);
	}
	
	public List<Ocorrencia> findAllAtivaPagina(Integer quantidade, Integer pagina) {

		return ocorrenciaDao.findAllAtivaPagina(quantidade, pagina);
	}
	
	public Integer findTotal() {
	
		return ocorrenciaDao.findTotal();
	}
	
	public Integer findTotalAtiva() {
		
		return ocorrenciaDao.findTotalAtiva();
	}
	
	public Ocorrencia insert(Ocorrencia ocorrencia) {
	    
		if (ocorrencia == null ||
			ocorrencia.getEndereco() == null || 
			ocorrencia.getResumo() == null ||
			ocorrencia.getDescricao() == null || 
			ocorrencia.getNiveisEmergencia() == null ||
			ocorrencia.getNiveisEmergencia().getId() == null || 
			ocorrencia.getNiveisEmergencia().getId() <= 0 ||
			ocorrencia.getEndereco() == null || 
			ocorrencia.getEndereco().getEndGeoref() == null || 
			ocorrencia.getEndereco().getLatitude() == null || 
			ocorrencia.getEndereco().getLongitude() == null ||
			ocorrencia.getNatureza1() == null || 
			ocorrencia.getNatureza1().getId() == null ||
			ocorrencia.getOcorrenciasStatus() == null ||
			ocorrencia.getOcorrenciasStatus().getId() == null) {
			return null;
		}
		
		if (ocorrencia.getEndereco().getLogradouro() == null) {
			
			Endereco endereco = ocorrencia.getEndereco();
						
			if (StringUtils.contains(ocorrencia.getEndereco().getEndGeoref(), ",")) {				
				
				try {
					String[] temp = ocorrencia.getEndereco().getEndGeoref().split(",");
					String[] temp1 = temp[1].split(" - ");
					
					endereco.setLogradouro(temp[0].trim());
					endereco.setNumero(temp1[0].trim());
					
				} catch (Exception e) {
	
					endereco.setLogradouro(endereco.getEndGeoref());
					endereco.setNumero("S/N");
					ocorrencia.setEndereco(endereco);
					
//					e.printStackTrace();
				}
				
			} else {

				try {

					String[] enderecoSplit = StringUtils.split(ocorrencia.getEndereco().getEndGeoref(), "-");
					endereco.setLogradouro(enderecoSplit[0].trim());
					endereco.setNumero("S/N");	
					
				} catch (Exception e) {
					
					endereco.setLogradouro(endereco.getEndGeoref());
					endereco.setNumero("S/N");
					ocorrencia.setEndereco(endereco);
					
//					e.printStackTrace();
				}					
			}			
		}		

		if (ocorrencia.getUsuario() == null || ocorrencia.getUsuario().getId() == null || ocorrencia.getUsuario().getId() <= 0) {

			ocorrencia.setFlag(0);
			
		} else {
			
			ocorrencia.setFlag(100);
		}

		ocorrencia = ocorrenciaDao.insert(ocorrencia);
		
		ocorrencia.setNiveisEmergencia(niveisEmergenciaService.find(ocorrencia.getNiveisEmergencia()));
		ocorrencia.setNatureza1(ocorrenciaDao.findDominio(ocorrencia.getNatureza1().getId()));
		ocorrencia.setOcorrenciasStatus(ocorrenciaDao.findDominio(ocorrencia.getOcorrenciasStatus().getId()));
		ocorrencia.setOcorrenciasVeiculos(ocorrenciaDao.findOcorrenciaVeiculos(ocorrencia));
		
		for (OcorrenciasVeiculo ocorrenciasVeiculo : ocorrencia.getOcorrenciasVeiculos()) {
			
			ocorrenciasVeiculo.setVeiculo(veiculoService.find(ocorrenciasVeiculo.getVeiculo()));
		}
/*
		if (ocorrencia.getUsuario() == null || ocorrencia.getUsuario().getId() == null || ocorrencia.getUsuario().getId() <= 0) {
			
			enviaNotificacao(ocorrencia, "ocorrencia/addBrowser");
			
		} else {
			
			enviaNotificacao(ocorrencia, "ocorrencia/add");
		}
*/				
		return ocorrencia;
		
	}

	public Ocorrencia update(Ocorrencia ocorrencia) {

		if (ocorrencia == null) {
			
			return null;
		}
		
		ocorrencia = ocorrenciaDao.update(ocorrencia);
//		ocorrencia = ocorrenciaDao.findOcorrencia(ocorrencia);
		
//		enviaNotificacao(ocorrencia, "ocorrencia/change");
		
		return ocorrencia;
	}
	
	public List<Ocorrencia> findOcorrencias() {

		return ocorrenciaDao.findOcorrencias();
	}
	
	public List<Ocorrencia> findOcorrenciasAtivas() {
		
		return ocorrenciaDao.findOcorrenciasAtivas();
	}
	
	public List<Ocorrencia> findOcorrenciasAtivasByToken(Token token) {
		
		return ocorrenciaDao.findOcorrenciasAtivasByToken(token);
	}

	public Ocorrencia findOcorrenciaEmAtendimentoByToken(Token token) {
		
		return ocorrenciaDao.findOcorrenciaEmAtendimentoByToken(token);
	}
	
	public Ocorrencia findOcorrencia(Ocorrencia ocorrencia) {

		return ocorrenciaDao.findOcorrencia(ocorrencia);
	}
	
	public List<Ocorrencia> findOcorrenciaAll(Date horaCriacao, Date horaCriacaoFim) {
		return ocorrenciaDao.findOcorrenciaAll(horaCriacao, horaCriacaoFim);
	}
	
	public List<Ocorrencia> findOcorrenciaByVeiculo(Veiculo veiculo) {
		
		return ocorrenciaDao.findOcorrenciaByVeiculo(veiculo);
	}
	
	public List<Ocorrencia> findOcorrenciaAbertasFechadasByVeiculo(Veiculo veiculo, Date inicio, Date fim, String status, int nivel){
		return ocorrenciaDao.findOcorrenciaAbertasFechadasByVeiculo(veiculo, inicio, fim, status, nivel);
	}

	public List<Ocorrencia> findOcorrenciaByVeiculo(Veiculo veiculo, Date inicio, Date fim, int nivel) {
		
		return ocorrenciaDao.findOcorrenciaByVeiculo(veiculo, inicio, fim, nivel);
	}
	
	public List<Ocorrencia> findOcorrenciaByAtendnete(Veiculo veiculo, Date inicio, Date fim, int nivel) {
		return ocorrenciaDao.findOcorrenciaByVeiculo(veiculo, inicio, fim, nivel);
	}

	public List<Ocorrencia> findOcorrenciaByAgente(Agente agente) {
		return ocorrenciaDao.findOcorrenciaByAgente(agente);
	}
	
	public List<Ocorrencia> findOcorrenciaByAgente(Agente agente, Date inicio, Date fim, String status, int nivel) {
		return ocorrenciaDao.findOcorrenciaByAgente(agente, inicio, fim, status, nivel);
	}
	
	public List<Dominio> findStatus() {
		
		return ocorrenciaDao.findStatus();
	}
	
	public List<Dominio> findNatureza() {
		
		return ocorrenciaDao.findNatureza();
	}
	
	public List<NiveisEmergencia> findNivelEmergencia() {
		
		return ocorrenciaDao.findNivelEmergencia();
	}

	public List<Usuario> findUsuarios() {
		
		return ocorrenciaDao.findUsuarios();
	}
	
	public List<Dominio> findGravidade() {

		return ocorrenciaDao.findGravidade();
	}
	
	public List<Dominio> findVitimaTipo() {

		return ocorrenciaDao.findVitimaTipo();
	}
	
	public List<Dominio> findMeioCadastro() {

		return ocorrenciaDao.findMeioCadastro();
	}
	
	public Ocorrencia cancelaOcorrencia(Ocorrencia ocorrencia) {
		
		if (ocorrencia != null) {
			
			ocorrencia = ocorrenciaDao.cancelaOcorrencia(ocorrencia);

			notificaDeleteOcorrencia(ocorrencia);
		}
		
		return ocorrencia;
	}
	
	public Ocorrencia encerrarOcorrencia(Ocorrencia ocorrencia) {
	
		if (ocorrencia != null) {

			ocorrenciaDao.update(ocorrencia);

			notificaDeleteOcorrencia(ocorrencia);
		} 
		
		return ocorrencia;
	}
	
	public List<OcorrenciasVeiculo> findOcorrenciaVeiculos(Ocorrencia ocorrencia) {

		return ocorrenciaDao.findOcorrenciaVeiculos(ocorrencia);
	}
	
	public void enviaNotificacao(Ocorrencia ocorrencia, String metodo) {
		
		COcorrencia cOcorrencia = new COcorrencia();
		cOcorrencia.parseFrom(ocorrencia);
		
		JSONObject jsonNode;
		JSONObject jsonResult = new JSONObject();
		
		Node node = new Node();	
		List<String> listaTokens = new ArrayList<String>();
				
		try {
			
			node.setMensagem(cOcorrencia);
			
			for (CVeiculo veiculo : cOcorrencia.getVeiculos()) {
				
				Token tokenVeiculo = new Token();
				tokenVeiculo = tokenService.findByVeiculo_id(veiculo.getId());
				
				if (tokenVeiculo != null) {
					listaTokens.add(tokenVeiculo.getToken());
				}
			}
			
			
			
			node.setListaTokens(listaTokens);

			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(node);	    
			jsonNode = new JSONObject(je.toString());
			jsonResult.put("result", jsonNode);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		socketIOTask.enviaMsg(metodo, jsonResult);		
		
	}

	public void enviaNotificacao(Ocorrencia ocorrencia, String metodo, Set<String> listaTokens) {
		
		List<String> listaTokensList = new ArrayList<String>();

		if (listaTokens != null) {			
			listaTokensList.clear();
			listaTokensList.addAll(listaTokens);
		}
		
		COcorrencia cOcorrencia = new COcorrencia();
		cOcorrencia.parseFrom(ocorrencia);	
		
		JSONObject jsonNode;
		JSONObject jsonResult = new JSONObject();
		
		Node node = new Node();	
				
		try {
			
			node.setMensagem(cOcorrencia);		
			node.setListaTokens(listaTokensList);

			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(node);	    
			jsonNode = new JSONObject(je.toString());
			jsonResult.put("result", jsonNode);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		socketIOTask.enviaMsg(metodo, jsonResult);		
		
	}
	
	
	public void enviaNotificacao(Ocorrencia ocorrencia, String metodo, Set<String> listaTokens, List<String> camposModificados) {
		
				
		List<String> listaTokensList = new ArrayList<String>();

		if (listaTokens != null) {			
			listaTokensList.clear();
			listaTokensList.addAll(listaTokens);
		}
		
		COcorrencia cOcorrencia = new COcorrencia();
		cOcorrencia.parseFrom(ocorrencia);	
		cOcorrencia.setDadosAlterados(camposModificados);
		
		JSONObject jsonNode;
		JSONObject jsonResult = new JSONObject();
		
		Node node = new Node();	
				
		try {
			
			node.setMensagem(cOcorrencia);		
			node.setListaTokens(listaTokensList);

			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(node);	    
			jsonNode = new JSONObject(je.toString());
			jsonResult.put("result", jsonNode);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		socketIOTask.enviaMsg(metodo, jsonResult);		
		
	}
	
	public Integer findTotalPorVeiculo(Veiculo veiculo) {

		return ocorrenciaDao.findTotalPorVeiculo(veiculo);
	}

	public void notificaAlteracaoOcorrencia(Ocorrencia ocorrenciaNova, Ocorrencia ocorrenciaAntiga) {
  
		Set<String> adicionar = new HashSet<String>();
		Set<String> editar = new HashSet<String>();
		Set<String> apagar = new HashSet<String>();
		Token token = null;
		
		if (ocorrenciaNova == null || ocorrenciaNova.getFlag() == null || ocorrenciaNova.getFlag() < 100) {
			
			return;
		}
		
		if (ocorrenciaAntiga == null || ocorrenciaAntiga.getFlag() == null || ocorrenciaAntiga.getFlag() < 100) {
			
			if (ocorrenciaNova.getOcorrenciasVeiculos() != null) {
				
				for (OcorrenciasVeiculo ocorrenciasVeiculo : ocorrenciaNova.getOcorrenciasVeiculos()) {
					
					token = tokenService.findByVeiculo_id(ocorrenciasVeiculo.getVeiculo().getId());
					
					if (token != null) {
						adicionar.add(token.getAgente_id().toString());											
					}
				}
			}

			if (ocorrenciaNova.getOcorrenciaAgentes() != null) {
				
				for (OcorrenciaAgente ocorrenciaAgentes : ocorrenciaNova.getOcorrenciaAgentes()) {
					
					token = tokenService.findByAgente_id(ocorrenciaAgentes.getAgente().getId());
					
					if (token != null) {
						adicionar.add(token.getAgente_id().toString());											
					}
				}
			}
			
		} else {
			
			Boolean novaOcorrencia = true; 
			Boolean apagarOcorrencia = true;

// Novos ou ediatados			
			if (ocorrenciaNova.getOcorrenciasVeiculos() != null) {
				
				for (OcorrenciasVeiculo ocorrenciasVeiculo : ocorrenciaNova.getOcorrenciasVeiculos()) {
					
					token = tokenService.findByVeiculo_id(ocorrenciasVeiculo.getVeiculo().getId());
					novaOcorrencia = true;
					
					if (ocorrenciaAntiga.getOcorrenciasVeiculos() != null) {

						for (OcorrenciasVeiculo ocorrenciasVeiculoBD : ocorrenciaAntiga.getOcorrenciasVeiculos()) {
							
							if (ocorrenciasVeiculo.getVeiculo().getId().equals(ocorrenciasVeiculoBD.getVeiculo().getId())) {
								
								novaOcorrencia = false;
								
								if (token != null && token.getAgente_id() != null) {
									editar.add(token.getAgente_id().toString());
								}
							}
						}
					}
					
					if (novaOcorrencia) {
						
						if (token != null && token.getAgente_id() != null) {
							adicionar.add(token.getAgente_id().toString());
						}
					}
				}
			}

			if (ocorrenciaNova.getOcorrenciaAgentes() != null) {
				
				for (OcorrenciaAgente ocorrenciaAgente : ocorrenciaNova.getOcorrenciaAgentes()) {
					
					token = tokenService.findByAgente_id(ocorrenciaAgente.getAgente().getId());
					novaOcorrencia = true;
					
					if (ocorrenciaAntiga.getOcorrenciaAgentes() != null) {

						for (OcorrenciaAgente ocorrenciaAgenteBD : ocorrenciaAntiga.getOcorrenciaAgentes()) {
							
							if (ocorrenciaAgente.getAgente().getId().equals(ocorrenciaAgenteBD.getAgente().getId())) {
								
								novaOcorrencia = false;
								
								if (token != null && token.getAgente_id() != null) {
									editar.add(token.getAgente_id().toString());
								}
							}
						}
					}
					
					if (novaOcorrencia) {
						
						if (token != null && token.getAgente_id() != null) {
							adicionar.add(token.getAgente_id().toString());
						}
					}
				}
			}
			
// Excluidos			
			if (ocorrenciaAntiga.getOcorrenciasVeiculos() != null) {
					
				for (OcorrenciasVeiculo ocorrenciasVeiculoBD : ocorrenciaAntiga.getOcorrenciasVeiculos()) {
					
					apagarOcorrencia = true;
					
					if (ocorrenciaNova.getOcorrenciasVeiculos() != null) {

						for (OcorrenciasVeiculo ocorrenciasVeiculo : ocorrenciaNova.getOcorrenciasVeiculos()) {
		
							if (ocorrenciasVeiculo.getVeiculo().getId().equals(ocorrenciasVeiculoBD.getVeiculo().getId())) {
							
								apagarOcorrencia = false;
							}
						}
					}
					
					if (apagarOcorrencia) {
						
						token = tokenService.findByVeiculo_id(ocorrenciasVeiculoBD.getVeiculo().getId());
						
						if (token != null && token.getAgente_id() != null) {
							apagar.add(token.getAgente_id().toString());
						}
					}
				}		
			}	
			
			// para Agentes
			if (ocorrenciaAntiga.getOcorrenciaAgentes() != null) {
				
				for (OcorrenciaAgente ocorrenciaAgenteBD : ocorrenciaAntiga.getOcorrenciaAgentes()) {
					
					apagarOcorrencia = true;
					
					if (ocorrenciaNova.getOcorrenciaAgentes() != null) {

						for (OcorrenciaAgente ocorrenciasAgente : ocorrenciaNova.getOcorrenciaAgentes()) {
		
							if (ocorrenciasAgente.getAgente().getId().equals(ocorrenciaAgenteBD.getAgente().getId())) {
							
								apagarOcorrencia = false;
							}
						}
					}
					
					if (apagarOcorrencia) {
						
						token = tokenService.findByAgente_id(ocorrenciaAgenteBD.getAgente().getId());
						
						if (token != null && token.getAgente_id() != null) {
							apagar.add(token.getAgente_id().toString());
						}
					}
				}		
			}			
		}
		
// Processa envio		
		if (!apagar.isEmpty()) {
			this.enviaNotificacao(ocorrenciaNova, "ocorrencia/device/delete", apagar);
		}
		
		if (!adicionar.isEmpty()) {
			this.enviaNotificacao(ocorrenciaNova, "ocorrencia/device/add", adicionar);
		}
		
		if (!editar.isEmpty()) {
			this.enviaNotificacao(ocorrenciaNova, "ocorrencia/device/change", editar);
		}	

	}
	
	private void notificaDeleteOcorrencia(Ocorrencia ocorrencia) {
		
		Set<String> apagar = new HashSet<String>();
		Token token = null;	
			
		if (ocorrencia.getOcorrenciasVeiculos() != null) {
			
			for (OcorrenciasVeiculo ocorrenciasVeiculo : ocorrencia.getOcorrenciasVeiculos()) {
				
				token = tokenService.findByVeiculo_id(ocorrenciasVeiculo.getVeiculo().getId());
				
				if (token != null) {
					apagar.add(token.getAgente_id().toString());											
				}
			}
		}

		if (!apagar.isEmpty()) {
			this.enviaNotificacao(ocorrencia, "ocorrencia/device/delete", apagar);
		}					
	}
}
