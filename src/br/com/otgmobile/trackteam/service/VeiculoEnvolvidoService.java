package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.VeiculoEnvolvidoDAO;
import br.com.otgmobile.trackteam.modal.ocorrencia.VeiculoEnvolvido;

@Component
public class VeiculoEnvolvidoService {

	private VeiculoEnvolvidoDAO veiculoEnvolvidoDAO;
	
	public VeiculoEnvolvidoService(VeiculoEnvolvidoDAO veiculoEnvolvidoDAO) {
		
		this.veiculoEnvolvidoDAO = veiculoEnvolvidoDAO;
	}
	
	public List<VeiculoEnvolvido> findAll() {

		return veiculoEnvolvidoDAO.findAll();
	}
}
