package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.ConfiguracaoDAO;
import br.com.otgmobile.trackteam.modal.Configuracao;
import br.com.otgmobile.trackteam.modal.Token;

@Component
public class ConfiguracaoService {

	private final ConfiguracaoDAO configuracaoDAO;
	
	public ConfiguracaoService(ConfiguracaoDAO configuracaoDAO) {
		
		this.configuracaoDAO = configuracaoDAO;
	}
	
	public List<Configuracao> findAll(Token token) {
		
		return configuracaoDAO.findAll(token);
	}
}
