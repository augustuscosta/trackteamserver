package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.RotaDAO;
import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.Rota;
import br.com.otgmobile.trackteam.modal.Token;

@Component
public class RotaService {

	private RotaDAO rotaDAO;
	
	public RotaService(RotaDAO rotaDAO) {
		
		this.rotaDAO = rotaDAO;
	}
	
	public List<Rota> findAll() {
		
		return rotaDAO.findAll();
	}
	
	public Rota find(Rota rota) {
		
		return rotaDAO.find(rota);
	}

	/**
	 * Traz uma pagina de cercas cadastrados
	 * 
	 * @return
	 */
	public List<Rota> findAllPagina(Integer quantidade, Integer pagina) {

		return rotaDAO.findAllPagina(quantidade, pagina);
	}
	
	/**
	 * 
	 * Retorna a quantidade total de cercas cadastradas
	 * @return
	 */
	public Integer findTotal() {

		return rotaDAO.findTotal();
	}
	
	public Rota insert(Rota rota) {

		return rotaDAO.insert(rota);
	}

	public List<Rota> findAllByToken(Token token) {

		return rotaDAO.findAllByToken(token);
	}

	public Rota update(Rota rota) {

		return rotaDAO.update(rota);
	}
	
	public Rota excluiGeoPontos(Rota rota) {
		
		return rotaDAO.excluiGeoPontos(rota);
	}

	public List<Rota> findByCerca(Cerca cerca) {
		
		return rotaDAO.findByCerca(cerca);
	}
}
