package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.CercaDAO;
import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.Token;

@Component
public class CercaService {

	private CercaDAO cercaDAO;
	
	public CercaService(CercaDAO cercaDAO) {
		
		this.cercaDAO = cercaDAO;
	}
	
	public List<Cerca> findAll() {
		
		return cercaDAO.findAll();
	}

	public Cerca find(Cerca cerca) {

		return cercaDAO.find(cerca);
	}

	/**
	 * Retorna true se ja existe uma cerca com o mesmo nome
	 * 
	 * @param cerca
	 * @return
	 */
	public boolean existe(Cerca cerca) {

		return cercaDAO.existe(cerca);
	}
	
	/**
	 * Traz uma pagina de cercas cadastrados
	 * 
	 * @return
	 */
	public List<Cerca> findAllPagina(Integer quantidade, Integer pagina) {

		return cercaDAO.findAllPagina(quantidade, pagina);
	}
	
	/**
	 * 
	 * Retorna a quantidade total de cercas cadastradas
	 * @return
	 */
	public Integer findTotal() {

		return cercaDAO.findTotal();
	}
	
	public Cerca insert(Cerca cerca) {

		return cercaDAO.insert(cerca);
	}
	
	/**
	 * Atualiza a cerca
	 * 
	 * @param cerca
	 * @return
	 */
	public Cerca update(Cerca cerca) {

		return cercaDAO.update(cerca);
	}

	public List<Cerca> findAllByToken(Token token) {

		return cercaDAO.findAllByToken(token);
	}	
}
