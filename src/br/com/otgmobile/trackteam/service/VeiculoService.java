package br.com.otgmobile.trackteam.service;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.VeiculoDao;
import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Local;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.Rota;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.tasks.SocketIOTask;

@Component
public class VeiculoService {

	private VeiculoDao veiculoDao;
	private SocketIOTask socketIOTask;
	
	public VeiculoService(VeiculoDao veiculoDao, SocketIOTask socketIOTask) {
		
		this.veiculoDao = veiculoDao;
		this.socketIOTask = socketIOTask;
	}
	
	public List<Veiculo> findAll() {

		return veiculoDao.findAll();
	}

	/**
	 * Traz todos os veiculos cadastrados com a posicao
	 * 
	 * @return
	 */
	
	public List<Veiculo> findAllComHistorico() {
		
		return veiculoDao.findAllComHistorico();
	}
	
	public List<Historico> findAllPosicao() {
		
		return veiculoDao.findAllPosicao();
	}
	
	public List<Veiculo> findAllPagina(Integer quantidade, Integer pagina) {

		return veiculoDao.findAllPagina(quantidade, pagina);
	}

	public Integer findTotal() {

		return veiculoDao.findTotal();
	}
	
	public Veiculo find(Veiculo veiculo) {
	
		return veiculoDao.find(veiculo);
	}

	public Veiculo excluirVeiculo(Veiculo veiculo) {

		veiculo = this.find(veiculo);
		return veiculoDao.excluirVeiculo(veiculo);
	}
	
	public List<Dominio> findTipo() {

		return veiculoDao.findTipo();
	}
	
	public List<Local> findLocal() {

		return veiculoDao.findLocal();
	}

	public Veiculo update(Veiculo veiculo) {
	
		return veiculoDao.update(veiculo);
	}
		
	public Veiculo insert(Veiculo veiculo) {
		
		if (veiculo.getCerca() == null || veiculo.getCerca().getId() == null || veiculo.getCerca().getId() <= 0) {
			
			veiculo.setCerca(null);
		}

		if (veiculo.getRota() == null || veiculo.getRota().getId() == null || veiculo.getRota().getId() <= 0) {
			
			veiculo.setRota(null);
		}
	
		return veiculoDao.insert(veiculo);
	}
	
	public Historico insertHistorico(Historico historico) {
		
		historico = veiculoDao.insertHistorico(historico);
		
		CHistorico cHistorico = new CHistorico();
		cHistorico.parseFrom(historico);
		
		JSONObject json;
		JSONObject jsonResult = new JSONObject();
		json = new JSONObject(cHistorico);
		
		try {
			
			jsonResult.put("result", json);
			socketIOTask.enviaMsg("historico/change", jsonResult);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return historico;
	}
	
	
	
	public List<Objeto> findAllObjetosAssociadosPagina(Integer quantidade,
			Integer pagina,Veiculo veiculo) {
		
		return veiculoDao.findAllObjetosAssociadosPagina(quantidade, pagina, veiculo);
	}

	public Integer findTotalObjetosAssociados(Veiculo veiculo) {
		
		return veiculoDao.findTotalObjetosAssociados(veiculo);
	}

	public List<Veiculo> findAllByToken(Token token) {

		return veiculoDao.findAllByToken(token);
	}

	public List<Historico> findAllPosicaoByToken(Token token) {

		return veiculoDao.findAllPosicaoByToken(token);
	}

	public Veiculo findByIdGps(String id) {
		
		return veiculoDao.findByIdGps(id);
	}
	
	public List<Veiculo> findByPlacaRenavam(String query){
		return veiculoDao.findByPlacaRenavam(query);
	}

	public Cerca excluirCerca(Cerca cerca) {
		
		cerca = veiculoDao.findCerca(cerca);
		return veiculoDao.excluirCerca(cerca);
	}

	public Rota excluirRota(Rota rota) {
		
		rota = veiculoDao.findRota(rota);
		return veiculoDao.excluirRota(rota);
	}
	
}
