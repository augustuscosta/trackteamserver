package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.RespostasDAO;
import br.com.otgmobile.trackteam.modal.RespostaEnquete;



@Component
public class RespostaService {

	private RespostasDAO respostaDao;
	
	public RespostaService(RespostasDAO respostaDao) {
		
		this.respostaDao = respostaDao;
	}
	
	public List<RespostaEnquete> findAllPagina(Integer quantidade, Integer pagina) {

		return respostaDao.findAllPagina(quantidade, pagina);
	}
	
	public Integer findTotal() {
	
		return respostaDao.findTotal();
	}
	
	public RespostaEnquete insert(RespostaEnquete resposta) {

		return respostaDao.insert(resposta);
	}

	public RespostaEnquete update(RespostaEnquete resposta) {

		return respostaDao.update(resposta);
	}
	
	public RespostaEnquete delete(RespostaEnquete resposta) {

		return respostaDao.delete(resposta);
	}
	
	public List<RespostaEnquete> findRespostaEnquetes() {

		return respostaDao.findAll();
	}
	
	public RespostaEnquete findRespostaEnquete(RespostaEnquete resposta) {

		return respostaDao.find(resposta);
	}
	
}
