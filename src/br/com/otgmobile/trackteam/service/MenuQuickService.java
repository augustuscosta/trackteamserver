package br.com.otgmobile.trackteam.service;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.MenuQuick;

@Component
public class MenuQuickService {
	
	public List<MenuQuick> list() {
		
		List<MenuQuick> listaMenu = new ArrayList<MenuQuick>();
		MenuQuick menu;
				
		menu = new MenuQuick();
		menu.setIcon("images/icones/page_white_copy.png");
		menu.setNome("Estatísticas");
		menu.setLink("#");
		listaMenu.add(menu);
		
		menu = new MenuQuick();
		menu.setIcon("images/icones/page_white_copy.png");
		menu.setNome("Controle Geral");
		menu.setLink("#");
		listaMenu.add(menu);
		
		return listaMenu;
	}	

	public List<MenuQuick> listMenuControleGeral() {
		
		List<MenuQuick> listaMenu = new ArrayList<MenuQuick>();
		MenuQuick menu;
				
		menu = new MenuQuick();
		menu.setIcon("images/icones/page_white_copy.png");
		menu.setNome("Estatísticas");
		menu.setLink("#");
		listaMenu.add(menu);
		
		menu = new MenuQuick();
		menu.setIcon("images/icones/page_white_copy.png");
		menu.setNome("Controle Geral");
		menu.setLink("#");
		listaMenu.add(menu);
		
		return listaMenu;
	}
	
}
