package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.EnqueteDAO;
import br.com.otgmobile.trackteam.modal.Enquete;

@Component
public class EnqueteService {

	private EnqueteDAO enqueteDeInteresseDAO;

	public EnqueteService(EnqueteDAO agenteDAO) {
		this.enqueteDeInteresseDAO = agenteDAO;
	}

	public List<Enquete> findAll() {
		return enqueteDeInteresseDAO.findAll();
	}

	public Enquete find(Enquete enquete) {
		return enqueteDeInteresseDAO.find(enquete);
	}

	public List<Enquete> findAllPagina(Integer quantidade, Integer pagina) {
		return enqueteDeInteresseDAO.findAllPagina(quantidade, pagina);
	}

	public Integer findTotal() {
		return enqueteDeInteresseDAO.findTotal();
	}

	public Enquete update(Enquete enquete) {
		enquete = enqueteDeInteresseDAO.update(enquete);
		return enquete;
	}

	public Enquete insert(Enquete enquete) {
		return enqueteDeInteresseDAO.insert(enquete);
	}

	public Enquete findDeletarAgente(Enquete enquete) {
		return enqueteDeInteresseDAO.findDeletarEnquete(enquete);

	}

}
