package br.com.otgmobile.trackteam.service;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.NiveisEmergenciaDAO;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;

@Component
public class NiveisEmergenciaService {

	private NiveisEmergenciaDAO niveisEmergenciaDAO;
	
	public NiveisEmergenciaService(NiveisEmergenciaDAO niveisEmergenciaDAO) {
		
		this.niveisEmergenciaDAO = niveisEmergenciaDAO;
	}
	
	public NiveisEmergencia find(NiveisEmergencia niveisEmergencia) {
		
		return niveisEmergenciaDAO.find(niveisEmergencia);		
	}
}
