package br.com.otgmobile.trackteam.service;

import java.lang.reflect.Field;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.LogDAO;
import br.com.otgmobile.trackteam.modal.CompareResult;
import br.com.otgmobile.trackteam.modal.Tabela;
import br.com.otgmobile.trackteam.modal.Log;
import br.com.otgmobile.trackteam.modal.session.UsuarioSession;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.util.Json;

@Component
public class LogService {

	private final LogDAO logDAO;
	private final Integer QUANTIDADE = 20;
	private final UsuarioService usuarioService;
	private final UsuarioSession usuarioSession;

	public LogService(LogDAO logDAO,
			UsuarioService usuarioService,
			UsuarioSession usuarioSession) {
		
		this.logDAO = logDAO;
		this.usuarioService = usuarioService;
		this.usuarioSession = usuarioSession;
	}

	public Tabela<Log> findPagina(Integer pagina, String inicio, String fim, String filtro) {
		
		if (pagina == null) {
			pagina = 0;
		}

		Tabela<Log> listaModelos = new Tabela<Log>();
		listaModelos.setTabela(logDAO.findPagina(QUANTIDADE, pagina, inicio, fim, filtro));
		
		Integer paginas = logDAO.findPaginaTotal(inicio, fim, filtro);
		Integer total = paginas / QUANTIDADE;
		
		if ((paginas % QUANTIDADE) > 0 ) {
			
			total++;
		}		
		
		listaModelos.setTotal(total);		
		
		return listaModelos;
	}
	/**
	 * Insere log no banco
	 * 
	 * @param tipo
	 * @param descricao
	 */
	public void gravaLog(String controller, String tipo, String descricao) {
		
		gravaLog(controller, tipo, descricao, null, null);			
	}
	
	/**
	 * Insere log no banco
	 * 
	 * @param tipo
	 * @param descricao 
	 * @param objetoPai
	 * @param objetoFilho
	 */
	public void gravaLog(String controller, String tipo, String descricao, Object objetoPai, Object objetoFilho) {
				
		Log log = new Log();
		//log.setController(sun.reflect.Reflection.getCallerClass(2).getSimpleName());
		log.setController("controller");
		log.setTipo(tipo);
		log.setDescricao(descricao);
		log.setDataCriacao(new Date());	
		
		if (objetoFilho != null) {			
			log.setValorAntigo(Json.export(objetoFilho));
		}

		if (objetoPai != null) {			
			log.setValorNovo(Json.export(objetoPai));
		}
		
		if (usuarioSession != null && usuarioSession.getId() != null) {
			log.setUsuario(usuarioService.find(new Usuario(usuarioSession.getId())));
		}
		
		logDAO.insert(log);

	}

	public Log find(Log log) {		
		return logDAO.find(log);
	}	
}
