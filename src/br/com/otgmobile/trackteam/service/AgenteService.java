package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.AgenteDAO;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.Token;

@Component
public class AgenteService {

	private AgenteDAO agenteDAO;
	private TokenService tokenService;
	
	public AgenteService(AgenteDAO agenteDAO,
			TokenService tokenService) {
		
		this.agenteDAO = agenteDAO;
		this.tokenService = tokenService;
	}
	
	public List<Agente> findAll() {

		return agenteDAO.findAll();
	}

	public List<Historico> findAllPosicao() {
	
		return agenteDAO.findAllPosicao();
	}
	
	public Agente findByLogin(Agente agente) {
		
		return agenteDAO.findByLogin(agente);
	}
	
	public Agente findByMatricula(Agente agente) {
		
		return agenteDAO.findByMatricula(agente);
	}
	
	public Agente find(Agente agente) {

		return agenteDAO.find(agente);
	}

	public List<Agente> findAllPagina(Integer quantidade, Integer pagina) {

		return agenteDAO.findAllPagina(quantidade, pagina);
	}

	public Integer findTotal() {

		return agenteDAO.findTotal();
	}
	
	public Agente update(Agente agente) {
	
		agente = agenteDAO.update(agente);
		Token token = tokenService.findByAgente_id(agente.getId());
		
		if (agente.getVeiculo() != null && token != null && token.getVeiculo_id() != agente.getVeiculo().getId()) {
			
			token.setVeiculo_id(agente.getVeiculo().getId());
			tokenService.update(token);
		}
		
		return agente;
	}
	
	public Agente insert(Agente agente) {
		
		return agenteDAO.insert(agente);
	}
	
	public List<Dominio> findStatus() {

		return agenteDAO.findStatus();
	}

	public List<Agente> findAllByToken(Token tokenDispositivo) {
		
		return agenteDAO.findAll();
	}

	public Agente findDeletarAgente(Agente agente) {
		
		return agenteDAO.findDeletarAgente(agente);
		
	}

	public List<Objeto> findAllObjetosAssociadosPagina(Integer quantidade,
			Integer pagina, Agente agente) {
		
		return agenteDAO.findAllObjetosAssociadosPagina(quantidade,pagina,agente);
	}

	public Integer findTotalObjetosAssociados(Agente agente) {
		
		return agenteDAO.findTotalObjetosAssociados(agente);
	}

	public List<Agente> findNaCercaComHistorico(String latitude, String longitude) {
		
		return agenteDAO.findNaCercaComHistorico(latitude, longitude);
	}	
}
