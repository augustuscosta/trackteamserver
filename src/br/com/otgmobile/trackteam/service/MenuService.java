package br.com.otgmobile.trackteam.service;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.MenuDAO;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.session.UsuarioSession;

@Component
public class MenuService {

	private MenuDAO menuDAO;
	private final UsuarioSession usuarioSession;
	
	public MenuService(MenuDAO menuDAO, UsuarioSession usuarioSession) {
		
		this.menuDAO = menuDAO;
		this.usuarioSession = usuarioSession;		
	}

	public List<Menu> listMenu() {
		
		Menu menu = new Menu();
		menu.setTipo(0);
		
		List<Menu> listaMenu = menuDAO.findByTipo(menu);
		List<Menu> listaFinal = new ArrayList<Menu>();
		Boolean adicionar;
		
		for(Menu elem : listaMenu) {
			
//			adicionar = false;
			adicionar = true;
/*					
			if (elem.getRole().equals("all")) {
				
				adicionar = true;
				
			} else {
			
				for (String role : usuarioSession.getRole()) {
										
					if (elem.getRole().equals(role)) {
	
						adicionar = true;
					}
				}
			}
*/			
			if (adicionar) {
								
				Menu menuTmp = new Menu();
				menuTmp.setTipo(elem.getId());
				
				elem.setSubMenu(menuDAO.findByTipo(menuTmp));				
				listaFinal.add(elem);
			}
		}
		
		return listaFinal;
	}
	
	public List<Menu> listMenuControleGeral() {
		
		Menu menu = new Menu();
		menu.setTipo(4);
		
		List<Menu> listaMenu = menuDAO.findByTipo(menu);
		List<Menu> listaFinal = new ArrayList<Menu>();
		Boolean adicionar;
		
		for(Menu elem : listaMenu) {
			
			adicionar = false;
			
			for (String role : usuarioSession.getRole()) {
				
				if (elem.getRole().equals(role) || elem.getRole().equals("all")) {

					adicionar = true;
				}
			}
			
			if (adicionar) {
				
				listaFinal.add(elem);
			}
		}
		
		return listaFinal;
	}
	
	public List<SubMenu> listSubMenuControleGeral(Menu menu) {
				
		return filtraSubMenu(menuDAO.findSubMenuByMenu(menu));
	}	
	
	public List<SubMenu> listSubMenuControleGeral(SubMenu subMenu) {
		
		return filtraSubMenu(menuDAO.findSubMenuByTag(subMenu));
	}	
	
	public List<SubMenu> filtraSubMenu(List<SubMenu> listaMenu) {

		List<SubMenu> listaFinal = new ArrayList<SubMenu>();
		Boolean adicionar;
				
		for(SubMenu elem : listaMenu) {
			
//			adicionar = false;
			adicionar = true;
/*		
			for (String role : usuarioSession.getRole()) {
				
				if (elem.getRole().equals(role) || elem.getRole().equals("all")) {

					adicionar = true;
				}				
			}
*/
			if (adicionar && elem.getAtivo() != null && elem.getAtivo() == true) {
				
				listaFinal.add(elem);
			} 
		}
				
		return listaFinal;
	}

	public Menu findByTag(Menu menu) {

		return menuDAO.findByTag(menu);
	}
	
	public SubMenu findByTag(SubMenu subMenu) {
		
		return menuDAO.findByTag(subMenu);
	}
	
	public Menu update(Menu menu) {

		return menuDAO.update(menu);
	}
	
	public Menu insert(Menu menu) {

		return menuDAO.insert(menu);
	}

	public List<SubMenu> ordenaSubMenus(List<SubMenu> menu) {
/*
		for (SubMenu listaSubMenu : menu) {
			
			List<SubMenu> itensDesordenados = listaSubMenu.getListaSubMenu();
			List<SubMenu> itensOrdenados = new ArrayList<SubMenu>();
			
			while (itensDesordenados.size() > 0) {
				
				for (int i=0; i < itensOrdenados.size(); i++) {
					
					if (itensDesordenados.get(0).getPosicao() <= itensOrdenados.get(i).getPosicao()) {
						
					}
				}
			}
		}
		
		return null;
*/
		return menu;
	}
/*	
	public List<SubMenu> ordenaElementosSubMenus(SubMenu subMenu) {
		
		for (SubMenu elem : subMenu.get) {
			
		}
	}
*/	
}

