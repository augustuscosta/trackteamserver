package br.com.otgmobile.trackteam.service;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.EnderecoDao;
import br.com.otgmobile.trackteam.modal.Endereco;

@Component
public class EnderecoService {

	private EnderecoDao enderecoDao;
	
	public EnderecoService(EnderecoDao enderecoDao) {
		
		this.enderecoDao = enderecoDao;
	}
	
	public Endereco insert(Endereco endereco) {
		
		return enderecoDao.insert(endereco);		
	}
}
