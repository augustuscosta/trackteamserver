package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.ViaturaRotaDao;
import br.com.otgmobile.trackteam.modal.ViaturaRota;

@Component
public class ViaturaRotaService {

	private ViaturaRotaDao viaturaRotaDao;
	
	public ViaturaRotaService(ViaturaRotaDao dao) {
		
		this.viaturaRotaDao = dao;
	}
	
	public void salvar(ViaturaRota viatura) {
		
		viaturaRotaDao.salvar(viatura);
	}
	
	public List<ViaturaRota> findAll() {
		
		return viaturaRotaDao.findAll();
	}
}
