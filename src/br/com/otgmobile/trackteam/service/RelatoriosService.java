package br.com.otgmobile.trackteam.service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.view.Results;

@Component
public class RelatoriosService<E> {

	private Result result;
	private HttpServletResponse response;
	
	public RelatoriosService(Result result,
			HttpServletResponse response) {
		
		this.result = result;
		this.response = response;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void geraRelatorio(String relatorio, HashMap parametros, List<E> listaDataSource) {
		
		JasperPrint print;
		
		try {
			
			InputStream ipStream  = this.getClass().getClassLoader().getResourceAsStream("/relatorios/" + relatorio + ".jasper");
			print = JasperFillManager.fillReport(ipStream, parametros, new JRBeanCollectionDataSource(listaDataSource));
			
			response.setContentType("application/pdf");
			response.addHeader("Content-disposition","attachment; filename=\"" + relatorio + ".pdf\"");
			
			JasperExportManager.exportReportToPdfStream(print, response.getOutputStream());
			
		} catch (Exception e) {
			
			e.printStackTrace();
			result.use(Results.http()).setStatusCode(401);
		}		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void geraRelatorioEcxel(String relatorio, HashMap parametros, List<E> listaDataSource) {
		
		JasperPrint print;

		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition","attachment; filename=\"" + relatorio + ".xls\"");
		
		try {
			
			InputStream ipStream  = this.getClass().getClassLoader().getResourceAsStream("/relatorios/" + relatorio + ".jasper");
			print = JasperFillManager.fillReport(ipStream, parametros, new JRBeanCollectionDataSource(listaDataSource));
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());
			exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
			exporterXLS.exportReport();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			result.use(Results.http()).setStatusCode(401);
		}
	}	
}
