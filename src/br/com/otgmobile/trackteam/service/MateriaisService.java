package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.MateriaisDAO;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Materiais;



@Component
public class MateriaisService {

	private MateriaisDAO materiaisDao;
	
	public MateriaisService(MateriaisDAO materiaisDao) {
		
		this.materiaisDao = materiaisDao;
	}
	
	public List<Materiais> findByFuncionalidade(Dominio funcionalidade) {
		
		return materiaisDao.findByFuncionalidade(funcionalidade);
	}
	
	public List<Materiais> findAllPagina(Integer quantidade, Integer pagina) {

		return materiaisDao.findAllPagina(quantidade, pagina);
	}
	
	public Integer findTotal() {
	
		return materiaisDao.findTotal();
	}
	
	public Materiais insert(Materiais materiais) {

		return materiaisDao.insert(materiais);
	}

	public Materiais update(Materiais materiais) {

		return materiaisDao.update(materiais);
	}
	
	public Materiais delete(Materiais materiais) {

		return materiaisDao.delete(materiais);
	}
	
	public List<Materiais> findMateriais() {

		return materiaisDao.findAll();
	}
	
	public Materiais findMaterial(Materiais materiais) {

		return materiaisDao.find(materiais);
	}
		
    public List<Dominio> findFuncionalidade() {
		
		return materiaisDao.findFuncionalidade();
	}
	
}
