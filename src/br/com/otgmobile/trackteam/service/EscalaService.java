package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.EscalaDAO;
import br.com.otgmobile.trackteam.modal.Escala;

@Component
public class EscalaService {

	private EscalaDAO escalaDao;

	public EscalaService(EscalaDAO escalaDao) {

		this.escalaDao = escalaDao;
	}

	public List<Escala> findAllPagina(Integer quantidade, Integer pagina) {

		return escalaDao.findAllPagina(quantidade, pagina);
	}

	public Integer findTotal() {

		return escalaDao.findTotal();
	}

	public Escala insert(Escala escala) {

		return escalaDao.insert(escala);
	}

	public Escala update(Escala escala) {

		return escalaDao.update(escala);
	}

	public Escala delete(Escala escala) {

		return escalaDao.delete(escala);
	}

	public List<Escala> findEscala() {

		return escalaDao.findAll();
	}

	public Escala find(Escala escala) {
		return escalaDao.find(escala);
	}

}
