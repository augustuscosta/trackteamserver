package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.UsuarioDao;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioGrupo;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioPagina;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioRole;

@Component
public class UsuarioService {

	private UsuarioDao usuarioDao;
	
	public UsuarioService(UsuarioDao usuarioDao) {
		
		this.usuarioDao = usuarioDao;
	}
	
	public Usuario findByLogin(Usuario usuario) {
	
		return usuarioDao.findByLogin(usuario);
	}
	
	public Usuario findByMatricula(Usuario usuario){
		
		return usuarioDao.findByMatricula(usuario);
	}
	
	public Usuario insert(Usuario usuario) {
		
		return usuarioDao.insert(usuario);		
	}
	
	public List<UsuarioGrupo> findGrupos() {

		return usuarioDao.findGrupos();
	}
	
	public UsuarioRole findRole(UsuarioRole role) {

		return usuarioDao.findRole(role);
	}
	
	public List<UsuarioRole> findRoles() {

		return usuarioDao.findRoles();
	}
	
	public UsuarioRole updateRole(UsuarioRole usuarioRole) {
		return usuarioDao.updateRole(usuarioRole);
	}
	
	public UsuarioRole findRoleById(Integer id) {

		return usuarioDao.findRoleById(id);
	}

	public Usuario findById(Integer id) {

		return usuarioDao.findById(id);
	}

	
	public List<Dominio> findStatus() {

		return usuarioDao.findStatus();
	}
	public Integer findTotal() {

		return usuarioDao.findTotal();
	}
	
	public List<Usuario> findAllPagina(Integer quantidade, Integer pagina) {

		return usuarioDao.findAllPagina(quantidade, pagina);
	}


	public Usuario update(Usuario usuario) {
		
		return usuarioDao.update(usuario);
	}

	public Usuario find(Usuario usuario) {
		
		return usuarioDao.find(usuario);
	}

	public Usuario findByCpf(Usuario usuario) {
		
		return usuarioDao.findByCpf(usuario);
	}

	public Usuario findDeletarUsuarios(Usuario usuario) {
		
		return usuarioDao.findDeletarUsuarios(usuario);
	}

	public List<UsuarioPagina> findPaginas() {

		return usuarioDao.findPaginas();
	}

	public boolean isRole(String role) {

		return usuarioDao.isRole(role);
	}

	public UsuarioRole insertRegra(UsuarioRole regra) {
		
		return usuarioDao.insertRegra(regra);		
	}

	public UsuarioPagina findPagina(UsuarioPagina pagina) {

		return usuarioDao.findPagina(pagina);
	}

	public List<UsuarioRole> findAllPaginaRegras(Integer quantidade, Integer pagina) {

		return usuarioDao.findAllPaginaRegras(quantidade, pagina);
	}
	
	public Integer findTotalPaginasRegras() {

		return usuarioDao.findTotalPaginasRegras();
	}
	
	public void delete(Integer id) {

		usuarioDao.findDeleteRegras(id);
	}
}
