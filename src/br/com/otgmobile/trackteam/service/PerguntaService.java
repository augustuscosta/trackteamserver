package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.PerguntaDAO;
import br.com.otgmobile.trackteam.modal.Pergunta;

@Component
public class PerguntaService {

	private PerguntaDAO enqueteDeInteresseDAO;

	public PerguntaService(PerguntaDAO agenteDAO) {
		this.enqueteDeInteresseDAO = agenteDAO;
	}

	public List<Pergunta> findAll() {
		return enqueteDeInteresseDAO.findAll();
	}

	public Pergunta find(Pergunta enquete) {
		return enqueteDeInteresseDAO.find(enquete);
	}

	public List<Pergunta> findAllPagina(Integer quantidade, Integer pagina) {
		return enqueteDeInteresseDAO.findAllPagina(quantidade, pagina);
	}

	public Integer findTotal() {
		return enqueteDeInteresseDAO.findTotal();
	}

	public Pergunta update(Pergunta enquete) {
		enquete = enqueteDeInteresseDAO.update(enquete);
		return enquete;
	}

	public Pergunta insert(Pergunta enquete) {
		return enqueteDeInteresseDAO.insert(enquete);
	}

	public Pergunta findDeletarAgente(Pergunta enquete) {
		return enqueteDeInteresseDAO.findDeletarPergunta(enquete);

	}

}
