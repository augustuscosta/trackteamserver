package br.com.otgmobile.trackteam.service;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.ObjetoDAO;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Materiais;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaPontoDeInteresse;

@Component
public class ObjetoService {

	private ObjetoDAO objetoDao;
	private OcorrenciaService ocorrenciaService;
	
	public ObjetoService(ObjetoDAO objetoDao, OcorrenciaService ocorrenciaService) {
		this.ocorrenciaService = ocorrenciaService;
		this.objetoDao = objetoDao;
	}
	
	public Objeto find(Objeto objeto) {
		return objetoDao.find(objeto);
	}
	
	public List<Objeto> findAllPagina(Integer quantidade, Integer pagina) {
		return objetoDao.findAllPagina(quantidade, pagina);
	}
	
	public Integer findTotal() {
		return objetoDao.findTotal();
	}
	
	public Objeto insert(Objeto objeto) {
		return objetoDao.insert(objeto);
	}

	public Objeto update(Objeto objeto) {
		return objetoDao.update(objeto);
	}
	
	public Objeto delete(Objeto objeto) {
		return objetoDao.delete(objeto);
	}
	
	public List<Objeto> findObjeto() {
		return objetoDao.findAll();
	}
	
	public List<Materiais> findMateriais() {
		return objetoDao.findMateriais();
	}

	public List<Materiais> findMateriais(Integer funcionalidade_id) {
		return objetoDao.findMateriais(funcionalidade_id);
	}
	
	public List<Materiais> findMateriais(Dominio funcionalidade) {
		return objetoDao.findMateriais(funcionalidade);
	}
		
    public List<Dominio> findEstado() {
		return objetoDao.findEstado();
	}
    
    public List<Objeto> findAllObjetosDisponiveisPagina(Integer quantidade,
			Integer pagina, Materiais material) {
		return objetoDao.findAllObjetosDisponiveisPagina(quantidade, pagina, material);
	}

	public Integer findTotalObjetosDisponiveis(Materiais material) {
		return objetoDao.findTotalObjetosDisponiveis(material);
	}

	public List<Objeto> findDisponiveisByQuery(String query) {
		return objetoDao.findDisponiveisByQuery(query);
	}
	
	public List<Objeto> findDisponiveisDosPontosDeInteresseByToken(Token token) {
		List<Objeto> toReturn = new ArrayList<Objeto>();
		for(Ocorrencia ocorrencia:ocorrenciaService.findOcorrenciasAtivasByToken(token)){
			if(ocorrencia.getOcorrenciaPontosDeInteresse() != null){
				for(OcorrenciaPontoDeInteresse ponto:ocorrencia.getOcorrenciaPontosDeInteresse()){
					if(ponto.getPontoDeinteresse().getListaObjetos() != null){
						for(Objeto objeto : ponto.getPontoDeinteresse().getListaObjetos()){
							toReturn.add(objeto);
						}
					}
				}
			}
		}
		return toReturn;
	}
	
}

