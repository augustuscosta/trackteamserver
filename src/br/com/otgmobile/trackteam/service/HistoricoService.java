package br.com.otgmobile.trackteam.service;

import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.HistoricoDAO;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.comunicacao.CHistorico;
import br.com.otgmobile.trackteam.tasks.SocketIOTask;

@Component
public class HistoricoService {

	private HistoricoDAO historicoDAO;
	private SocketIOTask socketIOTask;	

	public HistoricoService(HistoricoDAO historicoDAO,
			SocketIOTask socketIOTask) {

		this.historicoDAO = historicoDAO;
		this.socketIOTask = socketIOTask;
	}
	
	public List<Historico> getHistorico(Veiculo veiculo, Date inicio, Date fim) {

		return historicoDAO.getHistorico(veiculo, inicio, fim);
	}
	
	public Historico insert(Historico historico) {
		
		if (!historico.getLatitude().equals("0") && !historico.getLongitude().equals("0")) {
			
			historico = historicoDAO.insert(historico);	
			
			CHistorico cHistorico = new CHistorico();
			cHistorico.parseFrom(historico);
/*			
			JSONObject json;
			JSONObject jsonResult = new JSONObject();
			json = new JSONObject(cHistorico);
			
			try {
				
				jsonResult.put("result", json);
				socketIOTask.enviaMsg("historico/change", jsonResult);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
*/			
		}
		
		return historico;
	}
	
	public Historico getUltimoHistorico(Veiculo veiculo) {

		return historicoDAO.getUltimoHistorico(veiculo);
	}
	
	public void enviaPosicaoSocketIO(CHistorico cHistorico) {
		
		JSONObject json;
		JSONObject jsonResult = new JSONObject();
		json = new JSONObject(cHistorico);
		
		try {			
			jsonResult.put("result", json);
			socketIOTask.enviaMsg("historico/change", jsonResult);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
