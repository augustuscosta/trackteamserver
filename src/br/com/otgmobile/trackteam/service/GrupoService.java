package br.com.otgmobile.trackteam.service;

import java.util.List;

import br.com.caelum.vraptor.Resource;
import br.com.otgmobile.trackteam.dao.GrupoDAO;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioGrupo;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioRole;

@Resource
public class GrupoService {

	private GrupoDAO grupoDAO;

	public GrupoService(GrupoDAO grupoDAO) {

		this.grupoDAO = grupoDAO;
	}

	public UsuarioGrupo Salvar(UsuarioGrupo usuarioGrupo) {

		return grupoDAO.Salvar(usuarioGrupo);
	}

	public UsuarioGrupo update(UsuarioGrupo usuarioGrupo) {

		return grupoDAO.update(usuarioGrupo);		
	}
	
	public UsuarioGrupo find(UsuarioGrupo usuarioGrupo) {

		return grupoDAO.find(usuarioGrupo);
	}
	
	public UsuarioGrupo findNome(UsuarioGrupo usuarioGrupo) {

		return grupoDAO.findNome(usuarioGrupo);
	}

	public List<UsuarioRole> findRoles() {

		return grupoDAO.findRoles();
	}

	public UsuarioRole FindPagina(UsuarioRole paginas) {
		
		return grupoDAO.FindPagina(paginas);
	}

	public List<UsuarioGrupo> findAllPaginaGrupos(Integer quantidade,
			Integer pagina) {
		return grupoDAO.findAllPaginaGrupos(quantidade,pagina);
	}

	public Integer findTotalPaginasGrupos() {
		
		return grupoDAO.findTotalPaginasGrupos();
	}

	public void delete(UsuarioGrupo usuarioGrupo) {

		grupoDAO.delete(usuarioGrupo);
	}

}
