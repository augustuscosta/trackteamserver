package br.com.otgmobile.trackteam.util;

import br.com.otgmobile.trackteam.modal.poligono.Point2Df;

public class GeoReferencial {

	public static Boolean estaNoRaio(Point2Df centro, Integer raioInteger, Point2Df ponto) {	
		
		Float raio = raioInteger * 0.00001f;
		
//		System.out.println(Math.sqrt(((centro.x-ponto.x)*(centro.x-ponto.x))+((centro.y-ponto.y)*(centro.y-ponto.y))));
//		System.out.println(raio);
		
		if (Math.sqrt((centro.x-ponto.x)*(centro.x-ponto.x)+(centro.y-ponto.y)*(centro.y-ponto.y)) <= raio) {

			return true;

		} else {

			return false;

		}		
	}
}
