package br.com.otgmobile.trackteam.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConversaoFfmpeg {

	public static boolean executaComandoExec(String comando) {

		Runtime run = Runtime.getRuntime();
		Integer exitValue = 1;
		boolean doRetorno = false;

		try {

			String s = "";
			String saidaRun = "";
			Process proc = run.exec(comando);
			proc.waitFor();

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
			while ((s = stdInput.readLine()) != null) {
				saidaRun = s;
			}
			
			stdInput.close();
			stdError.close();

			exitValue = proc.exitValue();
			proc.destroy();

			System.out.println(saidaRun);

			if (!saidaRun.equals("ERROR") && exitValue <= 0) {

				doRetorno = true;
			}

		} catch (IOException e) {
			e.printStackTrace();

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (exitValue > 0) {

			doRetorno = false;
		}

		return doRetorno;
	}

}