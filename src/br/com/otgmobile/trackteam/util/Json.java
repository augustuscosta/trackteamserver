package br.com.otgmobile.trackteam.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Json {

	static public String export(String nomeDaVariavel, Object objeto) {
		
		String toRetorno = "";
		
		Gson gson = new Gson();
		
		JsonElement je = gson.toJsonTree(objeto);		
	    JsonObject jo = new JsonObject();
	    
	    if (nomeDaVariavel.equals("")) {

	    	jo = je.getAsJsonObject();
	    	
	    } else {
	    	
	    	jo.add(nomeDaVariavel, je);
	    }

	    toRetorno = jo.toString();
			
		return toRetorno;		
	}

		
	static public String export(Object objeto) {
    
		return export("list", objeto);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object importa(final Class clazz, final String jsonObject) {
		
		Gson gson = new Gson();		
		return gson.fromJson(jsonObject, clazz);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <E> List<E> importaArray(Class clazz, final String jsonObjects) {
		
		Gson gson = new Gson();
		JsonElement json = new JsonParser().parse(jsonObjects);
		JsonArray array= json.getAsJsonArray();

		Iterator iterator = array.iterator();

		List<E> toReturn = new ArrayList<E>();

		while(iterator.hasNext()){
			
		    JsonElement json2 = (JsonElement)iterator.next();
//		    T elem = gson.fromJson(json2, clazz);
		    // 
//		    System.out.println(json2.toString());
//		    toReturn.add((T) gson.fromJson(json2, clazz));
		    toReturn.add((E) Json.importa(clazz, json2.toString()));
		}

		return toReturn;
	}	
}
