package br.com.otgmobile.trackteam.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.interceptor.download.FileDownload;
import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.otgmobile.trackteam.exception.SalvarArquivoException;
import br.com.otgmobile.trackteam.modal.MenuQuick;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.service.MenuQuickService;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Util {

	static public String toJSON(Object objeto) {

		Gson gson = new Gson();
		JsonElement je = gson.toJsonTree(objeto);
		JsonObject jo = new JsonObject();
		jo.add("result", je);

		return jo.toString();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object fromGson(final Class clazz, final String jsonObject) {

		Gson gson = new Gson();

		return gson.fromJson(jsonObject, clazz);

	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> fromGsonArray(Class clazz,
			final String jsonObjects) {

		Gson gson = new Gson();
		JsonElement json = new JsonParser().parse(jsonObjects);
		JsonArray array = json.getAsJsonArray();

		@SuppressWarnings("rawtypes")
		Iterator iterator = array.iterator();

		List<T> toReturn = new ArrayList<T>();

		while (iterator.hasNext()) {

			JsonElement json2 = (JsonElement) iterator.next();
			// T elem = gson.fromJson(json2, clazz);
			//
			toReturn.add((T) gson.fromJson(json2, clazz));
		}

		return toReturn;
	}

	static public String formataJSON(String id, String token, String conteudo) {

		String json = "";
		json = "{'id':'" + id + "', " + "'token': '" + token + "', "
				+ "'conteudo': " + conteudo + "}";

		return json;
	}

	static public void notificacao(String tipo, Integer id, String node) {

		String json = "";
		json = "{'tipo':'" + tipo + "', " + "'id':'" + id + "'" + "}";

		Util.excutePost("http://192.168.0.198:8000/add/", "json=" + json);
	}

	static public void getMenu(Result result) {

		MenuQuickService menuQuickService = new MenuQuickService();

		List<MenuQuick> listaMenu = menuQuickService.list();
		result.include("menuQuick", listaMenu);
	}

	static public Timestamp longToDate(Long dataLong) {

		Timestamp data = null;

		if (dataLong != null && 
				dataLong > 0) {

			data = new Timestamp(dataLong);

		}

		return data;
	}

	static public long dateToLong(Timestamp data) {

		long dataLong = 0;

		if (data != null) {

			dataLong = data.getTime();
		}

		return dataLong;
	}

	public static String excutePost(String targetURL, String urlParameters) {

		URL url;
		HttpURLConnection connection = null;

		try {
			// Create connection
			url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded; charset=UTF-8");

			connection.setRequestProperty("Content-Length",
					"" + Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "pt-BR");

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			// Get Response

			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();

			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}

			rd.close();
			return response.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return null;

		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	public static String decodeFromDevice(String mensagem) {

		try {

			mensagem = new String(mensagem.getBytes("ISO-8859-1"), "UTF-8");

		} catch (UnsupportedEncodingException e) {

			System.out.println("Erro de parser");
		}

		return mensagem;
	}

	public static String md5(String senha) {
		String sen = "";
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
		sen = hash.toString(16);
		return sen;
	}

	public static String geraToken(Usuario usuario) {

		String sen = "";
		String chaveToken = "";

		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		Date today = new java.util.Date();
		chaveToken = usuario.getLogin() + usuario.getNome()
				+ usuario.getSenha() + new Timestamp(today.getTime());
		BigInteger hash = new BigInteger(1, md.digest(chaveToken.getBytes()));
		sen = hash.toString(8);

		hash = new BigInteger(1, md.digest(sen.getBytes()));
		sen = hash.toString(16);

		return sen;
	}

	public static void salvarImagemVideo(String idOcorrencia, String diretorioConf, String nomeDoArquivo, UploadedFile file) throws SalvarArquivoException {

		if ((!"".equals(idOcorrencia) && idOcorrencia != null) && ((file != null))) {

			File diretorio = new File(diretorioConf + idOcorrencia);
			if (!diretorio.exists()) {
				diretorio.mkdir();
			}

			FileOutputStream output = null;
			try {
				output = new FileOutputStream(new File(diretorio + "/" + nomeDoArquivo));
				IOUtils.copy(file.getFile(), output);
			} catch (IOException e) {
				throw new SalvarArquivoException();
			} finally {
				IOUtils.closeQuietly(output);
			}
		}
	}
	

	public List<Download> recuperarImagens(String diretorioConf,
			String idOcorrencia) {

		File fotoSalva = new File(diretorioConf + idOcorrencia);

		List<Download> lista = new ArrayList<Download>();

		for (File s : fotoSalva.listFiles()) {
			if(s.isFile())
				lista.add(new FileDownload(fotoSalva, "image/jpg", s.getName()));
		}

		return lista;
	}

	public List<Download> recuperarVideos(String diretorioConf,
			String idOcorrencia) {

		File videoSalvo = new File(diretorioConf + idOcorrencia);

		List<Download> lista = new ArrayList<Download>();

		for (File s : videoSalvo.listFiles()) {
			if(s.isFile())
				lista.add(new FileDownload(videoSalvo, "video/mp4", s.getName()));
		}

		return lista;
	}

	public static void salvarAssinatura(String idOcorrencia, String diretorioConf, String nomeDoArquivo, UploadedFile file) throws SalvarArquivoException {

		if ((!"".equals(idOcorrencia) && idOcorrencia != null) && ((file != null))) {

			File diretorio = new File(diretorioConf + idOcorrencia);
			if (!diretorio.exists()) {
				diretorio.mkdir();
			}
			
			diretorio = new File(diretorioConf + idOcorrencia + "/assinaturas");
			if (!diretorio.exists()) {
				diretorio.mkdir();
			}

			FileOutputStream output = null;
			try {
				output = new FileOutputStream(new File(diretorio + "/" + nomeDoArquivo));
				IOUtils.copy(file.getFile(), output);
			} catch (IOException e) {
				throw new SalvarArquivoException();
			} finally {
				IOUtils.closeQuietly(output);
			}
		}
	}
	
	public List<Download> recuperarAssinaturas(String diretorioConf, String idOcorrencia) {

		File fotoSalva = new File(diretorioConf + idOcorrencia + "/assinaturas");

		List<Download> lista = new ArrayList<Download>();

		for (File s : fotoSalva.listFiles()) {
			if(s.isFile())
				lista.add(new FileDownload(fotoSalva, "image/jpg", s.getName()));
		}

		return lista;
	}
	
	public static Date getZeroTimeDate(Date fecha) {
	    Date res = fecha;
	    Calendar calendar = Calendar.getInstance();

	    calendar.setTime( fecha );
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);

	    res = calendar.getTime();

	    return res;
	}

}
