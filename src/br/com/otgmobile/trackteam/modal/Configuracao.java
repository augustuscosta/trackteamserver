package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findConfiguracaoAll",query="select c from Configuracao c where c.token = null order by c.chave asc"),
	@NamedQuery(name="findConfiguracaoByToken",query="select c from Configuracao c where c.token.token = :token order by c.chave asc")
})

@Entity
@Table(name="configuracao")
public class Configuracao implements Serializable {
	
	private static final long serialVersionUID = -726781308638014741L;

	@Id @GeneratedValue
	private Integer id;
	
	@Column(name="chave")
	private String chave;	

	@Column(name="valor")
	private String valor;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="token_id")
	private Token token;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}	
}
