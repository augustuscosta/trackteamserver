package br.com.otgmobile.trackteam.modal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="regioesGeoPonto")
public class GeoPontoRegioes {

	@Id @GeneratedValue
	private Integer id;
	
	private String latitude;
	
	private String longitude;
/*	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="regiao_id")
    private Regioes regioes;
*/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
/*
	public Regioes getRegioes() {
		return regioes;
	}

	public void setRegioes(Regioes regioes) {
		this.regioes = regioes;
	}	
*/	
}
