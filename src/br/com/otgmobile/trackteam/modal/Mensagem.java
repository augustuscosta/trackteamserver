package br.com.otgmobile.trackteam.modal;

import java.util.List;

public class Mensagem {

	private String titulo;
	
	private List<String> mensagens;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<String> getMensagens() {
		return mensagens;
	}

	public void setMensagens(List<String> mensagens) {
		this.mensagens = mensagens;
	}
	
}
