package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;

@Entity
@Table(name="resposta_enquete")
public class RespostaEnquete implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	private Integer id;
	
	private String resposta;	
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pergunta_id")
	private Pergunta pergunta;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ocorrencia_id")
	private Ocorrencia ocorrencia;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	public Pergunta getPergunta() {
		return pergunta;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}

	public Ocorrencia getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(Ocorrencia ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

}
