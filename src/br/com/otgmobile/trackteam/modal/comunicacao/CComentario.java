package br.com.otgmobile.trackteam.modal.comunicacao;

import java.sql.Timestamp;

import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaApoio;

public class CComentario {

	private Integer id;
	
	private String descricao;

	private Long data;
	
	private Integer usuario_id;
	
	private Integer ocorrenciaId;
	
	private String usuario;
	
	public CComentario() {
		
		data = (long) 0;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getData() {
		return data;
	}

	public void setData(Long data) {
		this.data = data;
	}

	public Integer getUsuario_id() {
		return usuario_id;
	}

	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}

	public Integer getOcorrenciaId() {
		return ocorrenciaId;
	}
	
	public void setOcorrenciaId(Integer ocorrenciaId) {
		this.ocorrenciaId = ocorrenciaId;
	}	
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void parseFrom(OcorrenciaApoio comentario) {

		if (comentario.getId() != null) {
			
			this.id = comentario.getId();			
		} 
		
		if (comentario.getData() != null) {
			
			this.data = comentario.getData().getTime();
		}
		
		this.descricao = comentario.getTexto();
		this.usuario_id = comentario.getUsuario_id();		
	}
	
	public OcorrenciaApoio parseTo() {
		
		OcorrenciaApoio comentario = new OcorrenciaApoio();
		
		comentario.setId(id);
		comentario.setData(new Timestamp(data));
		comentario.setTexto(descricao);
		comentario.setUsuario_id(usuario_id);
		
		return comentario;
	}

	
}
