package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.usuario.Usuario;

public class CUsuario {

	private Integer id;
	private String nome;	
	private String matricula;	
	private String cpf;	
	private String login;	
	private String descricao;
	
	public CUsuario() { }
	
	public CUsuario(Usuario usuario) {
		
		parseFrom(usuario);
	}	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}	

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void parseFrom(Usuario usuario) {
		
		if (usuario != null) {
			this.id = usuario.getId();
			this.nome = usuario.getNome();
			this.matricula = usuario.getMatricula();	
			this.cpf = usuario.getCpf();		
			this.login = usuario.getLogin();		
			this.descricao = usuario.getDescricao();
		}
	}
	
	public Usuario parseTo() {
		
		
		if (id != null) {
			
			Usuario usuario = new Usuario();

			usuario.setId(id);
			usuario.setNome(nome);
			usuario.setMatricula(matricula);
			usuario.setCpf(cpf);
			usuario.setLogin(login);
			usuario.setDescricao(descricao);
			
			return usuario;

		} else {
			
			return null;
		}			
	}
}
