package br.com.otgmobile.trackteam.modal.comunicacao;

import java.sql.Timestamp;

import br.com.otgmobile.trackteam.modal.HistoricoAgente;

public class CHistoricoAgente {

		
	private Integer id;
	
	private Integer agente_id;
	
	private long dataHora;
	
	private String latitude;
	
	private String longitude;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getDataHora() {
		return dataHora;
	}

	public void setDataHora(long dataHora) {
		this.dataHora = dataHora;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public void parseFrom(HistoricoAgente historico) {

		this.id = historico.getId();
		
		if (historico.getDataHora() != null) {
			
			this.dataHora = historico.getDataHora().getTime();
		}
		
		this.latitude = historico.getLatitude();
		this.longitude = historico.getLongitude();
		this.agente_id = historico.getAgente_id();
		
	}
	
	public HistoricoAgente parseTo() {
		
		HistoricoAgente historico = new HistoricoAgente();
		
		historico.setId(this.id);
		historico.setDataHora(new Timestamp(this.dataHora));
		historico.setLatitude(this.latitude);
		historico.setLongitude(this.longitude);
		historico.setAgente_id(this.agente_id);
		
		return historico;
	}
}

