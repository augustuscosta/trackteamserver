package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.ocorrencia.Vitima;

public class CVitima {

	private Integer id;

	private String descricao;

	private Integer idade;
	
	private char sexo;

	public CVitima() {
		
		sexo = 'M';
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	
	public void parseFrom(Vitima vitima) {
		
		this.id = vitima.getId();
		this.descricao = vitima.getDescricao();
		this.idade = vitima.getIdade();		
		this.sexo = vitima.getSexo();
		
	}
	
	public Vitima parseTo() {
		
		Vitima vitima = new Vitima();

		if (this.id == null || this.id <= 0) {
			
			this.id = null;
		}
		
		vitima.setId(id);
		vitima.setDescricao(descricao);
		vitima.setIdade(idade);
		vitima.setSexo(sexo);
		
		return vitima;
	}
}
