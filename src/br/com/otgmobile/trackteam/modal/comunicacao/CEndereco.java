package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.Endereco;


public class CEndereco {

	private Integer id;

	private String bairro;

	private String endGeoref;

	private String latitude;

	private String logradouro;

	private String longitude;

	private String numero;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getEndGeoref() {
		return endGeoref;
	}

	public void setEndGeoref(String endGeoref) {
		this.endGeoref = endGeoref;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void parseFrom(Endereco endereco) {

		if (endereco != null) {
			
			this.id = endereco.getId();
			this.bairro = endereco.getBairro();
			this.endGeoref = endereco.getEndGeoref();
			this.latitude = endereco.getLatitude();
			this.logradouro = endereco.getLogradouro();
			this.longitude = endereco.getLongitude();
			this.numero = endereco.getNumero();
		}	
	}

	public Endereco parseTo() {

		Endereco endereco = new Endereco();

		if (this.id == null || this.id <= 0) {
			
			this.id = null;
		}
		
		endereco.setId(this.getId());
		endereco.setBairro(this.getBairro());
		endereco.setEndGeoref(this.getEndGeoref());
		endereco.setLatitude(this.getLatitude());
		endereco.setLogradouro(this.getLogradouro());
		endereco.setLongitude(this.getLongitude());
		endereco.setNumero(this.getNumero());
		
		return endereco;
	}

}
