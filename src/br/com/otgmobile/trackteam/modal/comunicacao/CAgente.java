package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.Agente;

public class CAgente {

	private Integer id;
	
	private String nome;	
	private String matricula;	
	private String cpf;	
	private String login;	
	private String descricao;
	private String latitude;
	private String longitude;
	
	private Integer veiculo_id;
	private Integer qntOcorrencias;
	private Boolean emAtendimento;
	
	private String token;
	
	public CAgente() { }
	
	public CAgente(Agente agente) {
		
		parseFrom(agente);
	}
	
	public void parseFrom(Agente agente) {

		this.id = agente.getId();
		if(agente.getUsuario().getNome() != null){			
			this.nome = agente.getUsuario().getNome();
		}
		this.matricula = agente.getUsuario().getMatricula();
		this.cpf = agente.getUsuario().getCpf();
		this.login = agente.getUsuario().getLogin();
		this.descricao = agente.getUsuario().getDescricao();
		this.latitude = agente.getLatitude();		
		this.longitude = agente.getLongitude();
		
		if (agente.getVeiculo() != null) {
			
			this.veiculo_id = agente.getVeiculo().getId();
		}
		this.token = null;
		
		this.qntOcorrencias= agente.getQntOcorrencias(); 
		this.emAtendimento = agente.getEmAtendimento();
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getVeiculo_id() {
		return veiculo_id;
	}

	public void setVeiculo_id(Integer veiculo_id) {
		this.veiculo_id = veiculo_id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Integer getQntOcorrencias() {
		return qntOcorrencias;
	}

	public void setQntOcorrencias(Integer qntOcorrencias) {
		this.qntOcorrencias = qntOcorrencias;
	}

	public Boolean getEmAtendimento() {
		return emAtendimento;
	}

	public void setEmAtendimento(Boolean emAtendimento) {
		this.emAtendimento = emAtendimento;
	}
}
