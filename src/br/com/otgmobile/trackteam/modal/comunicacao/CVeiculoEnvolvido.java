package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaVeiculoEnvolvido;

public class CVeiculoEnvolvido {

	private String ano;
	private String marca;
	private String modelo;
	private String placa;
	private String renavam;

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public void parseFrom(OcorrenciaVeiculoEnvolvido veiculoEnvolvido) {
		
		this.ano = "";
		this.marca = veiculoEnvolvido.getDescricao();
		this.modelo = veiculoEnvolvido.getDescricao();
		this.placa = veiculoEnvolvido.getPlaca();
		this.renavam = "";		
	}
	
	public OcorrenciaVeiculoEnvolvido parseTo() {
		
		OcorrenciaVeiculoEnvolvido veiculoEnvolvido = new OcorrenciaVeiculoEnvolvido();

		if (this.placa == null || "".equals(placa)) {
			
			return null;
		}
		
		veiculoEnvolvido.setPlaca(placa);
		veiculoEnvolvido.setDescricao(modelo);
		
		return veiculoEnvolvido;
	}
	
}

