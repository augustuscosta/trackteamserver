package br.com.otgmobile.trackteam.modal.comunicacao;

import java.util.ArrayList;
import java.util.List;

import br.com.otgmobile.trackteam.modal.GeoPontoRota;
import br.com.otgmobile.trackteam.modal.Rota;

public class CRota {

	private Integer id;
	
	private String descricao;
	
	private Long horainicio;
	
	private Long horafim;
	
	private Integer veiculo_id;
	
	private List<CGeoPonto> geoPonto;

	public CRota() { }
	
	public CRota(Rota rota) { 
		
		parseFrom(rota);
	}
	
	public List<CGeoPonto> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<CGeoPonto> geoPonto) {
		this.geoPonto = geoPonto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getHorainicio() {
		return horainicio;
	}

	public void setHorainicio(Long horainicio) {
		this.horainicio = horainicio;
	}

	public Long getHorafim() {
		return horafim;
	}

	public void setHorafim(Long horafim) {
		this.horafim = horafim;
	}

	public Integer getVeiculo_id() {
		return veiculo_id;
	}

	public void setVeiculo_id(Integer veiculo_id) {
		this.veiculo_id = veiculo_id;
	}
	
	public void parseFrom(Rota rota) {
		
		id = rota.getId();
		descricao = rota.getNome();
		horainicio = (long) 0;
		horafim = (long) 0;

		geoPonto = new ArrayList<CGeoPonto>();
		
		for (GeoPontoRota geoPontoRota : rota.getGeoPonto()) {
			
			CGeoPonto cGeoPonto = new CGeoPonto();
			cGeoPonto.parseFrom(geoPontoRota);
			
			geoPonto.add(cGeoPonto);
			
		}		
	}
}
