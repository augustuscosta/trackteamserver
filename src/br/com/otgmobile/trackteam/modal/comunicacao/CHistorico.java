package br.com.otgmobile.trackteam.modal.comunicacao;

import java.sql.Timestamp;

import br.com.otgmobile.trackteam.modal.Historico;

public class CHistorico {
	
	private Integer id;
	
	private Integer veiculo_id;
	
	private Integer agente_id;
	
	private long dataHora;
	
	private String latitude;
	
	private String longitude;
	
	private String temperatura;
	
	private String velocidade;
	
	private String tensaoBateria;
	
	private String rpm;

	public Integer getVeiculo_id() {
		return veiculo_id;
	}
	
	public void setVeiculo_id(Integer veiculo_id) {
		this.veiculo_id = veiculo_id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getDataHora() {
		return dataHora;
	}

	public void setDataHora(long dataHora) {
		this.dataHora = dataHora;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(String velocidade) {
		this.velocidade = velocidade;
	}

	public String getTensao_bateria() {
		return tensaoBateria;
	}

	public void setTensao_bateria(String tensao_bateria) {
		this.tensaoBateria = tensao_bateria;
	}

	public String getRpm() {
		return rpm;
	}

	public void setRpm(String rpm) {
		this.rpm = rpm;
	}	
	
	public Integer getAgente_id() {
		return agente_id;
	}

	public void setAgente_id(Integer agente_id) {
		this.agente_id = agente_id;
	}

	public void parseFrom(Historico historico) {

		this.id = historico.getId();
		
		if (historico.getDataHora() != null) {
			
			this.dataHora = historico.getDataHora().getTime();
		}
		
		this.latitude = historico.getLatitude();
		this.longitude = historico.getLongitude();
		this.temperatura = historico.getTemperatura();
		this.velocidade = historico.getVelocidade();
		this.tensaoBateria = historico.getTensao_bateria();
		this.rpm = historico.getRpm();
		this.veiculo_id = historico.getVeiculo_id();
		this.agente_id = historico.getAgente_id();
		
	}
	
	public Historico parseTo() {
		
		Historico historico = new Historico();
		
		historico.setId(this.id);
		historico.setDataHora(new Timestamp(this.dataHora));
		historico.setLatitude(this.latitude);
		historico.setLongitude(this.longitude);
		historico.setTemperatura(this.temperatura);
		historico.setVelocidade(this.velocidade);
		historico.setTensao_bateria(this.tensaoBateria);
		historico.setRpm(this.rpm);
		historico.setVeiculo_id(this.veiculo_id);
		historico.setAgente_id(agente_id);
		
		return historico;
	}
}
