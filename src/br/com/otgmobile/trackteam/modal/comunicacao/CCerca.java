package br.com.otgmobile.trackteam.modal.comunicacao;

import java.util.ArrayList;
import java.util.List;

import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.GeoPontoCerca;

public class CCerca {

	private Integer id;
	
	private String nome;
	
	private List<CGeoPonto> geoPonto;
	
	public CCerca() { }
	
	public CCerca(Cerca cerca) { 
		
		parseFrom(cerca);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<CGeoPonto> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<CGeoPonto> geoPonto) {
		this.geoPonto = geoPonto;
	}

	public void parseFrom(Cerca cerca) {

		if (cerca != null && cerca.getId() != null) {
			
			id = cerca.getId();
			nome = cerca.getNome();
			geoPonto = new ArrayList<CGeoPonto>();
			
			if (cerca.getGeoPonto() != null) {
				
				for (GeoPontoCerca geoPontoCerca : cerca.getGeoPonto()) {
					
					CGeoPonto cGeoPonto = new CGeoPonto();
					cGeoPonto.parseFrom(geoPontoCerca);
					
					geoPonto.add(cGeoPonto);
					
				}		
			}
		}
	}
}
