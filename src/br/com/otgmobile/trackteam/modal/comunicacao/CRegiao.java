package br.com.otgmobile.trackteam.modal.comunicacao;

import java.io.Serializable;
import java.util.List;

import org.codehaus.groovy.tools.shell.ParseCode;

import br.com.otgmobile.trackteam.modal.Regioes;

public class CRegiao implements Serializable {

	private static final long serialVersionUID = -1268699738290407077L;

	private String nome;
	
	private List<CGeoPonto> geoPonto;
	
	private CGeoPonto centro;

	public CRegiao() { }
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<CGeoPonto> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<CGeoPonto> geoPonto) {
		this.geoPonto = geoPonto;
	}

	public CGeoPonto getCentro() {
		return centro;
	}

	public void setCentro(CGeoPonto centro) {
		this.centro = centro;
	}
	
}
