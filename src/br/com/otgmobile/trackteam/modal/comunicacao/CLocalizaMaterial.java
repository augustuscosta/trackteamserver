package br.com.otgmobile.trackteam.modal.comunicacao;

import java.util.List;

public class CLocalizaMaterial {

	private Integer id;
	private String Descricao;
	private CEndereco endereco;
	private List<CObjeto> materiais;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	public CEndereco getEndereco() {
		return endereco;
	}
	public void setEndereco(CEndereco endereco) {
		this.endereco = endereco;
	}
	public List<CObjeto> getMateriais() {
		return materiais;
	}
	public void setMateriais(List<CObjeto> materiais) {
		this.materiais = materiais;
	}
	
	
}
