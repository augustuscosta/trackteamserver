package br.com.otgmobile.trackteam.modal.comunicacao;

public class Cabecalho {

	private String metodo;
	private Integer usuario_id;
	private String conteudo;
	
	public String getMetodo() {
		return metodo;
	}
	
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	
	public Integer getUsuario_id() {
		return usuario_id;
	}
	
	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}
	
	public String getConteudo() {
		return conteudo;
	}
	
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
}
