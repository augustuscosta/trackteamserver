package br.com.otgmobile.trackteam.modal.comunicacao;

import java.io.Serializable;

import br.com.otgmobile.trackteam.modal.GeoPontoCerca;
import br.com.otgmobile.trackteam.modal.GeoPontoRota;

public class CGeoPonto implements Serializable {

	private static final long serialVersionUID = 3882732984652475859L;

	private Integer id;
	
	private String latitude;
	
	private String longitude;
	
	private Integer rota_id;
	
	private Integer cerca_id;

	public CGeoPonto() {
		
		this.latitude = "";
		this.longitude = "";
	}
	
	public CGeoPonto(String latitude, String longitude) {
		
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Integer getRota_id() {
		return rota_id;
	}

	public void setRota_id(Integer rota_id) {
		this.rota_id = rota_id;
	}

	public Integer getCerca_id() {
		return cerca_id;
	}

	public void setCerca_id(Integer cerca_id) {
		this.cerca_id = cerca_id;
	}

	public void parseFrom(GeoPontoCerca geoPontoCerca) {

		id = geoPontoCerca.getId();
		latitude = geoPontoCerca.getLatitude();
		longitude = geoPontoCerca.getLongitude();
		rota_id = 0;
		cerca_id = geoPontoCerca.getCerca().getId();
		
	}
	
	public void parseFrom(GeoPontoRota geoPontoRota) {

		id = geoPontoRota.getId();
		latitude = geoPontoRota.getLatitude();
		longitude = geoPontoRota.getLongitude();
		rota_id = 0;
		cerca_id = geoPontoRota.getRota().getId();
		
	}	
}
