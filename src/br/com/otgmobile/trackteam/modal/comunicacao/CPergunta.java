package br.com.otgmobile.trackteam.modal.comunicacao;

import java.io.Serializable;

import br.com.otgmobile.trackteam.modal.Pergunta;

public class CPergunta implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String pergunta;
	private String tipoEntrada;
	
	public CPergunta() { }
	
	public CPergunta(Pergunta pergunta) {
		parseFrom(pergunta);
	}

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public String getTipoEntrada() {
		return tipoEntrada;
	}


	public void setTipoEntrada(String tipoEntrada) {
		this.tipoEntrada = tipoEntrada;
	}


	public void parseFrom(Pergunta pergunta) {

		this.id = pergunta.getId();
		this.pergunta = pergunta.getPergunta();
		this.tipoEntrada = pergunta.getTipoEntrada();
	}
}
