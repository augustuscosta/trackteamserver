package br.com.otgmobile.trackteam.modal.comunicacao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaAgente;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaApoio;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaEnquete;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaVeiculoEnvolvido;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciasVeiculo;
import br.com.otgmobile.trackteam.modal.ocorrencia.Solicitante;
import br.com.otgmobile.trackteam.modal.ocorrencia.Vitima;
import br.com.otgmobile.trackteam.util.Util;

public class COcorrencia {

	private Integer id;
	
	private Integer codigo;

	private String resumo;
		
	private String descricao;

	private Long horaCriacao;
	
	private Long horaCriacaofim;
	
	private Long horaChegada;

	private Long horaSaida;
	
	private Long inicioProgramado;
	
	private Long fimProgramado;
	
	private Long recebimento;
	
	private Long inicio;
	
	private Long fim;

	private CNivelEmergencia nivelEmergencia;

	private Boolean cancelado;

	private String razaoCancelamento;

	private CEndereco endereco;
	
	private boolean validada;

	private List<CVeiculo> veiculos;
	
	private List<CAgente> agentes;
	
	private List<CEnquete> enquetes;

    private List<CObjeto> materiais;

	private List<CVeiculoEnvolvido> veiculosEnvolvidos;
	
	private List<CVitima> vitimas;
	
	private List<CSolicitante> solicitantes;
	
	private CUsuario usuario;
	
	private Integer estado;
	
	private Integer natureza;

	private CDominio estadoObj;
	
	private CDominio naturezaObj;
	
	private CDominio meioCadastro;
	
	private List<String> dadosAlterados;
	
	private List<CComentario> comentarios;
	
	private Integer flag;

	public COcorrencia() {
		
		start();
	}

	public COcorrencia(Ocorrencia ocorrencia) {
		
		start();		
		parseFrom(ocorrencia);
	}
	
	private void start() {
		
		nivelEmergencia = new CNivelEmergencia();
		endereco = new CEndereco();
		veiculos = new ArrayList<CVeiculo>();
		vitimas = new ArrayList<CVitima>();		
		solicitantes = new ArrayList<CSolicitante>();
		veiculosEnvolvidos = new ArrayList<CVeiculoEnvolvido>();
		materiais = new ArrayList<CObjeto>();
		
		usuario = new CUsuario();
		estado = 0;
		natureza = 0;
		estadoObj = new CDominio();
		naturezaObj = new CDominio();
		meioCadastro = new CDominio();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public long getHoraCriacao() {
		return horaCriacao;
	}

	public void setHoraCriacao(long horaCriacao) {
		this.horaCriacao = horaCriacao;
	}

	public Long getHoraCriacaofim() {
		return horaCriacaofim;
	}

	public void setHoraCriacaofim(Long horaCriacaofim) {
		this.horaCriacaofim = horaCriacaofim;
	}

	public long getHoraChegada() {
		return horaChegada;
	}

	public void setHoraChegada(long horaChegada) {
		this.horaChegada = horaChegada;
	}

	public long getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(long horaSaida) {
		this.horaSaida = horaSaida;
	}
	
	public long getInicioProgramado() {
		return inicioProgramado;
	}

	public void setInicioProgramado(long inicioProgramado) {
		this.inicioProgramado = inicioProgramado;
	}

	public long getFimProgramado() {
		return fimProgramado;
	}

	public void setFimProgramado(long fimProgramado) {
		this.fimProgramado = fimProgramado;
	}

	public long getRecebimento() {
		return recebimento;
	}

	public void setRecebimento(long recebimento) {
		this.recebimento = recebimento;
	}

	public long getInicio() {
		return inicio;
	}

	public void setInicio(long inicio) {
		this.inicio = inicio;
	}

	public long getFim() {
		return fim;
	}

	public void setFim(long fim) {
		this.fim = fim;
	}

	public CNivelEmergencia getNivelEmergencia() {
		return nivelEmergencia;
	}

	public void setNivelEmergencia(CNivelEmergencia nivelEmergencia) {
		this.nivelEmergencia = nivelEmergencia;
	}

	public CEndereco getEndereco() {
		return endereco;
	}

	public void setEndereco(CEndereco endereco) {
		this.endereco = endereco;
	}

	public List<CVeiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<CVeiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public List<CAgente> getAgentes() {
		return agentes;
	}

	public void setAgentes(List<CAgente> agentes) {
		this.agentes = agentes;
	}

	public boolean isValidada() {
		return validada;
	}

	public void setValidada(boolean validada) {
		this.validada = validada;
	}

	public List<CObjeto> getMateriais() {
		return materiais;
	}

	public void setMateriais(List<CObjeto> materiais) {
		this.materiais = materiais;
	}
	
	public List<CVitima> getVitimas() {
		return vitimas;
	}

	public void setVitimas(List<CVitima> vitimas) {
		this.vitimas = vitimas;
	}

	public List<CSolicitante> getSolicitantes() {
		return solicitantes;
	}

	public void setSolicitantes(List<CSolicitante> solicitantes) {
		this.solicitantes = solicitantes;
	}
		
	public Boolean getCancelado() {
		return cancelado;
	}

	public void setCancelado(Boolean cancelado) {
		this.cancelado = cancelado;
	}

	public String getRazaoCancelamento() {
		return razaoCancelamento;
	}

	public void setRazaoCancelamento(String razaoCancelamento) {
		this.razaoCancelamento = razaoCancelamento;
	}
	
	public CUsuario getUsuario() {
		return usuario;
	}

	public void setUsuario(CUsuario usuario) {
		this.usuario = usuario;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Integer getNatureza() {
		return natureza;
	}

	public void setNatureza(Integer natureza) {
		this.natureza = natureza;
	}

	public CDominio getEstadoObj() {
		return estadoObj;
	}

	public void setEstadoObj(CDominio estadoObj) {
		this.estadoObj = estadoObj;
	}

	public CDominio getNaturezaObj() {
		return naturezaObj;
	}

	public void setNaturezaObj(CDominio naturezaObj) {
		this.naturezaObj = naturezaObj;
	}

	public List<CVeiculoEnvolvido> getVeiculosEnvolvidos() {
		return veiculosEnvolvidos;
	}

	public void setVeiculosEnvolvidos(List<CVeiculoEnvolvido> veiculosEnvolvidos) {
		this.veiculosEnvolvidos = veiculosEnvolvidos;
	}	

	public List<CComentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<CComentario> comentarios) {
		this.comentarios = comentarios;
	}
	
	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	
	public CDominio getMeioCadastro() {
		return meioCadastro;
	}

	public void setMeioCadastro(CDominio meioCadastro) {
		this.meioCadastro = meioCadastro;
	}

	public List<String> getDadosAlterados() {
		return dadosAlterados;
	}

	public void setDadosAlterados(List<String> dadosAlterados) {
		this.dadosAlterados = dadosAlterados;
	}
	
	public List<CEnquete> getEnquetes() {
		return enquetes;
	}

	public void setEnquetes(List<CEnquete> enquetes) {
		this.enquetes = enquetes;
	}

	public void parseFrom(Ocorrencia ocorrencia) {
		
		if(ocorrencia == null)
			return;

		if (ocorrencia.getId() != null && ocorrencia.getId() > 0) {
		
			this.nivelEmergencia = new CNivelEmergencia();
			this.endereco = new CEndereco();	
			this.usuario = new CUsuario();
			
			this.nivelEmergencia.parseFrom(ocorrencia.getNiveisEmergencia());
			this.endereco.parseFrom(ocorrencia.getEndereco());
			this.usuario.parseFrom(ocorrencia.getUsuario());
			
			this.estadoObj.parseFrom(ocorrencia.getOcorrenciasStatus());
			this.naturezaObj.parseFrom(ocorrencia.getNatureza1());
			this.meioCadastro.parseFrom(ocorrencia.getMeioCadastro());
						
			this.id = ocorrencia.getId();
			this.codigo = ocorrencia.getId();
			this.resumo = ocorrencia.getResumo();	
			this.flag = ocorrencia.getFlag();
			
			if(ocorrencia.getFlag() != null && ocorrencia.getFlag() == 100){
				this.validada = true; 
			}else{
				this.validada = false;
			}
			
			if (ocorrencia.getOcorrenciasStatus() != null && ocorrencia.getOcorrenciasStatus().getId() > 0) {
				this.estado = ocorrencia.getOcorrenciasStatus().getId();
			}
			
			if (ocorrencia.getNatureza1() != null && ocorrencia.getNatureza1().getId() > 0) {
				this.natureza = ocorrencia.getNatureza1().getId(); 
			}
			
			this.descricao = ocorrencia.getDescricao();
			
			if (ocorrencia.getHoraChegada() != null) {			
				this.horaChegada = ocorrencia.getHoraChegada().getTime();
			}
			
			if (ocorrencia.getHoraSaida() != null) {
				this.horaSaida = ocorrencia.getHoraSaida().getTime();
			}

			if (ocorrencia.getHoraCriacao() != null) {
				this.horaCriacao = ocorrencia.getHoraCriacao().getTime();
			}
			
			this.inicioProgramado = Util.dateToLong(ocorrencia.getInicioProgramado());
			this.fimProgramado = Util.dateToLong(ocorrencia.getFimProgramado());
			this.recebimento = Util.dateToLong(ocorrencia.getRecebimento());
			this.inicio = Util.dateToLong(ocorrencia.getInicio());
			this.fim = Util.dateToLong(ocorrencia.getFim());

			horaCriacao = trataLongZero(horaCriacao);		
			horaChegada = trataLongZero(horaChegada);
			horaSaida = trataLongZero(horaSaida);		
			inicioProgramado = trataLongZero(inicioProgramado);		
			fimProgramado = trataLongZero(fimProgramado);		
			recebimento = trataLongZero(recebimento);		
			inicio = trataLongZero(inicio);		
			fim = trataLongZero(fim);
			
			this.veiculos = new ArrayList<CVeiculo>();
			
			if (ocorrencia.getOcorrenciasVeiculos() != null) {
				
				for (OcorrenciasVeiculo veiculo : ocorrencia.getOcorrenciasVeiculos()) {
					
					CVeiculo veic = new CVeiculo();
					veic.parseFrom(veiculo.getVeiculo());
					
					veiculos.add(veic);
				}
			}

			this.agentes = new ArrayList<CAgente>();
			
			if (ocorrencia.getOcorrenciaAgentes() != null) {
				
				for (OcorrenciaAgente ocorrenciaAgente : ocorrencia.getOcorrenciaAgentes()) {
					
					CAgente agente = new CAgente();
					agente.parseFrom(ocorrenciaAgente.getAgente());
					
					agentes.add(agente);
				}
			}
			
			this.enquetes = new ArrayList<CEnquete>();
			
			if (ocorrencia.getOcorrenciaEnquetes() != null) {
				
				for (OcorrenciaEnquete ocorrenciaEnquete : ocorrencia.getOcorrenciaEnquetes()) {
					
					CEnquete enquete = new CEnquete();
					enquete.parseFrom(ocorrenciaEnquete.getEnquete());
					
					enquetes.add(enquete);
				}
			}
			
			this.materiais = new ArrayList<CObjeto>();
			
			if(ocorrencia.getListaObjetos() != null){
				
				for(Objeto obj : ocorrencia.getListaObjetos()){
					CObjeto cobj = new CObjeto();
					cobj.parseFrom(obj);
					
					materiais.add(cobj);
				}
				
			}
					
			this.vitimas = new ArrayList<CVitima>();
			
			if (ocorrencia.getVitimas() != null) {
				
				for (Vitima vitima : ocorrencia.getVitimas()) {
					
					CVitima cvitima = new CVitima();
					cvitima.parseFrom(vitima);
					
					vitimas.add(cvitima);			
				}				
			}
			
			this.solicitantes = new ArrayList<CSolicitante>();
			
			if (ocorrencia.getSolicitantes() != null) {
				
				for (Solicitante solicitante : ocorrencia.getSolicitantes()) {
					
					CSolicitante cSolicitante = new CSolicitante();
					cSolicitante.parseFrom(solicitante);
					
					solicitantes.add(cSolicitante);			
				}	
			}

			this.comentarios = new ArrayList<CComentario>();
			
			if (ocorrencia.getComentarios() != null) {
				
				for (OcorrenciaApoio comentario : ocorrencia.getComentarios()) {
					
					CComentario cComentario = new CComentario();
					cComentario.parseFrom(comentario);
					cComentario.setOcorrenciaId(ocorrencia.getId());
					
					comentarios.add(cComentario);			
				}	
			}
			
			if (ocorrencia.getListaVeiculosEnvolvidos() != null) {
				
				for (OcorrenciaVeiculoEnvolvido veiculoEnvolvido : ocorrencia.getListaVeiculosEnvolvidos()) {
					
					CVeiculoEnvolvido cVeiculoEnvolvido = new CVeiculoEnvolvido();
					cVeiculoEnvolvido.parseFrom(veiculoEnvolvido);
								
					this.veiculosEnvolvidos.add(cVeiculoEnvolvido);			
				}	
			}
			
		}
	}
	
	public Ocorrencia parseTo() {

		Ocorrencia ocorrencia = new Ocorrencia();
			
		ocorrencia.setId(id);
		ocorrencia.setResumo(resumo);
		ocorrencia.setDescricao(descricao);		
		
		if (horaChegada != null && horaChegada > 0) {
			
			ocorrencia.setHoraChegada(new Timestamp(horaChegada));
		} else {
			
			ocorrencia.setHoraChegada(null);			
		}
		
		if (horaSaida != null && horaSaida > 0) {
			
			ocorrencia.setHoraSaida(new Timestamp(horaSaida));
			
		} else { 
			
			ocorrencia.setHoraSaida(null);
		}
			
		ocorrencia.setInicioProgramado(Util.longToDate(inicioProgramado));
		ocorrencia.setFimProgramado(Util.longToDate(fimProgramado));
		ocorrencia.setRecebimento(Util.longToDate(recebimento));
		ocorrencia.setInicio(Util.longToDate(inicio));
		ocorrencia.setFim(Util.longToDate(fim));
		
		ocorrencia.setNiveisEmergencia(nivelEmergencia.parseTo());
		ocorrencia.setEndereco(endereco.parseTo());
		ocorrencia.setUsuario(usuario.parseTo());
		
		Dominio natureza = null;
		Dominio estado = null;
		
		if (this.natureza != null && this.natureza > 0) {
			
			natureza = new Dominio();
			natureza.setId(this.natureza);
		}
		ocorrencia.setNatureza1(natureza);
		
		if (this.estado != null && this.estado > 0) {
			
			estado = new Dominio();
			estado.setId(this.estado);
		}		
		ocorrencia.setOcorrenciasStatus(estado);

		List<OcorrenciasVeiculo> listaVeiculos = new ArrayList<OcorrenciasVeiculo>();
		
		for (CVeiculo veiculo : veiculos) {
					
			OcorrenciasVeiculo ocorrenciasVeiculo = new OcorrenciasVeiculo();
			ocorrenciasVeiculo.setVeiculo(veiculo.parseTo());
			listaVeiculos.add(ocorrenciasVeiculo);
		}
		ocorrencia.setOcorrenciasVeiculos(listaVeiculos);
		
		List<Objeto> objetos = new ArrayList<Objeto>();
		
		if (materiais != null) {			
			for(CObjeto cobj : materiais){
				Objeto nobj = new Objeto();
				nobj.setId(cobj.getId());
				nobj.setDescricao(cobj.getDescricao());
				nobj.setNumero_controle_interno(cobj.getCodigo());
				objetos.add(nobj);
			}
		}
		
		ocorrencia.setListaObjetos(objetos);
		
		List<Vitima> listaVitimas = new ArrayList<Vitima>();
			
		for (CVitima vitima : vitimas) {
						
			listaVitimas.add(vitima.parseTo());
		}

		ocorrencia.setVitimas(listaVitimas);
		List<Solicitante> listaSolicitante = new ArrayList<Solicitante>();
		
		for (CSolicitante solicitante : solicitantes) {
			
			listaSolicitante.add(solicitante.parseTo());			
		}				
		
		ocorrencia.setSolicitantes(listaSolicitante);

		List<OcorrenciaApoio> listaComentarios = new ArrayList<OcorrenciaApoio>();

		if (comentarios != null && comentarios.size() > 0) {
			
			for (CComentario comentario : comentarios) {
				
				listaComentarios.add(comentario.parseTo());			
			}				
		}
		
		ocorrencia.setComentarios(listaComentarios);
		
		List<OcorrenciaVeiculoEnvolvido> listaVeiculoEnvolvido = new ArrayList<OcorrenciaVeiculoEnvolvido>();
		
		for (CVeiculoEnvolvido veiculoEnvolvido : veiculosEnvolvidos) {
			
			listaVeiculoEnvolvido.add(veiculoEnvolvido.parseTo());			
		}				
		
		ocorrencia.setListaVeiculosEnvolvidos(listaVeiculoEnvolvido);
		
		horaCriacao = trataLongZero(horaCriacao);		
		horaChegada = trataLongZero(horaChegada);
		horaSaida = trataLongZero(horaSaida);		
		inicioProgramado = trataLongZero(inicioProgramado);		
		fimProgramado = trataLongZero(fimProgramado);		
		recebimento = trataLongZero(recebimento);		
		inicio = trataLongZero(inicio);		
		fim = trataLongZero(fim);
		
		return ocorrencia;
	}
	
	private Long trataLongZero(Long variavel) {
		
		if (variavel == null || variavel == 0) {
			
			variavel = null;
		}
		
		return variavel;
	}
}
