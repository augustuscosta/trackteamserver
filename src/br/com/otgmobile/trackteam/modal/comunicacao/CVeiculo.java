package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.ocorrencia.VeiculoEnvolvido;


public class CVeiculo {

	private Integer id;

	private String idOrgao;

	private String ano;

	private String modelo;

	private String placa;

	private String renavam;

	private String marca;
	
	private String cor;
	      
	private CLocal local;
	
	private CCerca cerca;
	
	public CVeiculo() {
		
		this.local = new CLocal();
		cerca = new CCerca();
	}
	
	public CVeiculo (Veiculo veiculo) {
		
		this.local = new CLocal();
		cerca = new CCerca();
		parseFrom(veiculo);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdOrgao() {
		return idOrgao;
	}

	public void setIdOrgao(String idOrgao) {
		this.idOrgao = idOrgao;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public CLocal getLocal() {
		return local;
	}

	public void setLocal(CLocal local) {
		this.local = local;
	}
    
	public CCerca getCerca() {
		return cerca;
	}

	public void setCerca(CCerca cerca) {
		this.cerca = cerca;
	}

	public void parseFrom(Veiculo veiculo) {
		
		if (veiculo != null) {
			
			this.local = new CLocal();
			
			this.id = veiculo.getId();
			
			if (veiculo.getIdOrgao() == null) {
				
				this.idOrgao = "-";
				
			} else {
				
				this.idOrgao = veiculo.getIdOrgao();
			}
			this.ano = veiculo.getAno();
			this.modelo = veiculo.getModelo();
			this.placa = veiculo.getPlaca();
			this.renavam = veiculo.getRenavam();
			this.marca = veiculo.getMarca();
			this.cor = veiculo.getCor();
			this.local.parseFrom(veiculo.getLocal()); 
			this.cerca.parseFrom(veiculo.getCerca());
//			this.historico.parseFrom(veiculo.getHistorico());
		}
		
	}
	
	public Veiculo parseTo() {
		
		Veiculo veiculo = new Veiculo();
		
		veiculo.setId(id);
		veiculo.setAno(ano);
		veiculo.setModelo(modelo);
		veiculo.setPlaca(placa);
		veiculo.setRenavam(renavam);
		veiculo.setMarca(marca);
		veiculo.setCor(cor);
		veiculo.setLocal(local.parseTo());
		veiculo.setCerca(null);
//		veiculo.setHistorico(historico.parseTo());
		
		return veiculo;
		
	}

	public void parseFrom(VeiculoEnvolvido veiculo) {
		
		if (veiculo != null) {
			
			this.id = 0;
			this.ano = "";
			this.modelo = veiculo.getModelo();
			this.placa = veiculo.getPlaca();
			this.renavam = "";
			this.marca = veiculo.getModelo();
			this.cor = veiculo.getCor();
			
			this.local = new CLocal();
			this.local.setMunicipio(veiculo.getCidade());
			this.local.setUf(veiculo.getUf());
		}		
	}	
}
