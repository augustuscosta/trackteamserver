package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.Objeto;

public class CObjeto {

	private Integer id;
	private String codigo;
	private String descricao;
	
	public CObjeto() { }
	
	public CObjeto(Objeto objeto) { 
		
		parseFrom(objeto);		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void parseFrom(Objeto objeto) {

		if (objeto != null && objeto.getId() != null && objeto.getId() > 0) {
			
			id = objeto.getId();
			
			if (objeto.getNumero_controle_interno() != null) {
				codigo = objeto.getNumero_controle_interno().toString();
			}
			
			descricao = "";
			
			if (objeto.getDescricao() != null && !objeto.getDescricao().equals("")) {
				
				if (objeto.getMateriais() != null && objeto.getMateriais().getTitulo() != null) {
					
					descricao = objeto.getMateriais().getTitulo() + " - " + objeto.getDescricao();
					
				} else {
					
					descricao = objeto.getDescricao();
				}
				
			} else if (objeto.getMateriais() != null && objeto.getMateriais().getTitulo() != null) {
				
				descricao = objeto.getMateriais().getTitulo();
			}		
		} 
	}
	
	public Objeto parseTo() {
		
		Objeto objeto = new Objeto();
		objeto.setId(id);
		objeto.setDescricao(descricao);
		
		return objeto;
	}
}
