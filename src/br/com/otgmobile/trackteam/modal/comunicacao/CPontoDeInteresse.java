package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.PontoDeInteresse;

public class CPontoDeInteresse {

	private Integer id;
	
	private String nome;	
	private String telefone;	
	private String email;	
	private String observacao;
	private String contato;
	private String latitude;
	private String longitude;
	private String bairro;
	private String endGeoref;
	private String logradouro;
	private String numero;
	
	public CPontoDeInteresse() { }
	
	public CPontoDeInteresse(PontoDeInteresse ponto) {
		
		parseFrom(ponto);
	}
	
	public void parseFrom(PontoDeInteresse ponto) {

		this.id = ponto.getId();		
		this.nome = ponto.getNome();
		this.observacao = ponto.getObservacao();
		this.telefone = ponto.getTelefone();
		this.email = ponto.getEmail();
		this.contato = ponto.getContato();
		this.latitude = ponto.getEndereco().getLatitude();		
		this.longitude = ponto.getEndereco().getLongitude();
		this.bairro = ponto.getEndereco().getBairro();
		this.endGeoref = ponto.getEndereco().getEndGeoref();
		this.logradouro = ponto.getEndereco().getLogradouro();
		this.numero = ponto.getEndereco().getNumero();
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getEndGeoref() {
		return endGeoref;
	}

	public void setEndGeoref(String endGeoref) {
		this.endGeoref = endGeoref;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
