package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.ocorrencia.Solicitante;

public class CSolicitante {

	private Integer id;

	private String cpf;

	private String nome;

	private String telefone1;

	private String telefone2;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	
	public void parseFrom(Solicitante solicitante) {
		
		this.id = solicitante.getId();
		this.cpf = solicitante.getCpf();
		this.nome = solicitante.getNome();
		this.telefone1 = solicitante.getTelefone1();
		this.telefone2 = solicitante.getTelefone2();
		
	}
	
	public Solicitante parseTo() {
		
		Solicitante solicitante = new Solicitante();
		
		if (this.id == null || this.id <= 0) {
			
			this.id = null;
		}
		
		solicitante.setId(this.id);
		solicitante.setCpf(this.cpf);
		solicitante.setNome(this.nome);
		solicitante.setTelefone1(this.telefone1);
		solicitante.setTelefone2(this.telefone2);
		
		return solicitante;
	}
}
