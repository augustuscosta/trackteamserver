package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;

public class CNivelEmergencia {
	
	private Integer id;

	private String cor;

	private String descricao;
	
	private String iconBorda;	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
			
	public String getIconBorda() {
		return iconBorda;
	}

	public void setIconBorda(String iconBorda) {
		this.iconBorda = iconBorda;
	}

	public void parseFrom(NiveisEmergencia nivelEmergencia) {
		
		if (nivelEmergencia != null) {
			
			this.id = nivelEmergencia.getId();
			this.cor = nivelEmergencia.getCor();
			this.descricao = nivelEmergencia.getDescricao();
			this.iconBorda = nivelEmergencia.getIconBorda();
		}
	}
	
	public NiveisEmergencia parseTo() {
		
		NiveisEmergencia niveisEmergencia = new NiveisEmergencia();
		
		niveisEmergencia.setId(this.id);
		niveisEmergencia.setCor(this.cor);
		niveisEmergencia.setDescricao(this.descricao);
		niveisEmergencia.setIconBorda(iconBorda);
		
		return niveisEmergencia;
		
	}	
}
