package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.Local;

public class CLocal {

	private Integer id;
	
	private String municipio;
	
	private String uf;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}
	
	public void parseFrom(Local local) {
		
		if (local != null) {
			
			this.id = local.getId();
			this.municipio = local.getMunicipio();
			this.uf = local.getUf();
		}
		
	}
	
	public Local parseTo() {
		
		Local local = new Local();
		
		local.setId(this.id);
		local.setMunicipio(this.municipio);
		local.setUf(this.uf);
		
		return local;
	}
}
