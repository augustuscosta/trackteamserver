package br.com.otgmobile.trackteam.modal.comunicacao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.otgmobile.trackteam.modal.Enquete;
import br.com.otgmobile.trackteam.modal.Pergunta;

public class CEnquete implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String nome;	
	private String descricao;	
	private List<CPergunta> perguntas;
	
	public CEnquete() { }
	
	public CEnquete(Enquete enquete) {
		
		parseFrom(enquete);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public List<CPergunta> getPerguntas() {
		return perguntas;
	}

	public void setPerguntas(List<CPergunta> perguntas) {
		this.perguntas = perguntas;
	}

	public void parseFrom(Enquete enquete) {

		this.id = enquete.getId();		
		this.nome = enquete.getNome();
		this.descricao = enquete.getDescricao();
		
		if(enquete.getPerguntas() == null) return;
		if(getPerguntas() == null) setPerguntas(new ArrayList<CPergunta>());
		
		for(Pergunta pergunta:enquete.getPerguntas()){
			getPerguntas().add(new CPergunta(pergunta));
		}
	}
	
}
