package br.com.otgmobile.trackteam.modal.comunicacao;

import br.com.otgmobile.trackteam.modal.Dominio;

public class CDominio {

	private Integer id;
	
	private String valor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public void parseFrom(Dominio dominio) {
		
		if (dominio != null) {
			this.id = dominio.getId();
			this.valor = dominio.getValor();
		}
	}
	
	public Dominio parseTo() {
		
		
		if (id != null) {
			
			Dominio dominio = new Dominio();

			dominio.setId(id);
			dominio.setValor(valor);
			
			return dominio;

		} else {
			
			return null;
		}			
	}	
}
