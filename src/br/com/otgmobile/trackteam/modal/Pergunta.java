package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="pergunta")
public class Pergunta implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue 
	private Integer id;
	
	private String pergunta;
	private String tipoEntrada;
	
	@OneToMany(mappedBy="pergunta", cascade = CascadeType.ALL, targetEntity=RespostaEnquete.class)   
	private List<RespostaEnquete> respostas;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public String getTipoEntrada() {
		return tipoEntrada;
	}

	public void setTipoEntrada(String tipoEntrada) {
		this.tipoEntrada = tipoEntrada;
	}

	public List<RespostaEnquete> getRespostas() {
		return respostas;
	}

	public void setRespostas(List<RespostaEnquete> respostas) {
		this.respostas = respostas;
	}
}
