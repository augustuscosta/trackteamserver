package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="pontos_de_interesse")
public class PontoDeInteresse implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue 
	private Integer id;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="codigo", unique=true)
	private String codigo;
	
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="email")
	private String email;
	
	@Column(name="contato")
	private String contato;
	
	@Column(name="observacao")
	private String observacao;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="endereco_id")
	private Endereco endereco;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name="objeto_ponto_de_interesse", joinColumns={@JoinColumn(name="objeto_id")}, 
		inverseJoinColumns={@JoinColumn(name="ponto_de_interesse_id")})    
	private List<Objeto> listaObjetos;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Objeto> getListaObjetos() {
		return listaObjetos;
	}

	public void setListaObjetos(List<Objeto> listaObjetos) {
		this.listaObjetos = listaObjetos;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	

}
