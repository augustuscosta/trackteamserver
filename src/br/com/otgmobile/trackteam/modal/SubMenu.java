package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="submenu")
public class SubMenu implements Serializable {

	private static final long serialVersionUID = -6600532323270929194L;

	@Id @GeneratedValue
	private Integer id;

	@Column(length=50)
	private String tag;
	
	@Column(length=50)
	private String nome;
	
	private String link;
	
	private String icon;

	@Column(length=100)
	private String role;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="submenu_id")
	private List<SubMenu> listaSubMenu;

	@Column(name="submenu_id")
	private Integer submenuId;
	
	private Integer posicao;

	private Integer menu_id;

	private Boolean ativo;

	public Integer getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(Integer menu_id) {
		this.menu_id = menu_id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<SubMenu> getListaSubMenu() {
		return listaSubMenu;
	}

	public void setListaSubMenu(List<SubMenu> listaSubMenu) {
		this.listaSubMenu = listaSubMenu;
	}

	public Integer getPosicao() {
		return posicao;
	}

	public void setPosicao(Integer posicao) {
		this.posicao = posicao;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Integer getSubmenuId() {
		return submenuId;
	}

	public void setSubmenuId(Integer submenuId) {
		this.submenuId = submenuId;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}	
}
