package br.com.otgmobile.trackteam.modal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findTokenById",query="select t from Token t WHERE t.id = ?1"),
	@NamedQuery(name="findTokenByAgenteId",query="select t from Token t WHERE t.agente_id = ?1"),
	@NamedQuery(name="findTokenByVeiculoId",query="select t from Token t WHERE t.veiculo_id = ?1")
})

@Entity
@Table(name="token")
public class Token {
	
	@Id @GeneratedValue
	private Integer id;
	
	private String token;
	
	private Integer veiculo_id;
	
	private Integer agente_id;
	
	private Boolean inativo;
	
	private String versao;

	public Token() {
		
	}
	
	public Token(String token) {
		
		this.token = token;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getVeiculo_id() {
		return veiculo_id;
	}

	public void setVeiculo_id(Integer veiculo_id) {
		this.veiculo_id = veiculo_id;
	}

	public Integer getAgente_id() {
		return agente_id;
	}

	public void setAgente_id(Integer agente_id) {
		this.agente_id = agente_id;
	}

	public Boolean isInativo() {
		
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public Boolean getInativo() {
		return inativo;
	}
	
}
