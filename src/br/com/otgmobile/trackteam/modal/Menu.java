package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="menu")
public class Menu implements Serializable {

	private static final long serialVersionUID = -4567769767202478466L;

	@Id @GeneratedValue
	private Integer id;

	@Column(length=50)
	private String nome;
	
	private String link;
	
	private String icon;

	@Column(length=100)
	private String role;
	
	private Integer menu_id;
	
	private Integer posicao;

	private String tag;
	
	private Boolean ativo;
	
	@Transient
	private List<Menu> subMenu;

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="menu_id")
	private List<SubMenu> listaSubMenu;
	
	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getTipo() {
		return menu_id;
	}

	public void setTipo(Integer menu_id) {
		this.menu_id = menu_id;
	}

	public Integer getPosicao() {
		return posicao;
	}

	public void setPosicao(Integer posicao) {
		this.posicao = posicao;
	}

	public List<SubMenu> getListaSubMenu() {
		return listaSubMenu;
	}

	public void setListaSubMenu(List<SubMenu> listaSubMenu) {
		this.listaSubMenu = listaSubMenu;
	}

	public List<Menu> getSubMenu() {
		return subMenu;
	}

	public void setSubMenu(List<Menu> subMenu) {
		this.subMenu = subMenu;
	}
}
