package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name="enquete")
public class Enquete implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue 
	private Integer id;
	
	private String nome;
	private String descricao;
	
	 @ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
		@JoinTable(name="enquetes_perguntas", joinColumns={@JoinColumn(name="enquete_id")}, 
			inverseJoinColumns={@JoinColumn(name="pergunta_id")})
	private List<Pergunta> perguntas;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Pergunta> getPerguntas() {
		return perguntas;
	}

	public void setPerguntas(List<Pergunta> perguntas) {
		this.perguntas = perguntas;
	}

}
