package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findHistoricoPorVeiculo",query="select h from Historico h WHERE h.veiculo_id = ?1 and h.dataHora > ?2 and h.dataHora < ?3 order by h.dataHora"),
	@NamedQuery(name="findHistoricoUltimoPorVeiculo",query="select h from Historico h WHERE h.veiculo_id = ?1 order by h.dataHora")
})

@Entity
@Table(name="historico")
public class Historico implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	private Integer id;
	
	@Column(name="data_hora")	
	private Timestamp dataHora;
	
	private String latitude;
	
	private String longitude;
	
	private String temperatura;
	
	private String velocidade;
	
	private String tensao_bateria;
	
	private String rpm;

	private Integer veiculo_id;
	private Integer agente_id;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getDataHora() {
		return dataHora;
	}

	public void setDataHora(Timestamp dataHora) {
		this.dataHora = dataHora;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(String velocidade) {
		this.velocidade = velocidade;
	}

	public String getTensao_bateria() {
		return tensao_bateria;
	}

	public void setTensao_bateria(String tensao_bateria) {
		this.tensao_bateria = tensao_bateria;
	}

	public String getRpm() {
		return rpm;
	}

	public void setRpm(String rpm) {
		this.rpm = rpm;
	}

	public Integer getVeiculo_id() {
		return veiculo_id;
	}

	public void setVeiculo_id(Integer veiculo_id) {
		this.veiculo_id = veiculo_id;
	}

	public Integer getAgente_id() {
		return agente_id;
	}

	public void setAgente_id(Integer agente_id) {
		this.agente_id = agente_id;
	}
	
}