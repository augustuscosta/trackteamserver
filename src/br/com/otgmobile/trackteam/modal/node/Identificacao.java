package br.com.otgmobile.trackteam.modal.node;

public class Identificacao {

	private String tipo;
	private String token;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
