package br.com.otgmobile.trackteam.modal.node;

import java.util.List;

public class Node {

	private List<String> listaTokens;
	private Object mensagem;
	
	public List<String> getListaTokens() {
		return listaTokens;
	}
	public void setListaTokens(List<String> listaTokens) {
		this.listaTokens = listaTokens;
	}
	public Object getMensagem() {
		return mensagem;
	}
	public void setMensagem(Object mensagem) {
		this.mensagem = mensagem;
	}
}
