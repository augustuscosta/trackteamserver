package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@NamedQueries({
	@NamedQuery(name="findCercaByNome",query="select c from Cerca c where c.nome = ?1"),	
	@NamedQuery(name="findCercaPagina",query="select c from Cerca c order by c.nome asc"),
	@NamedQuery(name="findCercaPaginaTotal",query="select COUNT(c.id) from Cerca c")	
})


@Entity
@Table(name="cerca")
public class Cerca implements Serializable {

	private static final long serialVersionUID = -2295205581663900644L;

	@Id @GeneratedValue
	private Integer id;
	
	private String nome;
	
	@OneToMany(mappedBy="cerca", cascade = CascadeType.ALL, targetEntity=GeoPontoCerca.class)   
	private List<GeoPontoCerca> geoPonto;
	
	public Cerca() { }
	
	public Cerca(Integer id) { 
		
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<GeoPontoCerca> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<GeoPontoCerca> geoPonto) {
		this.geoPonto = geoPonto;
	}

}
