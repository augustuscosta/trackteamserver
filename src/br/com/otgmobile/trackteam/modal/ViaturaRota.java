package br.com.otgmobile.trackteam.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class ViaturaRota {
	
	@Id @GeneratedValue
	private Long id;
	
	private Double latitude;
	private Double longitude;
	private Date data;

	@Column (name="viatura_id")
	private Long viatura_id;
	
	@Column (name="cerca_id")	
	private Long cerca;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	public Long getViatura_id() {
		return viatura_id;
	}
	public void setViatura_id(Long viatura_id) {
		this.viatura_id = viatura_id;
	}
	
	public Long getCerca() {
		return cerca;
	}
	public void setCerca(Long cerca) {
		this.cerca = cerca;
	}
}
