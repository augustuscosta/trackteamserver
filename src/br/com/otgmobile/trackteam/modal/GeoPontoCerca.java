package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cercaGeoPonto")
public class GeoPontoCerca implements Serializable {

	private static final long serialVersionUID = -8732125154779617786L;

	@Id @GeneratedValue
	private Integer id;
	
	private String latitude;
	
	private String longitude;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cerca_id")
    private Cerca cerca;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Cerca getCerca() {
		return cerca;
	}

	public void setCerca(Cerca cerca) {
		this.cerca = cerca;
	}
}
