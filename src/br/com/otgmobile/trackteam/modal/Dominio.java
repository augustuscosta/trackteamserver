package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findDominioById",query="select d from Dominio d WHERE d.id = ?1")	
})

@Entity
@Table(name="dominio")
public class Dominio implements Serializable {
	
	private static final long serialVersionUID = -4853624694034819847L;

	@Id @GeneratedValue
	private Integer id;
	
	@Column(name="nome_tabela")
	private String nomeTabela;

	@Column(name="nome_campo")	
	private String nomeCampo;
	
	private String valor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeTabela() {
		return nomeTabela;
	}

	public void setNomeTabela(String nomeTabela) {
		this.nomeTabela = nomeTabela;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	
}
