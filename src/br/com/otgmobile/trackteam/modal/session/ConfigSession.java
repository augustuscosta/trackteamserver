package br.com.otgmobile.trackteam.modal.session;

import java.util.ResourceBundle;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;

@Component
@SessionScoped
public class ConfigSession {

	private String socketUrl;
	
	private String socketServer;
	
	private Boolean start;

	public void setStart() {
				
		this.start = true;
		
		final ResourceBundle resourceConfig = java.util.ResourceBundle.getBundle("config");			
		this.socketUrl = resourceConfig.getString("socket_url");
		this.socketServer = resourceConfig.getString("socket_server");
	}

	public String getSocketUrl() {
		return socketUrl;
	}

	public void setSocketUrl(String socketUrl) {
		this.socketUrl = socketUrl;
	}

	public String getSocketServer() {
		return socketServer;
	}

	public void setSocketServer(String socketServer) {
		this.socketServer = socketServer;
	}

	public Boolean getStart() {
		return start;
	}

	public void setStart(Boolean start) {
		this.start = start;
	}	
}
