package br.com.otgmobile.trackteam.modal.session;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;
import br.com.otgmobile.trackteam.modal.comunicacao.CRegiao;

@Component
@SessionScoped
public class UsuarioSession implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String matricula;
	
	private String cpf;
	
	private String nome;
	
	private String descricao;
	
	private List<String> role;
	
	private List<CRegiao> regioes;
	
	public UsuarioSession() {
		role = new ArrayList<String>();		
		regioes = new ArrayList<CRegiao>();		
	}
	
	public void logout() {
		
		id = 0;
		matricula = "";
		nome = "";
		descricao = "";
		role = new ArrayList<String>();		
		regioes = new ArrayList<CRegiao>();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<String> getRole() {
		return role;
	}

	public void setRole(List<String> role) {
		this.role = role;
	}

	public List<CRegiao> getRegioes() {
		return regioes;
	}

	public void setRegioes(List<CRegiao> regioes) {
		this.regioes = regioes;
	}
}
