package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

public class Tabela<E> implements Serializable {

	private static final long serialVersionUID = -1634672398776257094L;

	private List<E> tabela;
	
	private Integer total;


	public List<E> getTabela() {
		return tabela;
	}

	public void setTabela(List<E> tabela) {
		this.tabela = tabela;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
}
