package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="materiais")
public class Materiais implements Serializable {

	private static final long serialVersionUID = 1L;

		
	@Id @GeneratedValue
	private Integer id;

	private String titulo;
	
	@ManyToOne
	@JoinColumn(name="dominio_id")
	private Dominio funcionalidade;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Dominio getFuncionalidade() {
		return funcionalidade;
	}

	public void setFuncionalidade(Dominio funcionalidade) {
		this.funcionalidade = funcionalidade;
	}
	
}