package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findRotaPagina",query="select r from Rota r order by r.nome asc"),
	@NamedQuery(name="findRotaPaginaTotal",query="select COUNT(r.id) from Rota r")	
})

@Entity
@Table(name="rota")
public class Rota implements Serializable {

	private static final long serialVersionUID = -2295205581663900644L;

	@Id @GeneratedValue
	private Integer id;
	
	private String nome;
	
	@OneToMany(mappedBy="rota", cascade = CascadeType.ALL, targetEntity=GeoPontoRota.class)   
	private List<GeoPontoRota> geoPonto;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cerca_id")
	private Cerca cerca;
	
	public Rota() { }
	
	public Rota(Integer id) { 
		
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<GeoPontoRota> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<GeoPontoRota> geoPonto) {
		this.geoPonto = geoPonto;
	}

	public Cerca getCerca() {
		return cerca;
	}

	public void setCerca(Cerca cerca) {
		this.cerca = cerca;
	}

}
