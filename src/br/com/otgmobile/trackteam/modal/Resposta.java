package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

public class Resposta implements Serializable {

	private static final long serialVersionUID = -9166388984328794317L;

	private Integer id;
	
	private String erro;
	
	private String sucesso;
	
	private String status;

	private String nomeId;
	
	private String titulo;
	
	private List<String> mensagens;

	public String getNomeId() {
		return nomeId;
	}

	public void setNomeId(String nomeId) {
		this.nomeId = nomeId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public String getSucesso() {
		return sucesso;
	}

	public void setSucesso(String sucesso) {
		this.sucesso = sucesso;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<String> getMensagens() {
		return mensagens;
	}

	public void setMensagens(List<String> mensagens) {
		this.mensagens = mensagens;
	}		
	
	
}
