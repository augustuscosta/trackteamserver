package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;

@NamedQueries({
	@NamedQuery(name="findByIdGps",query="select v from Veiculo v where v.idGps = ?1")		
})


@Entity
@Table(name="veiculos")
public class Veiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Integer id;

	private String idOrgao;
	
	private String ano;

	private String modelo;

	private String placa;

	private String renavam;

	private String marca;
	
	private String cor;
	
	private String idGps;
	
    @ManyToOne
	@JoinColumn(name="cerca_id")
	private Cerca cerca;

    @ManyToOne
	@JoinColumn(name="rota_id")
	private Rota rota;
    
    @ManyToOne
	@JoinColumn(name="local_tipo_id")
	private Local local;
    
    @ManyToOne
	@JoinColumn(name="dominio_tipo_id")
	private Dominio tipoVeiculo;

    
    @ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name="objeto_veiculo", joinColumns={@JoinColumn(name="veiculo_id")}, 
		inverseJoinColumns={@JoinColumn(name="objeto_id")})
	private List<Objeto> listaObjetos;
    
    private String latitude;
    private String longitude;
    
    @Transient
    private List<Ocorrencia> listaOcorrencia;

    @Transient
    private Historico historico;
    
    @Transient 
    private Integer qntOcorrencias;
 
    @Transient 
    private Boolean emAtendimento;
    
    public Veiculo() {
    }

	public Veiculo(Integer id) {

		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdOrgao() {
		return idOrgao;
	}

	public void setIdOrgao(String idOrgao) {
		this.idOrgao = idOrgao;
	}

	public String getAno() {
		return this.ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRenavam() {
		return this.renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public Dominio getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(Dominio tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public Cerca getCerca() {
		return cerca;
	}

	public void setCerca(Cerca cerca) {
		this.cerca = cerca;
	}

	public List<Ocorrencia> getListaOcorrencia() {
		return listaOcorrencia;
	}

	public void setListaOcorrencia(List<Ocorrencia> listaOcorrencia) {
		this.listaOcorrencia = listaOcorrencia;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public List<Objeto> getListaObjetos() {
		return listaObjetos;
	}

	public void setListaObjetos(List<Objeto> listaObjetos) {
		this.listaObjetos = listaObjetos;
	}

	public String getIdGps() {
		return idGps;
	}

	public void setIdGps(String idGps) {
		this.idGps = idGps;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Historico getHistorico() {
		return historico;
	}

	public void setHistorico(Historico historico) {
		this.historico = historico;
	}

	public Integer getQntOcorrencias() {
		return qntOcorrencias;
	}

	public void setQntOcorrencias(Integer qntOcorrencias) {
		this.qntOcorrencias = qntOcorrencias;
	}

	public Boolean getEmAtendimento() {
		return emAtendimento;
	}

	public void setEmAtendimento(Boolean emAtendimento) {
		this.emAtendimento = emAtendimento;
	}
}