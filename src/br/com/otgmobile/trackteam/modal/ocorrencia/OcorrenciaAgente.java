package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.otgmobile.trackteam.modal.Agente;

@Entity
@Table(name="agentes_atende_ocorrencias")
public class OcorrenciaAgente implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Integer id;

	@Column(name="data_hora")
	private Timestamp dataHora;

	@Column(name="hora_fim")
	private Timestamp horaFim;

	@Column(name="hora_inicio")
	private Timestamp horaInicio;
   
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="agente_id")
	private Agente agente;
    
    @Column(name="ativo")
    private Boolean ativo;
    
    @Column(name="observacao")
    private String observacao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getDataHora() {
		return dataHora;
	}

	public void setDataHora(Timestamp dataHora) {
		this.dataHora = dataHora;
	}

	public Timestamp getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(Timestamp horaFim) {
		this.horaFim = horaFim;
	}

	public Timestamp getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Timestamp horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}