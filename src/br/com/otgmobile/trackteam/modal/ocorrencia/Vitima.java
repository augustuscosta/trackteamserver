package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.otgmobile.trackteam.modal.Dominio;



/**
 * The persistent class for the vitimas database table.
 * 
 */
@Entity
@Table(name="vitimas")
public class Vitima implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Integer id;

	private String descricao;

	private Integer idade;
	
	private char sexo;
	
    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="dominio_gravidade_id")
	private Dominio gravidade;

    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="dominio_tipo_id")
	private Dominio tipo;
 
    public Vitima() {
    }

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public Dominio getGravidade() {
		return gravidade;
	}

	public void setGravidade(Dominio gravidade) {
		this.gravidade = gravidade;
	}

	public Dominio getTipo() {
		return tipo;
	}

	public void setTipo(Dominio tipo) {
		this.tipo = tipo;
	}

}