package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.otgmobile.trackteam.modal.PontoDeInteresse;

@Entity
@Table(name="pontos_de_interesse_ocorrencias")
public class OcorrenciaPontoDeInteresse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Integer id;
   
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ponto_de_interesse_id")
	private PontoDeInteresse pontoDeinteresse;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PontoDeInteresse getPontoDeinteresse() {
		return pontoDeinteresse;
	}

	public void setPontoDeinteresse(PontoDeInteresse pontoDeinteresse) {
		this.pontoDeinteresse = pontoDeinteresse;
	}
    
    
}