package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ocorrencias_apoio")
public class OcorrenciaApoio implements Serializable {

	private static final long serialVersionUID = 8293989126684116293L;

	@Id @GeneratedValue
	private Integer id;
	
	private Timestamp data;
	
	private Integer usuario_id;

	private String texto;
		
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Timestamp getData() {
		return data;
	}
	public void setData(Timestamp data) {
		this.data = data;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public Integer getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}

}
