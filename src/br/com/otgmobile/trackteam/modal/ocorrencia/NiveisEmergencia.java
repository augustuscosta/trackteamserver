package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the niveis_emergencia database table.
 * 
 */
@Entity
@Table(name="niveis_emergencia")
public class NiveisEmergencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Integer id;

	private String cor;

	private String descricao;
	
	private String iconBorda;

    public String getIconBorda() {
		return iconBorda;
	}

	public void setIconBorda(String iconBorda) {
		this.iconBorda = iconBorda;
	}

	public NiveisEmergencia() {
    }

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCor() {
		return this.cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}