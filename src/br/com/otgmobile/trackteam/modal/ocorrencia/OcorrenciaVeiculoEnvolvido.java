package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="ocorrencias_veiculos_envolvidos")
public class OcorrenciaVeiculoEnvolvido implements Serializable {

	private static final long serialVersionUID = 5064262731528132147L;

	@Id @GeneratedValue 
	private Integer id;

	private String placa;

	private String descricao;
	
	@Transient
	private VeiculoEnvolvido veiculoEnvolvido;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public VeiculoEnvolvido getVeiculoEnvolvido() {
		return veiculoEnvolvido;
	}

	public void setVeiculoEnvolvido(VeiculoEnvolvido veiculoEnvolvido) {
		this.veiculoEnvolvido = veiculoEnvolvido;
	}

}
