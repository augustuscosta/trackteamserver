package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.otgmobile.trackteam.modal.Veiculo;


/**
 * The persistent class for the ocorrencias_veiculos database table.
 * 
 */
@Entity
@Table(name="veiculos_atende_ocorrencias")
public class OcorrenciasVeiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Integer id;

	@Column(name="data_hora")
	private Timestamp dataHora;

	@Column(name="hora_fim")
	private Timestamp horaFim;

	@Column(name="hora_inicio")
	private Timestamp horaInicio;
/*
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ocorrencia_id")      
	private Ocorrencia ocorrencia;
*/    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="veiculo_id")
	private Veiculo veiculo;
    
    @Column(name="ativo")
    private Boolean ativo;
    
    @Column(name="observacao")
    private String observacao;

    public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getMotivo() {
		return observacao;
	}

	public void setMotivo(String observacao) {
		this.observacao = observacao;
	}

	public OcorrenciasVeiculo() {
    }
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getDataHora() {
		return this.dataHora;
	}

	public void setDataHora(Timestamp dataHora) {
		this.dataHora = dataHora;
	}

	public Timestamp getHoraFim() {
		return this.horaFim;
	}

	public void setHoraFim(Timestamp horaFim) {
		this.horaFim = horaFim;
	}

	public Timestamp getHoraInicio() {
		return this.horaInicio;
	}

	public void setHoraInicio(Timestamp horaInicio) {
		this.horaInicio = horaInicio;
	}
	
	public Veiculo getVeiculo() {
		return this.veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}


}