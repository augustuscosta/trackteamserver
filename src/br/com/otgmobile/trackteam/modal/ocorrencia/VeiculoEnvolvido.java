package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vw_veiculos", schema="veiculo")
public class VeiculoEnvolvido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VEIC_ID", insertable=false, updatable=false)
	private String placa;

	@Column(name="NOME_MODELO", insertable=false, updatable=false)
	private String modelo;

	@Column(name="NOME_CATEGORIA", insertable=false, updatable=false)
	private String categoria;	
	
	@Column(name="NOME_COR", insertable=false, updatable=false)
	private String cor;

	@Column(name="NOME_CIDADE", insertable=false, updatable=false)
	private String cidade;
	
	@Column(name="NOME_UF", insertable=false, updatable=false)
	private String uf;
	   	
    public VeiculoEnvolvido() {
    	
    }

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}    
}