package br.com.otgmobile.trackteam.modal.ocorrencia;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.OrderBy;

import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Endereco;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;

@NamedQueries({
	@NamedQuery(name="findOcorrenciasAtivasByVeiculo",query="select o from Ocorrencia o JOIN o.ocorrenciasVeiculos ov WHERE ov.veiculo.id = :veiculo and o.horaEncerramento is null and o.flag >= 100"),
	@NamedQuery(name="findOcorrenciasAtivasByAgente",query="select o from Ocorrencia o JOIN o.ocorrenciaAgentes oa WHERE oa.agente.id = :agente and o.horaEncerramento is null and o.flag >= 100"),
	
	@NamedQuery(name="findOcorrenciasEmAtendimentoByVeiculo",query="select o from Ocorrencia o JOIN o.ocorrenciasVeiculos ov WHERE ov.veiculo.id = :veiculo and o.inicio is not null and o.horaChegada is null and o.fim is null"),
	@NamedQuery(name="findOcorrenciasEmAtendimentoByAgente",query="select o from Ocorrencia o JOIN o.ocorrenciaAgentes oa WHERE oa.agente.id = :agente and o.inicio is not null and o.horaChegada is null and o.fim is null"),		

	@NamedQuery(name="findOcorrenciaVeiculos",query="select ov from Ocorrencia o JOIN o.ocorrenciasVeiculos ov WHERE o.id = ?1"),
	@NamedQuery(name="findOcorrenciasTotalPorVeiculo",query="select COUNT(o.id) from Ocorrencia o JOIN o.ocorrenciasVeiculos ov JOIN ov.veiculo v where v.id = :veiculo")
})

@Entity
@Table(name="ocorrencias")
public class Ocorrencia implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue 
	private Integer id;

	private String resumo;

	private String descricao;
	
	@Column(name="hora_chegada")
	private Timestamp horaChegada;

	@Column(name="hora_saida")
	private Timestamp horaSaida;

	@Column(name="hora_inicio_programado")
	private Timestamp inicioProgramado;
	
	@Column(name="hora_fim_programado")
	private Timestamp fimProgramado;
	
	@Column(name="hora_recebimento")
	private Timestamp recebimento;
	
	@Column(name="hora_inicio")
	private Timestamp inicio;
	
	@Column(name="hora_fim")
	private Timestamp fim;	

	@Column(name="hora_encerramento")
	private Timestamp horaEncerramento;	

	@Column(name="hora_criacao")
	private Timestamp horaCriacao;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="niveis_emergencia_id")
	private NiveisEmergencia niveisEmergencia;

	@ManyToOne(fetch=FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="endereco_id")
	private Endereco endereco;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="dominio_natureza_ini")
	private Dominio natureza1;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="dominio_natureza_fim")
	private Dominio natureza2;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="dominio_status_id")
	private Dominio ocorrenciasStatus;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="dominio_meio_id")
	private Dominio meioCadastro;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="usuario_id")
	private Usuario usuario;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="usuario_id_criacao")
	private Usuario usuarioCriacao;	
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="ocorrencia_id")
	private List<OcorrenciasVeiculo> ocorrenciasVeiculos;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="ocorrencia_id")
	private List<OcorrenciaAgente> ocorrenciaAgentes;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="ocorrencia_id")
	private List<OcorrenciaPontoDeInteresse> ocorrenciaPontosDeInteresse;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="ocorrencia_id")
	private List<OcorrenciaEnquete> ocorrenciaEnquetes;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="ocorrencia_id")
	private List<Vitima> vitimas;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="ocorrencia_id")
	private List<OcorrenciaVeiculoEnvolvido> listaVeiculosEnvolvidos;
	
    @ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinTable(name="ocorrencias_solicitantes", joinColumns={@JoinColumn(name="ocorrencia_numero_ocorrencia")}, 
		inverseJoinColumns={@JoinColumn(name="solicitante_id")})
	private List<Solicitante> solicitantes;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name="objeto_ocorrencia", joinColumns={@JoinColumn(name="ocorrencia_id")}, 
	inverseJoinColumns={@JoinColumn(name="objeto_id")})
	private List<Objeto> listaObjetos;

	@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ocorrencia_id")
	@OrderBy(clause="data DESC")
    private List<OcorrenciaApoio> comentarios;

	@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ocorrencia_id")
    private List<OcorrenciaImagem> listaImagens;
	
	@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ocorrencia_id")
    private List<OcorrenciaAssinatura> listaAssinaturas;
	
	@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ocorrencia_id")
    private List<OcorrenciaVideo> listaVideos;
	
	private Integer flag;
	
	@Transient
	private Integer total;
	@Transient
	private String valor;
	@Transient
	private String pontosString;
    
    public Ocorrencia() {
    }
    
    public Ocorrencia(Integer id) {
    	
    	this.id = id;
    }
    
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Timestamp getHoraChegada() {
		return this.horaChegada;
	}
	
	public void setHoraChegada(Timestamp horaChegada) {
		this.horaChegada = horaChegada;
	}

	public Timestamp getHoraSaida() {
		return this.horaSaida;
	}

	public void setHoraSaida(Timestamp horaSaida) {
		this.horaSaida = horaSaida;
	}

	public Timestamp getInicioProgramado() {
		return inicioProgramado;
	}

	public void setInicioProgramado(Timestamp inicioProgramado) {
		this.inicioProgramado = inicioProgramado;
	}

	public Timestamp getFimProgramado() {
		return fimProgramado;
	}

	public void setFimProgramado(Timestamp fimProgramado) {
		this.fimProgramado = fimProgramado;
	}

	public Timestamp getRecebimento() {
		return recebimento;
	}

	public void setRecebimento(Timestamp recebimento) {
		this.recebimento = recebimento;
	}

	public Timestamp getInicio() {
		return inicio;
	}

	public void setInicio(Timestamp inicio) {
		this.inicio = inicio;
	}

	public Timestamp getFim() {
		return fim;
	}

	public void setFim(Timestamp fim) {
		this.fim = fim;
	}

	public String getResumo() {
		return this.resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public Endereco getEndereco() {
		return this.endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public Dominio getNatureza1() {
		return natureza1;
	}

	public void setNatureza1(Dominio natureza1) {
		this.natureza1 = natureza1;
	}

	public Dominio getNatureza2() {
		return natureza2;
	}

	public void setNatureza2(Dominio natureza2) {
		this.natureza2 = natureza2;
	}

	public NiveisEmergencia getNiveisEmergencia() {
		return this.niveisEmergencia;
	}

	public void setNiveisEmergencia(NiveisEmergencia niveisEmergencia) {
		this.niveisEmergencia = niveisEmergencia;
	}
	
	public Dominio getOcorrenciasStatus() {
		return this.ocorrenciasStatus;
	}

	public void setOcorrenciasStatus(Dominio ocorrenciasStatus) {
		this.ocorrenciasStatus = ocorrenciasStatus;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioCriacao() {
		return usuarioCriacao;
	}

	public void setUsuarioCriacao(Usuario usuarioCriacao) {
		this.usuarioCriacao = usuarioCriacao;
	}

	public List<OcorrenciasVeiculo> getOcorrenciasVeiculos() {
		return ocorrenciasVeiculos;
	}

	public void setOcorrenciasVeiculos(List<OcorrenciasVeiculo> ocorrenciasVeiculos) {
		this.ocorrenciasVeiculos = ocorrenciasVeiculos;
	}

	public List<OcorrenciaAgente> getOcorrenciaAgentes() {
		return ocorrenciaAgentes;
	}

	public void setOcorrenciaAgentes(List<OcorrenciaAgente> ocorrenciaAgentes) {
		this.ocorrenciaAgentes = ocorrenciaAgentes;
	}

	public List<Vitima> getVitimas() {
		return this.vitimas;
	}

	public void setVitimas(List<Vitima> vitimas) {
		this.vitimas = vitimas;
	}

	public List<Solicitante> getSolicitantes() {
		return solicitantes;
	}

	public void setSolicitantes(List<Solicitante> solicitantes) {
		this.solicitantes = solicitantes;
	}

	public List<OcorrenciaVeiculoEnvolvido> getListaVeiculosEnvolvidos() {
		return listaVeiculosEnvolvidos;
	}

	public void setListaVeiculosEnvolvidos(
			List<OcorrenciaVeiculoEnvolvido> listaVeiculosEnvolvidos) {
		this.listaVeiculosEnvolvidos = listaVeiculosEnvolvidos;
	}

	public List<Objeto> getListaObjetos() {
		return listaObjetos;
	}

	public void setListaObjetos(List<Objeto> listaObjetos) {
		this.listaObjetos = listaObjetos;
	}

	public List<OcorrenciaApoio> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<OcorrenciaApoio> comentarios) {
		this.comentarios = comentarios;
	}

	public Dominio getMeioCadastro() {
		return meioCadastro;
	}

	public void setMeioCadastro(Dominio meioCadastro) {
		this.meioCadastro = meioCadastro;
	}

	public Timestamp getHoraEncerramento() {
		return horaEncerramento;
	}

	public void setHoraEncerramento(Timestamp horaEncerramento) {
		this.horaEncerramento = horaEncerramento;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public List<OcorrenciaImagem> getListaImagens() {
		return listaImagens;
	}

	public void setListaImagens(List<OcorrenciaImagem> listaImagens) {
		this.listaImagens = listaImagens;
	}

	public List<OcorrenciaVideo> getListaVideos() {
		return listaVideos;
	}

	public void setListaVideos(List<OcorrenciaVideo> listaVideos) {
		this.listaVideos = listaVideos;
	}

	public Timestamp getHoraCriacao() {
		return horaCriacao;
	}

	public void setHoraCriacao(Timestamp horaCriacao) {
		this.horaCriacao = horaCriacao;
	}

	public List<OcorrenciaPontoDeInteresse> getOcorrenciaPontosDeInteresse() {
		return ocorrenciaPontosDeInteresse;
	}

	public void setOcorrenciaPontosDeInteresse(
			List<OcorrenciaPontoDeInteresse> ocorrenciaPontosDeInteresse) {
		this.ocorrenciaPontosDeInteresse = ocorrenciaPontosDeInteresse;
	}

	public List<OcorrenciaAssinatura> getListaAssinaturas() {
		return listaAssinaturas;
	}

	public void setListaAssinaturas(List<OcorrenciaAssinatura> listaAssinaturas) {
		this.listaAssinaturas = listaAssinaturas;
	}

	public List<OcorrenciaEnquete> getOcorrenciaEnquetes() {
		return ocorrenciaEnquetes;
	}

	public void setOcorrenciaEnquetes(List<OcorrenciaEnquete> ocorrenciaEnquetes) {
		this.ocorrenciaEnquetes = ocorrenciaEnquetes;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getPontosString() {
		pontosString = "";
		if(ocorrenciaPontosDeInteresse !=null){
			for(OcorrenciaPontoDeInteresse op:ocorrenciaPontosDeInteresse){
				pontosString += op.getPontoDeinteresse().getNome() + " ";
			}
		}
		return pontosString;
	}

	public void setPontosString(String pontosString) {
		this.pontosString = pontosString;
	}
}