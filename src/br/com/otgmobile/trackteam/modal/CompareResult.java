package br.com.otgmobile.trackteam.modal;


import org.apache.commons.lang3.StringUtils;

public class CompareResult {

	private String atributos;
	
	private String valores;

	public CompareResult() {
		this.atributos = "";
		this.valores = "";
	}
	
	public String getAtributos() {
		return atributos;
	}

	public void setAtributos(String atributos) {
		this.atributos = atributos;
	}

	public String getValores() {
		return valores;
	}

	public void setValores(String valores) {
		this.valores = valores;
	}

	public void addAtributo(String atributo) {	
		this.atributos += StringUtils.replace(atributo, "@", "#") + "@";		
	}

	public void addValor(String valor) {
		this.valores += StringUtils.replace(valor, "@", "#") + "@";
	}

	public void add(CompareResult cr) {

		this.atributos += cr.getAtributos();
		this.valores += cr.getValores();
	}

}
