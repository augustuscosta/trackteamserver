package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.otgmobile.trackteam.modal.usuario.Usuario;

@NamedQueries({
	@NamedQuery(name="findAgenteByLogin",query="select a from Agente a WHERE a.usuario.login = ?1"),
//	@NamedQuery(name="findAgenteNaCerca",query="select a from Agente a WHERE (a.latitude = ?1 or a.longitude = ?2) order by a.id desc"),
//	@NamedQuery(name="findAgenteNaCerca",query="select o from Ocorrencia o where o.ocorrenciaAgentes.agente.id = :agente"),	
	@NamedQuery(name="findAllAgentePosicao",query="select a from Agente a")
})


@Entity
@Table(name="agente")
public class Agente implements Serializable {

	private static final long serialVersionUID = -3606024845545527412L;

	@Id @GeneratedValue
	private Integer id;
	
	private String latitude;
	
	private String longitude;
	
	@Column(name="ultima_atualizacao")
	private Timestamp ultimaAtualizacao;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="usuario_id")
	private Usuario usuario;

    @ManyToOne(fetch=FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="veiculo_id")
	private Veiculo veiculo;

    @ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name="objeto_agente", joinColumns={@JoinColumn(name="objeto_id")}, 
		inverseJoinColumns={@JoinColumn(name="agente_id")})    
	private List<Objeto> listaObjetos;
    
    
    @Transient
    private HistoricoAgente posicao;
     
    @Transient 
    private Integer qntOcorrencias;

    @Transient 
    private Boolean emAtendimento;
    
    public Agente() { }
    
    public Agente(Integer id) { 
    	
    	this.id = id;
    }
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Timestamp getUltimaAtualizacao() {
		return ultimaAtualizacao;
	}

	public void setUltimaAtualizacao(Timestamp ultimaAtualizacao) {
		this.ultimaAtualizacao = ultimaAtualizacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public HistoricoAgente getPosicao() {
		return posicao;
	}

	public void setPosicao(HistoricoAgente posicao) {
		this.posicao = posicao;
	}

	public List<Objeto> getListaObjetos() {
		return listaObjetos;
	}

	public void setListaObjetos(List<Objeto> listaObjetos) {
		this.listaObjetos = listaObjetos;
	}

	public Integer getQntOcorrencias() {
		return qntOcorrencias;
	}

	public void setQntOcorrencias(Integer qntOcorrencias) {
		this.qntOcorrencias = qntOcorrencias;
	}

	public Boolean getEmAtendimento() {
		return emAtendimento;
	}

	public void setEmAtendimento(Boolean emAtendimento) {
		this.emAtendimento = emAtendimento;
	}			
}
