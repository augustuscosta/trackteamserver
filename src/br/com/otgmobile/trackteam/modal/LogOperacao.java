package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({
	@NamedQuery(name="findLogOperacao",query="select lo from LogOperacao lo where lo.controller = :controller and lo.metodo = :metodo")
})

//@Entity
//@Table(name="logs_operacao")
public class LogOperacao implements Serializable {

	private static final long serialVersionUID = 9212130536039535825L;

//	@GeneratedValue @Id
	private Integer id;
	
	private String controller;
	
	private String metodo;
	
	private String descricao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
