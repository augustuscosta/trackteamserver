package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findObjetoDisponiveisPorQuery",query="select o from Objeto o where upper(o.numero_controle_interno) like ?1 or " +
			"upper(o.descricao) like ?2 or " +
			"upper(o.materiais.titulo) like ?3")	
})

@Entity
@Table(name="objeto")
public class Objeto implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@Id @GeneratedValue
	private Integer id;

	@ManyToOne
	@JoinColumn(name="material_id")
	private Materiais materiais;
	
	private String numero_controle_interno;
	
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name="dominio_id")
	private Dominio estado;

	public Objeto() { }
	
	public Objeto(Integer itemId) {

		this.id = itemId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Materiais getMateriais() {
		return materiais;
	}

	public void setMateriais(Materiais materiais) {
		this.materiais = materiais;
	}

	public String getNumero_controle_interno() {
		return numero_controle_interno;
	}

	public void setNumero_controle_interno(String numero_controle_interno) {
		this.numero_controle_interno = numero_controle_interno;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Dominio getEstado() {
		return estado;
	}

	public void setEstado(Dominio estado) {
		this.estado = estado;
	}
	
}