package br.com.otgmobile.trackteam.modal.usuario;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findAllPaginaGrupos",query="select g from UsuarioGrupo g order by g.nome"),
	@NamedQuery(name="findTotalPaginasGrupos",query="select COUNT(g.id) from UsuarioGrupo g")
})


@Entity
@Table(name="usuarios_grupo")
public class UsuarioGrupo implements Serializable {

	private static final long serialVersionUID = -8334393418865624593L;
	
	@Id @GeneratedValue
	private Integer id;
	
	private String nome;
	
	@Column(name="grupoativo")
	private Boolean grupoAtivo;

    @ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="usuarios_grupo_pagina", joinColumns={@JoinColumn(name="grupo_id")}, 
		inverseJoinColumns={@JoinColumn(name="pagina_id")})
	private List<UsuarioPagina> paginas;
    
    public UsuarioGrupo() { }
    
    public UsuarioGrupo(Integer id) { 
    	this.id = id;
    }
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getGrupoAtivo() {
		return grupoAtivo;
	}

	public void setGrupoAtivo(Boolean grupoAtivo) {
		this.grupoAtivo = grupoAtivo;
	}

	public List<UsuarioPagina> getPaginas() {
		return paginas;
	}

	public void setPaginas(List<UsuarioPagina> paginas) {
		this.paginas = paginas;
	}
	
	
}
