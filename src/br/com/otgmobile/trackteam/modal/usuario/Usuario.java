package br.com.otgmobile.trackteam.modal.usuario;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Regioes;

@Entity
@Table(name="usuarios")
public class Usuario implements Serializable {
	
	private static final long serialVersionUID = -1065527705777471280L;

	@Id @GeneratedValue
	private Integer id;
	
	private String matricula;
	
	private String cpf;
	
	private String nome;

	private String login;
	
	private String senha;
	
	private String descricao;
	
	public Usuario() { }
	
	public Usuario(Integer id) {
		
		this.id = id;
	}
	
    @ManyToOne
	@JoinColumn(name="dominio_status_id")
	private Dominio status; 

    @ManyToOne
	@JoinColumn(name="grupo_id")
	private UsuarioGrupo grupo;   
    
    @ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="usuarios_regioes", joinColumns={@JoinColumn(name="usuario_id")}, 
		inverseJoinColumns={@JoinColumn(name="regioes_id")})
    private List<Regioes> regioes;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula.toLowerCase();
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Dominio getStatus() {
		return status;
	}

	public void setStatus(Dominio status) {
		this.status = status;
	}

	public UsuarioGrupo getGrupo() {
		return grupo;
	}

	public void setGrupo(UsuarioGrupo grupo) {
		this.grupo = grupo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login.toLowerCase();
	}

	public List<Regioes> getRegioes() {
		return regioes;
	}

	public void setRegioes(List<Regioes> regioes) {
		this.regioes = regioes;
	}

	
	
}
