package br.com.otgmobile.trackteam.modal.usuario;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="verificaRegraNome",query="select r from UsuarioRole r where r.role = ?1"),
	@NamedQuery(name="findRole",query="select r from UsuarioRole r where r.id=?1"),
	@NamedQuery(name="findRoleById",query="select r from UsuarioRole r where r.id=?1"),
	@NamedQuery(name="findAllPaginaRegra",query="select r from UsuarioRole r order by r.role"),	
	@NamedQuery(name="findTotalPaginasRegra",query="select COUNT(r.id) from UsuarioRole r"),
	@NamedQuery(name="findPaginaId",query="select r from UsuarioRole r where r.id = ?1")
})

@Entity
@Table(name="usuarios_role")
public class UsuarioRole implements Serializable {

	private static final long serialVersionUID = -600355825228446815L;

	@Id @GeneratedValue
	private Integer id;
	
	private String role;

    @ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="usuarios_role_pagina", joinColumns={@JoinColumn(name="role_id")}, 
		inverseJoinColumns={@JoinColumn(name="pagina_id")})
	private List<UsuarioPagina> listaPaginas;
    
    public UsuarioRole() {}
    
    public UsuarioRole(Integer id){
    	this.id=id;
    }
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<UsuarioPagina> getListaPaginas() {
		return listaPaginas;
	}

	public void setListaPaginas(List<UsuarioPagina> listaPaginas) {
		this.listaPaginas = listaPaginas;
	}
}
