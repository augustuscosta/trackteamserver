package br.com.otgmobile.trackteam.modal.usuario;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findPaginas",query="select p from UsuarioPagina p order by p.pagina"),
	@NamedQuery(name="findPagina",query="select p from UsuarioPagina p where p.id = ?1")
})

@Entity
@Table(name="usuarios_pagina")
public class UsuarioPagina implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id	@GeneratedValue
	private Integer id;
	
	private String pagina;
	
	private String descricao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
