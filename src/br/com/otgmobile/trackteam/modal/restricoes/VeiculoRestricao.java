package br.com.otgmobile.trackteam.modal.restricoes;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name="findVeiculoRestricao",query="select v from VeiculoRestricao v where v.placa like ?1 or v.chassi like ?2 or v.renavam like ?3")	
})

@Entity
@Table(name="restricao_veiculo")
public class VeiculoRestricao implements Serializable {

	private static final long serialVersionUID = -4847501395123301258L;

	@GeneratedValue @Id
	private Integer id;
	
	private String placa;
	private String chassi;
	private String renavam;
	private String descricao;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="restricao_id")		
	private Restricao restricao;
		
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getChassi() {
		return chassi;
	}
	public void setChassi(String chassi) {
		this.chassi = chassi;
	}
	public String getRenavam() {
		return renavam;
	}
	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Restricao getRestricao() {
		return restricao;
	}
	public void setRestricao(Restricao restricao) {
		this.restricao = restricao;
	}		
}
