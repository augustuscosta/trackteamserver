package br.com.otgmobile.trackteam.modal.restricoes;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@NamedQueries({
	@NamedQuery(name="findPessoaRestricao",query="select p from PessoaRestricao p where p.nome like ?1 or p.rg like ?2 or p.cpf like ?3 or p.cnh like ?4 or p.passaporte like ?5")	
})

@Entity
@Table(name = "restricao_pessoa")
public class PessoaRestricao implements Serializable {

	private static final long serialVersionUID = 9152565225208259244L;

	@GeneratedValue @Id
	private Integer id;

	private String rg;
	private String cpf;
	private String cnh;
	private String passaporte;
	private String nome;

	private String descricao;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="restricao_id")	
	private Restricao restricao;
	
	@Column(name="restricao_id", insertable=false, updatable=false)
	private Integer restricaoId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public String getPassaporte() {
		return passaporte;
	}

	public void setPassaporte(String passaporte) {
		this.passaporte = passaporte;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Restricao getRestricao() {
		return restricao;
	}

	public void setRestricao(Restricao restricao) {
		this.restricao = restricao;
	}

	public Integer getRestricaoId() {
		return restricaoId;
	}

	public void setRestricaoId(Integer restricaoId) {
		this.restricaoId = restricaoId;
	}
}
