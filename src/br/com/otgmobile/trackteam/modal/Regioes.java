package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="regioes")
public class Regioes implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1391120329352080255L;


	@Id @GeneratedValue
	private Integer id;
	
	
	@JoinColumn(name="nome")
	private String nome;
	
//	@OneToMany(mappedBy="regioes", cascade = CascadeType.ALL)   
	@OneToMany(cascade = CascadeType.ALL)
//	@JoinTable(name = "regioesGeoPonto", joinColumns="regiao_id")	
	@JoinColumn(name = "regiao_id")
	private List<GeoPontoRegioes> geoPonto;
	
	@Transient
	private GeoPontoRegioes centro;
	
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public List<GeoPontoRegioes> getGeoPonto() {
		return geoPonto;
	}


	public void setGeoPonto(List<GeoPontoRegioes> geoPonto) {
		this.geoPonto = geoPonto;
	}


	public GeoPontoRegioes getCentro() {
		return centro;
	}


	public void setCentro(GeoPontoRegioes centro) {
		this.centro = centro;
	}
	
}
