package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import br.com.otgmobile.trackteam.modal.usuario.Usuario;

@NamedQueries({	
	@NamedQuery(name="findLogPaginaTotal",query="select COUNT(e.id) from Log e where UPPER(e.descricao) like :filtro"),
	@NamedQuery(name="findLogPaginaTotalData",query="select COUNT(e.id) from Log e where UPPER(e.descricao) like :filtro and e.dataCriacao >= :inicio and e.dataCriacao <= :fim"),
	@NamedQuery(name="findLogPagina",query="select e from Log e where UPPER(e.descricao) like :filtro order by e.dataCriacao desc"),
	@NamedQuery(name="findLogPaginaData",query="select e from Log e where UPPER(e.descricao) like :filtro and e.dataCriacao >= :inicio and e.dataCriacao <= :fim order by e.dataCriacao desc")	
})

//SELECT * FROM sometable WHERE UPPER(textfield) LIKE (UPPER('value') || '%');

@Entity
@Table(name="logs")
public class Log implements Serializable {

	private static final long serialVersionUID = 5391106093975576483L;

	@GeneratedValue @Id
	private Integer id;
	
	private String controller;

	@Column(name="metodo")
	private String tipo;
	
	private String descricao;
	
	@Column(name="valor_novo",columnDefinition="LONGTEXT")
	private String valorNovo;
	
	@Column(name="valor_antigo",columnDefinition="LONGTEXT")
	private String valorAntigo;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="id_usuario")		
	private Usuario usuario;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Column(name="data_criacao")
	private Date dataCriacao;

	public Log() { }
	
	public Log(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getValorNovo() {
		return valorNovo;
	}

	public void setValorNovo(String valorNovo) {
		this.valorNovo = valorNovo;
	}

	public String getValorAntigo() {
		return valorAntigo;
	}

	public void setValorAntigo(String valorAntigo) {
		this.valorAntigo = valorAntigo;
	}
}
