package br.com.otgmobile.trackteam.modal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="rotaGeoPonto")
public class GeoPontoRota implements Serializable {

	private static final long serialVersionUID = -8732125154779617786L;

	@Id @GeneratedValue
	private Integer id;
	
	private String latitude;
	
	private String longitude;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="rota_id")
    private Rota rota;
    
	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
