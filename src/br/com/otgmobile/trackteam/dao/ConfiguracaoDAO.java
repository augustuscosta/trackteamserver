package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Configuracao;
import br.com.otgmobile.trackteam.modal.Token;

@Component
public class ConfiguracaoDAO {

	private EntityManager entityManager;
	
	public ConfiguracaoDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}

	@SuppressWarnings("unchecked")
	public List<Configuracao> findAll(Token token) {
	
		Query query1 = this.entityManager.createNamedQuery("findConfiguracaoAll");	
		List<Configuracao> resultado = query1.getResultList(); 
		
		Query query2 = this.entityManager.createNamedQuery("findConfiguracaoByToken");
		query2.setParameter("token", token.getToken());
		resultado.addAll(query2.getResultList()); 
		
		return resultado;
	}	
}
