package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Materiais;
import br.com.otgmobile.trackteam.modal.Objeto;


@Component
public class ObjetoDAO {

	private EntityManager entityManager;
	
	public ObjetoDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}
	
	/**
	 * Busca todos os Objetos do banco
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Objeto> findAll() {
	
		StringBuilder sql = new StringBuilder("select o from Objeto o order by o.id asc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		List<Objeto> resultado = query.getResultList(); 

		return resultado;
	}

	/**
	 * Traz um objeto pelo id
	 * 
	 * @return
	 */
	public Objeto find(Objeto Objeto) {
		
		StringBuilder sql = new StringBuilder("select o from Objeto o where o.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, Objeto.getId());
		
		@SuppressWarnings("unchecked")
		List<Objeto> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Objeto();
		}
	}

	/**
	 * Traz uma pagina de Objetos cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Objeto> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select o from Objeto o order by o.id asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Objeto> resultado = query.getResultList();
		
		return resultado;
	}
	
	/**
	 * 
	 * Retorna a quantidade total de Objetos cadastrados
	 * @return
	 */
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(o.id) from Objeto o");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();
	}
	
	/**
	 * Busca todas os estado do banco
	 * 
	 * @return
	 */
	public List<Dominio> findEstado() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeCampo = 'estado' and d.nomeTabela = 'objeto' order by d.id asc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList();

		return resultado;
	}
	
	/**
	 * Busca todos os materiais do banco
	 * 
	 * @return
	 */
	public List<Materiais> findMateriais() {
		
		StringBuilder sql = new StringBuilder("select m from Materiais m order by m.titulo");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Materiais> resultado = query.getResultList();

		return resultado;
	}

	/**
	 * Busca todos os materiais associados a uma funcionalidade do banco
	 * 
	 * @return
	 */
	public List<Materiais> findMateriais(Dominio funcionalidade) {
		
		return findMateriais(funcionalidade.getId());
	}
	
	/**
	 * Busca todos os materiais associados a uma funcionalidade do banco
	 * 
	 * @return
	 */
	public List<Materiais> findMateriais(Integer funcionalidade_id) {
		
		StringBuilder sql = new StringBuilder("select m from Materiais m where m.funcionalidade.id = ? order by m.titulo");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, funcionalidade_id);
	
		@SuppressWarnings("unchecked")
		List<Materiais> resultado = query.getResultList();

		return resultado;
	}
	
	
	/**
	 * Insere um Objeto no banco
	 * 
	 * @param objeto
	 * @return
	 */
	public Objeto insert(Objeto Objeto) {
		
		entityManager.persist(Objeto);
		return Objeto;
	}
	
	
	/**
	 * Altera um Objeto no banco de dados
	 * 
	 * @param objeto
	 */
	public Objeto update(Objeto Objeto) {
	
		entityManager.merge(Objeto);
		
		return Objeto;
	}
	
	
	/**
	 * Remove um Objeto no banco de dados
	 * 
	 * @param objeto
	 */
	public Objeto delete(Objeto Objeto) {
	
		entityManager.remove(Objeto);
		
		return Objeto;
	}
	
	public List<Objeto> findAllObjetosDisponiveisPagina(Integer quantidade,
			Integer pagina, Materiais material) {

		StringBuilder sql = new StringBuilder("select v.listaObjetos from Veiculo v");
		Query query = this.entityManager.createQuery(sql.toString());
		@SuppressWarnings("unchecked")
		List<Integer> listaObjetosVeiculos = query.getResultList();		
		
		StringBuilder sqlAgente = new StringBuilder("select a.listaObjetos from Agente a");
		Query queryAgente = this.entityManager.createQuery(sqlAgente.toString());		
		@SuppressWarnings("unchecked")
		List<Integer> listaObjetosAgente = queryAgente.getResultList();	
		
		StringBuilder sqlPonto = new StringBuilder("select i.listaObjetos from PontoDeInteresse i");
		Query queryPonto = this.entityManager.createQuery(sqlPonto.toString());		
		@SuppressWarnings("unchecked")
		List<Integer> listaObjetosPonto = queryPonto.getResultList();	
		
		if (listaObjetosPonto.isEmpty() && listaObjetosVeiculos.isEmpty() && !listaObjetosAgente.isEmpty()) {
			
			sql = new StringBuilder("select o from Objeto o where o.materiais.id = :id and " +
					"o NOT IN :listaAgentes " +
					"order by o.materiais.titulo asc");
			
			query = this.entityManager.createQuery(sql.toString());
			query.setParameter("listaAgentes", listaObjetosAgente);
			query.setParameter("id", material.getId());		
			
		}else if(!listaObjetosVeiculos.isEmpty() && listaObjetosAgente.isEmpty() && listaObjetosPonto.isEmpty()){
			
			sql = new StringBuilder("select o from Objeto o where o.materiais.id = :id and " +
					"o NOT IN :listaVeiculos " +
					"order by o.materiais.titulo asc");
			
			query = this.entityManager.createQuery(sql.toString());
			query.setParameter("listaVeiculos", listaObjetosVeiculos);
			query.setParameter("id", material.getId());		
			
		}else if(listaObjetosVeiculos.isEmpty() && listaObjetosAgente.isEmpty() && !listaObjetosPonto.isEmpty()){
			
			sql = new StringBuilder("select o from Objeto o where o.materiais.id = :id and " +
					"o NOT IN :listaPontos " +
					"order by o.materiais.titulo asc");
			
			query = this.entityManager.createQuery(sql.toString());
			query.setParameter("listaPontos", listaObjetosPonto);
			query.setParameter("id", material.getId());		
			
		}else if(!listaObjetosVeiculos.isEmpty() && !listaObjetosAgente.isEmpty() && listaObjetosPonto.isEmpty()){
			
		
			sql = new StringBuilder("select o from Objeto o where o.materiais.id = :id and " +
					"o NOT IN :listaVeiculos and o NOT IN :listaAgentes " +
					"order by o.materiais.titulo asc");
			
			query = this.entityManager.createQuery(sql.toString());
			query.setParameter("listaAgentes", listaObjetosAgente);
			query.setParameter("listaVeiculos", listaObjetosVeiculos);
			query.setParameter("id", material.getId());		
			
		}else if(listaObjetosVeiculos.isEmpty() && !listaObjetosAgente.isEmpty() && !listaObjetosPonto.isEmpty()){
			
		
			sql = new StringBuilder("select o from Objeto o where o.materiais.id = :id and " +
					"o NOT IN :listaPontos and o NOT IN :listaAgentes " +
					"order by o.materiais.titulo asc");
			
			query = this.entityManager.createQuery(sql.toString());
			query.setParameter("listaAgentes", listaObjetosAgente);
			query.setParameter("listaPontos", listaObjetosPonto);
			query.setParameter("id", material.getId());		
			
		}else if(!listaObjetosVeiculos.isEmpty() && listaObjetosAgente.isEmpty() && !listaObjetosPonto.isEmpty()){
			
		
			sql = new StringBuilder("select o from Objeto o where o.materiais.id = :id and " +
					"o NOT IN :listaVeiculos and o NOT IN :listaPontos " +
					"order by o.materiais.titulo asc");
			
			query = this.entityManager.createQuery(sql.toString());
			query.setParameter("listaPontos", listaObjetosPonto);
			query.setParameter("listaVeiculos", listaObjetosVeiculos);
			query.setParameter("id", material.getId());		
			
		}else{
			
			sql = new StringBuilder("select o from Objeto o where o.materiais.id = :id");
			query = this.entityManager.createQuery(sql.toString());			
			query.setParameter("id", material.getId());		
			
		}
		
//		query.setParameter("id", material.getId());		
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);

		@SuppressWarnings("unchecked")
		List<Objeto> resultado = query.getResultList();

		return resultado;
		
	}

	public Integer findTotalObjetosDisponiveis(Materiais material) {
		StringBuilder sql = new StringBuilder("select COUNT(o.id) from Objeto o where o.materiais.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, material.getId());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();
	}

	public List<Objeto> findDisponiveisByQuery(String queryString) {

		Query query = this.entityManager.createNamedQuery("findObjetoDisponiveisPorQuery");	
		query.setParameter(1, "%" + queryString.toUpperCase() + "%");
		query.setParameter(2, "%" + queryString.toUpperCase() + "%");
		query.setParameter(3, "%" + queryString.toUpperCase() + "%");
		
		@SuppressWarnings("unchecked")
		List<Objeto> resultado = query.getResultList();
		
		return resultado;
	}
		
}
