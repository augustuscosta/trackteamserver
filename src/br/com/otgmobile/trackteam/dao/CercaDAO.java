package br.com.otgmobile.trackteam.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.GeoPontoCerca;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.service.VeiculoService;

@Component
public class CercaDAO {

	private EntityManager entityManager;
	private final VeiculoService veiculoService;
	
	public CercaDAO(EntityManager entityManager,
			VeiculoService veiculoService) {
		
		this.entityManager = entityManager;
		this.veiculoService = veiculoService;
	}

	/**
	 * Busca todas as cercas do banco
	 * 
	 * @return
	 */
	public List<Cerca> findAll() {
	
		StringBuilder sql = new StringBuilder("select c from Cerca c order by c.nome asc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Cerca> resultado = query.getResultList(); 

		return resultado;
	}

	/**
	 * Busca uma cerca do banco
	 * 
	 * @return
	 */
	public Cerca find(Cerca cerca) {
	
		StringBuilder sql = new StringBuilder("select c from Cerca c where c.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, cerca.getId());
		
		@SuppressWarnings("unchecked")
		List<Cerca> resultado = query.getResultList(); 
		entityManager.detach(resultado);
		entityManager.flush();
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Cerca();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Cerca> findAllPagina(Integer quantidade, Integer pagina) {
						
		Query query = this.entityManager.createNamedQuery("findCercaPagina");	
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);

		List<Cerca> resultado = query.getResultList(); 
		
		return resultado;
	}

	public Integer findTotal() {

		Query query = this.entityManager.createNamedQuery("findCercaPaginaTotal");			
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}

	@SuppressWarnings("unchecked")
	public boolean existe(Cerca cerca) {
								
		Query query = this.entityManager.createNamedQuery("findCercaByNome");	
		query.setParameter(1, cerca.getNome());
		List<Cerca> resultado = query.getResultList();
				
		if (resultado.size() > 0) {
			
			return true;
		}
		
		return false;
	}
	
	public Cerca insert(Cerca cerca) {
		
		entityManager.persist(cerca);
		entityManager.flush();
		return cerca;		
	}
	
	public Cerca update(Cerca cerca) {
		
		entityManager.detach(cerca);
		
		Cerca cercaBD = new Cerca();
		cercaBD.setId(cerca.getId());
		cercaBD = find(cercaBD);

		List<GeoPontoCerca> listaPontosAntigos = cercaBD.getGeoPonto();
		cercaBD.setGeoPonto(null);

		for (GeoPontoCerca ponto : listaPontosAntigos) {
			
			entityManager.remove(ponto);
		}
	
		for(GeoPontoCerca ponto : cerca.getGeoPonto()) {
		
			ponto.setCerca(cerca);
		}
		
		cercaBD.setGeoPonto(cerca.getGeoPonto());
		cercaBD.setNome(cerca.getNome());
		
		entityManager.merge(cercaBD);
		entityManager.flush();
	
		return cerca;		
	}

	public List<Cerca> findAllByToken(Token token) {

		List<Cerca> listaCercas = new ArrayList<Cerca>();
		
		if (token.getVeiculo_id() != null && token.getVeiculo_id() > 0) {
		
			Veiculo veiculo = new Veiculo();
			veiculo.setId(token.getVeiculo_id());
						
			veiculo = veiculoService.find(veiculo);
			listaCercas.add(veiculo.getCerca());
		}
		
		return listaCercas;
	}	
}
