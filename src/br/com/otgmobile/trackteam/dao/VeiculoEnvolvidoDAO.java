package br.com.otgmobile.trackteam.dao;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.ocorrencia.VeiculoEnvolvido;

@Component
public class VeiculoEnvolvidoDAO {

	private EntityManager entityManager;
	
	public VeiculoEnvolvidoDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}	
	
	/**
	 * Traz todos os veiculos cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<VeiculoEnvolvido> findAll() {
				
		Query query = this.entityManager.createQuery("select v from VeiculoEnvolvido v").setMaxResults(100);		
		Collection<VeiculoEnvolvido> resultado = query.getResultList(); 
	
		return (List<VeiculoEnvolvido>) resultado;
	}
	
}
