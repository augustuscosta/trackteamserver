package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.ViaturaRota;

@Component
public class ViaturaRotaDao {

	private EntityManager entityManager;
	
	public ViaturaRotaDao(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}
	
	public void salvar(ViaturaRota viatura) {

		entityManager.persist(viatura);

	}
	
	public List<ViaturaRota> findAll() {

		Query query = this.entityManager.createQuery("select v from ViaturaRota v " +
				"where v.id IN (SELECT MAX(id) FROM ViaturaRota group by viatura_id)");
/*		
		SELECT * from cpbordo.viatura_rota WHERE id IN (
				  SELECT MAX(id) FROM cpbordo.viatura_rota group by viatura_id
				)
*/		
		@SuppressWarnings("unchecked")
		List<ViaturaRota> resultado = query.getResultList(); 

		return resultado;

	}	
}
