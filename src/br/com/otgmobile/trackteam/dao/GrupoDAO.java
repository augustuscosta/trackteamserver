package br.com.otgmobile.trackteam.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioGrupo;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioRole;

@Component
public class GrupoDAO {
	
	private EntityManager entityManager;

	public GrupoDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}

	public UsuarioGrupo Salvar(UsuarioGrupo usuarioGrupo) {
		
		entityManager.persist(usuarioGrupo);		
		return usuarioGrupo;
	}

	public UsuarioGrupo update(UsuarioGrupo usuarioGrupo) {
		
		entityManager.merge(usuarioGrupo);		
		return usuarioGrupo;
	}
	
	public UsuarioGrupo find(UsuarioGrupo usuarioGrupo) {

		StringBuilder sql = new StringBuilder("select ug from UsuarioGrupo ug where ug.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, usuarioGrupo.getId());
		
		@SuppressWarnings("unchecked")
		List<UsuarioGrupo> resultado = query.getResultList();
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new UsuarioGrupo();
		}	
	}
	
	public UsuarioGrupo findNome(UsuarioGrupo usuarioGrupo) {
		
		StringBuilder sql = new StringBuilder("select ug from UsuarioGrupo ug where ug.nome = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, usuarioGrupo.getNome());
		
		@SuppressWarnings("unchecked")
		List<UsuarioGrupo> resultado = query.getResultList();
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new UsuarioGrupo();
		}	
		
	}

	public List<UsuarioRole> findRoles() {
		
		Query query = this.entityManager.createNamedQuery("findAllPaginaRegra");	
		
		@SuppressWarnings("unchecked")
		List<UsuarioRole> resultado = query.getResultList(); 		
		
		if (resultado.size() > 0) {
			
			return resultado;
		}
		
		return new ArrayList<UsuarioRole>();
	}

	public UsuarioRole FindPagina(UsuarioRole paginas) {
		
		Query query = this.entityManager.createNamedQuery("findPaginaId");	
		query.setParameter(1, paginas.getId());
		
		@SuppressWarnings("unchecked")
		List<UsuarioRole> resultado = query.getResultList(); 		
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
		}	
		
		return null;
	}

	public List<UsuarioGrupo> findAllPaginaGrupos(Integer quantidade,
			Integer pagina) {
		
		Query query = this.entityManager.createNamedQuery("findAllPaginaGrupos");	
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		@SuppressWarnings("unchecked")
		List<UsuarioGrupo> resultado = query.getResultList(); 
		
		return resultado;
	}

	public Integer findTotalPaginasGrupos() {
		
		Query query = this.entityManager.createNamedQuery("findTotalPaginasGrupos");		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();
	}

	public void delete(UsuarioGrupo usuarioGrupo) {

		entityManager.remove(this.find(usuarioGrupo));
		entityManager.flush();		
	}	
}
