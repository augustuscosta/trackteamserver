package br.com.otgmobile.trackteam.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.GeoPontoRota;
import br.com.otgmobile.trackteam.modal.Rota;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;

@Component
public class RotaDAO {

	private EntityManager entityManager;
	private final AgenteDAO agenteDAO;
	private final VeiculoDao veiculoDao;
	
	public RotaDAO(EntityManager entityManager,
			AgenteDAO agenteDAO,
			VeiculoDao veiculoDao) {
		
		this.entityManager = entityManager;
		this.agenteDAO = agenteDAO;
		this.veiculoDao = veiculoDao;
	}

	/**
	 * Busca todas as rotas do banco
	 * 
	 * @return
	 */
	public List<Rota> findAll() {
	
		StringBuilder sql = new StringBuilder("select r from Rota r order by r.nome asc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Rota> resultado = query.getResultList(); 

		return resultado;
	}

	/**
	 * Busca todas as rotas do banco
	 * 
	 * @return
	 */
	public List<Rota> findByCerca(Cerca cerca) {
	
		System.out.println("opa");
		String sql = "select r from Rota r where r.cerca.id = ? order by r.nome asc";
		Query query = this.entityManager.createQuery(sql);
		query.setParameter(1, cerca.getId());
	
		@SuppressWarnings("unchecked")
		List<Rota> resultado = query.getResultList(); 

		return resultado;
	}
	
	/**
	 * Busca uma rota do banco
	 * 
	 * @return
	 */
	public Rota find(Rota rota) {
	
		StringBuilder sql = new StringBuilder("select r from Rota r where r.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, rota.getId());
		
		@SuppressWarnings("unchecked")
		List<Rota> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Rota();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Rota> findAllPagina(Integer quantidade, Integer pagina) {
						
		Query query = this.entityManager.createNamedQuery("findRotaPagina");	
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);

		List<Rota> resultado = query.getResultList(); 
		
		return resultado;
	}

	public Integer findTotal() {

		Query query = this.entityManager.createNamedQuery("findRotaPaginaTotal");			
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	public Rota insert(Rota rota) {
		
		entityManager.persist(rota);
		return rota;		
	}

	public List<Rota> findAllByToken(Token token) {

		List<Rota> listaRotas = new ArrayList<Rota>();
		
		if (token.getAgente_id() != null && token.getAgente_id() > 0) {
			
			Agente agente = new Agente();
			agente.setId(token.getAgente_id());
			agente = agenteDAO.find(agente);
			
//			listaRotas.add();
			
			
		} else if (token.getVeiculo_id() != null && token.getVeiculo_id() > 0) {

			Veiculo veiculo = new Veiculo();
			veiculo.setId(token.getVeiculo_id());
			veiculo = veiculoDao.find(veiculo);
			
			listaRotas.add(veiculo.getRota());
		}
		
		return listaRotas;
	}

	public Rota update(Rota rota) {

		entityManager.merge(rota);       
		entityManager.flush();
		
		return rota;
	}
	
	public Rota excluiGeoPontos(Rota rota) {

		for (GeoPontoRota geoPonto : rota.getGeoPonto()) {
			
			entityManager.remove(geoPonto);
		}
		
		rota.setGeoPonto(new ArrayList<GeoPontoRota>());
		return rota;
	}	
}