package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Escala;

@Component
public class EscalaDAO {

	private EntityManager entityManager;

	public EscalaDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@SuppressWarnings("unchecked")
	public List<Escala> findAll() {

		StringBuilder sql = new StringBuilder(
				"select e from Escala e order by entrada");
		Query query = this.entityManager.createQuery(sql.toString());

		List<Escala> resultado = query.getResultList();

		return resultado;
	}

	public Escala find(Escala escala) {

		StringBuilder sql = new StringBuilder(
				"select e from Escala e where e.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, escala.getId());

		@SuppressWarnings("unchecked")
		List<Escala> resultado = query.getResultList();

		if (resultado.size() > 0) {

			return resultado.get(0);
		} else {

			return new Escala();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Escala> findAllPagina(Integer quantidade, Integer pagina) {

		StringBuilder sql = new StringBuilder(
				"select e from Escala e order by entrada");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);

		List<Escala> resultado = query.getResultList();

		return resultado;
	}

	public Integer findTotal() {

		StringBuilder sql = new StringBuilder(
				"select COUNT(e.id) from Escala e");
		Query query = this.entityManager.createQuery(sql.toString());

		Number resultado = (Number) query.getSingleResult();

		return resultado.intValue();
	}

	public Escala insert(Escala escala) {
		entityManager.persist(escala);
		return escala;
	}

	public Escala update(Escala escala) {

		entityManager.merge(escala);

		return escala;
	}

	public Escala delete(Escala escala) {

		entityManager.remove(escala);
		entityManager.flush();

		return escala;
	}

}
