package br.com.otgmobile.trackteam.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Log;
import br.com.otgmobile.trackteam.modal.LogOperacao;

@Component
public class LogDAO extends GenericDao {

	public LogDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public LogOperacao find(String controller, String metodo) {
		
		Query query = this.entityManager.createNamedQuery("findLogOperacao");	
		query.setParameter("controller", controller);
		query.setParameter("metodo", metodo);
		
		LogOperacao resultado = null;

		try {
			
			resultado = (LogOperacao) query.getSingleResult();
			
		} catch (Exception e) {
//			e.printStackTrace();
		}
				
		return resultado;		
	}
	
	public Integer findPaginaTotal(String inicio, String fim, String filtro) {

		Query query;
		Date dataInicio;
		Date dataFim;
		
		if (filtro == null) {
			filtro = "";
		}
		
		filtro = filtro.toUpperCase();
		
		if (inicio == null || inicio.equals("")) {		
			
			query = this.entityManager.createNamedQuery("findLogPaginaTotal");
			
		} else {
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
			
			try {				
				dataInicio = sdf.parse(inicio + " 00:00:00");
			} catch (Exception e) {
				dataInicio = new Date();
			}

			try {
				dataFim = sdf.parse(fim + " 23:59:59");
			} catch (Exception e) {
				dataFim = new Date();
			}
			
			query = this.entityManager.createNamedQuery("findLogPaginaTotalData");
			query.setParameter("inicio", dataInicio);
			query.setParameter("fim", dataFim);
		}
		
		query.setParameter("filtro", "%" + filtro + "%");		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();
	}

	@SuppressWarnings("unchecked")
	public List<Log> findPagina(Integer quantidade, Integer pagina, String inicio, String fim, String filtro) {
		
		Query query;
		Date dataInicio;
		Date dataFim;
		
		if (filtro == null) {
			filtro = "";
		}
		
		filtro = filtro.toUpperCase();
		
		if (inicio == null || inicio.equals("")) {		
			
			query = this.entityManager.createNamedQuery("findLogPagina");
			
		} else {
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
			
			try {				
				dataInicio = sdf.parse(inicio + " 00:00:00");
			} catch (Exception e) {
				dataInicio = new Date();
			}

			try {
				dataFim = sdf.parse(fim + " 23:59:59");
			} catch (Exception e) {
				dataFim = new Date();
			}
			
			query = this.entityManager.createNamedQuery("findLogPaginaData");
			query.setParameter("inicio", dataInicio);
			query.setParameter("fim", dataFim);
		}
		
		query.setParameter("filtro", "%" + filtro + "%");
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);

		List<Log> resultado = query.getResultList(); 
		
		return resultado;
	}	
}
