package br.com.otgmobile.trackteam.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Endereco;

@Component
public class EnderecoDao {

	private EntityManager entityManager;

	public EnderecoDao(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}

	/**
	 * Adiciona um endereco no banco de dados 
	 * 
	 * @param endereco
	 * @return
	 */
	public Endereco insert(Endereco endereco) {
		
		entityManager.persist(endereco);
		return endereco;
	}	
}
