package br.com.otgmobile.trackteam.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	private Connection connection = null;

	public Connection getConnection() {

		try {
			
			Class.forName("org.postgresql.Driver");
//			String db = "jdbc:postgresql://192.168.0.188:5432/ocorrencia?user=postgres&password=foto@123";
			String db = "jdbc:postgresql://192.168.0.109:5432/ocorrencias?user=postgres&password=foto123postgres";
			
			connection = DriverManager.getConnection(db);

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}

		return connection;

	}
}
