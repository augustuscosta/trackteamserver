package br.com.otgmobile.trackteam.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.restricoes.PessoaRestricao;
import br.com.otgmobile.trackteam.modal.restricoes.VeiculoRestricao;

@Component
public class RestricaoDAO {

	private EntityManager entityManager;

	public RestricaoDAO(EntityManager entityManager) {		
		this.entityManager = entityManager;
	}
	
	public VeiculoRestricao insertVeiculo(VeiculoRestricao veiculo) {
		
		entityManager.persist(veiculo);		
		return veiculo;		
	}

	public PessoaRestricao insertPessoa(PessoaRestricao pessoa) {
		
		entityManager.persist(pessoa);		
		return pessoa;	
	}

	public List<PessoaRestricao> findPessoa(String consulta) {

// p.nome like ?1 and p.rg like ?2 and p.cpf like ?3 and p.cnh like ?4 and p.passaporte like ?5")		
		Query query = this.entityManager.createNamedQuery("findPessoaRestricao");	
		query.setParameter(1, "%" + consulta + "%");
		query.setParameter(2, "%" + consulta + "%");
		query.setParameter(3, "%" + consulta + "%");
		query.setParameter(4, "%" + consulta + "%");
		query.setParameter(5, "%" + consulta + "%");
		
		@SuppressWarnings("unchecked")
		List<PessoaRestricao> resultado = query.getResultList();
				
		if (resultado.size() > 0) {
			
			entityManager.flush();
			return resultado;
			
		} else {
			
			return new ArrayList<PessoaRestricao>();		
		}		
	}

	public List<VeiculoRestricao> findVeiculo(String placa, String chassi, String renavam) {

		Query query = this.entityManager.createNamedQuery("findVeiculoRestricao");	
		query.setParameter(1, "%" + placa + "%");
		query.setParameter(2, "%" + chassi + "%");
		query.setParameter(3, "%" + renavam + "%");
		
		@SuppressWarnings("unchecked")
		List<VeiculoRestricao> resultado = query.getResultList();
				
		if (resultado.size() > 0) {
			
			entityManager.flush();
			return resultado;
			
		} else {
			
			return new ArrayList<VeiculoRestricao>();		
		}		
	}
}
