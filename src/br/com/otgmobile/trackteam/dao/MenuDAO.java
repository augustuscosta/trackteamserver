package br.com.otgmobile.trackteam.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.SubMenu;

@Component
public class MenuDAO {

	private EntityManager entityManager;

	public MenuDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}

	public Menu findByTag(Menu menu) {

		StringBuilder sql = new StringBuilder("select m from Menu m where m.tag=?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, menu.getTag());
		
		@SuppressWarnings("unchecked")
		List<Menu> resultado = query.getResultList(); 
	
		if (resultado.size() > 0) {
		
			return resultado.get(0);
		} else {
			
			return menu;
		}		
	}
	
	public SubMenu findByTag(SubMenu subMenu) {

		StringBuilder sql = new StringBuilder("select m from SubMenu m where m.tag=? order by m.posicao asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, subMenu.getTag());
		
		@SuppressWarnings("unchecked")
		List<SubMenu> resultado = query.getResultList(); 
	
		if (resultado.size() > 0) {
		
			return resultado.get(0);
		} else {
			
			return subMenu;
		}		
	}
	
	public List<Menu> findByTipo(Menu menu) {

		StringBuilder sql = new StringBuilder("select m from Menu m where m.menu_id=? and m.ativo = true order by m.posicao");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, menu.getTipo());
		
		@SuppressWarnings("unchecked")
		List<Menu> resultado = query.getResultList(); 
		
		return resultado;
		
	}
	
	public List<SubMenu> findSubMenuByMenu(Menu menu) {
		
		StringBuilder sql = new StringBuilder("select m from SubMenu m where m.menu_id=? order by m.posicao ASC");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, menu.getId());
		
		@SuppressWarnings("unchecked")
		List<SubMenu> resultado = query.getResultList(); 

		return resultado;
		
	}	
	
	@SuppressWarnings("unchecked")
	public List<SubMenu> findSubMenuByTag(SubMenu subMenu) {
	
		StringBuilder sql;
		Query query;
		
		if (subMenu.getMenu_id() != null) {

			sql = new StringBuilder("select m from SubMenu m where m.menu_id=? order by m.posicao ASC");
			query = this.entityManager.createQuery(sql.toString());
			query.setParameter(1, subMenu.getMenu_id());
			
		} else {
			
			sql = new StringBuilder("select m from SubMenu m inner join m.listaSubMenu lm where lm.tag=? order by lm.posicao ASC");
			query = this.entityManager.createQuery(sql.toString());
			query.setParameter(1, subMenu.getTag());

		}		
		
		List<SubMenu> resultado = query.getResultList(); 
		
		if (resultado.size() > 0) {
			
			subMenu = resultado.get(0);

			Menu menu = new Menu();
			menu.setId(subMenu.getMenu_id());
			
			return findSubMenuByMenu(menu);			
		
		} else {
			
			return new ArrayList<SubMenu>();
		}		
	}	
	
	/**
	 * Altera um menu no banco de dados
	 * 
	 * @param menu
	 */
	public Menu update(Menu menu) {
	
		entityManager.merge(menu);
//		entityManager.flush();
		
		return menu;
	}
	
	/**
	 * Insere um menu no banco
	 * 
	 * @param menu
	 * @return
	 */
	public Menu insert(Menu menu) {
		
		entityManager.persist(menu);
		return menu;		
	}	
}
