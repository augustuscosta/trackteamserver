package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.GeoPontoRegioes;
import br.com.otgmobile.trackteam.modal.Regioes;

@Component
public class RegioesDAO {
	
	private EntityManager entityManager;

	public RegioesDAO(EntityManager entityManager) {		
		this.entityManager = entityManager;
	}
	
	public Regioes insert(Regioes regioes) {
		
		entityManager.persist(regioes);
		return regioes;
	}
	
	/**
	 * Traz uma pagina de regioes cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Regioes> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select a from Regioes a order by a.id");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Regioes> resultado = query.getResultList(); 
		
		return resultado;
	}

	public Integer findTotal() {
		
		StringBuilder sql = new StringBuilder("select COUNT(a.id) from Regioes a");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();	
	}

	public Regioes find(Regioes regioes) {
		StringBuilder sql = new StringBuilder("select r from Regioes r where r.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, regioes.getId());
		
		@SuppressWarnings("unchecked")
		List<Regioes> resultado = query.getResultList(); 
		entityManager.detach(resultado);
		entityManager.flush();
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Regioes();
		}
	}

	public List<Regioes> findAll() {
		
		StringBuilder sql = new StringBuilder("select r from Regioes r order by r.nome asc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Regioes> resultado = query.getResultList(); 

		return resultado;
	}

	public Regioes update(Regioes regioes) {
		
		entityManager.detach(regioes);
		
		Regioes regioesBD = new Regioes();
		regioesBD.setId(regioes.getId());
		regioesBD = find(regioesBD);

		List<GeoPontoRegioes> listaPontosAntigos = regioesBD.getGeoPonto();
		regioesBD.setGeoPonto(null);

		for (GeoPontoRegioes ponto : listaPontosAntigos) {
			
			entityManager.remove(ponto);
		}
/*	
		for(GeoPontoRegioes ponto : regioes.getGeoPonto()) {
		
			ponto.setRegioes(regioes);
		}
*/		
		regioesBD.setGeoPonto(regioes.getGeoPonto());
		regioesBD.setNome(regioes.getNome());
		
		entityManager.merge(regioesBD);
		entityManager.flush();
	
		return regioes;
	}

	@SuppressWarnings("unchecked")
	public boolean existe(Regioes regioes) {
		
		Query query = this.entityManager.createNamedQuery("findCercaByNome");	
		query.setParameter(1, regioes.getNome());
		List<Regioes> resultado = query.getResultList();
				
		if (resultado.size() > 0) {
			
			return true;
		}
		
		return false;
	}

	
	@SuppressWarnings("unchecked")
	public Regioes findRegioesExcluir(Regioes regioes) {			
		
			StringBuilder sql = new StringBuilder("select r from Regioes r ");
			sql.append("where r.id = ?");
			
			Query query = this.entityManager.createQuery(sql.toString());
			query.setParameter(1, regioes.getId());
			
			List<Regioes> resultado = query.getResultList();
			
			if (resultado.size() > 0) {
				entityManager.remove(resultado.get(0));
				entityManager.flush();
				return regioes;
			}
			return regioes;				
	}

	public Regioes findRegioesId(Integer id) {
		
		StringBuilder sql = new StringBuilder("select r from Regioes r where r.id = ?");
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, id);
		
		List<Regioes> resultado = query.getResultList();			
						
		return resultado.get(0);
	}
	
	public Regioes findRegioesNome(Regioes regioes){
		
		StringBuilder sql = new StringBuilder("select r from Regioes r where r.nome = ?");
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, regioes.getNome());
		
		List<Regioes> resultado = query.getResultList();			
						
		return resultado.get(0);		
		
	}
	
}
