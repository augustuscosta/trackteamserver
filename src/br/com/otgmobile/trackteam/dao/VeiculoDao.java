package br.com.otgmobile.trackteam.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Cerca;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Local;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.Rota;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;

@Component
public class VeiculoDao {

	private EntityManager entityManager;
	
	public VeiculoDao(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}	
	
	/**
	 * Traz todos os veiculos cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Veiculo> findAll() {
		
		Query query = this.entityManager.createQuery("select v from Veiculo v order by v.id desc");	
		Collection<Veiculo> resultado = query.getResultList(); 
	
		return (List<Veiculo>) resultado;
	}

	/**
	 * Traz todos os veiculos cadastrados com a posicao
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Veiculo> findAllComHistorico() {
		
		Query query = this.entityManager.createQuery("select v from Veiculo v where (v.latitude != null or v.longitude != null) order by v.id desc");
		
		Collection<Veiculo> resultado = query.getResultList(); 
/*	    
		for (Veiculo veiculo : resultado) {
			
			query = this.entityManager.createQuery("select h from Historico h where veiculo_id = ? and (h.latitude != '0') and (h.longitude != '0') order by h.dataHora asc limit 1");
			query.setParameter(1, veiculo.getId());

			veiculo.setHistorico(query.getResultList());	
		}
*/		
		return (List<Veiculo>) resultado;
	}
	
	@SuppressWarnings("unchecked")
	public List<Historico> findAllPosicao() {

		String sql = "SELECT h " +
		"FROM Historico h " +
		"WHERE id IN (SELECT MAX(h1.id) as id FROM Historico h1 GROUP BY h1.veiculo_id) and longitude != '0.0' and latitude != '0.0' " +
		"order by h.dataHora desc, h.veiculo_id";
		
		Query query = this.entityManager.createQuery(sql);
		
		Collection<Historico> resultado = query.getResultList(); 

		return (List<Historico>) resultado;
	}	
	

	/**
	 * Traz uma pagina de veiculos cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Veiculo> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select v from Veiculo v order by v.id asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Veiculo> resultado = query.getResultList(); 
		
		return resultado;
	}

	/**
	 * 
	 * Retorna a quantidade total de veiculos cadastrados
	 * @return
	 */
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(v.id) from Veiculo v");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	/**
	 * Traz todos os veiculos cadastrados
	 * 
	 * @return
	 */
	public Veiculo find(Veiculo veiculo) {
		
		StringBuilder sql = new StringBuilder("select v from Veiculo v where v.id = ? order by v.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, veiculo.getId());
		
		@SuppressWarnings("unchecked")
		List<Veiculo> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Veiculo();
		}
	}	
	
	/**
	 * Exclui um veiculo
	 * 
	 * @param veiculo
	 */
	public Veiculo excluirVeiculo(Veiculo veiculo) {
		
		entityManager.remove(veiculo);
		entityManager.flush();
		
		return veiculo;
	}
	
	/**
	 * Busca todos os tipos do banco
	 * 
	 * @return
	 */
	public List<Dominio> findTipo() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeTabela='veiculo' and d.nomeCampo='tipo'");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList(); 

		return resultado;
	}	
	
	/**
	 * Busca todos os locais do banco
	 * 
	 * @return
	 */
	public List<Local> findLocal() {
		
		StringBuilder sql = new StringBuilder("select l from Local l");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Local> resultado = query.getResultList(); 

		return resultado;
	}	

	/**
	 * Altera um veiculo no banco de dados
	 * 
	 * @param veiculo
	 */
	public Veiculo update(Veiculo veiculo) {
	
		entityManager.merge(veiculo);
		entityManager.flush();
		
		return veiculo;
	}
	
	/**
	 * Insere um veiculo no banco
	 * 
	 * @param agente
	 * @return
	 */
	public Veiculo insert(Veiculo veiculo) {
		
		entityManager.persist(veiculo);
		return veiculo;		
	}	
	
	/**
	 * Insere um historico no banco de dados
	 * 
	 * @param historico
	 * @return historico
	 */
	public Historico insertHistorico(Historico historico) {
		
		entityManager.persist(historico);
		return historico;	
	}

	public List<Objeto> findAllObjetosAssociadosPagina(Integer quantidade,
			Integer pagina, Veiculo veiculo) {
	
		StringBuilder sql = new StringBuilder("select v.listaObjetos from Veiculo v where v.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, veiculo.getId());
		
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Objeto> resultado = query.getResultList(); 
		
		return resultado;
		
		
	}

	public Integer findTotalObjetosAssociados(Veiculo veiculo) {
		
		StringBuilder sql = new StringBuilder("select v.listaObjetos from Veiculo v where v.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, veiculo.getId());
		
		List<Objeto> resultado = query.getResultList(); 
		
		return resultado.size();	
	}

	public List<Objeto> findAllObjetosDisponiveisPagina(Integer quantidade,
			Integer pagina, Integer id) {
		
		StringBuilder sql = new StringBuilder("select o from Objeto o, objeto_veiculo vo where o.id <> vo.objeto_id and vo.veiculo_id = ? order by o.id asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, id);
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);

		@SuppressWarnings("unchecked")
		List<Objeto> resultado = query.getResultList();

		return resultado;
		
	}

	public List<Veiculo> findAllByToken(Token token) {

		List<Veiculo> listaVeiculos = new ArrayList<Veiculo>();
		
		return listaVeiculos;
	}

	public List<Historico> findAllPosicaoByToken(Token token) {

		List<Historico> listaHistoricos = new ArrayList<Historico>();
		
		return listaHistoricos;
	}

	public Veiculo findByIdGps(String id) {

		Query query = this.entityManager.createNamedQuery("findByIdGps");	
		query.setParameter(1, id);
		@SuppressWarnings("unchecked")
		List<Veiculo> resultado = query.getResultList();
				
		if (resultado.size() > 0) {
			
			return resultado.get(0);
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Veiculo> findByPlacaRenavam(String query){
		try{
			StringBuilder sql = new StringBuilder("SELECT v FROM Veiculo v where placa LIKE ? OR renavam LIKE ?");
			Query q = this.entityManager.createQuery(sql.toString());
			q.setParameter(1, query);
			q.setParameter(2, query);
			
			return q.getResultList();
			
		}catch (NoResultException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Traz todos os veiculos cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Cerca findCerca(Cerca cerca) {
		
		Query query = this.entityManager.createQuery("select c from Cerca c where c.id = :id");
		query.setParameter("id", cerca.getId());
		
		Cerca resultado = null;
		
		try {
			resultado = (Cerca) query.getSingleResult();
			
		} catch (Exception e) {

		}
	
		return resultado;
	}
	
	public Cerca excluirCerca(Cerca cerca) {
		
		entityManager.remove(cerca);
		entityManager.flush();
		
		return cerca;
	}

	public Rota findRota(Rota rota) {
		
		Query query = this.entityManager.createQuery("select r from Rota r where r.id = :id");
		query.setParameter("id", rota.getId());
		
		Rota resultado = null;
		
		try {
			resultado = (Rota) query.getSingleResult();
			
		} catch (Exception e) {

		}
	
		return resultado;
	}
	
	public Rota excluirRota(Rota rota) {
		
		entityManager.remove(rota);
		entityManager.flush();
		
		return rota;
	}
}
