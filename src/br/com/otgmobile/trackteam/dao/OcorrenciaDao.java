package br.com.otgmobile.trackteam.dao;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.Veiculo;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciasVeiculo;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.service.EnderecoService;

@Component
public class OcorrenciaDao {

	private EntityManager entityManager;
	private EnderecoService enderecoService;
	
	public OcorrenciaDao(EntityManager entityManager, EnderecoService enderecoService) {
		
		this.entityManager = entityManager;
		this.enderecoService = enderecoService;
	}

	/**
	 * Busca todas ocorrencias do banco
	 * 
	 * @return
	 */
	public List<Ocorrencia> findOcorrencias() {
		
		StringBuilder sql = new StringBuilder("select o from Ocorrencia o order by o.id desc where o.horaEncerramento is null");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 

		return resultado;
	}
	
	/**
	 * Busca todas ocorrencias do banco
	 * 
	 * @return
	 */
	public List<Ocorrencia> findOcorrenciaAll(Date horaCriacao, Date horaCriacaoFim){
		
		StringBuilder sql = new StringBuilder("select distinct o from Ocorrencia o");		
		sql.append(" inner join o.niveisEmergencia n");
		sql.append(" where o.horaCriacao > ? and o.horaCriacao < ?");
		
		/*
		if (status != "todos") {
			sql.append(" and o.horaEncerramento is not null");
		} else {
			sql.append(" and o.horaEncerramento is null");
		}
		*/
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, horaCriacao);
		query.setParameter(2, horaCriacaoFim);
	
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 

		return resultado;
	}
	
	/**
	 * Busca todas ocorrencias ativas do banco
	 * 
	 * @return
	 */
	public List<Ocorrencia> findOcorrenciasAtivas() {
		
		StringBuilder sql = new StringBuilder("select o from Ocorrencia o where hora_encerramento is null and flag <= 100 order by o.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 

		return resultado;
	}
	
	/**
	 * Busca todas ocorrencias do banco
	 * 
	 * @return
	 */
	
	public Ocorrencia findOcorrencia(Ocorrencia ocorrencia) {

		StringBuilder sql = new StringBuilder("select o from Ocorrencia o ");
		sql.append("where o.id = ?");

		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, ocorrencia.getId());
				
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 
		
		if (resultado.size() > 0) {
			
//			this.entityManager.detach(resultado.get(0));
			return resultado.get(0);
/*			
			entityManager.detach(resultado.get(0));	
			Ocorrencia ocor = new Ocorrencia();
			ocor = resultado.get(0);
			
			return ocor;
*/			
		} else {
			
			return new Ocorrencia();
		}
		
	}

	/**
	 * Busca a ocorrencia associada a um veiculo
	 * 
	 * @return
	 */
	public List<Ocorrencia> findOcorrenciaByVeiculo(Veiculo veiculo) {
/*			
   Query query = entityManager.createQuery("select b from Business b " +
                        "left join fetch b.campaigns c " +
                        "left join fetch c.promotions where b.id=:id");
query.setParameter("id", b.getId());

*/		
		StringBuilder sql = new StringBuilder("select distinct o from Ocorrencia o " +
				"inner join o.ocorrenciasVeiculos ov " +
				"inner join ov.veiculo v " +
				"where v.id = ? and o.horaEncerramento = null");				

		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, veiculo.getId());
				
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 
		
		return resultado;		
	}

	
	/**
	 * Busca a ocorrencia abertas ou fechadas, associadas a um veiculo
	 * 
	 * @return
	 */
	public List<Ocorrencia> findOcorrenciaAbertasFechadasByVeiculo(Veiculo veiculo, Date inicio, Date fim, String status, int nivel) {
		
		
		StringBuilder sql = new StringBuilder("select distinct o from Ocorrencia o ");
				sql.append("inner join o.ocorrenciasVeiculos ov ");
				sql.append("inner join ov.veiculo v ");
				sql.append("inner join o.niveisEmergencia n ");
				sql.append(" where v.id = ? and o.horaCriacao > ? and o.horaCriacao < ? and n.id = ?");
		
		if (status.equals("encerrada")) {
			sql.append(" and o.horaEncerramento is not null");
		} else {
			sql.append(" and o.horaEncerramento is null");
		}

		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, veiculo.getId());
		query.setParameter(2, inicio);
		query.setParameter(3, fim);
		query.setParameter(4, nivel);
				
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	/**
	 * Busca a ocorrencia associada a um agente
	 * 
	 * @return
	 */
	public List<Ocorrencia> findOcorrenciaByAgente(Agente agente) {
	
		Query query = this.entityManager.createQuery("select distinct o from Ocorrencia o " +
				"inner join o.ocorrenciaAgentes ov " +
				"inner join ov.agente a " +
				"where a.id = :agente and o.horaEncerramento = null");
		query.setParameter("agente", agente.getId());
		
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	/**
	 * Busca a ocorrencia associada a um agente
	 * 
	 * @return
	 */
	public List<Ocorrencia> findOcorrenciaByAgente(Agente agente, Date inicio, Date fim, String status, int nivel) {
		
		
		StringBuilder sql = new StringBuilder("select distinct o from Ocorrencia o inner join o.ocorrenciaAgentes oa");		
		sql.append(" inner join oa.agente v inner join o.niveisEmergencia n");
		sql.append(" where v.id = ? and o.horaCriacao > ? and o.horaCriacao < ?  and n.id = ?");
		sql.append(" order by o.horaCriacao");
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, agente.getId());
		query.setParameter(2, inicio);
		query.setParameter(3, fim);
		query.setParameter(4, nivel);
		
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	public List<Ocorrencia> findOcorrenciaByVeiculo(Veiculo veiculo, Date inicio, Date fim, int nivel) {
		
		StringBuilder sql = new StringBuilder("select distinct o from Ocorrencia o inner join o.ocorrenciasVeiculos ov");		
		sql.append(" inner join ov.veiculo v inner join o.niveisEmergencia n");
		sql.append(" where v.id = ? and o.horaCriacao > ? and o.horaCriacao < ?  and n.id = ?");
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, veiculo.getId());
		query.setParameter(2, inicio);
		query.setParameter(3, fim);
		query.setParameter(4, nivel);
		
		@SuppressWarnings("unchecked")
		List<Ocorrencia> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	/**
	 * Traz uma pagina de ocorrencias cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Ocorrencia> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select o from Ocorrencia o order by o.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Ocorrencia> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	@SuppressWarnings("unchecked")
	public List<Ocorrencia> findAllAtivaPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select o from Ocorrencia o where hora_encerramento is null and flag <= 100 order by o.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Ocorrencia> resultado = query.getResultList(); 
		
		return resultado;
	}

	/**
	 * 
	 * Retorna a quantidade total de ocorrencias cadastrados
	 * @return
	 */
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(o.id) from Ocorrencia o");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	public Integer findTotalAtiva() {

		StringBuilder sql = new StringBuilder("select COUNT(o.id) from Ocorrencia o where hora_encerramento is null and flag <= 100");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	/**
	 * Insere ocorrencia no banco de dados
	 * 
	 * @param ocorrencia
	 */
	public Ocorrencia insert(Ocorrencia ocorrencia) {
		
		if (ocorrencia.getHoraCriacao() == null) {			
			ocorrencia.setHoraCriacao(new Timestamp(new Date().getTime()));
		}
		
		if (ocorrencia.getEndereco().getId() == null || ocorrencia.getEndereco().getId() == 0) {
			
			ocorrencia.setEndereco(enderecoService.insert(ocorrencia.getEndereco()));
		}
	
		entityManager.persist(ocorrencia);
		entityManager.flush();

		return ocorrencia;
	}

	/**
	 * Altera uma ocorrencia no banco de dados
	 * 
	 * @param ocorrencia
	 */
	public Ocorrencia update(Ocorrencia ocorrencia) {

		entityManager.merge(ocorrencia);
		entityManager.flush();
		
		return ocorrencia;
	}

	/**
	 * Cancela uma ocorrencia
	 * 
	 * @param ocorrencia
	 */
	public Ocorrencia cancelaOcorrencia(Ocorrencia ocorrencia) {
		
		ocorrencia = findOcorrencia(ocorrencia);
		
		List<Dominio> listaStatus = findStatus();
		
		for(Dominio status : listaStatus) {
			
			if ("CANCELADO".equals(status.getValor().toUpperCase())) {
				ocorrencia.setOcorrenciasStatus(status);
			}
		}
		
		entityManager.persist(ocorrencia);
		
		return ocorrencia;
		
	}
	
	/**
	 * Busca todos os status do banco
	 * 
	 * @return
	 */
	public List<Dominio> findStatus() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeTabela='status' and d.nomeCampo='ocorrencia'");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList(); 

		return resultado;
	}

	/**
	 * Busca todos os status do banco
	 * 
	 * @return
	 */
	public List<Dominio> findVitimaTipo() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeTabela='vitima' and d.nomeCampo='condicao'");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList(); 

		return resultado;
	}
	
	/**
	 * Busca todas as naturezas do banco
	 * 
	 * @return
	 */
	public List<Dominio> findNatureza() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeTabela='natureza'");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList(); 

		return resultado;
	}

	public List<NiveisEmergencia> findNivelEmergencia() {
		
		StringBuilder sql = new StringBuilder("select n from NiveisEmergencia n order by n.id");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<NiveisEmergencia> resultado = query.getResultList(); 

		return resultado;
	}

	public List<Usuario> findUsuarios() {
		
		StringBuilder sql = new StringBuilder("select u from Ocorrencia o join o.usuario u where hora_encerramento is null order by u.nome");		
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Usuario> resultado = query.getResultList(); 

		return resultado;
	}
	
	/**
	 * Busca todas as gravidades do banco
	 * 
	 * @return
	 */
	public List<Dominio> findGravidade() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeTabela='gravidade'");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList(); 

		return resultado;
	}

	public List<OcorrenciasVeiculo> findOcorrenciaVeiculos(Ocorrencia ocorrencia) {

		Query query = this.entityManager.createNamedQuery("findOcorrenciaVeiculos");	
		query.setParameter(1, ocorrencia.getId());
			
		@SuppressWarnings("unchecked")
		List<OcorrenciasVeiculo> resultado = query.getResultList(); 

		return resultado;
	}
	
	public List<Ocorrencia> findOcorrenciasAtivasByToken(Token token) {

		Query query1 = this.entityManager.createNamedQuery("findOcorrenciasAtivasByVeiculo");	
		query1.setParameter("veiculo", token.getVeiculo_id());

		Query query2 = this.entityManager.createNamedQuery("findOcorrenciasAtivasByAgente");	
		query2.setParameter("agente", token.getAgente_id());

		List<Ocorrencia> resultadoList = query1.getResultList();	
		resultadoList.addAll(query2.getResultList()); 
		
		Set<Ocorrencia> resultadoSet = new HashSet<Ocorrencia>();
		resultadoSet.addAll(resultadoList);
		
		List<Ocorrencia> resultado = new ArrayList<Ocorrencia>();
		resultado.addAll(resultadoSet);
		
		return resultado;
	}

	
	public Ocorrencia findOcorrenciaEmAtendimentoByToken(Token token) {

		Query query1 = this.entityManager.createNamedQuery("findOcorrenciasEmAtendimentoByVeiculo");	
		query1.setParameter("veiculo", token.getVeiculo_id());

		Query query2 = this.entityManager.createNamedQuery("findOcorrenciasEmAtendimentoByAgente");	
		query2.setParameter("agente", token.getAgente_id());

		List<Ocorrencia> resultadoList = query1.getResultList();	
		resultadoList.addAll(query2.getResultList());
		
		Set<Ocorrencia> resultadoSet = new HashSet<Ocorrencia>();
		resultadoSet.addAll(resultadoList);
		
		List<Ocorrencia> resultado = new ArrayList<Ocorrencia>();
		resultado.addAll(resultadoSet);
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
			
		} else {
			
			return null;		
		}		
	}
	
	public Dominio findDominio(Integer id) {
		
		Query query = this.entityManager.createNamedQuery("findDominioById");	
		query.setParameter(1, id);
			
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList(); 
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
			
		} else {
						
			return new Dominio();		
		}
		
	}

	public List<Dominio> findMeioCadastro() {

		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeTabela='meio_ocorrencia'");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList(); 

		return resultado;
		
	}

	public Integer findTotalPorVeiculo(Veiculo veiculo) {

		Query query = this.entityManager.createNamedQuery("findOcorrenciasTotalPorVeiculo");	
		query.setParameter("veiculo", veiculo.getId());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();
	}
}
