package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Pergunta;

@Component
public class PerguntaDAO {

	private EntityManager entityManager;
	
	public PerguntaDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public List<Pergunta> findAll() {
	
		StringBuilder sql = new StringBuilder("select a from Pergunta a order by a.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		List<Pergunta> resultado = query.getResultList(); 	
		
		return resultado;
	}
	
	public Pergunta find(Pergunta pergunta) {
		
		StringBuilder sql = new StringBuilder("select a from Pergunta a where a.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, pergunta.getId());
		
		@SuppressWarnings("unchecked")
		List<Pergunta> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Pergunta();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Pergunta> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select a from Pergunta a order by a.nome asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Pergunta> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(a.id) from Pergunta a");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	public Pergunta update(Pergunta ponto) {
	
		entityManager.merge(ponto);
		
		return ponto;
	}
	
	public Pergunta insert(Pergunta ponto) {
		entityManager.persist(ponto);
		return ponto;		
	}
	


	public Pergunta findDeletarPergunta(Pergunta pergunta) {
		
		StringBuilder sql = new StringBuilder("select a from Pergunta a where a.id = ?");		
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, pergunta.getId());
		
		@SuppressWarnings("unchecked")
		List<Pergunta> resultado = query.getResultList();
		
		if (resultado.size() > 0) {
			entityManager.remove(resultado.get(0));
			entityManager.flush();
			return pergunta;
		}
		
		return pergunta;	
	}
}
