package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Enquete;

@Component
public class EnqueteDAO {

	private EntityManager entityManager;
	
	public EnqueteDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public List<Enquete> findAll() {
	
		StringBuilder sql = new StringBuilder("select a from Enquete a order by a.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		List<Enquete> resultado = query.getResultList(); 	
		
		return resultado;
	}
	
	public Enquete find(Enquete enquete) {
		
		StringBuilder sql = new StringBuilder("select a from Enquete a where a.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, enquete.getId());
		
		@SuppressWarnings("unchecked")
		List<Enquete> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Enquete();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Enquete> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select a from Enquete a order by a.nome asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Enquete> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(a.id) from Enquete a");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	public Enquete update(Enquete ponto) {
	
		entityManager.merge(ponto);
		
		return ponto;
	}
	
	public Enquete insert(Enquete ponto) {
		entityManager.persist(ponto);
		return ponto;		
	}
	


	public Enquete findDeletarEnquete(Enquete enquete) {
		
		StringBuilder sql = new StringBuilder("select a from Enquete a where a.id = ?");		
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, enquete.getId());
		
		@SuppressWarnings("unchecked")
		List<Enquete> resultado = query.getResultList();
		
		if (resultado.size() > 0) {
			entityManager.remove(resultado.get(0));
			entityManager.flush();
			return enquete;
		}
		
		return enquete;	
	}
}
