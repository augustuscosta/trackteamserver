package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Materiais;


@Component
public class MateriaisDAO {

	private EntityManager entityManager;
	
	public MateriaisDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}
	
	/**
	 * Busca todos os Materiais do banco
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Materiais> findAll() {
	
		StringBuilder sql = new StringBuilder("select m from Materiais m order by m.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		List<Materiais> resultado = query.getResultList(); 

		return resultado;
	}

	/**
	 * Busca Materiais por funcionalidade
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Materiais> findByFuncionalidade(Dominio funcionalidade) {
	
		StringBuilder sql = new StringBuilder("select m from Materiais m where m.funcionalidade.id = ? order by m.titulo asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, funcionalidade.getId());
	
		List<Materiais> resultado = query.getResultList(); 

		return resultado;
	}
	
	/**
	 * Traz um material pelo id
	 * 
	 * @return
	 */
	public Materiais find(Materiais materiais) {
		
		StringBuilder sql = new StringBuilder("select m from Materiais m where m.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, materiais.getId());
		
		@SuppressWarnings("unchecked")
		List<Materiais> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Materiais();
		}
	}

	/**
	 * Traz uma pagina de materiais cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Materiais> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select m from Materiais m order by m.id asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Materiais> resultado = query.getResultList();
		
		return resultado;
	}
	
	/**
	 * 
	 * Retorna a quantidade total de materiais cadastrados
	 * @return
	 */
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(m.id) from Materiais m");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();
	}
	
	/**
	 * Busca todas as funcionalidades do banco
	 * 
	 * @return
	 */
	public List<Dominio> findFuncionalidade() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeCampo = 'descricao' and d.nomeTabela = 'funcionalidade' order by d.id asc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList();

		return resultado;
	}
	
	
	/**
	 * Insere um Material no banco
	 * 
	 * @param material
	 * @return
	 */
	public Materiais insert(Materiais materiais) {
		
		entityManager.persist(materiais);
		return materiais;
	}
	
	
	/**
	 * Altera um Material no banco de dados
	 * 
	 * @param material
	 */
	public Materiais update(Materiais materiais) {
	
		entityManager.merge(materiais);
		
		return materiais;
	}
	
	
	/**
	 * Remove um Material no banco de dados
	 * 
	 * @param material
	 */
	public Materiais delete(Materiais materiais) {
	
		entityManager.remove(materiais);
		
		return materiais;
	}
	
	
		
}
