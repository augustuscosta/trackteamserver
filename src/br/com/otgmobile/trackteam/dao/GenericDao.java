package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.util.StringUtils;

@Component
public class GenericDao {

	public final EntityManager entityManager;
	
	public GenericDao(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}
	
	/**
	 * Busca um objeto genérico pela chave primária
	 * 
	 * @param objeto
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> E find(E objeto) { 
		PersistenceUnitUtil util = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
		Object idPrimaryKey = util.getIdentifier(objeto);
			
		return (E) entityManager.find(objeto.getClass(), idPrimaryKey);
	}

	/**
	 * Busca um objeto genérico pela chave campo
	 * 
	 * @param campo
	 * @param objeto
	 * @return
	 */
	public <E> E findBy(String campo, E objeto) {
       
		E doReturn = null;
		String campoGet = "get" + StringUtils.capitalize(campo);
			
		try {
			Object valor = objeto.getClass().getMethod(campoGet).invoke(objeto);
			Query query = entityManager.createQuery("select g FROM " + objeto.getClass().getName() + " g where g." + campo + " = :campo" );
			query.setParameter("campo", valor);
			
			doReturn = getResultAsSingle(query);
			
		} catch (Exception e) {
//			e.printStackTrace();
		} 		
        
        return doReturn;
	}
	
	/**
	 * Busca todos os elementos do tipo do objeto
	 * 
	 * @param objeto
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> List<E> findAll(E objeto) {

		Query query = entityManager.createQuery("select g FROM " + objeto.getClass().getName() + " g" );
			
		return query.getResultList();
	}

	/**
	 * Insere um elemento ao banco
	 * 
	 * @param objeto
	 * @return
	 */
	public <E> E insert(E objeto) {
		entityManager.persist(objeto);
		entityManager.flush();
		return objeto;
	}

	/**
	 * Faz o update de um elemento no banco
	 * 
	 * @param objeto
	 * @return
	 */
	public <E> E update(E objeto) {

		entityManager.merge(objeto);
		entityManager.flush();
		
		return objeto;
	}

	/**
	 * Exclui um elemento do banco
	 * 
	 * @param objeto
	 * @return
	 */
	public <E> E remove(E objeto) {

		entityManager.remove(objeto);
		entityManager.flush();
		
		return objeto;
	}
	
	/**
	 * Executa a query e retorna um elemento
	 * 
	 * @param query
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> E getResultAsSingle(Query query) {

		E resultado = (E) query.getSingleResult();
		
		return resultado;
	}

	/**
	 * Executa a query e retorna uma lista de elemento
	 * 
	 * @param query
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> List<E> getResultAsArray(Query query) {

		List<E> resultado = query.getResultList();
		
		return resultado;
	}

}
