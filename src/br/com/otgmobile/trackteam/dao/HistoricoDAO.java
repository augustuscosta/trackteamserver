package br.com.otgmobile.trackteam.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.Veiculo;

@Component
public class HistoricoDAO {

	private EntityManager entityManager;

	public HistoricoDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Historico insert(Historico historico) {

		entityManager.persist(historico);
		return historico;
	}

	public List<Historico> getHistorico(Veiculo veiculo, Date inicio, Date fim) {
		
		Query query = this.entityManager.createNamedQuery("findHistoricoPorVeiculo");	
		query.setParameter(1, veiculo.getId());
		query.setParameter(2, inicio);
		query.setParameter(3, fim);
			
		@SuppressWarnings("unchecked")
		List<Historico> resultado = query.getResultList(); 

		return resultado;	
	}	
	
	public Historico getUltimoHistorico(Veiculo veiculo) {
			
		Query query = this.entityManager.createNamedQuery("findHistoricoUltimoPorVeiculo");	
		query.setParameter(1, veiculo.getId());
		query.setMaxResults(1);
			
		@SuppressWarnings("unchecked")
		List<Historico> resultado = query.getResultList(); 
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);		
			
		} else {
			
			return null;
		}			
	}		
}
