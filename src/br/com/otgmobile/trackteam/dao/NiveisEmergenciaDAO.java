package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;

@Component
public class NiveisEmergenciaDAO {

	private EntityManager entityManager;
	
	public NiveisEmergenciaDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}
	
	/**
	 * Busca um nivel de emergencia por ID do banco
	 * 
	 * @return
	 */
	public NiveisEmergencia find(NiveisEmergencia niveisEmergencia) {
			
		StringBuilder sql = new StringBuilder("select n from NiveisEmergencia n ");
		sql.append("where n.id = ?");

		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, niveisEmergencia.getId());
				
		@SuppressWarnings("unchecked")
		List<NiveisEmergencia> resultado = query.getResultList(); 

		if (resultado.size() > 0) {

			return resultado.get(0);
		} else {
			
			return new NiveisEmergencia();
		}
		
	}	
	
}
