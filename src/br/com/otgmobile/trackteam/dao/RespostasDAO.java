package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.RespostaEnquete;


@Component
public class RespostasDAO {

	private EntityManager entityManager;
	
	public RespostasDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public List<RespostaEnquete> findAll() {
	
		StringBuilder sql = new StringBuilder("select m from RespostaEnquete m order by m.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		List<RespostaEnquete> resultado = query.getResultList(); 

		return resultado;
	}
	
	public RespostaEnquete find(RespostaEnquete resposta) {
		
		StringBuilder sql = new StringBuilder("select m from RespostaEnquete m where m.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, resposta.getId());
		
		@SuppressWarnings("unchecked")
		List<RespostaEnquete> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new RespostaEnquete();
		}
	}

	@SuppressWarnings("unchecked")
	public List<RespostaEnquete> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select m from RespostaEnquete m order by m.id asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<RespostaEnquete> resultado = query.getResultList();
		
		return resultado;
	}
	
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(m.id) from RespostaEnquete m");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();
	}
	
	
	public RespostaEnquete insert(RespostaEnquete resposta) {
		
		entityManager.persist(resposta);
		return resposta;
	}
	
	public RespostaEnquete update(RespostaEnquete resposta) {
	
		entityManager.merge(resposta);
		
		return resposta;
	}
	
	public RespostaEnquete delete(RespostaEnquete resposta) {
	
		entityManager.remove(resposta);
		
		return resposta;
	}
	
	
		
}
