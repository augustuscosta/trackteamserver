package br.com.otgmobile.trackteam.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioGrupo;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioPagina;
import br.com.otgmobile.trackteam.modal.usuario.UsuarioRole;

@Component
public class UsuarioDao {

	private EntityManager entityManager;

	public UsuarioDao(EntityManager entityManager) {

		this.entityManager = entityManager;
	}

	/**
	 * Busca um usuario pelo login
	 * 
	 * @return
	 */
	public Usuario findByLogin(Usuario usuario) {

		Query query = this.entityManager
				.createQuery("select u from Usuario u where u.login=?");
		query.setParameter(1, usuario.getLogin().toLowerCase());

		@SuppressWarnings("unchecked")
		List<Usuario> resultado = query.getResultList();

		if (resultado.size() > 0) {

			return resultado.get(0);
		} else {

			return new Usuario();
		}
	}
	
	public Usuario findByMatricula(Usuario usuario) {

		Query query = this.entityManager
				.createQuery("select u from Usuario u where u.matricula=?");
		query.setParameter(1, usuario.getMatricula());

		@SuppressWarnings("unchecked")
		List<Usuario> resultado = query.getResultList();

		if (resultado.size() > 0) {

			return resultado.get(0);
		} else {

			return new Usuario();
		}
	}

	public Usuario insert(Usuario usuario) {

		entityManager.persist(usuario);
		return usuario;
	}

	/**
	 * Busca todos os grupos do banco
	 * 
	 * @return
	 */
	public List<UsuarioGrupo> findGrupos() {

		StringBuilder sql = new StringBuilder(
				"select g from UsuarioGrupo g order by g.nome");
		Query query = this.entityManager.createQuery(sql.toString());

		@SuppressWarnings("unchecked")
		List<UsuarioGrupo> resultado = query.getResultList();

		return resultado;
	}
	
	public UsuarioRole findRole(UsuarioRole role) {
		
		Query query = this.entityManager.createNamedQuery("findRole");
		query.setParameter(1, role.getId());
		UsuarioRole resultado = null;
		
		try{
			 resultado =  (UsuarioRole) query.getSingleResult();
		}catch(Exception ex){
			return null;
		}
		
		return resultado;

	}
	
	/**
	 * Bosca uma role pelo id do banco de dados
	 * 
	 * @return role do Usuario
	 */
	public UsuarioRole findRoleById(Integer id) {
		
		Query query = this.entityManager.createNamedQuery("findRoleById");
		query.setParameter(1, id);
		UsuarioRole resultado = null;
		try{
			 resultado =  (UsuarioRole) query.getSingleResult();
		}catch(Exception ex){
			return null;
		}
		
		return resultado;

	}
	
	/**
	 * Bosca todas as roles do banco de dados
	 * 
	 * @return Lista de Role
	 */
	public List<UsuarioRole> findRoles() {

		StringBuilder sql = new StringBuilder(
				"select r from UsuarioRole r order by r.role");
		Query query = this.entityManager.createQuery(sql.toString());

		@SuppressWarnings("unchecked")
		List<UsuarioRole> resultado = query.getResultList();

		return resultado;

	}
	
	/**
	 * O Usuario que representado pelo id passado.
	 * @param id do Usuario
	 * @return O Usuario pertecente ao id.
	 */
	public Usuario findById(Integer id) {

		Query query = this.entityManager.createQuery("select u from Usuario u where u.id=?");
		query.setParameter(1, id);

		@SuppressWarnings("unchecked")
		List<Usuario> resultado = query.getResultList();

		if (resultado.size() > 0) {

			return resultado.get(0);
		} else {

			return new Usuario();
		}
	}
	
	public UsuarioRole updateRole(UsuarioRole role){
		entityManager.merge(role);		
		return role;		
	}

	/**
	 * Busca todos os status do banco
	 * 
	 * @return
	 */
	public List<Dominio> findStatus() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeTabela='status' and d.nomeCampo='usuario'");
		Query query = this.entityManager.createQuery(sql.toString());

		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList();

		return resultado;
	}

	/**
	 * Retorna a quantidade total de usuarios cadastrados.
	 * 
	 * @return A quantidade de Usuarios cadastrados.
	 */
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(a.id) from Usuario a");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	/**
	 * Traz uma pagina de usuarios cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Usuario> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select a from Usuario a order by a.nome asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Usuario> resultado = query.getResultList(); 
		
		return resultado;
	}


	public Usuario update(Usuario usuario) {
		
		entityManager.persist(usuario);
		entityManager.flush();

		return usuario;
	}

	public Usuario find(Usuario usuario) {
		
		StringBuilder sql = new StringBuilder("select a from Usuario a where a.id = ?");

		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, usuario.getId());
		
		@SuppressWarnings("unchecked")
		List<Usuario> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Usuario();
		}
		
	}

	public Usuario findByCpf(Usuario usuario) {
		
		Query query = this.entityManager
				.createQuery("select u from Usuario u where u.cpf=?");
		query.setParameter(1, usuario.getCpf());

		@SuppressWarnings("unchecked")
		List<Usuario> resultado = query.getResultList();

		if (resultado.size() > 0) {

			return resultado.get(0);
		} else {

			return new Usuario();
		}
	}

	public Usuario findDeletarUsuarios(Usuario usuario) {
		
		StringBuilder sql = new StringBuilder("select a from Usuario a where a.id = ?");		
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, usuario.getId());
		
		@SuppressWarnings("unchecked")
		List<Usuario> resultado = query.getResultList();
		
		if (resultado.size() > 0) {
			entityManager.remove(resultado.get(0));
			entityManager.flush();
			return usuario;
		}
		return usuario;	
	}

	public List<UsuarioPagina> findPaginas() {
		
		Query query = this.entityManager.createNamedQuery("findPaginas");	
		
		@SuppressWarnings("unchecked")
		List<UsuarioPagina> resultado = query.getResultList(); 		
		
		if (resultado.size() > 0) {
			
			return resultado;
		}
		
		return new ArrayList<UsuarioPagina>();	
	}

	public boolean isRole(String role) {

		Query query = this.entityManager.createNamedQuery("verificaRegraNome");	
		query.setParameter(1, role);
		
		@SuppressWarnings("unchecked")
		List<UsuarioRole> resultado = query.getResultList(); 		
				
		return (resultado.size() > 0);
	}

	public UsuarioRole insertRegra(UsuarioRole regra) {

		entityManager.persist(regra);
		return regra;	
	}

	public void findDeleteRegras(Integer id){
		entityManager.remove(this.findRoleById(id));
		entityManager.flush();	
	}
	
	public UsuarioPagina findPagina(UsuarioPagina pagina) {

		Query query = this.entityManager.createNamedQuery("findPagina");	
		query.setParameter(1, pagina.getId());
		
		@SuppressWarnings("unchecked")
		List<UsuarioPagina> resultado = query.getResultList(); 		
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
		}	
		
		return null;
	}

	public List<UsuarioRole> findAllPaginaRegras(Integer quantidade,
			Integer pagina) {

		Query query = this.entityManager.createNamedQuery("findAllPaginaRegra");	
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		@SuppressWarnings("unchecked")
		List<UsuarioRole> resultado = query.getResultList(); 
		
		return resultado;	
	}
	
	public Integer findTotalPaginasRegras() {

		Query query = this.entityManager.createNamedQuery("findTotalPaginasRegra");		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
}
