package br.com.otgmobile.trackteam.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Agente;
import br.com.otgmobile.trackteam.modal.Dominio;
import br.com.otgmobile.trackteam.modal.Historico;
import br.com.otgmobile.trackteam.modal.HistoricoAgente;
import br.com.otgmobile.trackteam.modal.Objeto;

@Component
public class AgenteDAO {

	private EntityManager entityManager;
	
	public AgenteDAO(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}
	
	/**
	 * Busca todos os Agentes do banco
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Agente> findAll() {
	
		StringBuilder sql = new StringBuilder("select a from Agente a order by a.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		List<Agente> resultado = query.getResultList(); 

		for (Agente agente : resultado) {
			
			query = this.entityManager.createQuery("select h from HistoricoAgente h where agente_id = ? order by h.dataHora, h.id desc limit 1");
			query.setParameter(1, agente.getId());

			List<HistoricoAgente> listaHistorico = query.getResultList();
			
			if (listaHistorico.size() > 0) {
			
				agente.setPosicao(listaHistorico.get(0));
			}
		}		
		
		return resultado;
	}

	@SuppressWarnings("unchecked")
	public List<Historico> findAllPosicao() {
/*
		String sql = "SELECT h " +
		"FROM Historico h " +
		"WHERE id IN (SELECT MAX(h1.id) as id FROM Historico h1 GROUP BY h1.agente_id) " +
		"order by h.agente_id";
*/		
		Query query = this.entityManager.createNamedQuery("findAllAgentePosicao");		
		Collection<Agente> agentes = query.getResultList(); 
		
		List<Historico> historicos = new ArrayList<Historico>();
		
		for (Agente agente : agentes) {
			
			Historico historico = new Historico();
			historico.setAgente_id(agente.getId());
			historico.setDataHora(agente.getUltimaAtualizacao());
			historico.setLatitude(agente.getLatitude());
			historico.setLongitude(agente.getLongitude());
			
			if (agente.getVeiculo() != null) {				
				historico.setVeiculo_id(agente.getVeiculo().getId());
			}
			
			historicos.add(historico);
		}

		return historicos;
	}	
	/**
	 * Traz um agente pelo id
	 * 
	 * @return
	 */
	public Agente find(Agente agente) {
		
		StringBuilder sql = new StringBuilder("select a from Agente a where a.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, agente.getId());
		
		@SuppressWarnings("unchecked")
		List<Agente> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new Agente();
		}
	}

	/**
	 * Traz uma pagina de agentes cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Agente> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select a from Agente a order by a.usuario.nome asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Agente> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	/**
	 * 
	 * Retorna a quantidade total de agentes cadastrados
	 * @return
	 */
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(a.id) from Agente a");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	/**
	 * Altera um agente no banco de dados
	 * 
	 * @param agente
	 */
	public Agente update(Agente agente) {
	
		entityManager.merge(agente);
		
		return agente;
	}
	
	/**
	 * Insere um agente no banco
	 * 
	 * @param agente
	 * @return
	 */
	public Agente insert(Agente agente) {
		
		entityManager.persist(agente.getUsuario());
		entityManager.persist(agente);
		return agente;		
	}
	
	/**
	 * Busca todos os status do banco
	 * 
	 * @return
	 */
	public List<Dominio> findStatus() {
		
		StringBuilder sql = new StringBuilder("select d from Dominio d where d.nomeTabela='status' and d.nomeCampo='usuario'");
		Query query = this.entityManager.createQuery(sql.toString());
	
		@SuppressWarnings("unchecked")
		List<Dominio> resultado = query.getResultList(); 

		return resultado;
	}

	public Agente findByLogin(Agente agente) {
		
		Query query = this.entityManager.createNamedQuery("findAgenteByLogin");	
		query.setParameter(1, agente.getUsuario().getLogin());
			
		@SuppressWarnings("unchecked")
		List<Agente> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);	
			
		} else {
			
			return null;			
		}
	}
	
	public Agente findByMatricula(Agente agente) {
		
		StringBuilder sql = new StringBuilder("select a from Agente a where a.usuario.matricula = ?");		
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, agente.getUsuario().getMatricula());
			
		@SuppressWarnings("unchecked")
		List<Agente> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);	
			
		} else {
			
			return null;			
		}
	}

	public Agente findDeletarAgente(Agente agente) {
		
		StringBuilder sql = new StringBuilder("select a from Agente a where a.id = ?");		
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, agente.getId());
		
		@SuppressWarnings("unchecked")
		List<Agente> resultado = query.getResultList();
		
		if (resultado.size() > 0) {
			entityManager.remove(resultado.get(0));
			entityManager.flush();
			return agente;
		}
		
		return agente;	
	}

	@SuppressWarnings("unchecked")
	public List<Objeto> findAllObjetosAssociadosPagina(Integer quantidade,
			Integer pagina, Agente agente) {
		
		StringBuilder sql = new StringBuilder("select a.listaObjetos from Agente a where a.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, agente.getId());
		
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Objeto> resultado = query.getResultList(); 
		
		return resultado;
	}

	@SuppressWarnings("unchecked")
	public Integer findTotalObjetosAssociados(Agente agente) {
		
		StringBuilder sql = new StringBuilder("select a.listaObjetos from Agente a where a.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, agente.getId());
		
		List<Objeto> resultado = query.getResultList(); 
		
		return resultado.size();
	}

	@SuppressWarnings("unchecked")
	public List<Agente> findNaCercaComHistorico(String latitude, String longitude) {
		
//		Query query = this.entityManager.createNamedQuery("findAgenteNaCerca");	
		Query query = this.entityManager.createQuery("select a from Agente a WHERE (a.latitude != null or a.longitude != null) order by a.id desc");
		
		List<Agente> resultado = query.getResultList(); 
	
		return resultado;
	}
		
}
