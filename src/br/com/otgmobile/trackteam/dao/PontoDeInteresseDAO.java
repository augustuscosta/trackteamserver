package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Objeto;
import br.com.otgmobile.trackteam.modal.PontoDeInteresse;
import br.com.otgmobile.trackteam.service.EnderecoService;

@Component
public class PontoDeInteresseDAO {

	private EntityManager entityManager;
	private EnderecoService enderecoService;
	
	public PontoDeInteresseDAO(EntityManager entityManager, EnderecoService enderecoService) {
		
		this.entityManager = entityManager;
		this.enderecoService = enderecoService;
	}
	
	/**
	 * Busca todos os pontos de interesse do banco
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PontoDeInteresse> findAll() {
	
		StringBuilder sql = new StringBuilder("select a from PontoDeInteresse a order by a.id desc");
		Query query = this.entityManager.createQuery(sql.toString());
	
		List<PontoDeInteresse> resultado = query.getResultList(); 	
		
		return resultado;
	}
	/**
	 * Traz um ponto pelo id
	 * 
	 * @return
	 */
	public PontoDeInteresse find(PontoDeInteresse ponto) {
		
		StringBuilder sql = new StringBuilder("select a from PontoDeInteresse a where a.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, ponto.getId());
		
		@SuppressWarnings("unchecked")
		List<PontoDeInteresse> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new PontoDeInteresse();
		}
	}
	
	public PontoDeInteresse findByCodigo(PontoDeInteresse ponto) {
		
		StringBuilder sql = new StringBuilder("select a from PontoDeInteresse a where a.codigo = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, ponto.getCodigo());
		
		@SuppressWarnings("unchecked")
		List<PontoDeInteresse> resultado = query.getResultList(); 

		if (resultado.size() > 0) {
			
			return resultado.get(0);
		} else {
		
			return new PontoDeInteresse();
		}
	}

	/**
	 * Traz uma pagina de pontos cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PontoDeInteresse> findAllPagina(Integer quantidade, Integer pagina) {
						
		StringBuilder sql = new StringBuilder("select a from PontoDeInteresse a order by a.nome asc");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<PontoDeInteresse> resultado = query.getResultList(); 
		
		return resultado;
	}
	
	/**
	 * 
	 * Retorna a quantidade total de pontos cadastrados
	 * @return
	 */
	public Integer findTotal() {

		StringBuilder sql = new StringBuilder("select COUNT(a.id) from PontoDeInteresse a");
		Query query = this.entityManager.createQuery(sql.toString());
		
		Number resultado = (Number) query.getSingleResult();
		
		return resultado.intValue();		
	}
	
	/**
	 * Altera um ponto no banco de dados
	 * 
	 * @param ponto
	 */
	public PontoDeInteresse update(PontoDeInteresse ponto) {
	
		entityManager.merge(ponto);
//		entityManager.flush();
		
		return ponto;
	}
	
	/**
	 * Insere um ponto no banco
	 * 
	 * @param ponto
	 * @return
	 */
	public PontoDeInteresse insert(PontoDeInteresse ponto) {
		if (ponto.getEndereco().getId() == null || ponto.getEndereco().getId() == 0) {
			ponto.setEndereco(enderecoService.insert(ponto.getEndereco()));
		}
		entityManager.persist(ponto);
		return ponto;		
	}
	


	public PontoDeInteresse findDeletarPontoDeInteresse(PontoDeInteresse ponto) {
		
		StringBuilder sql = new StringBuilder("select a from PontoDeInteresse a where a.id = ?");		
		
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, ponto.getId());
		
		@SuppressWarnings("unchecked")
		List<PontoDeInteresse> resultado = query.getResultList();
		
		if (resultado.size() > 0) {
			entityManager.remove(resultado.get(0));
			entityManager.flush();
			return ponto;
		}
		
		return ponto;	
	}
	
	@SuppressWarnings("unchecked")
	public List<Objeto> findAllObjetosAssociadosPagina(Integer quantidade, Integer pagina, PontoDeInteresse ponto) {
		
		StringBuilder sql = new StringBuilder("select a.listaObjetos from PontoDeInteresse a where a.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, ponto.getId());
		
		query.setFirstResult(quantidade * pagina);
		query.setMaxResults(quantidade);
		
		List<Objeto> resultado = query.getResultList(); 
		
		return resultado;
	}

	@SuppressWarnings("unchecked")
	public Integer findTotalObjetosAssociados(PontoDeInteresse ponto) {
		
		StringBuilder sql = new StringBuilder("select a.listaObjetos from PontoDeInteresse a where a.id = ?");
		Query query = this.entityManager.createQuery(sql.toString());
		query.setParameter(1, ponto.getId());
		
		List<Objeto> resultado = query.getResultList(); 
		
		return resultado.size();
	}
}
