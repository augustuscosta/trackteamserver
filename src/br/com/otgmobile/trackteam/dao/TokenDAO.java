package br.com.otgmobile.trackteam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.modal.Token;

@Component
public class TokenDAO extends GenericDao {

	private EntityManager entityManager;

	public TokenDAO(EntityManager entityManager) {
		
		super(entityManager);
		this.entityManager = entityManager;
	}
	
	/**
	 * Traz o token gravado no banco de dados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Token find(Token token) {
	
		String sql = "SELECT h FROM Token h WHERE h.token = ?";
		
		Query query = this.entityManager.createQuery(sql);
		query.setParameter(1, token.getToken());
		
		List<Token> resultado = query.getResultList();
		
		if (resultado.size() > 0) {
			
			return resultado.get(0);
			
		} else {

			return new Token();
		}
	}

	public Token insert(Token token) {

		entityManager.persist(token);
		
		return token;
	}

	public void limpa(Token token) {

		Query query = this.entityManager.createNamedQuery("findTokenById");	
		query.setParameter(1, token.getId());
			
		@SuppressWarnings("unchecked")
		List<Token> resultado = query.getResultList(); 

		if (resultado.size() > 0) {		
			
			for (Token elem : resultado) {
				entityManager.remove(elem);
			}
		} 

		query = this.entityManager.createNamedQuery("findTokenByAgenteId");	
		query.setParameter(1, token.getAgente_id());
			
		@SuppressWarnings("unchecked")
		List<Token> resultado1 = query.getResultList(); 

		if (resultado1.size() > 0) {	
			for (Token elem : resultado1) {
				entityManager.remove(elem);
			}
		} 

		query = this.entityManager.createNamedQuery("findTokenByVeiculoId");	
		query.setParameter(1, token.getVeiculo_id());
			
		@SuppressWarnings("unchecked")
		List<Token> resultado2 = query.getResultList(); 

		if (resultado2.size() > 0) {		
			for (Token elem : resultado2) {
				entityManager.remove(elem);
			}
		} 
	}

	public Token findByVeiculo_id(Integer id) {

		Query query = this.entityManager.createNamedQuery("findTokenByVeiculoId");	
		query.setParameter(1, id);
			
		@SuppressWarnings("unchecked")
		List<Token> resultado = query.getResultList(); 

		if (resultado.size() > 0) {	
			
			return resultado.get(0);
			
		} else {
			
			return null;
		}		
	}

	public Token findByAgente_id(Integer id) {

		Query query = this.entityManager.createNamedQuery("findTokenByAgenteId");	
		query.setParameter(1, id);
			
		@SuppressWarnings("unchecked")
		List<Token> resultado = query.getResultList(); 

		if (resultado.size() > 0) {	
			
			return resultado.get(0);
			
		} else {
			
			return null;
		}
	}

	public Token update(Token token) {

		entityManager.persist(token);
		entityManager.flush();
		
		return token;
	}	
}
