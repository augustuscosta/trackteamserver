package br.com.otgmobile.trackteam;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.ocorrencia.NiveisEmergencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciasVeiculo;
import br.com.otgmobile.trackteam.modal.session.UsuarioSession;
import br.com.otgmobile.trackteam.modal.usuario.Usuario;
import br.com.otgmobile.trackteam.service.MenuService;
import br.com.otgmobile.trackteam.service.OcorrenciaService;
import br.com.otgmobile.trackteam.service.UsuarioService;
import br.com.otgmobile.trackteam.service.VeiculoService;
import br.com.otgmobile.trackteam.util.Json;

@Resource
public class IndexController {

	private final Result result;
	private final MenuService menuService;
	private final OcorrenciaService ocorrenciaService;
	private VeiculoService veiculoService;
	private final UsuarioService usuarioService;
	private final UsuarioSession usuarioSession;

	public IndexController(Result result, 
			MenuService menuService, 
			OcorrenciaService ocorrenciaService, 
			VeiculoService veiculoService,
			UsuarioService usuarioService,
			UsuarioSession usuarioSession) {
		
		this.result = result;
		this.menuService = menuService;
		this.ocorrenciaService = ocorrenciaService;
		this.veiculoService = veiculoService;	
		this.usuarioService = usuarioService;
		this.usuarioSession = usuarioSession; 
	}
	
	@MonkeySecurity(role="all")
	@Path("/")
	public void index() {
	
		List<Menu> listaMenu = menuService.listMenu();
		result.include("menuQuick", listaMenu);
				
		List<NiveisEmergencia> listaNiveisEmergencias = ocorrenciaService.findNivelEmergencia();		
		result.include("listaNiveisEmergencias", listaNiveisEmergencias);	
		
		Usuario usuario = usuarioService.find(new Usuario(usuarioSession.getId()));
		
		result.include("regioes", Json.export(usuario.getRegioes()));			

	}

	@Path("/teste/")
	public void teste() {
		
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(9025);
		
		List<OcorrenciasVeiculo> listaOcorrenciasVeiculos = ocorrenciaService.findOcorrenciaVeiculos(ocorrencia);
		
		System.out.println(listaOcorrenciasVeiculos.size());
		
		for (OcorrenciasVeiculo ov : listaOcorrenciasVeiculos) {
			
			System.out.println(ov.getVeiculo().getPlaca());
		}
		
	}

}
