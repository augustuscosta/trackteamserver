package br.com.otgmobile.trackteam.tasks;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.otgmobile.trackteam.modal.Escala;
import br.com.otgmobile.trackteam.modal.ocorrencia.Ocorrencia;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaAgente;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaEnquete;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaPontoDeInteresse;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciaVeiculoEnvolvido;
import br.com.otgmobile.trackteam.modal.ocorrencia.OcorrenciasVeiculo;
import br.com.otgmobile.trackteam.modal.ocorrencia.Solicitante;
import br.com.otgmobile.trackteam.modal.ocorrencia.Vitima;
import br.com.otgmobile.trackteam.util.Util;

public class EscalaThread implements Runnable {
	
	
	private Thread myselfThread = null;
	private EntityManager entityManager;
	
	public EscalaThread(){
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("default");
		entityManager = factory.createEntityManager();
	}

	public void start() {
		if (myselfThread == null) {
			myselfThread = new Thread(this);
			myselfThread.setPriority(Thread.MIN_PRIORITY);
			myselfThread.start();
		}
	}
	
	@Override
	public void run() {
		for (;;) {
			try {
				
				
				System.out.println("Escalas thread!");

				List<Escala> escalas = getEscalasEmOperacao();
				System.out.println("Em operação: " + escalas.size());
				entityManager.getTransaction().begin();
				for (Escala escala : escalas) {

					copiarOcorrencias(escala.getOcorrencias());
					escala.setLastSync(new Date());
					entityManager.merge(escala);
				}
				entityManager.getTransaction().commit();

				entityManager.clear();
				Thread.sleep(5 * 60000);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					Thread.sleep(5 * 60000);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}

		}
	}
	
	
	private void copiarOcorrencias(List<Ocorrencia> ocorrencias) {
		
		for(Ocorrencia ocorrencia:ocorrencias){
			Ocorrencia toinsert = copyOcorrencia(ocorrencia);
			insertOcorrencia(toinsert);
		}
		
	}

	private Ocorrencia copyOcorrencia(Ocorrencia ocorrencia) {
		Ocorrencia toReturn = new Ocorrencia();
		toReturn.setResumo(ocorrencia.getResumo());
		toReturn.setDescricao(ocorrencia.getDescricao());
		toReturn.setDescricao(ocorrencia.getDescricao());
		toReturn.setInicioProgramado(getDateForToday(ocorrencia.getInicioProgramado()));
		toReturn.setFimProgramado(getDateForToday(ocorrencia.getFimProgramado()));
		toReturn.setNiveisEmergencia(ocorrencia.getNiveisEmergencia());
		toReturn.setEndereco(ocorrencia.getEndereco());
		toReturn.setNatureza1(ocorrencia.getNatureza1());
		toReturn.setNatureza2(ocorrencia.getNatureza2());
		toReturn.setOcorrenciasStatus(ocorrencia.getOcorrenciasStatus());
		toReturn.setMeioCadastro(ocorrencia.getMeioCadastro());
		toReturn.setUsuario(ocorrencia.getUsuario());
		toReturn.setUsuarioCriacao(ocorrencia.getUsuarioCriacao());
		
		List<OcorrenciasVeiculo> ocorrenciasVeiculos = new ArrayList<OcorrenciasVeiculo>();
		for(OcorrenciasVeiculo ocorrenciaVeiculo:ocorrencia.getOcorrenciasVeiculos()){
			OcorrenciasVeiculo obj = new OcorrenciasVeiculo();
			obj.setAtivo(ocorrenciaVeiculo.getAtivo());
			obj.setDataHora(ocorrenciaVeiculo.getDataHora());
			obj.setHoraFim(ocorrenciaVeiculo.getHoraFim());
			obj.setHoraInicio(ocorrenciaVeiculo.getHoraInicio());
			obj.setMotivo(ocorrenciaVeiculo.getMotivo());
			obj.setVeiculo(ocorrenciaVeiculo.getVeiculo());
			ocorrenciasVeiculos.add(obj);
		}
		toReturn.setOcorrenciasVeiculos(ocorrenciasVeiculos);
		
		List<OcorrenciaAgente> ocorrenciasAgentes = new ArrayList<OcorrenciaAgente>();
		for(OcorrenciaAgente ocorrenciaAgente:ocorrencia.getOcorrenciaAgentes()){
			OcorrenciaAgente obj = new OcorrenciaAgente();
			obj.setAtivo(ocorrenciaAgente.getAtivo());
			obj.setDataHora(ocorrenciaAgente.getDataHora());
			obj.setHoraFim(ocorrenciaAgente.getHoraFim());
			obj.setHoraInicio(ocorrenciaAgente.getHoraInicio());
			obj.setObservacao(ocorrenciaAgente.getObservacao());
			obj.setAgente(ocorrenciaAgente.getAgente());
			ocorrenciasAgentes.add(obj);
		}
		toReturn.setOcorrenciaAgentes(ocorrenciasAgentes);
		
		List<OcorrenciaPontoDeInteresse> ocorrenciasPontosDeInteresses = new ArrayList<OcorrenciaPontoDeInteresse>();
		for(OcorrenciaPontoDeInteresse ocorrenciaPontoDeInteresse:ocorrencia.getOcorrenciaPontosDeInteresse()){
			OcorrenciaPontoDeInteresse obj = new OcorrenciaPontoDeInteresse();
			obj.setPontoDeinteresse(ocorrenciaPontoDeInteresse.getPontoDeinteresse());
			ocorrenciasPontosDeInteresses.add(obj);
		}
		toReturn.setOcorrenciaPontosDeInteresse(ocorrenciasPontosDeInteresses);
		
		List<OcorrenciaEnquete> ocorrenciasEnquetes = new ArrayList<OcorrenciaEnquete>();
		for(OcorrenciaEnquete ocorrenciaEnquete:ocorrencia.getOcorrenciaEnquetes()){
			OcorrenciaEnquete obj = new OcorrenciaEnquete();
			obj.setEnquete(ocorrenciaEnquete.getEnquete());
			ocorrenciasEnquetes.add(obj);
		}
		toReturn.setOcorrenciaEnquetes(ocorrenciasEnquetes);
		
		List<Vitima> vitimas = new ArrayList<Vitima>();
		for(Vitima vitima:ocorrencia.getVitimas()){
			Vitima obj = new Vitima();
			obj.setDescricao(vitima.getDescricao());
			obj.setGravidade(vitima.getGravidade());
			obj.setIdade(vitima.getIdade());
			obj.setSexo(vitima.getSexo());
			obj.setTipo(vitima.getTipo());
			vitimas.add(obj);
		}
		
		toReturn.setVitimas(vitimas);
		
		List<OcorrenciaVeiculoEnvolvido> ocorrenciasVeiculosEnvolvidos = new ArrayList<OcorrenciaVeiculoEnvolvido>();
		for(OcorrenciaVeiculoEnvolvido ocorrenciaVeiculoEnvolvido:ocorrencia.getListaVeiculosEnvolvidos()){
			OcorrenciaVeiculoEnvolvido obj = new OcorrenciaVeiculoEnvolvido();
			obj.setVeiculoEnvolvido(ocorrenciaVeiculoEnvolvido.getVeiculoEnvolvido());
			ocorrenciasVeiculosEnvolvidos.add(obj);
		}
		toReturn.setListaVeiculosEnvolvidos(ocorrenciasVeiculosEnvolvidos);
		
		List<Solicitante> solicitantes = new ArrayList<Solicitante>();
		for(Solicitante solicitante:ocorrencia.getSolicitantes()){
			Solicitante obj = new Solicitante();
			obj.setCpf(solicitante.getCpf());
			obj.setNome(solicitante.getNome());
			obj.setTelefone1(solicitante.getTelefone1());
			obj.setTelefone2(solicitante.getTelefone2());
			solicitantes.add(obj);
		}
		
		toReturn.setSolicitantes(solicitantes);
		
		toReturn.setFlag(ocorrencia.getFlag());
		toReturn.setHoraCriacao(new Timestamp(new Date().getTime()));
		return toReturn;
	}

	private Timestamp getDateForToday(Timestamp date) {
		Date dateToParse = new Date(date.getTime());
		Date newDate = new Date();
		
		Calendar calendar = Calendar.getInstance();

	    calendar.setTime( dateToParse );
	    int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
	    int minute = calendar.get(Calendar.MINUTE);
	    int second = calendar.get(Calendar.SECOND);
	    int millisecond = calendar.get(Calendar.MILLISECOND);
	    
	    calendar.setTime( newDate );
	    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
	    calendar.set(Calendar.MINUTE, minute);
	    calendar.set(Calendar.SECOND, second);
	    calendar.set(Calendar.MILLISECOND, millisecond);
	    
	    Timestamp time = new Timestamp(calendar.getTimeInMillis());
		return time;
	}

	private void insertOcorrencia(Ocorrencia toinsert) {
		entityManager.persist(toinsert);
	}

	@SuppressWarnings("unchecked")
	private List<Escala> getEscalas(){
		StringBuilder sql = new StringBuilder(
				"select e from Escala e order by entrada");
		Query query = this.entityManager.createQuery(sql.toString());
		List<Escala> resultado = query.getResultList();
		return resultado;
	}
	
	private List<Escala> getEscalasEmOperacao(){
		List<Escala> escalas = getEscalas();
		List<Escala> toReturn = new ArrayList<Escala>();
		
		for(Escala escala:escalas){
			if(escala.getAtivo()){
				Date hoje = Util.getZeroTimeDate(new Date());
				Date entrada = Util.getZeroTimeDate(escala.getEntrada());
				Date saida = Util.getZeroTimeDate(escala.getSaida());
				if(hoje.getTime() > entrada.getTime() && hoje.getTime() < saida.getTime()){
					if(escala.getLastSync() == null || Util.getZeroTimeDate(escala.getLastSync()).getTime() < Util.getZeroTimeDate(new Date()).getTime()){
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(new Date());
						int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
						if(dayOfWeek == Calendar.MONDAY){
							if(escala.getSegunda())
								toReturn.add(escala);
						}else if(dayOfWeek == Calendar.TUESDAY){
							if(escala.getTerca())
								toReturn.add(escala);
						}else if(dayOfWeek == Calendar.WEDNESDAY){
							if(escala.getQuarta())
								toReturn.add(escala);
						}else if(dayOfWeek == Calendar.THURSDAY){
							if(escala.getQuinta())
								toReturn.add(escala);
						}else if(dayOfWeek == Calendar.FRIDAY){
							if(escala.getSexta())
								toReturn.add(escala);
						}else if(dayOfWeek == Calendar.SATURDAY){
							if(escala.getSabado())
								toReturn.add(escala);
						}else if(dayOfWeek == Calendar.SUNDAY){
							if(escala.getDomingo())
								toReturn.add(escala);
						}
						
					}
				}
			}
		}
		
		return toReturn;
	}

}
