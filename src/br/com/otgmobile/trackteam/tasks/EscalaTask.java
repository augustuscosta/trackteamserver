package br.com.otgmobile.trackteam.tasks;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;

//@Component
//@ApplicationScoped
public class EscalaTask {
	
    public EscalaTask() {
    	start();
    }

    public void start() {
    	EscalaThread escalaThread = new EscalaThread();
    	escalaThread.start();
    }
} 