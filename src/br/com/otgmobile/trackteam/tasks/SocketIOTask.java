package br.com.otgmobile.trackteam.tasks;

import java.net.URISyntaxException;
import java.util.List;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import br.com.otgmobile.trackteam.dao.TokenDAO;
import br.com.otgmobile.trackteam.modal.Token;
import br.com.otgmobile.trackteam.modal.node.Identificacao;
import br.com.otgmobile.trackteam.service.TokenService;
import br.com.otgmobile.trackteam.util.Json;
import br.com.otgmobile.trackteam.util.Util;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

@Component
@ApplicationScoped
public class SocketIOTask {

	private Socket socket;
	private final EntityManager entityManager;
	private final TokenService tokenService;

    public SocketIOTask() {

    	EntityManagerFactory factory = Persistence.createEntityManagerFactory("default");
    	entityManager = factory.createEntityManager();
    	tokenService = new TokenService(new TokenDAO(entityManager)); 

		final ResourceBundle resourceConfig = java.util.ResourceBundle.getBundle("config");			
		String socketServer = resourceConfig.getString("socket_server");
		
    	try {
			socket = IO.socket(socketServer);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
    	
    	socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			conectou();
    		}

    	});
    	
    	socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Caiu a conexção com o SocketIO"); 
				open();
    		}

    	});
    	
    	socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Erro de conexção SocketIO"); 
				open();
    		}

    	});
    	
    	socket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Time out SocketIO"); 
				open();
    		}

    	});

    	socket.on("device/get/id", new Emitter.Listener() {

		  @Override
		  public void call(Object... args) {
			  System.out.println("Recebeu uma mensagem de " + args[0]);
			  retornaIdUsuario(args[0].toString());	
		  }

    	});
    	connect();
    }

    public void connect() {
    	System.out.println("Conectando com o SocketIO"); 
    	socket.connect();
    }
    
    public void open() {
    	System.out.println("Abrindo conexção com o SocketIO"); 
    	socket.open();
    }
	   
    public void enviaMsg(String metodo, JSONObject msg) {
    	    	
		try {
						
			socket.emit(metodo, msg);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
    } 
 
    public void enviaMsg(String metodo, Object objeto) {

		JSONObject jsonNode;
		JSONObject jsonResult = new JSONObject();

		try {
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(objeto);	    
			jsonNode = new JSONObject(je.toString());
			jsonResult.put("result", jsonNode);
			
			socket.emit(metodo, jsonResult);
			
		} catch (Exception e) {			
			e.printStackTrace();
		}	
    } 
    
    public void conectou(){
    	System.out.println("Conectou com SocketIO");
		Identificacao dados = new Identificacao();
		dados.setTipo("servidor");
		dados.setToken(null);
		JSONObject json = new JSONObject(dados);
		enviaMsg("sessao/token", json);
		sincronizaTokenComNode();
    }

	public void retornaIdUsuario(String json) {
		
		System.out.println(json);
		Token token = (Token) Util.fromGson(Token.class, json);
		TokenDAO tokenDAO = new TokenDAO(entityManager);
		TokenService tokenService = new TokenService(tokenDAO);
		token = tokenService.find(token);
		enviaMsg("device/notifica/id", token);
	}
	
	public void sincronizaTokenComNode() {
		
		entityManager.getTransaction().begin();
		List<Token> listaTokens = tokenService.findAll();
		entityManager.getTransaction().commit();

    	JSONObject jsonNode;
		JSONObject jsonResult = new JSONObject();
   	
		try {

			jsonNode = new JSONObject(Json.export("tokens", listaTokens));
			jsonResult.put("result", jsonNode);
			this.enviaMsg("servidor/token/sincroniza", jsonResult);		
			
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Erro ao enviar mensagem");
		}
		
	}	
}
