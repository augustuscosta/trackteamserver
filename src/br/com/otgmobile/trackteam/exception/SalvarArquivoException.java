package br.com.otgmobile.trackteam.exception;


public class SalvarArquivoException extends Exception {

	private static final long serialVersionUID = -4731955342222190077L;

	public SalvarArquivoException() {
		super("Problemas ao salvar o arquivos em disco");
	}
}
