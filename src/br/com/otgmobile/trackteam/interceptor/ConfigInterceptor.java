package br.com.otgmobile.trackteam.interceptor;

import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.http.VRaptorRequest;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.otgmobile.trackteam.modal.session.ConfigSession;

@Intercepts
public class ConfigInterceptor implements Interceptor {

	private Result result;
	private VRaptorRequest vRaptorRequest;
	private ConfigSession configSession;
	
	public ConfigInterceptor(Result result,
			VRaptorRequest vRaptorRequest,
			ConfigSession configSession) {
		
		this.result = result;
		this.vRaptorRequest = vRaptorRequest;
		this.configSession = configSession;
	}
	
	public boolean accepts(ResourceMethod method) {

		if (configSession.getStart() == null || !configSession.getStart()) {
			
			configSession.setStart();
		}
			
		result.include("contexto", vRaptorRequest.getContextPath());		
		result.include("config", configSession);

		return true;
	}
	
	public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) {
	
		stack.next(method, resourceInstance);
	}	
}
