package br.com.otgmobile.trackteam.interceptor;

import java.lang.annotation.Annotation;
import java.util.List;

import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.otgmobile.trackteam.annotation.MenuTag;
import br.com.otgmobile.trackteam.modal.Menu;
import br.com.otgmobile.trackteam.modal.SubMenu;
import br.com.otgmobile.trackteam.modal.session.UsuarioSession;
import br.com.otgmobile.trackteam.service.MenuService;

@Intercepts
public class MenuInterceptor implements Interceptor {

	private Result result;
	private final MenuService menuService;
	private final UsuarioSession usuarioSession;
	
	public MenuInterceptor(Result result,  MenuService menuService, UsuarioSession usuarioSession) {
		
		this.result = result;	
		this.menuService = menuService;
		this.usuarioSession = usuarioSession;
	}

	public boolean accepts(ResourceMethod method) {
		
		return true;
	}
	
	public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) {

		if (usuarioSession != null && usuarioSession.getId() != null && usuarioSession.getId() > 0) {

			SubMenu subMenu = new SubMenu();
			
			Annotation annotation;
			
			if (method.getMethod().isAnnotationPresent(MenuTag.class)) {
			
				annotation = method.getMethod().getAnnotation(MenuTag.class);		
				
			} else { 
				
				annotation = method.getResource().getType().getAnnotation(MenuTag.class);
			}
	
			if(annotation instanceof MenuTag){
	
				MenuTag myAnnotation = (MenuTag) annotation;
				
				subMenu.setTag(myAnnotation.tag());
			}	
			
			if (subMenu.getTag() != null && !"".equals(subMenu.getTag()) && result.included().get("subMenu") == null) {
							
				subMenu = menuService.findByTag(subMenu);							
				List<SubMenu> listaSubMenu = menuService.listSubMenuControleGeral(subMenu);
				
				listaSubMenu = menuService.ordenaSubMenus(listaSubMenu);
				
				result.include("listaSubMenu",listaSubMenu);
				result.include("subMenu", subMenu);	
			}
			
			if (result.included().get("menuQuick") == null) {
			
				List<Menu> listaMenu = menuService.listMenu();
				result.include("menuQuick", listaMenu);
			}
		}
		
		stack.next(method, resourceInstance);

	}
}

