package br.com.otgmobile.trackteam.interceptor;

import java.lang.annotation.Annotation;

import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.http.VRaptorRequest;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.otgmobile.trackteam.annotation.MonkeySecurity;
import br.com.otgmobile.trackteam.controller.LoginController;
import br.com.otgmobile.trackteam.modal.session.UsuarioSession;

@Intercepts
public class LoginInterceptor implements Interceptor {

	private Result result;
	private VRaptorRequest vRaptorRequest;
	private final UsuarioSession usuarioSession;

	public LoginInterceptor(Result result, UsuarioSession usuarioSession,
			VRaptorRequest vRaptorRequest) {

		this.result = result;
		this.usuarioSession = usuarioSession;
		this.vRaptorRequest = vRaptorRequest;
	}

	public boolean accepts(ResourceMethod method) {

		result.include("url", vRaptorRequest.getContextPath());
		result.include("contexto", vRaptorRequest.getContextPath());

		return (method.getMethod().isAnnotationPresent(MonkeySecurity.class) || method
				.getResource().getType()
				.isAnnotationPresent(MonkeySecurity.class));
	}

	public void intercept(InterceptorStack stack, ResourceMethod method,
			Object resourceInstance) {

		Boolean liberado = false;

		if (usuarioSession == null || usuarioSession.getId() == null
				|| usuarioSession.getId() <= 0) {

		} else {

			Annotation annotation;

			if (method.getMethod().isAnnotationPresent(MonkeySecurity.class)) {

				annotation = method.getMethod().getAnnotation(
						MonkeySecurity.class);

			} else {

				annotation = method.getResource().getType()
						.getAnnotation(MonkeySecurity.class);
			}

			if (annotation instanceof MonkeySecurity) {

				MonkeySecurity myAnnotation = (MonkeySecurity) annotation;
				if (myAnnotation.role().equals("all")) {
					liberado = true;
				} else {
					// usuarioSession.getRole().size();

					for (String pagina : usuarioSession.getRole()) {

						if (pagina.equals(myAnnotation.role())) {

							liberado = true;
						}
					}
				}
			}
		}

//		liberado = true;
		
		if (liberado) {

			stack.next(method, resourceInstance);

		} else {

			result.redirectTo(LoginController.class).login();
		}
	}
}